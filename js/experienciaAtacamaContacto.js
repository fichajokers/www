angular.module('starter.controllers')


.controller('experienciaAtacamaContactoCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaGeolocation','$ionicModal','ratingConfig','$ionicLoading','$sce',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaGeolocation,$ionicModal,ratingConfig,$ionicLoading,$sce) {
    console.log("detalle experiencia controller: "+ $rootScope.id_producto_servicio_turistico);
    console.log("rootScope : ",$rootScope);
    $rootScope.experienciaDetalleMostrar = null;
    $scope.item = $rootScope.experienciaSeleccionadaNuevoHome;

    $scope.cerrarModalExperienciaAtacamaContacto = function(){
    	 $rootScope.modalExperienciaAtacamaContacto.hide();
  	}

  	$scope.goToNuevoHome = function(){
      $rootScope.modalDetalleExperiencia.remove();
      $rootScope.modalExperienciaAtacamaContacto.hide();
    }

    $scope.trustAsHtml = function(string) {
      return $sce.trustAsHtml(string);
    };

    $scope.hrefweb = function(link){
      window.open(link,'_system');
    }

    $scope.link = function(url){
      window.open(url,"_system",'location=yes');
    }

}]);