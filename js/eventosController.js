angular.module('starter.controllers')


.controller('eventosCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaCamera','$cordovaFileTransfer','$timeout','$ionicLoading','$ionicModal','$sce',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaCamera,$cordovaFileTransfer,$timeout,$ionicLoading,$ionicModal,$sce) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.evento = [];

  $scope.filtroActual = "todas las comunas";

  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/getComunas.php",
    data: $httpParamSerializer({}),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si recato bien los datos
    console.log(response);
    $scope.comunas = response.data.comuna;
    $scope.comunas.unshift({
      id_comuna : -1,
       nombre : "Todas las comunas"
    });
    angular.forEach($scope.comunas, function(value, key) {
        console.log(value, key);
        value.top_distancia = key*50;
    });
    
    console.log($scope.comunas);
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });
  
  //$scope.evento.foto = "https://mcdonalds.com.gt/wp-content/uploads/2013/01/Ronald-McDonald.png";

  /**
   * [Rescatar todos los eventos de la base de datos.]
   * 
   * @return {[Array]}  $scope.eventos [Array con eventos]
   */
  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/getEventos.php",
    data: $httpParamSerializer({}),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si guardó correctamente.
    $scope.eventos = response.data.eventos;
    $rootScope.eventosFiltro = $scope.eventos;
    console.log($scope.eventos);
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });

  $scope.top = {
    "margin-top" : 1,
  }


  $scope.mostrarMarcadorEvento = function(evento){
    $rootScope.mostrarEventoSeleccionado(evento.georeferenciacion_evento_latitud,evento.georeferenciacion_evento_longitud,evento.titulo_evento,evento);
    $scope.cerrarModalEventos();
  }

  $scope.cerrarModalEventos = function(){
    console.log("cerar modal cerrarModalEventos");
    // if( ionic.Platform.isWebView() )
    //   $rootScope.map.setClickable(true);
    $rootScope.volverServicioYAtractivo2 = false;
    $rootScope.modalEventos.remove();
  }

  //para transformar el texto a html, ya que no pasaba la etiqueta style=".."
  $scope.trustAsHtml = function(string) {
    return $sce.trustAsHtml(string);
  };

  $scope.hrefWeb = function(link){
    window.open(link,'_blank','location=yes');
  }

  $scope.mailToCorreo = function(link){
    window.open('mailto:${link}','_system');
  }

  

  $scope.openModalEventoSeleccionado = function(evento){
    $rootScope.evento_seleccionado = evento;
    $rootScope.desdeEventosController = true;
    $ionicModal.fromTemplateUrl('templates/eventoSeleccionado.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalEventoSeleccionado = modal;
      $rootScope.modalEventoSeleccionado.show();
    });
  }

  if ($rootScope.enEvento == true) {
    console.log("evento root: ", $rootScope.evento_seleccionado);
    $rootScope.enEvento = false;
    $scope.openModalEventoSeleccionado($rootScope.evento_seleccionado);

  }

  $scope.mostrarFiltro = function(){
    console.log("comuna.nombre: ", $scope.comuna);
      if( $scope.mostrarMenuFiltro ){
        $scope.mostrarMenuFiltro = false;
      }else{
        $scope.mostrarMenuFiltro = true;
      }
  }

  $scope.filtrar = function(filtro){
    console.log(filtro);
    var contador = 0;
    $rootScope.eventosFiltro = [];

    if(filtro == -1){
      $rootScope.eventosFiltro = $scope.eventos;
      $scope.filtroActual  = "todas las comunas";
    }else{
      angular.forEach($scope.eventos, function(value, key) {
        if( value.nombre_comuna == $scope.comunas[filtro].nombre){
          $rootScope.eventosFiltro[contador] = value;
          contador +=1;
        }
      });

      angular.forEach($scope.comunas, function(value, key) {
        if(value.id_comuna == filtro){
          $scope.filtroActual  = value.nombre;
        }
      });
    }

    $scope.mostrarMenuFiltro = false;
    console.log("alo",$scope.eventosFiltro, "eventos",$scope.eventosFiltro );
  }

  

}]);