angular.module('starter.controllers')


.controller('subirImagenCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCamera','$cordovaFileTransfer','$httpParamSerializer','$ionicPopup','$ionicLoading','$ionicModal','$ionicNativeTransitions' ,function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCamera,$cordovaFileTransfer,$httpParamSerializer,$ionicPopup,$ionicLoading,$ionicModal, $ionicNativeTransitions) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.rutaImagen        = "";
  $scope.hola              = [];
  $scope.holaTotal         = [];
  $scope.id_usuario        = localStorage.getItem("id_usuario");
  $scope.nombre            = [];
  $scope.nombre.comentario = "";


  $scope.url="http://200.14.68.107/atacamaGoMultimedia/";
  $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/getImagenItemTuristico.php",
      data: $httpParamSerializer({
          "id_item_turistico": $rootScope.id_item_turistico,
  }),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si guardó correctamente.
      if(response.data.resultado == "ok"){
          console.log(response.data);
          $scope.imagenes      = response.data.item_turistico;
          angular.forEach($scope.imagenes, function(value, key) {
              $scope.holaTotal[key] = { 
                  "foto"                      : $scope.url + value.foto,
                  "comentario"                : value.comentario,
                  "id_usuario"                : value.id_usuario,
                  "id_archivo_item_turistico" : value.id_archivo_item_turistico,
                  "nombre"                    : value.nombre_archivo_item
              };
              
              if( key < 4){
                  $scope.hola[key] = { 
                      "foto"                      : $scope.url + value.foto,
                      "comentario"                : value.comentario,
                      "id_usuario"                : value.id_usuario,
                      "id_archivo_item_turistico" : value.id_archivo_item_turistico,
                      "nombre"                    : value.nombre_archivo_item
                  };
              }
          });

          //console.log( response.data.id_usuario, localStorage.getItem("id_usuario") )
          /*if($rootScope.item_seleccionado.id_usuario == localStorage.getItem("id_usuario")){
              //console.log("EDITAR");
              $scope.noEdita = false;
          }*/

      }else if ( response.data.resultado == "no data" ){
          $rootScope.toast('No hay imágenes', 'short');
          //console.log( response.data.id_usuario, localStorage.getItem("id_usuario") )
          /*if($rootScope.item_seleccionado.id_usuario == localStorage.getItem("id_usuario")){
              //console.log("EDITAR");
              $scope.noEdita = false;
          }*/
          //$scope.cerrarModalModificarImagen();
      }else{
          $rootScope.toast('error, intenta nuevamente', 'short');
      }
  }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short')
  });

  $scope.cerrarModalSubirImagen = function(){
    $rootScope.map.clear();
    $rootScope.map.off();
    $rootScope.map.remove();
    $rootScope.modalMenuAcciones.remove();
    $rootScope.modalSubirImagen.remove();
    $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
        "type": "slide",
        "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
        "duration": 400, // in milliseconds (ms), default 400
    });
  }

  //----------------Subir imagenes-------------------//
    $scope.takePhoto = function () {
        var options = {
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.CAMERA,
            mediaType       : Camera.MediaType.PICTURE,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            saveToPhotoAlbum: false
        };
   
        $ionicLoading.show({
            template: 'Cargando...'
        });

        $cordovaCamera.getPicture().then(function (imageData) {
            if( imageData == null){
              $rootScope.toast("Debes seleccionar una imagén de tu movil.","short");
            }else{
              console.log(imageData);
              imageData = imageData.replace(/^(.+?\.(png|jpe?g)).*$/i, '$1');
              console.log(imageData);
              // var image           = document.getElementById('imagenSacadaFotoSubir');
              // image.style.display = 'block';
              // image.src           = imageData;
              $scope.rutaImagen   = imageData;
              $scope.tieneSrc     = true;
              $scope.nombreImagen = Math.floor((Math.random() * 9999999) + 1)+"_"+imageData.substr(imageData.lastIndexOf('/') + 1);

              $ionicLoading.hide();
              $scope.addImagenItem();
            }
            
        }, function (err) {
            console.log(err);
            $ionicLoading.hide();
            // An error occured. Show a message to the user
        });
    }
                
    $scope.choosePhoto = function () {
        var options = {
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType      : Camera.PictureSourceType.SAVEDPHOTOALBUM,
            mediaType       : Camera.MediaType.PICTURE,
            correctOrientation : true
        };

        $ionicLoading.show({
            template: 'Cargando...'
        });


        $cordovaCamera.getPicture(options).then(function (imageData) {
          if( imageData == null){
            $rootScope.toast("Debes seleccionar una imagén de tu movil.","short");
          }else{

            console.log(imageData);
            imageData = imageData.replace(/^(.+?\.(png|jpe?g)).*$/i, '$1');
            // var image2 = document.getElementById('imagenSacadaFotoSubir');
            // image2.style.display   = 'block';
            // image2.src             = imageData;

            $scope.rutaImagen     = imageData;
            $scope.tieneSrc       = true;
            $scope.nombreImagen   = Math.floor((Math.random() * 9999999) + 1)+"_"+imageData.substr(imageData.lastIndexOf('/') + 1);

            $ionicLoading.hide();
            $scope.addImagenItem();
          }
        }, function (err) {
            $ionicLoading.hide();
            //alert(err);
            // An error occured. Show a message to the user
        });
    }

    $scope.addImagenItem = function(){
      if( $scope.nombreImagen != undefined ){

        var options = {
          fileKey: "fotoItem",
          fileName: $scope.nombreImagen,
          chunkedMode: false
        };

        // $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirImagenItem.php", $scope.rutaImagen, options).then(function(result) {
        //   $rootScope.toast('Imagen agregada', 'short');
        // }, function(err) {
        //   $rootScope.toast('Error agregando imagen', 'short');
        //   console.log(err);
        // }, function (progress) {
        //     // constant progress updates
        // });

        /*$http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addArchivoItemTuristico.php",
          data: $httpParamSerializer({
            "id_item_turistico"   : $rootScope.id_item_turistico,
            "id_usuario"          : localStorage.getItem("id_usuario"),
            "nombre_archivo_item" : $scope.nombreImagen,
            "tipo_archivo_item"   : 2
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data);
          if(response.data.resultado == "ok"){
            $rootScope.toast("imagen agregada.","short");
            

            $scope.hola.unshift({ 
                        "foto"                      : $scope.rutaImagen,
                        "comentario"                : "",
                        "id_usuario"                : localStorage.getItem("id_usuario"),
                        "id_archivo_item_turistico" : response.data.id_archivo_item_turistico,
                        "nombre"                    : $scope.nombreImagen

            });
            $rootScope.toast("imagen agregada.","short");
                    
            var image2 = document.getElementById('imagenSacadaFotoSubir');
            image2.style.display   = 'block';
            image2.src             = "";
            $scope.tieneSrc        = false;
            $ionicLoading.hide();
            $scope.noEdita = false;
            $scope.nombreImagen = undefined;
          }
          else
            $rootScope.toast("Ha ocurrido un error intenta nuevamente","short");
          
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet',"short");
        });*/
        $ionicLoading.hide();
        $ionicPopup.show({
                  template: '<input type="text" ng-model="nombre.comentario" placeholder="Escriba aquí">',
                  title: 'Agrega un comentario',
                  // subTitle: 'comentario',
                  scope: $scope,
                  buttons: [
                    { 
                      text: '<i class="icon ion-close-round"></i>',
                      type:'popclose',
                      onTap: function(e) {
                        // alert("dsaasddasdasdasdasasd");

                        // var image2 = document.getElementById('imagenSacadaFotoSubir');
                        // image2.style.display = 'block';
                        // image2.src = "";
                        // $scope.tieneSrc = false;
                        // $ionicLoading.hide();
                        // $scope.noEdita = false;
                        // $scope.nombreImagen = undefined;
                        }
                    },{
                      text: '<b>Aceptar</b>',
                      type: 'button-positive asd',
                      onTap: function(e) {
                        if (!$scope.nombre.comentario) {
                          //don't allow the user to close unless he enters nombre password
                          e.preventDefault();
                        } else {
                          console.log("comentario: "+$scope.nombre.comentario);

                            $ionicLoading.show({
                                template: 'Cargando...'
                            });

                            $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirImagenItem.php", $scope.rutaImagen, options).then(function(result) {
                              $rootScope.toast('Imagen subida', 'short');
                            }, function(err) {
                              $rootScope.toast('Error agregando imagen', 'short');
                              console.log(err);
                            }, function (progress) {
                              // constant progress updates
                            });
                            console.log($scope.nombre.comentario);
                            $http({
                              method: "POST",
                              url: "http://200.14.68.107/atacamaGo/addArchivoItemTuristico.php",
                              data: $httpParamSerializer({
                                "id_item_turistico"   : $rootScope.id_item_turistico,
                                "id_usuario"          : localStorage.getItem("id_usuario"),
                                "nombre_archivo_item" : $scope.nombreImagen,
                                "tipo_archivo_item"   : 2,
                                "comentario"          : $scope.nombre.comentario
                              }),
                              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                            }).then(function(response){ //ok si guardó correctamente.
                              console.log(response.data);
                              if(response.data.resultado == "ok"){
                                $rootScope.toast("Imagen agregada.","short");
                                

                                $scope.hola.unshift({ 
                                            "foto"                      : $scope.rutaImagen,
                                            "comentario"                : $scope.nombre.comentario,
                                            "id_usuario"                : localStorage.getItem("id_usuario"),
                                            "id_archivo_item_turistico" : response.data.id_archivo_item_turistico,
                                            "nombre"                    : $scope.nombreImagen

                                });
                                $rootScope.toast("imagen agregada.","short");
                                        
                                // var image2 = document.getElementById('imagenSacadaFotoSubir');
                                // image2.style.display   = 'block';
                                // image2.src             = "";
                                // $scope.tieneSrc        = false;
                                // $ionicLoading.hide();
                                // $scope.noEdita = false;
                                // $scope.nombreImagen = undefined;

                              }else
                                $rootScope.toast("Ha ocurrido un error intenta nuevamente","short");
                              $ionicLoading.hide();
                              $scope.noEdita = false;
                            }, function(){ //Error de conexión
                              $rootScope.toast('Verifica tu conexión a internet',"short");
                              $ionicLoading.hide();
                              $scope.noEdita = false;
                            });

                          return $scope.nombre.comentario;
                        }
                      }
                    }
                  ]
                });
$scope.$watch("nombre.comentario", function(newValue, oldValue){
  console.log("nuevo: " + newValue);
  console.log("viejo: " + oldValue);
          if (newValue.trim().length > 15){
              $scope.nombre.comentario = newValue.trim().substring(0,15);
          }
        });
      }else{
        $rootScope.toast('Debes seleccionar una imagen',"short");
        $ionicLoading.hide();
      }
        
    }

    $scope.eliminarImagen = function(imagen,index){
        $ionicLoading.show({
            template: 'Cargando...'
        });
        console.log("#eawfaerferf aef aefef esf sefasef");
            $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/deleteImagenItemTuristico.php",
                data: $httpParamSerializer({
                    "id_archivo_item_turistico" : imagen.id_archivo_item_turistico,
                    "nombre"                    : imagen.nombre
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
                console.log(response.data);
                if( response.data.resultado == "ok"){
                    $rootScope.toast('Imagen eliminada correctamente',"short");
                    $scope.hola.splice(index, 1);    
                }else{
                    $rootScope.toast('Error eliminando imagen',"short");
                }
                $ionicLoading.hide();
            }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet',"short");
                $ionicLoading.hide();
            });    
    }

    $scope.openModalSubirImagen = function(){
      //$rootScope.modalSubirImagen.remove();
      //Modal subirImagen...
      /*$ionicModal.fromTemplateUrl('templates/subirImagen.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalSubirImagen = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $scope.cerrarModalSubirImagen();
        $rootScope.modalSubirImagen.show();
      });*/
    }

    $scope.openModalAgregarVideo = function(){

      //Modal Agregar Video...
      $ionicModal.fromTemplateUrl('templates/agregarVideo.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarVideo = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $rootScope.modalSubirImagen.remove();
        $rootScope.modalAgregarVideo.show();
      });
        
    }

    $scope.openModalAgregarAudio = function(){
      //Modal Agregar Audio...
      $ionicModal.fromTemplateUrl('templates/agregarAudio.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarAudio = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
          $rootScope.modalSubirImagen.remove();
        $rootScope.modalAgregarAudio.show();
      });
  
    }

    $scope.openModalAgregarDescripcion = function(){
      $rootScope.modalSubirImagen.remove();
      //Modal Agregar descripcion...
      /*$ionicModal.fromTemplateUrl('templates/agregarDescripcion.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarDescripcion = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $scope.cerrarModalSubirImagen();
        $rootScope.modalAgregarDescripcion.show();
      });*/

    }

    $scope.openModalAgregarComentario = function(){
      
      //Modal Agregar Comentario...
      $ionicModal.fromTemplateUrl('templates/agregarComentario.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarComentario = modal;
        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $rootScope.modalSubirImagen.remove();
        $rootScope.modalAgregarComentario.show();
      });
    }

    $scope.openModalAgregarCalificar = function(){
      
      //Modal Agregar Calificar...
      $ionicModal.fromTemplateUrl('templates/agregarCalificar.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarCalificar = modal;
        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $rootScope.modalSubirImagen.remove();
        $rootScope.modalAgregarCalificar.show();
      });
  
    }

    $scope.atrasModalSubirImagen = function() {
        $rootScope.modalSubirImagen.remove();
    }

}]);