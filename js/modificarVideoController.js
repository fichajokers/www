angular.module('starter.controllers')


.controller('modificarVideoCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$httpParamSerializer','$ionicModal','$cordovaFileTransfer','$cordovaCamera','$ionicPopup','$ionicLoading',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$httpParamSerializer,$ionicModal,$cordovaFileTransfer,$cordovaCamera,$ionicPopup,$ionicLoading) {
  // With the new view caching in Ionic, Controllers are only called
  
  $scope.imagenes    = [];
  $scope.hola        = [];
  $scope.videos      = [];
  $scope.videosTotal = [];
  $scope.nombreVideo = "";
  $scope.rutaVideo   = "";
  $scope.cantVideos  = 3;
  $scope.noEdita     = false;
  $scope.id_usuario  = localStorage.getItem("id_usuario");
  $scope.nombreFinal = "";

  if( $rootScope.tipo_seleccionado == "patrimonio" ){
    $scope.estiloBarraModificar = {
      "background-color" : "#825012"
    }
  }
  if( $rootScope.tipo_seleccionado == "servicio" ){
    $scope.estiloBarraModificar = {
      "background-color" : "#775DA5"
    }
  }
  if( $rootScope.tipo_seleccionado == "item" ){
    $scope.estiloBarraModificar = {
      "background-color" : "#AF115E"
    }
  }

  console.log( $rootScope.item_seleccionado.id );
  $scope.item = $rootScope.item_seleccionado;

  if( $rootScope.tipo_seleccionado == "item" ){
        $scope.url="http://200.14.68.107/atacamaGoMultimedia/";
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getVideoItemTuristico.php",
          data: $httpParamSerializer({
            "id_item_turistico": $rootScope.item_seleccionado.id,
        }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data);
          if(response.data.resultado == "ok"){
            $scope.video = response.data.item_turistico;

            angular.forEach($scope.video, function(value, key) {
              $scope.videosTotal[key] = { 
                "video"                     : $scope.url + value.video,
                "nombre"                    : value.nombre,
                "fecha"                     : value.fecha,
                "id_usuario"                : value.id_usuario,
                "id_archivo_item_turistico" : value.id_archivo_item_turistico,
                "nombre_delete"             : value.nombre_delete

              };

              if(key < 4){
                $scope.videos[key] = { 
                  "video"                     : $scope.url + value.video,
                  "nombre"                    : value.nombre,
                  "fecha"                     : value.fecha,
                  "id_usuario"                : value.id_usuario,
                  "id_archivo_item_turistico" : value.id_archivo_item_turistico,
                  "nombre_delete"             : value.nombre_delete
                };  
              }
              
            });

            console.log( $rootScope.item_seleccionado.id_usuario, localStorage.getItem("id_usuario") )
            if($rootScope.item_seleccionado.id_usuario == localStorage.getItem("id_usuario")){
              console.log("EDITAR");
              $scope.noEdita = false;
            }
            
          }else if ( response.data.resultado == "no data" ){
            $rootScope.toast('No hay videos', 'short');
            console.log( $rootScope.item_seleccionado.id_usuario, localStorage.getItem("id_usuario") )
            if($rootScope.item_seleccionado.id_usuario == localStorage.getItem("id_usuario")){
              console.log("EDITAR");
              $scope.noEdita = false;
            }
            //$scope.cerrarModalModificarVideo();
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
    }

    if( $rootScope.tipo_seleccionado == "servicio" ){
        $scope.noEdita = false;
        $scope.url="http://200.14.68.107/atacamaGoMultimedia/";
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getVideoServicio.php",
          data: $httpParamSerializer({
            "id_servicio": $rootScope.item_seleccionado.id,
        }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          if(response.data.resultado == "ok"){
            $scope.video = response.data.servicio;

            angular.forEach($scope.video, function(value, key) {
              $scope.videosTotal[key] = { 
                "video" :$scope.url + value.video,
                "nombre" : value.nombre,
                "fecha" : value.fecha,
                "id_usuario"                    : value.id_usuario,
                "id_archivo_servicio_turistico" : value.id_archivo_servicio_turistico,
                "nombre_delete"             : value.nombre_delete
              };

              if(key < 4){
                $scope.videos[key] = { 
                  "video"  : $scope.url + value.video,
                  "nombre" : value.nombre,
                  "fecha" : value.fecha,
                  "id_usuario"                    : value.id_usuario,
                  "id_archivo_servicio_turistico" : value.id_archivo_servicio_turistico,
                  "nombre_delete"             : value.nombre_delete
                };  
              }
              
            });
            
          }else if ( response.data.resultado == "no data" ){
            $rootScope.toast('No hay videos', 'short');
            //$scope.cerrarModalModificarVideo();
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
      
    }

    if( $rootScope.tipo_seleccionado == "patrimonio" ){
        $scope.noEdita = false;
        $scope.url="http://200.14.68.107/atacamaGoMultimedia/";
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getVideoPatrimonio.php",
          data: $httpParamSerializer({
            "id_patrimonio": $rootScope.item_seleccionado.id,
        }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          if(response.data.resultado == "ok"){
            console.log(response.data.patrimonio);
            $scope.video = response.data.patrimonio;

            angular.forEach($scope.video, function(value, key) {
              $scope.videosTotal[key] = { 
                "video"  : $scope.url + value.video,
                "nombre" : value.nombre,
                "fecha" : value.fecha,
                "id_usuario"            : value.id_usuario,
                "id_archivo_patrimonio" : value.id_archivo_patrimonio,
                "nombre_delete"             : value.nombre_delete
              };

              if(key < 4){
                $scope.videos[key] = { 
                  "video"  : $scope.url + value.video,
                  "nombre" : value.nombre,
                  "fecha" : value.fecha,
                  "id_usuario"            : value.id_usuario,
                  "id_archivo_patrimonio" : value.id_archivo_patrimonio,
                  "nombre_delete"             : value.nombre_delete
                };  
              }
              
            });

          }else if ( response.data.resultado == "no data" ){
            $rootScope.toast('No hay videos', 'short');
            //$scope.cerrarModalModificarVideo();
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
      
    }
      
  $scope.openModalVisorVideo = function(video) {

    //modal visor de video...
    $ionicModal.fromTemplateUrl('templates/visorVideo.html', {
      scope: $scope
    }).then(function(modal) {
      
      $scope.modalVisorvideo = modal;
      
      console.log("video que llega: " + video);
      $scope.videoUrl = null;
      $scope.videoUrl = video;
      $scope.modalVisorvideo.show();

    });
  }

  $scope.cerrarModalVisorvideo = function(){
    //document.getElementById('video123').src='';
    $scope.modalVisorvideo.remove();
  }

  $scope.cerrarModalModificarVideo = function(){   
    $rootScope.modalModificarVideo.remove();
  }

  $scope.eliminarVideo = function(imagen,index){
        $ionicLoading.show({
            template: 'Cargando...'
        });

        if( $rootScope.tipo_seleccionado == "item" ){
            $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/deleteVideoItemTuristico.php",
                data: $httpParamSerializer({
                    "id_archivo_item_turistico" : imagen.id_archivo_item_turistico,
                    "nombre"                    : imagen.nombre_delete
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
                console.log(response.data);
                if( response.data.resultado == "ok"){
                    $rootScope.toast('Video eliminado correctamente',"short");
                    $scope.videos.splice(index, 1);

                    angular.forEach($scope.videosTotal, function(value, key) {
                      if(key == index){
                        $scope.videosTotal.splice(index, 1);                        
                      }
                    });

                }else{
                    $rootScope.toast('Error eliminando imagen',"short");
                }
                $ionicLoading.hide();
            }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet',"short");
                $ionicLoading.hide();
            });    
        }

        if( $rootScope.tipo_seleccionado == "servicio" ){
            $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/deleteVideoServicio.php",
                data: $httpParamSerializer({
                    "id_archivo_servicio_turistico"   : imagen.id_archivo_servicio_turistico,
                    "nombre"                          : imagen.nombre_delete
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
                console.log(response.data);
                if( response.data.resultado == "ok"){
                    $rootScope.toast('Video eliminado correctamente',"short");
                    $scope.videos.splice(index, 1);

                    angular.forEach($scope.videosTotal, function(value, key) {
                      if(key == index){
                        $scope.videosTotal.splice(index, 1);                        
                      }
                    });
                }else{
                    $rootScope.toast('Error eliminando imagen',"short");
                }
                $ionicLoading.hide();
            }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet',"short");
                $ionicLoading.hide();
            });
        }
        
        if( $rootScope.tipo_seleccionado == "patrimonio" ){
            $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/deleteVideoPatrimonio.php",
                data: $httpParamSerializer({
                    "id_archivo_patrimonio"   : imagen.id_archivo_patrimonio,
                    "nombre"                  : imagen.nombre_delete
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
                console.log(response.data);
                if( response.data.resultado == "ok"){
                    $rootScope.toast('Video eliminado correctamente',"short");
                    $scope.videos.splice(index, 1);

                    angular.forEach($scope.videosTotal, function(value, key) {
                      if(key == index){
                        $scope.videosTotal.splice(index, 1);                        
                      }
                    });
                    
                }else{
                    $rootScope.toast('Error eliminando imagen',"short");
                }
                $ionicLoading.hide();
            }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet',"short");
                $ionicLoading.hide();
            });  
        }
  }

  $scope.chooseVideo = function () {
        var options = {
            quality         : 10,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType      : Camera.PictureSourceType.PHOTOLIBRARY,
            mediaType       : Camera.MediaType.VIDEO,
            correctOrientation : true
        };
   
        $cordovaCamera.getPicture(options).then(function (videoData) {
            $scope.rutaVideo     = videoData;
            $scope.nombreVideo   = Math.floor((Math.random() * 9999999) + 1)+"_"+videoData.substr(videoData.lastIndexOf('/') + 1);
            $scope.modalNombrevideo();
            $scope.addVideoItem();
            //console.log($scope.rutaVideo, $scope.nombreVideo);
        }, function (err) {
          //alert(err);
            // An error occured. Show a message to the user
        });
    }
  

  $scope.capturarVideo = function() {
    var options = { 
      quality  : 10,
      limit    : 1, 
      duration : 15 
    };

    $cordovaCapture.captureVideo(options).then(function(videoData) {
      // Success! Video data is here
      $scope.rutaVideo   = videoData[0].fullPath;
      //console.log(videoData);
      $scope.nombreVideo = Math.floor((Math.random() * 9999999) + 1)+"_"+videoData[0].name;
      $scope.modalNombrevideo();
      $scope.addVideoItem();
    }, function(err) {
      // An error occurred. Show a message to the user
    });
  }

  $scope.addVideoItem = function(){
      $ionicLoading.show({
        template: 'Cargando...'
      });

      var posOptions = {timeout: 10000, enableHighAccuracy: false};
      navigator.geolocation.getCurrentPosition(
        function(position){
          var lat  = position.coords.latitude;
          var long = position.coords.longitude;
          $scope.addVideoItemGeo(lat,long);
        },function(err) {
          $scope.addVideoItemGeo(0,0);
        },posOptions
      );
  }

  $scope.addVideoItemGeo = function(lat,long){
    if( $scope.nombreVideo != "" ){
      $ionicLoading.show({
        template: 'Cargando...'
      });

      var options = {
        fileKey: "videoItem",
        fileName: $scope.nombreVideo,
        chunkedMode: false,
        mimeType: "video/mp4"
      };

      $scope.noEdita = true;
      console.log("Nombre imagen" + $scope.nombre);
      
      if( $rootScope.tipo_seleccionado == "item" ){
        $scope.nombreFinal = $scope.nombre.nombre;
        $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirVideoItem.php", $scope.rutaVideo, options).then(function(result) {
          //
        }, function(err) {
          $rootScope.toast('Error agregando video', 'short');
          $ionicLoading.hide();
          console.log(err);
        }, function (progress) {
          // constant progress updates
          console.log("subiendo video...");
        });

        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addArchivoItemTuristico.php",
          data: $httpParamSerializer({
            "id_item_turistico"   : $rootScope.item_seleccionado.id,
            "id_usuario"          : localStorage.getItem("id_usuario"),
            "nombre_archivo_item" : $scope.nombreVideo,
            "tipo_archivo_item"   : 3,
            "nombre_video"        : $scope.nombreFinal,
            "lat"                 : lat,
            "long"                : long
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data);
          if(response.data.resultado == "ok"){
            $scope.videos.unshift({ 
              "video" : $scope.rutaVideo,
              "nombre" : $scope.nombreFinal,
              "fecha" : 'Ahora',
              "id_usuario"                : localStorage.getItem("id_usuario"),
              "id_archivo_item_turistico" : response.data.id_archivo_item_turistico,
              "nombre_delete"   : $scope.nombreImagen
            });

            $scope.videosTotal.unshift({
              "video"                         : $scope.rutaVideo,
              "nombre"                        : $scope.nombreFinal,
              "fecha"                         : 'Ahora',
              "id_usuario"                    : localStorage.getItem("id_usuario"),
              "id_archivo_item_turistico"     : response.data.id_archivo_servicio_turistico,
              "nombre_delete"                 : $scope.nombreVideo
            });

            $rootScope.toast("Video agregado.","short");

            $scope.noEdita = false;
            $scope.nombreVideo = "";
            $ionicLoading.hide();
          }else
            $rootScope.toast("Ha ocurrido un error intenta nuevamente","short");
          $ionicLoading.hide();
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet',"short");
          $ionicLoading.hide();
        });
      }

      if( $rootScope.tipo_seleccionado == "servicio" ){
        $scope.nombreFinal = $scope.nombre.nombre;
        $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirVideoServicio.php", $scope.rutaVideo, options).then(function(result) {
          //
        }, function(err) {
          $rootScope.toast('Error agregando video', 'short');
          $ionicLoading.hide();
          console.log(err);
        }, function (progress) {
          // constant progress updates
          console.log("subiendo video...");
        });
        
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addArchivoServicio.php",
          data: $httpParamSerializer({
            "id_servicio"           : $rootScope.item_seleccionado.id,
            "id_usuario"            : localStorage.getItem("id_usuario"),
            "ruta_archivo_servicio" : $scope.nombreVideo,
            "tipo_archivo_servicio" : 3,
            "nombre_video"          : $scope.nombre.nombre,
            "lat"                   : lat,
            "long"                  : long
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data);
          console.log( "nombre video: " + $scope.nombre.nombre );
          if(response.data.resultado == "ok"){
            $scope.videos.unshift({ 
              "video"                         : $scope.rutaVideo,
              "nombre"                        : $scope.nombreFinal,
              "fecha"                         : 'Ahora',
              "id_usuario"                    : localStorage.getItem("id_usuario"),
              "id_archivo_servicio_turistico" : response.data.id_archivo_servicio_turistico,
              "nombre_delete"                 : $scope.nombreVideo
            });

            $scope.videosTotal.unshift({
              "video"                         : $scope.rutaVideo,
              "nombre"                        : $scope.nombreFinal,
              "fecha"                         : 'Ahora',
              "id_usuario"                    : localStorage.getItem("id_usuario"),
              "id_archivo_servicio_turistico" : response.data.id_archivo_servicio_turistico,
              "nombre_delete"                 : $scope.nombreVideo
            });

            $rootScope.toast("Video agregado.","short"); 
            $scope.noEdita = false;
            $scope.nombreVideo = "";

          }else
            $rootScope.toast("Ha ocurrido un error intenta nuevamente","short");
          $ionicLoading.hide();
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet',"short");
          $ionicLoading.hide();
        })
        
      }

      if( $rootScope.tipo_seleccionado == "patrimonio" ){     
        $scope.nombreFinal = $scope.nombre.nombre; 
        $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirVideoPatrimonio.php", $scope.rutaVideo, options).then(function(result) {
          //
        }, function(err) {
          $rootScope.toast('Error agregando video', 'short');
          console.log(err);
          $ionicLoading.hide();
        }, function (progress) {
          // constant progress updates
          console.log("subiendo video...");
        });
        
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addArchivoPatrimonio.php",
          data: $httpParamSerializer({
            "id_patrimonio"           : $rootScope.item_seleccionado.id,
            "id_usuario"              : localStorage.getItem("id_usuario"),
            "ruta_archivo_patrimonio" : $scope.nombreVideo,
            "tipo_archivo_patrimonio" : 3,
            "nombre_video"            : $scope.nombre.nombre,
            "lat"                     : lat,
            "long"                    : long
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data);
          if(response.data.resultado == "ok"){
            $scope.videos.unshift({ 
              "video"                 : $scope.rutaVideo,
              "nombre"                : $scope.nombreFinal,
              "fecha"                 : 'Ahora',
              "id_usuario"            : localStorage.getItem("id_usuario"),
              "id_archivo_patrimonio" : response.data.id_archivo_patrimonio,
              "nombre_delete"         : $scope.nombreVideo
            });

            $scope.videosTotal.unshift({
              "video"                         : $scope.rutaVideo,
              "nombre"                        : $scope.nombreFinal,
              "fecha"                         : 'Ahora',
              "id_usuario"                    : localStorage.getItem("id_usuario"),
              "id_archivo_patrimonio" : response.data.id_archivo_servicio_turistico,
              "nombre_delete"                 : $scope.nombreVideo
            });

            $rootScope.toast("Video agregado.","short");

            $scope.noEdita = false;
            $scope.nombreVideo = "";     
          }else
            $rootScope.toast("Ha ocurrido un error intenta nuevamente","short");
          $ionicLoading.hide();
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet',"short");
          $ionicLoading.hide();
        });
        
      }
      $scope.nombre.nombre  = ""; 
    }else{
      $rootScope.toast('Debes seleccionar un video',"short");
      $ionicLoading.hide();
    }
      
  }

  $scope.masVideos = function(cantVideos){
    console.log("entro a mas videos");
    $scope.videos = [];
    //obtengo los comentarios 20, 30, 40, etc...
    angular.forEach($scope.videosTotal, function(value, key) {
      console.log($scope.videosTotal);
      if( key < (cantVideos*4) ){
        $scope.videos.push($scope.videosTotal[key]);    
      }
    });
    $scope.cantVideos ++;
  }

  $scope.modalNombrevideo = function(){
    $scope.nombre = {};

    // An elaborate, custom popup
    var myPopup = $ionicPopup.show({
      template: '<input type="text" ng-model="nombre.nombre">',
      title: 'Nombre video',
      subTitle: 'Dale un nombre al video',
      scope: $scope,
      buttons: [
        { text: 'Cancelar' },
        {
          text: '<b>Aceptar</b>',
          type: 'button-positive',
          onTap: function(e) {
            if (!$scope.nombre.nombre) {
              //don't allow the user to close unless he enters nombre password
              e.preventDefault();
            } else {
              console.log($scope.nombre.nombre);
              return $scope.nombre.nombre;
            }
          }
        }
      ]
    });

    myPopup.then(function(res) {
      console.log('Tapped!', res);
    });
  }
}]);