angular.module('starter.controllers')


.controller('eventoSeleccionadoCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaCamera','$cordovaFileTransfer','$timeout','$ionicLoading','$ionicModal','$sce','$ionicNativeTransitions',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaCamera,$cordovaFileTransfer,$timeout,$ionicLoading,$ionicModal,$sce,$ionicNativeTransitions) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.mostrarVerMapa = true;
  console.log( $rootScope.evento_seleccionado );

  $scope.cerrarModalEventoSeleccionado = function(){
    console.log("cerar modal cerrarModalEventoSeleccionado");
    //alert()
    if( $rootScope.desdeEventosController == false ){
      console.log("desdeEventosController");
      $rootScope.map.setClickable(true);
    }

    if( $rootScope.fromMapa == true ){
      console.log("frommapa");
      $rootScope.map.setClickable(true);
    }else{
      console.log("vacio")
    }
    
    $rootScope.fromMapa = false;
    $rootScope.desdeEventosController = false;
    $rootScope.modalEventoSeleccionado.remove();
  }

  //para transformar el texto a html, ya que no pasaba la etiqueta style=".."
  $scope.trustAsHtml = function(string) {
    return $sce.trustAsHtml(string);$ionicNativeTransitions
  };

  $scope.hrefWeb = function(link){
    window.open(link,'_blank','location=yes');
  }

  $scope.mailToCorreo = function(link){
    window.open('mailto:${link}','_system');
  }

  $scope.volverServiciosAtractivos = function(){
    //$rootScope.modalEventos.remove();
    console.log("volverServiciosAtractivos");
    $rootScope.modalEventoSeleccionado.remove();
    console.log("evento: ",$rootScope.volverServicioYAtractivo2);
    if ($rootScope.volverServicioYAtractivo2 == true) {
      $rootScope.modalEventos.remove();
      $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
        "type": "slide",
        "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
        "duration": 400, // in milliseconds (ms), default 400
      });
    }else{
      //$rootScope.map.setClickable(true);
      $rootScope.map.clear();
      $rootScope.map.off();
      $rootScope.map.remove();
      
      $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
          "type": "slide",
          "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
          "duration": 400, // in milliseconds (ms), default 400
      });
    }
    
  }

  $scope.verEvento = function(){
    $rootScope.marcadorEvento = $rootScope.evento_seleccionado;
    //$scope.ocultarModalReservas();
    $rootScope.modalEventoSeleccionado.remove();
    $rootScope.modalEventos.remove();
    $state.go("app.eventoVer");
  }


  if( $rootScope.fromMapa == true ){
    $scope.mostrarVerMapa = false;
  }else{

  }

  $scope.openModalCompartir = function(){
    //console.log("scope root: ",$rootScope);
    $rootScope.enEvento = true;
    //console.log("evento",$rootScope.evento_seleccionado.id_evento);
    $rootScope.fromEventoSeleccionado = true;

    console.log("$rootScope.evento_seleccionado",$rootScope.evento_seleccionado.es_evento);

    $rootScope.linkTwitter  = encodeURI("http://www.atacama-go.cl/eventos/"+$rootScope.evento_seleccionado.es_evento+"/" + $rootScope.evento_seleccionado.id_evento);
    $rootScope.linkFacebook = encodeURI("http://www.atacama-go.cl/eventos/"+$rootScope.evento_seleccionado.es_evento+"/" + $rootScope.evento_seleccionado.id_evento);

    //Modal Modificar Descripcion...
    $ionicModal.fromTemplateUrl('templates/compartir.html', {
      scope: $rootScope,
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalModificarCompartir = modal;
      
      $rootScope.modalModificarCompartir.show();

    });
  }

}]);