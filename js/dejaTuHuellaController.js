angular.module('starter.controllers')


.controller('dejaTuHuellaCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$ionicModal','$httpParamSerializer','$timeout','$sce','$cordovaCamera','$cordovaFileTransfer','$cordovaSocialSharing','$ionicPopup','$ionicLoading','$ionicNativeTransitions',function($scope,$http,$state,$rootScope,ionicDatePicker,$ionicModal,$httpParamSerializer,$timeout,$sce,$cordovaCamera,$cordovaFileTransfer,$cordovaSocialSharing,$ionicPopup,$ionicLoading,$ionicNativeTransitions) {
    console.log("dejaTuHuellaCtrl");
    $scope.opcionSeleciconada            = false;
    $scope.menuAbiertoUnavez             = false;
    $scope.mostrarBotonesRegistrarhuella = false;
    $scope.verMishuellas                 = false;
    $scope.verTodashuellas               = false;
    $scope.goBack                        = false;
    $scope.titulo                        = "DEJA TU HUELLA";
    $rootScope.desdeOtraVista            = true; 
    $scope.showFiltroAtractivo           = false;
    $scope.showFiltroServicio            = false;

    $scope.servicios_seleccionados       = [];
    $scope.patrimonios_seleccionados     = [];
    $scope.miRuta                        = [];

    width = window.screen.height;
    var alturaMapa = width - 166;
    $scope.styleMapa = {
        "height" : "100%",
        "width"  : "100%"
    };

    document.addEventListener("deviceready", function() {

        var div = document.getElementById("map_canvasDejaTuHuella");
        var COPIAPO = new plugin.google.maps.LatLng(-27.3690175,-70.6756632);

        $rootScope.map = plugin.google.maps.Map.getMap(div,{
            'backgroundColor' : 'white',
            'mapType'         : plugin.google.maps.MapTypeId.ROADMAP,
            'controls' : {
                'compass'          : true,
                'myLocationButton' : true,
                'indoorPicker'     : true,
                'zoom'             : false
            },
            'gestures': {
                'scroll' : true,
                'tilt'   : true,
                'rotate' : true,
                'zoom'   : true
            },
            'camera': {
                'latLng' : COPIAPO,
                'tilt'   : 0,
                'zoom'   : 10,
                'bearing': 0
            }
        });

        $rootScope.map.addEventListener(plugin.google.maps.event.MAP_READY, onMapReady);

    }, false);

    function onMapReady() {
      console.log($rootScope.map);

      // Usage case #4:
      // top = 30px
      // right = 50px
      // bottom = 20px
      // left = 10px
      $rootScope.map.setPadding( 350, 0 );

        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getMenuPatrimonio.php",
          data: {},
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si recato bien los datos
            if(response.data.resultado == "ok"){
              $rootScope.atractivosTuristicos = response.data.sub_tipo_atractivo;
              //console.log($rootScope.atractivosTuristicos);
              $rootScope.atractivosTuristicos.unshift({
                "id_sub_tipo_atractivo" : -1,
                "nombre"                : "Ver todos los atractivos",
                "seleccionado"          : false
              })
              $scope.sub_tipo_atractivo = response.data.sub_tipo_atractivo;
              //console.log($scope.sub_tipo_atractivo);
            }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });

        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getMenuServicios.php",
          data: {},
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si recato bien los datos
            console.log(response.data);

            if(response.data.resultado == "ok"){
              $scope.serviciosTuristicosMenu = response.data.tipo_servicio;
              $scope.serviciosTuristicosMenu.unshift({
                "id_tipo_servicio" : -1,
                "nombre"           : "Ver todos los servicios",
                "seleccionado"     : false
              });
              $scope.tipo_servicio = response.data.tipo_servicio;
            }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });

    }

    function onBtnClicked() {
        map.showDialog();
    }

    

    $scope.cerrarDejaTuHuella = function(){
        //alert($scope.menuAbiertoUnavez);
        if( $scope.menuAbiertoUnavez == true ){
            if( $scope.opcionSeleciconada == true ){
              $scope.opcionSeleciconada = false;
            }else{
              //$rootScope.map.remove();comente esto(por transicion)
              //$state.go("app.nuevoHome");
              //$rootScope.volverExploraYDescubre = true;
              $rootScope.map.clear();
              $rootScope.map.off();
              $rootScope.map.remove();
              $scope.opcionSeleciconada = false;

              $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
                "type": "slide",
                "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
                "duration": 400, // in milliseconds (ms), default 400
              });
            }
        }else{
          if( $scope.verTodashuellas ){
              $rootScope.map.clear();
              $rootScope.map.off();
              $rootScope.map.remove();
          }
          if( $scope.verMishuellas ){
              $rootScope.map.clear();
              $rootScope.map.off();
              $rootScope.map.remove();
          }

          $rootScope.map.clear();
          $rootScope.map.off();
          $rootScope.map.remove();

          //$rootScope.map.remove(); comente esto(por transicion)

          //$rootScope.volverExploraYDescubre = true;
          //$state.go("app.nuevoHome");
          $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
            "type": "slide",
            "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
            "duration": 400, // in milliseconds (ms), default 400
          });
        }
        
    }

    $scope.opcion = function(id){
        $scope.menuAbiertoUnavez = true;
        if(id == 1){
            $scope.titulo = "REGISTRA TU HUELLA";
            $scope.opcionSeleciconada = true;
            $scope.mostrarBotonesRegistrarhuella = true;

            $scope.registroHuella={
                color:"#3A2B86"
            };
            $scope.miHuella={
                color: "#4D4D4D"
            };
            $scope.verHuellas={
                color: "#4D4D4D"
            }

            //var COPIAPO = new plugin.google.maps.LatLng(-27.3690175,-70.6756632);
            //
            //
            $rootScope.map.getCameraPosition(function(camera) {
                var lat = camera.target.lat;
                var lng = camera.target.lng;
              
                console.log( lat,lng);
                var POS_MARCADOR = new plugin.google.maps.LatLng(lat, lng);

                $rootScope.map.addMarker({
                    'position': POS_MARCADOR,
                    'draggable': true,
                    'anchor':  [30, 35],
                    icon: {
                        url: "www/img/deja_tu_huella/deja_huella_azul.png",
                        size: { width: 40, height: 35 },
                    },        
                }, function(marker) {
                    if ($rootScope.marcadorNuevoItem != null) {
                      $rootScope.marcadorNuevoItem.remove();
                    }else{
                      console.log("no existe marcador previo"); 
                    }
                    $rootScope.marcadorNuevoItem = marker;
                    marker.addEventListener(plugin.google.maps.event.MARKER_DRAG_END, function(marker) {
                    marker.getPosition(function(latLng) {
                        //marker.setTitle(latLng.toUrlValue());
                        //marker.showInfoWindow();
                        //var coor = split(",",)
                        console.log(latLng);
                        $rootScope.lat = latLng.lat;
                        $rootScope.lon = latLng.lng;
                      });
                    });
                    $rootScope.map.getCameraPosition(function(latLng) {
                      console.log("deja lista la posicion");
                      console.log(latLng);
                      $rootScope.lat = latLng.target.lat;
                      $rootScope.lon = latLng.target.lng;
                      console.log($rootScope.lat);
                      console.log($rootScope.lon);
                    });
                });
            });

            $rootScope.map.setClickable(false);
            var myPopup = $ionicPopup.show({
              template: '<p CLASS="fuenteRoboto fuenteModal">Presionar prolongadamente para mover marcador.</p>',
              title: 'Aviso',
              scope: $scope,
              buttons: [
                { 
                    text: '<i class="icon ion-close-round"></i>',
                    type:'popclose',
                      onTap: function(e) {
                        $rootScope.map.setClickable(true);
                      }
                },
                {
                  text: '<b>Aceptar</b>',
                  type: 'button-positive asd',
                  onTap: function(e) {
                    $rootScope.map.setClickable(true);
                  }
                }
              ]
            });
        }

        if(id == 2){
            $scope.opcionSeleciconada = false;
            $scope.verHuella(0);

            $scope.registroHuella={
                color:"#4D4D4D"
            };
            $scope.miHuella={
                color: "#3A2B86"
            };
            $scope.verHuellas={
                color: "#4D4D4D"
            }
        }

        if(id == 3){
            $scope.opcionSeleciconada = false;
            $scope.verHuella(1);

            $scope.registroHuella={
                color:"#4D4D4D"
            };
            $scope.miHuella={
                color: "#4D4D4D"
            };
            $scope.verHuellas={
                color: "#3A2B86"
            }
        }
    }

    $scope.addHuella = function(){
        $rootScope.map.clear();
        $rootScope.map.off();
        $scope.mostrarBotonesRegistrarhuella = false;
        $scope.opcionSeleciconada = false;
        $scope.agregarhuellaPhp($rootScope.lat,$rootScope.lon);
    }

    $scope.descartarHuella = function(){
        // $rootScope.map.clear();
        // $rootScope.map.off();
        $rootScope.marcadorNuevoItem.remove();
        $scope.opcionSeleciconada = false;
        $scope.mostrarBotonesRegistrarhuella = false;
    }

    $scope.verHuella = function(valor){
      if(valor == 0){
        $scope.verMishuellas= true;
        $scope.titulo = "MIS HUELLAS";
        console.log("estas viendo la huella");
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getItemTuristicoUsuario.php",
          data: $httpParamSerializer({
            "id_usuario": localStorage.getItem("id_usuario")
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si recato bien los datos
          console.log(response.data.item_turistico);
          if(response.data.resultado == "ok"){
             
            $rootScope.map.clear();
            $rootScope.map.off();
            
            angular.forEach($scope.patrimonios, function(value, key) {

                var ubicacion = new plugin.google.maps.LatLng(
                  parseFloat(value.direccion_georeferenciada_latitud),
                  parseFloat(value.direccion_georeferenciada_longitud) );

                //var url = "www/img/patrimonios/"+value.icono+".png";
                var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                $rootScope.map.addMarker({
                  'position': ubicacion,
                  'title': value.nombre_item_turistico,
                  'anchor':  [30, 45],
                  icon: {
                    url: url,
                    size: { width: 30, height: 45 },
                  },
                  zIndex: 1
                },function(marker) {
                  //accion al hacer clic en el marcador
                  $scope.markerArrayPatrimonio[key] = marker;
                  $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                  $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                  marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                    $rootScope.tipo_seleccionado = "patrimonio";
                    $rootScope.item_seleccionado = value;
                    $scope.openModalMenuAccionesModificar();
                  });
                });
              });

              $scope.markerArrayServiciosTuristicos = [];

              angular.forEach($scope.serviciosTuristicos, function(value, key) {  
               
                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/servicios/"+value.icono+".png";
                    var url = "www/img/servicios/"+value.icono+"Oculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                        //accion al hacer clic en el marcador
                        $scope.markerArrayServiciosTuristicos[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "servicio";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                        });
                    });
                    
              });
            
            $scope.markerArrayItemUsuario = [];
            
            angular.forEach(response.data.item_turistico, function(value, key) {  
              var ubicacion = new plugin.google.maps.LatLng(
              parseFloat(value.direccion_georeferenciada_latitud),
              parseFloat(value.direccion_georeferenciada_longitud) );

              var url = "www/img/deja_tu_huella/mis_huellas.png";

              $rootScope.map.addMarker({
                'position': ubicacion,
                'title': value.nombre,
                'draggable': false,
                'anchor':  [30, 35],
                icon: {
                  url: url,
                  size: { width: 30, height: 35 },
                },
                zIndex: 1
              },function(marker) {
                
                //accion al hacer clic en el marcador
                $scope.markerArrayItemUsuario[key] = marker;
                  marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                    console.log("seleccionaste a un item");
                    $rootScope.imagen_deja_tu_huella = true;
                    $rootScope.tipo_seleccionado = "item";
                    $rootScope.item_seleccionado = value;
                    $scope.openModalMenuAccionesModificar();
                });
              });
                                        
            });
          }
          else{
            console.log("else");
            $scope.markerArrayItemUsuario =[];
            $rootScope.map.clear();

          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });
      }

      if(valor == 1){
        $scope.verTodashuellas= true;
        $scope.titulo = "TODAS LAS HUELLAS";
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getItemTuristicoTotal.php",
          data: $httpParamSerializer({
            "id_usuario": localStorage.getItem("id_usuario")
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si recato bien los datos
            console.log(response.data.item_turistico);
            if(response.data.resultado == "ok"){
              
              $rootScope.map.clear();
              $rootScope.map.off();
              
              angular.forEach($scope.patrimonios, function(value, key) {

                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "www/img/patrimonios/"+value.icono+".png";
                  var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'anchor':  [30, 45],
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayPatrimonio[key] = marker;
                    $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                    $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "patrimonio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });
                });

                $scope.markerArrayServiciosTuristicos = [];

                angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                 
                      var ubicacion = new plugin.google.maps.LatLng(
                        parseFloat(value.direccion_georeferenciada_latitud),
                        parseFloat(value.direccion_georeferenciada_longitud) );

                      //var url = "www/img/servicios/"+value.icono+".png";
                      var url = "www/img/servicios/"+value.icono+"Oculto.png";

                      $rootScope.map.addMarker({
                        'position': ubicacion,
                        'title': value.nombre_item_turistico,
                        'draggable': false,
                        'anchor':  [30, 45],
                        icon: {
                          url: url,
                          size: { width: 30, height: 45 },
                        },
                        zIndex: 1
                      },function(marker) {
                          //accion al hacer clic en el marcador
                          $scope.markerArrayServiciosTuristicos[key] = marker;
                          marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                            $rootScope.tipo_seleccionado = "servicio";
                            $rootScope.item_seleccionado = value;
                            $scope.openModalMenuAccionesModificar();
                          });
                      });
                      
                });
                
              $scope.markerArrayItemTotal = [];
              $scope.itemTuristicosVerTotal = response.data.item_turistico;

              angular.forEach(response.data.item_turistico, function(value, key) {  
                               
              var ubicacion = new plugin.google.maps.LatLng(
                parseFloat(value.direccion_georeferenciada_latitud),
                parseFloat(value.direccion_georeferenciada_longitud) );

                if( value.id_usuario == localStorage.getItem("id_usuario") ){
                  var url = "www/img/deja_tu_huella/mis_huellas.png";  
                }else{
                  var url = "www/img/deja_tu_huella/deja_huella_azul.png";
                }
                

                $rootScope.map.addMarker({
                  'position': ubicacion,
                  'title': value.nombre,
                  'draggable': false,
                  'anchor':  [30, 35],
                  icon: {
                    url: url,
                    size: { width: 30, height: 35 },
                  },
                  zIndex: 1
                },function(marker) {
                  //accion al hacer clic en el marcador
                  $scope.markerArrayItemTotal[key] = marker;
                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      console.log("seleccionaste a un item");
                      $rootScope.imagen_deja_tu_huella = true;
                      $rootScope.tipo_seleccionado = "item";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                  });
                });
                                    
              });
            }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });
      }
         
    }

    $scope.openModalMenuAccionesModificar = function(){

      //Modal menu Acciones Modiciar...
      $ionicModal.fromTemplateUrl('templates/menuAccionesModificar.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalMenuAccionesModificar = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);

        $rootScope.modalMenuAccionesModificar.show();
      
      });
    }

    $scope.agregarhuellaPhp = function(lat, lon){          
          $rootScope.POS_MARCADOR = new plugin.google.maps.LatLng(lat, lon);
          
          //obtenemos la direccion del lugar....
          var request = {
            'position': $rootScope.POS_MARCADOR
          };

          plugin.google.maps.Geocoder.geocode(request, function(results) {
            if (results.length) {
              var result = results[0];
              var position = result.position; 
              var address = {
                numero: result.subThoroughfare || "",
                calle: result.thoroughfare || "",
                ciudad: result.locality || "",
                region: result.adminArea || "",
                postal: result.postalCode || "",
                pais:   result.country || ""
              };

              
              //GUARDAR LA DIRECCION ...
              $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/addItemTuristico.php",
                data: $httpParamSerializer({
                  "id_usuario"                         : localStorage.getItem("id_usuario"),
                  "direccion_georeferenciada_longitud" : lon,
                  "direccion_georeferenciada_latitud"  : lat,
                  "direccion_item"                     : address.calle,
                  "numero_direccion_item"              : address.numero,
                  "ciudad"                             : address.ciudad,
                  "id_ruta_recorrida_turista"          : ""
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si guardó correctamente.
                console.log(response);
                if(response.data.resultado == "ok"){
    
                  //guardar el id_item_turistico para guardar la descripcion titulos videos etc.
                  $rootScope.id_item_turistico = response.data.id_item_turistico;
                  var id_item_turistico_creado_usuario = response.data.id_item_turistico;

                  $ionicModal.fromTemplateUrl('templates/menuAcciones.html', {
                      scope: $rootScope,
                      animation: 'slide-in-up',
                      backdropClickToClose: false,
                  }).then(function(modal) {
                      $rootScope.modalMenuAcciones = modal;
                      $rootScope.map.setClickable(false);
                      $rootScope.modalMenuAcciones.show();
                  });

                  /*creamos el marcador...*/
                  var POS_MARCADOR = new plugin.google.maps.LatLng(lat, lon);
                  var url = 'www/img/deja_tu_huella/mis_huellas.png';
                  $rootScope.map.addMarker({
                    'position': POS_MARCADOR,
                    'draggable': false,
                    'anchor':  [30, 35],
                    icon: {
                      url: url,
                      size: { width: 30, height: 35 },
                    }
                    
                  },function(marker) {
                      //accion al hacer clic en el marcador
                      
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        var marcadorNuevo = {};
                        $http({
                          method: "POST",
                          url: "http://200.14.68.107/atacamaGo/getItemTuristicoNuevo.php",
                          data: $httpParamSerializer({
                            "id_item_turistico": id_item_turistico_creado_usuario,
                            "id_usuario":localStorage.getItem("id_usuario")
                        }),
                          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                        }).then(function(response){ //ok si guardó correctamente.
                          //console.log(response);
                          if(response.data.resultado == "ok"){
                            
                            marcadorNuevo = {
                              "descripcion"                        : response.data.item_turistico.descripcion,
                              "nombre"                             : response.data.item_turistico.nombre,
                              "direccion_georeferenciada_latitud"  : "",
                              "direccion_georeferenciada_longitud" : "",
                              "foto"                               : response.data.item_turistico.foto,
                              "id"                                 : id_item_turistico_creado_usuario,
                              "id_usuario"                         : response.data.item_turistico.id_usuario
                            };

                            $rootScope.tipo_seleccionado = "item";
                            $rootScope.item_seleccionado = marcadorNuevo;
                            $ionicLoading.hide();
                            $scope.openModalMenuAccionesModificar();
                          }else if ( response.data.resultado == "no data" ){
                            //$rootScope.toast('lugar aún no tiene audios', 'short');
                            $ionicLoading.hide();
                          }else{
                            $rootScope.toast('error, intenta nuevamente', 'short');
                            $ionicLoading.hide();
                          }
                        }, function(){ //Error de conexión
                          $rootScope.toast('Verifica tu conexión a internet', 'short');
                          $ionicLoading.hide();
                        });

                      });   
                  });
                  /*fin crear marcador...*/
                  $rootScope.toast('Item agregado', 'short');
                  $ionicLoading.hide();
                }
                else{
                  //$scope.cerrarModalFiguraGps();
                  $rootScope.toast('Sólo se pueden crear ítems en Atacama', 'short');
                  $ionicLoading.hide();
                }

              }, function(){ //Error de conexión
                  $rootScope.toast('Verifica tu conexión a internet', 'short');
                  $ionicLoading.hide();
              });
              //GUARDAR LA DIRECCION ...  
            } else {
              $rootScope.toast('No se encuentra el terreno', 'short');
              $ionicLoading.hide();
            }
          });
    }

    $scope.mostrarFiltroServicios = function(){

      if( $scope.showFiltroServicio ){
        $scope.showFiltroServicio = false;
        $scope.showFiltroAtractivo = false;
      }else{
        $scope.showFiltroServicio = true;
        $scope.showFiltroAtractivo = false;
      }
      
    }

    $scope.mostrarFiltroAtractivos = function(){

      if( $scope.showFiltroAtractivo ){
        $scope.showFiltroAtractivo = false;
        $scope.showFiltroServicio = false;
      }else{
        $scope.showFiltroAtractivo = true;
        $scope.showFiltroServicio = false;
      }
    }

    $scope.mostrarPatrimonios = function(id_sub_tipo_atractivo,index){

      /*document.getElementById("containerLoading").className = "fadeIn";
      if( $scope.popoverPatrimonios.isShown() ){
        $scope.popoverPatrimonios.hide();  
      }*/
      //$scope.showFiltroAtractivo = false;
      console.log(id_sub_tipo_atractivo,index);
      //Cambiar clase al elemento seleccionado
      if( index != -1 ){
        if($scope.sub_tipo_atractivo[index].seleccionado == false)
          $scope.sub_tipo_atractivo[index].seleccionado = true;
        else
          $scope.sub_tipo_atractivo[index].seleccionado = false;
      }

      //Evaluar si se "presionó todos los patrimonios"
      if(id_sub_tipo_atractivo == -1){
        //seleccionar todos los sub tipos atractivos....
        if( $scope.seleccionadoMostrarTodos == false){
          $scope.seleccionadoMostrarTodos = true;
          //además SELECCIONAR todos los elementos
          angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
            value.seleccionado = true;
          });
        }else{
          $scope.seleccionadoMostrarTodos = false;
          //ademas DES SELECCIONAR todos los elementos....
          angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
            value.seleccionado = false;
          });

          //elimnar marcadores de patrimonio...
          $scope.patrimonios = [];
        }
        
        //Cambiar todas los botones a activos...
        //var menuPatrimonio   = document.getElementsByClassName("menuPatrimonio");
        //menuPatrimonio.class = "menuPatrimonio";
      } 

      $scope.showHidePatrimonio = false;
      if($scope.showHidePatrimonio == false){ //está oculto, por lo tanto mostrarlo....
        //console.log("Llamar a php");
        //conseguir patrimonios

        if( id_sub_tipo_atractivo == -1){
          //console.log($scope.patrimonios_seleccionados.length, $scope.sub_tipo_atractivo.length + 1);
          if($scope.patrimonios_seleccionados.length == $scope.sub_tipo_atractivo.length+1){
            //estan todos seleccionados, por lo tanto vaciar el arreglo
            $scope.patrimonios_seleccionados = [];
          }else{

            $scope.patrimonios_seleccionados = [];
            angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
              $scope.patrimonios_seleccionados.push(value.id_sub_tipo_atractivo)     
            });
          }
        }

        if( $scope.patrimonios_seleccionados.length == 0 ){
          $scope.patrimonios_seleccionados.push(id_sub_tipo_atractivo);  
        }else{
          if($scope.patrimonios_seleccionados.indexOf(id_sub_tipo_atractivo) == -1) {
            $scope.patrimonios_seleccionados.push(id_sub_tipo_atractivo);  
          }else
            $scope.patrimonios_seleccionados.splice($scope.patrimonios_seleccionados.indexOf(id_sub_tipo_atractivo), 1);
        }

        if( $scope.patrimonios_seleccionados.length == 0 ){
          $rootScope.map.clear();
          $rootScope.map.off();
          $scope.markerArrayPatrimonio = [];
          $scope.patrimonios = [];

          $scope.markerArrayServiciosTuristicos = [];

                  angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                   
                        var ubicacion = new plugin.google.maps.LatLng(
                          parseFloat(value.direccion_georeferenciada_latitud),
                          parseFloat(value.direccion_georeferenciada_longitud) );

                        //var url = "www/img/servicios/"+value.icono+".png";
                        var url = "www/img/servicios/"+value.icono+"Oculto.png";

                        $rootScope.map.addMarker({
                          'position': ubicacion,
                          'title': value.nombre_item_turistico,
                          'draggable': false,
                          'anchor':  [30, 45],
                          icon: {
                            url: url,
                            size: { width: 30, height: 45 },
                          },
                          zIndex: 1
                        },function(marker) {
                            //accion al hacer clic en el marcador
                            $scope.markerArrayServiciosTuristicos[key] = marker;
                            marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                              $rootScope.tipo_seleccionado = "servicio";
                              $rootScope.item_seleccionado = value;
                              $scope.openModalMenuAccionesModificar();
                            });
                        });
                        
                  });

                  $scope.markerArrayItemUsuario = [];
              
                  angular.forEach($scope.itemTuristicosVer, function(value, key) {                 
                    
                    var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                    if( value.id_usuario == localStorage.getItem("id_usuario") ){
                      var url = "www/img/deja_tu_huella/mis_huellas.png";  
                    }else{
                      var url = "www/img/deja_tu_huella/deja_huella_azul.png";
                    }

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 35],
                      icon: {
                        url: url,
                        size: { width: 30, height: 35 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayItemUsuario[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "item";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                      });
                    });
                                        
                  });

                  $scope.markerArrayItemTotal = [];
              
                  angular.forEach($scope.itemTuristicosVerTotal, function(value, key) {                 
                    
                    var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                    if( value.id_usuario == localStorage.getItem("id_usuario") ){
                      var url = "www/img/deja_tu_huella/mis_huellas.png";  
                    }else{
                      var url = "www/img/deja_tu_huella/deja_huella_azul.png";
                    }

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayItemTotal[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "item";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                      });
                    });
                                        
                  });
        }else{
          $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/getPatrimonioNuevo.php",
            data: $httpParamSerializer({
              id_sub_tipo_atractivo     : id_sub_tipo_atractivo,
              patrimonios_seleccionados : JSON.stringify($scope.patrimonios_seleccionados),
              id_usuario                : localStorage.getItem("id_usuario")
            }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si guardó correctamente
              if(response.data.resultado == "ok"){
                //console.log("Lista patrimoinios: ");
                //console.log(response.data);

                var actualizarMarcadores = true;
                //evaluar si se rescatan patrimoinos o no....
                
                if( $scope.seleccionadoMostrarTodos == false ){
                  var contatorMarcadores = 0;
                  angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
                    
                    if( value.seleccionado == false ){
                      contatorMarcadores++;
                    }
                  });
                  
                }

                if( contatorMarcadores == $scope.sub_tipo_atractivo.length ){
                  $scope.patrimonios_seleccionados = [];
                  $scope.showHidePatrimonio = false;
                  
                  $scope.patrimonios = [];
                }else{
                   $scope.patrimonios = response.data.patrimonio;
                   $scope.showHidePatrimonio = true; //mantener visto...
                }
                
                console.log( $scope.patrimonios );

                $scope.agregarPatrimonios = 1;

                $rootScope.map.clear();
                $rootScope.map.off();

                $scope.markerArrayPatrimonio = [];

                angular.forEach($scope.patrimonios, function(value, key) {

                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/patrimonios/"+value.icono+".png";
                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayPatrimonio[key] = marker;
                      $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                      $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "patrimonio";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                    });
                  });

                  $scope.markerArrayServiciosTuristicos = [];

                  angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                   
                        var ubicacion = new plugin.google.maps.LatLng(
                          parseFloat(value.direccion_georeferenciada_latitud),
                          parseFloat(value.direccion_georeferenciada_longitud) );

                        //var url = "www/img/servicios/"+value.icono+".png";
                        var url = "www/img/servicios/"+value.icono+"Oculto.png";

                        $rootScope.map.addMarker({
                          'position': ubicacion,
                          'title': value.nombre_item_turistico,
                          'draggable': false,
                          'anchor':  [30, 45],
                          icon: {
                            url: url,
                            size: { width: 30, height: 45 },
                          },
                          zIndex: 1
                        },function(marker) {
                            //accion al hacer clic en el marcador
                            $scope.markerArrayServiciosTuristicos[key] = marker;
                            marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                              $rootScope.tipo_seleccionado = "servicio";
                              $rootScope.item_seleccionado = value;
                              $scope.openModalMenuAccionesModificar();
                            });
                        });
                        
                  });

                  $scope.markerArrayItemUsuario = [];
              
                  angular.forEach($scope.itemTuristicosVer, function(value, key) {                 
                    
                    var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                    if( value.id_usuario == localStorage.getItem("id_usuario") ){
                      var url = "www/img/deja_tu_huella/mis_huellas.png";  
                    }else{
                      var url = "www/img/deja_tu_huella/deja_huella_azul.png";
                    }

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 35],
                      icon: {
                        url: url,
                        size: { width: 30, height: 35 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayItemUsuario[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "item";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                      });
                    });
                                        
                  });

                  $scope.markerArrayItemTotal = [];
              
                  angular.forEach($scope.itemTuristicosVerTotal, function(value, key) {                 
                    
                    var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                    if( value.id_usuario == localStorage.getItem("id_usuario") ){
                      var url = "www/img/deja_tu_huella/mis_huellas.png";  
                    }else{
                      var url = "www/img/deja_tu_huella/deja_huella_azul.png";
                    }

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayItemTotal[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "item";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                      });
                    });
                                        
                  });

                  $scope.agregarPatrimonios = 0;

                  //si esta activo la aventura o ruta dinámica mostrar los items dinamicos...
                  if( $scope.btnTerminarAventura == true ){

                    $scope.markerArrayServiciosTuristicosDinamicos = [];
                              
                    angular.forEach($scope.serviciosDinamica, function(value, key) {  
                               
                                    var ubicacion = new plugin.google.maps.LatLng(
                                      parseFloat(value.direccion_georeferenciada_latitud),
                                      parseFloat(value.direccion_georeferenciada_longitud) );

                                    //var url = "www/img/servicios/"+value.icono+".png";
                                    var url = "www/img/servicios/"+value.icono+"Oculto.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.nombre_item_turistico,
                                      'draggable': false,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                        //accion al hacer clic en el marcador
                                        $scope.markerArrayServiciosTuristicosDinamicos[key] = marker;
                                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                          $rootScope.tipo_seleccionado = "servicio";
                                          $rootScope.item_seleccionado = value;
                                          $scope.openModalMenuAccionesModificar();
                                        });
                                    });
                                    
                              });

                    $scope.markerArrayPatrimonioDinamicos = [];

                    angular.forEach($scope.patrimoniosDinamica, function(value, key) {

                                  var ubicacion = new plugin.google.maps.LatLng(
                                    parseFloat(value.direccion_georeferenciada_latitud),
                                    parseFloat(value.direccion_georeferenciada_longitud) );

                                  //var url = "www/img/patrimonios/"+value.icono+".png";
                                  var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                                  $rootScope.map.addMarker({
                                    'position': ubicacion,
                                    'title': value.nombre_item_turistico,
                                    'draggable': false,
                                    'anchor':  [30, 45],
                                    /*icon: {
                                      url: url,
                                      size: { width: 30, height: 45 },
                                    },*/
                                    zIndex: 1
                                  },function(marker) {
                                    //accion al hacer clic en el marcador
                                    $scope.markerArrayPatrimonioDinamicos[key] = marker;
                                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                      $rootScope.tipo_seleccionado = "patrimonio";
                                      $rootScope.item_seleccionado = value;
                                      $scope.openModalMenuAccionesModificar();
                                    });
                                  });
                    });

                    $scope.markerItemDinamicos = [];
                    angular.forEach($scope.itemDinamico, function(value, key) {

                                  var ubicacion = new plugin.google.maps.LatLng(
                                    parseFloat(value.direccion_georeferenciada_latitud),
                                    parseFloat(value.direccion_georeferenciada_longitud) );

                                  //var url = "www/img/patrimonios/"+value.icono+".png";
                                  if( value.id_usuario == localStorage.getItem("id_usuario") ){
                                    var url = "www/img/deja_tu_huella/mis_huellas.png";  
                                  }else{
                                    var url = "www/img/deja_tu_huella/deja_huella_azul.png";
                                  }

                                  $rootScope.map.addMarker({
                                    'position': ubicacion,
                                    'title': value.nombre_item_turistico,
                                    'draggable': false,
                                    'anchor':  [30, 45],
                                    icon: {
                                      url: url,
                                      size: { width: 30, height: 45 },
                                    },
                                    zIndex: 1
                                  },function(marker) {
                                    //accion al hacer clic en el marcador
                                    $scope.markerItemDinamicos[key] = marker;
                                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                      $rootScope.tipo_seleccionado = "item";
                                      $rootScope.item_seleccionado = value;
                                      $scope.openModalMenuAccionesModificar();
                                    });
                                  });
                    });
                  }
                  //document.getElementById("containerLoading").className = "fadeOut";
                  $scope.bloquearMostrarPatrimonios = false;
              }
          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
          });
        }
        
      }else{ //esta visible, por lo tanto ocultarlo

      }
    }

    //Mostrar Servicios, ocurre cuando se aprieta "mostrar todos" o alguna
    //categoria de servicios....
    $scope.mostrarServiciosTuristicos = function(id_tipo_servicio,index){
      //document.getElementById("containerLoading").className = "fadeIn";
      //$scope.popoverServicios.hide();
      //Cambiar clase al elemento seleccionado
      //$scope.showFiltroServicio = false;

      if( index != -1 ){
        if($scope.tipo_servicio[index].seleccionado == false)
          $scope.tipo_servicio[index].seleccionado = true;
        else
          $scope.tipo_servicio[index].seleccionado = false;
      }

      //Evaluar si se "presionó todos los patrimonios"
      if(id_tipo_servicio == -1){
        //seleccionar todos los sub tipos servicios....
        if( $scope.seleccionadoMostrarTodosServicio == false){
          $scope.seleccionadoMostrarTodosServicio = true;
          //además SELECCIONAR todos los elementos
          angular.forEach($scope.tipo_servicio, function(value, key) {
            value.seleccionado = true;
          });
        }else{
          $scope.seleccionadoMostrarTodosServicio = false;
          //ademas DES SELECCIONAR todos los elementos....
          angular.forEach($scope.tipo_servicio, function(value, key) {
            value.seleccionado = false;
          });

          //elimnar marcadores de patrimonio...
          $scope.serviciosTuristicos = [];
        }
        
        //Cambiar todas los botones a activos...
        //var menuServicios   = document.getElementsByClassName("menuServicios");
        //menuServicios.class = "menuServicios";
      } 

      $scope.showHideServicio = false;
      if($scope.showHideServicio == false){ //está oculto, por lo tanto mostrarlo....

        if( id_tipo_servicio == -1){
          if($scope.servicios_seleccionados.length == $scope.tipo_servicio.length+1){
            //estan todos seleccionados, por lo tanto vaciar el arreglo
            $scope.servicios_seleccionados = [];
          }else{

            $scope.servicios_seleccionados = [];
            angular.forEach($scope.tipo_servicio, function(value, key) {
              $scope.servicios_seleccionados.push(value.id_tipo_servicio)     
            });
          }
        }
        /*---------------------------------*/
        if( $scope.servicios_seleccionados.length == 0 ){
          $scope.servicios_seleccionados.push(id_tipo_servicio);
        }else{
          if($scope.servicios_seleccionados.indexOf(id_tipo_servicio) == -1) {
            $scope.servicios_seleccionados.push(id_tipo_servicio);
          }else
            $scope.servicios_seleccionados.splice($scope.servicios_seleccionados.indexOf(id_tipo_servicio), 1);
        }

        console.log(id_tipo_servicio,index,$scope.servicios_seleccionados);
        if( $scope.servicios_seleccionados.length == 0 ){
          $rootScope.map.clear();
          $rootScope.map.off();

          $scope.markerArrayServiciosTuristicos = [];
          $scope.serviciosTuristicos = [];

          $scope.markerArrayPatrimonio = [];

                angular.forEach($scope.patrimonios, function(value, key) {

                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/patrimonios/"+value.icono+".png";
                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayPatrimonio[key] = marker;
                      $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                      $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "patrimonio";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                    });
                });

                $scope.markerArrayItemUsuario = [];
              
                angular.forEach($scope.itemTuristicosVer, function(value, key) {                 
                    
                  var ubicacion = new plugin.google.maps.LatLng(
                  parseFloat(value.direccion_georeferenciada_latitud),
                  parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "www/img/posicionGpsTuristaOculto.png";
                  if( value.id_usuario == localStorage.getItem("id_usuario") ){
                    var url = "www/img/deja_tu_huella/mis_huellas.png";  
                  }else{
                    var url = "www/img/deja_tu_huella/deja_huella_azul.png";
                  }

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': false,
                    'anchor':  [30, 35],
                    icon: {
                      url: url,
                      size: { width: 30, height: 35 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayItemUsuario[key] = marker;
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "item";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                    });
                  });
                                        
                });

                $scope.markerArrayItemTotal = [];
              
                angular.forEach($scope.itemTuristicosVerTotal, function(value, key) {                 
                    
                    var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/posicionGpsTuristaOculto.png";
                    if( value.id_usuario == localStorage.getItem("id_usuario") ){
                      var url = "www/img/deja_tu_huella/mis_huellas.png";  
                    }else{
                      var url = "www/img/deja_tu_huella/deja_huella_azul.png";
                    }

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayItemTotal[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "item";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                      });
                    });
                                        
                });

        }else{
          //conseguir Servivios tuiristicos
          $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/getServiciosTuristicosNuevo.php",
            data: $httpParamSerializer({
              id_tipo_servicio        : id_tipo_servicio,
              servicios_seleccionados : JSON.stringify($scope.servicios_seleccionados),
              id_usuario              : localStorage.getItem("id_usuario")
            }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si guardó correctamente
              if(response.data.resultado == "ok"){
                console.log(response.data);

                var actualizarMarcadoresServicios = true;
                //evaluar si se rescatan servicios o no....
                
                if( $scope.seleccionadoMostrarTodosServicio == false ){
                  var contatorMarcadoresServicio = 0;
                  angular.forEach($scope.tipo_servicio, function(value, key) {
                    
                    if( value.seleccionado == false ){
                      contatorMarcadoresServicio++;
                    }
                  });     
                }

                if( contatorMarcadoresServicio == $scope.tipo_servicio.length ){
                  $scope.servicios_seleccionados = [];
                  $scope.showHideServicio        = false;
                  $scope.serviciosTuristicos     = [];
                }else{
                   $scope.serviciosTuristicos    = response.data.servicios;
                   $scope.showHideServicio = true; //mantener visto...
                }
                
                $scope.agregarServicios = 1;
                
                $rootScope.map.clear();
                $rootScope.map.off();
                $scope.markerArrayServiciosTuristicos = [];

                angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                 
                      var ubicacion = new plugin.google.maps.LatLng(
                        parseFloat(value.direccion_georeferenciada_latitud),
                        parseFloat(value.direccion_georeferenciada_longitud) );

                      //var url = "www/img/servicios/"+value.icono+".png";
                      var url = "www/img/servicios/"+value.icono+"Oculto.png";

                      $rootScope.map.addMarker({
                        'position': ubicacion,
                        'title': value.nombre_item_turistico,
                        'draggable': false,
                        'anchor':  [30, 45],
                        icon: {
                          url: url,
                          size: { width: 30, height: 45 },
                        },
                        zIndex: 1
                      },function(marker) {
                          //accion al hacer clic en el marcador
                          $scope.markerArrayServiciosTuristicos[key] = marker;
                          marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                            $rootScope.tipo_seleccionado = "servicio";
                            $rootScope.item_seleccionado = value;
                            $scope.openModalMenuAccionesModificar();
                          });
                      });
                      
                });

                $scope.markerArrayPatrimonio = [];

                angular.forEach($scope.patrimonios, function(value, key) {

                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/patrimonios/"+value.icono+".png";
                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayPatrimonio[key] = marker;
                      $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                      $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "patrimonio";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                    });
                });

                $scope.markerArrayItemUsuario = [];
              
                angular.forEach($scope.itemTuristicosVer, function(value, key) {                 
                    
                  var ubicacion = new plugin.google.maps.LatLng(
                  parseFloat(value.direccion_georeferenciada_latitud),
                  parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "www/img/posicionGpsTuristaOculto.png";
                  if( value.id_usuario == localStorage.getItem("id_usuario") ){
                    var url = "www/img/deja_tu_huella/mis_huellas.png";  
                  }else{
                    var url = "www/img/deja_tu_huella/deja_huella_azul.png";
                  }

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': false,
                    'anchor':  [30, 35],
                    icon: {
                      url: url,
                      size: { width: 30, height: 35 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayItemUsuario[key] = marker;
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "item";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                    });
                  });
                                        
                });

                $scope.markerArrayItemTotal = [];
              
                angular.forEach($scope.itemTuristicosVerTotal, function(value, key) {                 
                    
                    var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/posicionGpsTuristaOculto.png";
                    if( value.id_usuario == localStorage.getItem("id_usuario") ){
                      var url = "www/img/deja_tu_huella/mis_huellas.png";  
                    }else{
                      var url = "www/img/deja_tu_huella/deja_huella_azul.png";
                    }

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayItemTotal[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "item";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                      });
                    });
                                        
                });

                //si esta activo la aventura o ruta dinámica mostrar los items dinamicos...
                if( $scope.btnTerminarAventura == true ){

                    $scope.markerArrayServiciosTuristicosDinamicos = [];
                              
                    angular.forEach($scope.serviciosDinamica, function(value, key) {  
                               
                                    var ubicacion = new plugin.google.maps.LatLng(
                                      parseFloat(value.direccion_georeferenciada_latitud),
                                      parseFloat(value.direccion_georeferenciada_longitud) );

                                    //var url = "www/img/servicios/"+value.icono+".png";
                                    var url = "www/img/servicios/"+value.icono+"Oculto.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.nombre_item_turistico,
                                      'draggable': false,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                        //accion al hacer clic en el marcador
                                        $scope.markerArrayServiciosTuristicosDinamicos[key] = marker;
                                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                          $rootScope.tipo_seleccionado = "servicio";
                                          $rootScope.item_seleccionado = value;
                                          $scope.openModalMenuAccionesModificar();
                                        });
                                    });
                                    
                              });

                    $scope.markerArrayPatrimonioDinamicos = [];

                    angular.forEach($scope.patrimoniosDinamica, function(value, key) {

                                  var ubicacion = new plugin.google.maps.LatLng(
                                    parseFloat(value.direccion_georeferenciada_latitud),
                                    parseFloat(value.direccion_georeferenciada_longitud) );

                                  //var url = "www/img/patrimonios/"+value.icono+".png";
                                  var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                                  $rootScope.map.addMarker({
                                    'position': ubicacion,
                                    'title': value.nombre_item_turistico,
                                    'draggable': false,
                                    'anchor':  [30, 45],
                                    icon: {
                                      url: url,
                                      size: { width: 30, height: 45 },
                                    },
                                    zIndex: 1
                                  },function(marker) {
                                    //accion al hacer clic en el marcador
                                    $scope.markerArrayPatrimonioDinamicos[key] = marker;
                                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                      $rootScope.tipo_seleccionado = "patrimonio";
                                      $rootScope.item_seleccionado = value;
                                      $scope.openModalMenuAccionesModificar();
                                    });
                                  });
                    });

                    $scope.markerItemDinamicos = [];
                    angular.forEach($scope.itemDinamico, function(value, key) {

                                  var ubicacion = new plugin.google.maps.LatLng(
                                    parseFloat(value.direccion_georeferenciada_latitud),
                                    parseFloat(value.direccion_georeferenciada_longitud) );

                                  //var url = "www/img/patrimonios/"+value.icono+".png";
                                  var url = "www/img/posicionGpsTuristaOculto.png";

                                  $rootScope.map.addMarker({
                                    'position': ubicacion,
                                    'title': value.nombre_item_turistico,
                                    'draggable': false,
                                    'anchor':  [30, 45],
                                    icon: {
                                      url: url,
                                      size: { width: 30, height: 45 },
                                    },
                                    zIndex: 1
                                  },function(marker) {
                                    //accion al hacer clic en el marcador
                                    $scope.markerItemDinamicos[key] = marker;
                                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                      $rootScope.tipo_seleccionado = "item";
                                      $rootScope.item_seleccionado = value;
                                      $scope.openModalMenuAccionesModificar();
                                    });
                                  });
                    });
                }

                //document.getElementById("containerLoading").className = "fadeOut";
                $scope.bloquearMostrarServicios = false;
              }
          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
          });
        }
        
      }else{ //esta visible, por lo tanto ocultarlo

      }
    }

    $scope.cerrarModalFiltro = function(){
      $scope.showFiltroServicio  = false;
      $scope.showFiltroAtractivo = false;
    }

    $rootScope.verMisHuellasFromEliminar = function(){
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getItemTuristicoUsuario.php",
          data: $httpParamSerializer({
            "id_usuario": localStorage.getItem("id_usuario")
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si recato bien los datos
          console.log(response.data.item_turistico);
          if(response.data.resultado == "ok"){
             
            // $rootScope.map.clear();
            // $rootScope.map.off();
            $scope.markerArrayItemUsuario = [];
            $scope.itemTuristicosVer = response.data.item_turistico;
            
            angular.forEach(response.data.item_turistico, function(value, key) {  
              var ubicacion = new plugin.google.maps.LatLng(
              parseFloat(value.direccion_georeferenciada_latitud),
              parseFloat(value.direccion_georeferenciada_longitud) );

              var url = "www/img/deja_tu_huella/mis_huellas.png";

              $rootScope.map.addMarker({
                'position': ubicacion,
                'title': value.nombre,
                'draggable': false,
                'anchor':  [30, 35],
                icon: {
                  url: url,
                  size: { width: 30, height: 35 },
                },
                zIndex: 1
              },function(marker) {
                
                      //accion al hacer clic en el marcador
                $scope.markerArrayItemUsuario[key] = marker;
                  marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                    console.log("seleccionaste a un item");
                    $rootScope.imagen_deja_tu_huella = true;
                    $rootScope.tipo_seleccionado = "item";
                    $rootScope.item_seleccionado = value;
                    $scope.openModalMenuAccionesModificar();
                });
              });
                                        
            });
          }
          else{
            console.log("else");
            $scope.markerArrayItemUsuario =[];
            $rootScope.map.clear();

          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });
    }
    
}]);