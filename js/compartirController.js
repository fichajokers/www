angular.module('starter.controllers')


.controller('compartirCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$httpParamSerializer','$ionicLoading','$cordovaSocialSharing','$ionicPopup', '$ionicNativeTransitions',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$httpParamSerializer,$ionicLoading,$cordovaSocialSharing,$ionicPopup, $ionicNativeTransitions) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.item = $rootScope.item_seleccionado;

  if( width > 321 && width <= 360 ){

    $scope.tituloCompartir = {
      "height"        : "20px",
      "margin-top"    : "5%",
      "margin-right"  : "1%"
    }

  };

  if( width > 361 && width <= 480 ){

    $scope.tituloCompartir = {
      "height"        : "20px",
      "margin-top"    : "5%",
      "margin-right"  : "1%"
    }

  }

  if( width > 481 && width <= 600 ){

    $scope.tituloCompartir = {
      "height"        : "20px",
      "margin-top"    : "5%",
      "margin-right"  : "1%"
    }


  }

  if( width == 768  ){

    $scope.tituloCompartir = {
      "height"        : "20px",
      "margin-top"    : "2%",
      "margin-right"  : "1%"
    }

  }

  if( $rootScope.fromEventoSeleccionado == true ){
    $scope.item = [];
    $scope.item.nombre = $rootScope.evento_seleccionado.titulo_evento;
    $rootScope.fromEventoSeleccionado = false;
  }
  console.log($rootScope.linkTwitter);
  console.log($rootScope.linkFacebook);

  if( $rootScope.tipo_seleccionado == "patrimonio" ){
    $scope.iconoCompartir = "img/menuAcciones/invCompartirAtractivo.png"
    $scope.imagenTipo = "img/blancos_atractivos/"+$scope.item.icono+".png"
    $scope.estiloBarraModificar = {
      "background-color" : "#825012"
    }
  }
  if( $rootScope.tipo_seleccionado == "servicio" ){
    $scope.iconoCompartir = "img/menuAcciones/compartir.png"
    $scope.imagenTipo = "img/servicios_blancos/"+$scope.item.id_tipo_servicio+".png"
    $scope.estiloBarraModificar = {
      "background-color" : "#775DA5"
    }
  }
  if( $rootScope.tipo_seleccionado == "item" ){
    $scope.estiloBarraModificar = {
      "background-color" : "#3B3C97"
    }
  }

  if($rootScope.enEvento == true){
    console.log("viene de evento");

    $scope.imagenTipo = "img/evento/evento.png"
    $scope.iconoCompartir = "img/evento/compartir.png"
    $scope.estiloBarraModificar = {
      "background-color" : "#AF105E"
    }
    $scope.iconoCompartir = "img/evento/compartir.png"
  }

  $scope.atrasModalCompartir = function(){
      $rootScope.enEvento = false;
      $rootScope.modalModificarCompartir.remove();
  }
  
  $scope.cerrarModalCompartir = function(){
    console.log("cerrarModalCompartir");
    if($rootScope.enEvento == true){
      $rootScope.enEvento = false;
      $rootScope.modalModificarCompartir.remove();
      $rootScope.modalEventoSeleccionado.remove();
      //$rootScope.modalEventos.remove();
      //$rootScope.modalMenuAccionesModificar.remove();

      /*$rootScope.map.clear();
      $rootScope.map.off();
      $rootScope.map.remove();*/

      /*$ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
          "type": "slide",
          "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
          "duration": 400, // in milliseconds (ms), default 400
      });*/
      $rootScope.map.setClickable(true);
    }else {
      $rootScope.modalModificarCompartir.remove();
      $rootScope.modalMenuAccionesModificar.remove();
      $rootScope.map.setClickable(true);
    }
          
  }

  $scope.compartirFacebook = function(){
        $ionicLoading.show({
            template: 'Cargando...'
        });

        link = encodeURI($rootScope.linkFacebook);  

        console.log("compartirFacebook",link);
        $ionicLoading.hide();
        $cordovaSocialSharing.shareViaFacebook(link,null,link)
        .then(function(result) {
            console.log("ok",result);
        }, function(err) {
            $ionicLoading.hide();
            console.log("error",err);
            $ionicPopup.alert({
              title: 'Aviso!',
              template: 'Debes tener la aplicación de facebook para compartir la imagen',
              okText: 'Aceptar', // String (default: 'OK'). The text of the OK button.
              okType: 'btn-morado asd', // String (default: 'button-positive'). The type of the OK button.
            });
        });
    }

    $scope.compartirTwitter  = function(){
        link = encodeURI($rootScope.linkTwitter);

        console.log("compartirTwitter", link);
        $ionicLoading.hide();
        $cordovaSocialSharing.shareViaTwitter("www.atacama-go.cl", null, link)
        .then(function(result) {
            $ionicLoading.hide();
            console.log("ok",result);
        }, function(err) {
            $ionicPopup.alert({
              title: 'Aviso!',
              template: 'Debes tener la aplicación de twitter para compartir la imagen',
              okText: 'Aceptar', // String (default: 'OK'). The text of the OK button.
              okType: 'btn-morado asd', // String (default: 'button-positive'). The type of the OK button.
            });
        });
    }

}]);