angular.module('starter.controllers')


.controller('rutasTipicasCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaGeolocation','$ionicModal','ratingConfig','$ionicLoading','$cordovaNetwork','$ionicNativeTransitions',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaGeolocation,$ionicModal,ratingConfig,$ionicLoading,$cordovaNetwork,$ionicNativeTransitions) {
  // With the new view caching in Ionic, Controllers are only called
  
  $scope.agregarMarkerDinamico       = 0;
  $scope.markerArrayItemRutaDinamica = [];
  $scope.calificacion                = {};
  $scope.filtro                      = "todas";
  $scope.watchPositionCont           = 0;
  $scope.miRuta                      = [];
  $scope.mostrarMenuFiltro           = false;
  $scope.filtroActual                = "todas las rutas";
    

  console.log($scope.rutasOffline);
    //atras rutas tipicas
  	$scope.cerrarModalrutastipicas = function(){
    
	    console.log("cerar modal");
      $rootScope.disableBtnRutasTipicas = false;
    	//if( ionic.Platform.isAndroid() )
    	  //$rootScope.map.setClickable(true);

    	$rootScope.modalRutasTipicas.hide();

       /*$rootScope.map.clear();
       $rootScope.map.off();
       $rootScope.map.remove();
       $rootScope.volverExploraYDescubre = true;
       $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
         "type": "slide",
         "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
         "duration": 0, // in milliseconds (ms), default 400
       });*/

      /*if( $rootScope.atrasRecorrerRutaTipicas ==true ){
         $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
           "type": "slide",
           "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
           "duration": 100, // in milliseconds (ms), default 400
         });
      }*/
  	}

    //conseguir coordenadas de item_turistico
    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/getItemTuristico.php",
      data: $httpParamSerializer({}),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ 
      if(response.data.resultado == "ok"){
        console.log(response.data.item_turistico);
        $scope.marcadores = response.data.item_turistico;
      }
    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
    });

  	$http({
    	method: "POST",
    	url: "http://200.14.68.107/atacamaGo/getRutasTipicas.php",
    	data: $httpParamSerializer({
      	"id_usuario": localStorage.getItem("id_usuario")
    	}),
    	headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  	}).then(function(response){ //ok si recato bien los datos
      	$scope.rutas_tipicas    = response.data.rutas_tipicas;
        $scope.rutas_tipicas_aux = response.data.rutas_tipicas;
      	console.log($scope.rutas_tipicas);

  	}, function(){ //Error de conexión
	    $rootScope.toast('Verifica tu conexión a internet', 'short');
      //cargar los datos de las rutas ofline
      //
      if( localStorage.getItem('rutasOffline') == 'undefined' || localStorage.getItem('rutasOffline') == null ){
        console.log("no hay rutas");
      }
      else{
        var retrievedObject = localStorage.getItem('rutasOffline');
      $scope.rutasOffline = JSON.parse(retrievedObject);
      }
      

      //$scope.rutas_tipicas     = $scope.rutasOffline.ruta;
      //$scope.rutas_tipicas_aux = $scope.rutasOffline.ruta;
      console.log($scope.rutasOffline);

      $scope.rutas_tipicas = [];
      angular.forEach($scope.rutasOffline, function(value, key) {
        $scope.rutas_tipicas.push(value.ruta);
        console.log("Rutas tipicas: ", $scope.rutas_tipicas);
      });

  	});

  	$scope.recorrerRuta = function(id_ruta,estado_ruta){
       $scope.cerrarModalrutastipicas();
      /*$ionicLoading.show({
        template: 'Cargando...'
      });

  		console.log(id_ruta);
      $rootScope.id_ruta_tipica_seleccionada = id_ruta;
      $rootScope.estado_ruta_tipica_seleccionada = estado_ruta;
  		$http({
	    	method: "POST",
	    	url: "http://200.14.68.107/atacamaGo/getCoordenadasRutaTipica.php",
	    	data: $httpParamSerializer({
	      	"id_ruta": id_ruta
	    	}),
	    	headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	  	}).then(function(response){ //ok si recato bien los datos

	      	//console.log(response.data);
	      	
          //console.log(estado_ruta);
          //evaluar si la ruta esta 'en curso' o no...
          if( estado_ruta == 'en curso' ){
            console.log('en curso');
            $rootScope.btnRecorriendoRuta = true;
          }else{
            console.log('Otra cosa');
            $rootScope.btnRecorrerRuta = true;
          }

          if(ionic.Platform.isWebView()){
            $rootScope.map.clear();
            $rootScope.map.off();
          }
          

          //$scope.watchMarcadores();
              
          //se dibuja la ruta en el homeController, en mostrarRecomendados()
          $rootScope.coorRutaTipica = response.data.ruta;


          //dibujar ruta en el mapa....
          $scope.ruta = [];
          angular.forEach($rootScope.coorRutaTipica, function(value, key) {
            var inicio = new plugin.google.maps.LatLng(value.inicio_lat,value.inicio_lng);
            var fin    = new plugin.google.maps.LatLng(value.fin_lat,value.fin_lng);
            //$scope.ruta.push(inicio);
            //$scope.ruta.push(fin);            
                    
            //var idx = 0;
            //
            var flightPlanCoordinates = [
              {lat: parseFloat(value.inicio_lat), lng: parseFloat(value.inicio_lng)},
              {lat: parseFloat(value.fin_lat), lng: parseFloat(value.fin_lng)},
            ];


            $rootScope.map.addPolyline({
              'points': flightPlanCoordinates,
              'color' : "red",
              'width': 3,
              'geodesic': true
            }, function(polyline) {
              polyline.on(plugin.google.maps.event.OVERLAY_CLICK, function() {
                /*polyline.setColor(["green", "blue", "orange", "red"][idx++]);
                idx = idx > 3 ? 0 : idx;*
              });
            });
            //console.log(flightPlanCoordinates);
            if( $rootScope.coorRutaTipica.length == key + 1 ){
              $ionicLoading.hide();
              $rootScope.iniciarRutaEncurso();
              $scope.cerrarModalrutastipicas();
            }
          });

          
          $scope.cerrarModalrutastipicas();

	  	}, function(){ //Error de conexión
		    $rootScope.toast('Verifica tu conexión a internet', 'short');
	  	});*/

  	}

    $scope.openModalMenuAccionesModificar = function(){

      //Modal menu Acciones Modiciar...
      $ionicModal.fromTemplateUrl('templates/menuAccionesModificar.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalMenuAccionesModificar = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);

        $rootScope.modalMenuAccionesModificar.show();
      
      });
    }

    $scope.openModalSaberMas = function(ruta){
      $rootScope.rutaSeleccionada = ruta;
      //Modal menu Acciones Modiciar...
      $ionicModal.fromTemplateUrl('templates/saberMas.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalSaberMas = modal;
        $rootScope.modalSaberMas.show();
      });
    }
   
    $scope.addCalificacion = function(ruta,calificacion){
      $ionicLoading.show({
        template: 'Cargando...'
      });

      var posOptions = {timeout: 10000, enableHighAccuracy: false};
      navigator.geolocation.getCurrentPosition(
        function(position){
          var lat  = position.coords.latitude;
          var long = position.coords.longitude;
          $scope.addCalificacionGeo(lat,long,ruta,calificacion);
        },function(err) {
          $scope.addCalificacionGeo(0,0,ruta,calificacion);
        },posOptions
      );
    }

    $scope.addCalificacionGeo = function(lat,long,ruta,calificacion){
      console.log("Calificacion: " + calificacion);
      $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addCalificacionRutasTipicas.php",
          data: $httpParamSerializer({
            id_usuario   : localStorage.getItem("id_usuario"),
            id_ruta      : ruta.id_ruta,
            calificacion : calificacion,
            "lat"        : lat,
            "long"       : long
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ 
          console.log(response.data);
          $ionicLoading.hide();

          if(response.data.resultado == "ok"){
            //console.log(response.data.item_turistico);
            //$scope.marcadores = response.data.item_turistico;
            $rootScope.toast('Ruta calificada', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
      });
    }

    $scope.mostrarFiltro = function(){
      if( $scope.mostrarMenuFiltro ){
        $scope.mostrarMenuFiltro = false;
      }else{
        $scope.mostrarMenuFiltro = true;
      }
    }

    $scope.filtrar = function(filtro){
      console.log(filtro);
      $scope.rutas_tipicas = {};
      if(filtro == 'todas'){
        $scope.rutas_tipicas = $scope.rutas_tipicas_aux;
        $scope.filtroActual  = "Todas las rutas";
        $scope.mostrarMenuFiltro = false;
      }

      if(filtro == 'enRecorrido'){
        var contador = 0;
        angular.forEach($scope.rutas_tipicas_aux, function(value, key) {
          if(value.estado_ruta == 'en curso'){
            $scope.rutas_tipicas[contador] = value;
            contador +=1;
          }      
        });
        $scope.filtroActual  = "En recorrido";
        $scope.mostrarMenuFiltro = false;
      }

      if(filtro == 'recorridas'){
        var contador = 0;
        angular.forEach($scope.rutas_tipicas_aux, function(value, key) {
          if(value.estado_ruta == 'completa'){

            $scope.rutas_tipicas[contador] = value;
            contador +=1;
          }      
        });
        $scope.filtroActual  = "Recorridas";
        $scope.mostrarMenuFiltro = false;
      }

      if(filtro == 'sinRecorrer'){
        var contador = 0;
        angular.forEach($scope.rutas_tipicas_aux, function(value, key) {
          if(value.estado_ruta == null){
            $scope.rutas_tipicas[contador] = value;
            contador +=1;
          }      
        });
        $scope.filtroActual  = "Sin recorrer";
        $scope.mostrarMenuFiltro = false;
      }
      //console.log("Rutas_tipicas:",$scope.rutas_tipicas);
    }

    $scope.detalleRuta = function(idRuta){
      //$rootScope.rutaSeleccionada = idRuta;
      //Modal menu Acciones Modiciar...
      $ionicModal.fromTemplateUrl('templates/saberMas.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalSaberMas = modal;
        $rootScope.modalSaberMas.show();
      
      });
    }

    /*if ($rootScope.atrasRecorrerRutaTipicas == true){
      $rootScope.atrasRecorrerRutaTipicas = false;
      $scope.openModalSaberMas($rootScope.rutaSeleccionada);

    }*/

    /*if( $rootScope.abrirSaberMas == true){
      $rootScope.abrirSaberMas = false;
      $scope.openModalSaberMas($rootScope.rutaSeleccionada);
    }*/

}]);