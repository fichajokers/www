angular.module('starter.controllers')


.controller('agregarDescripcionCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$httpParamSerializer','$ionicLoading','$ionicModal','$ionicNativeTransitions',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$httpParamSerializer,$ionicLoading,$ionicModal,$ionicNativeTransitions) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.item = [];
  $scope.cerrarModalAgregarDescripcion = function(){   
    $scope.item = [];
    $rootScope.map.clear();
    $rootScope.map.off();
    $rootScope.map.remove();
    $rootScope.modalMenuAcciones.remove();
    $rootScope.modalAgregarDescripcion.remove();
    $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
        "type": "slide",
        "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
        "duration": 400, // in milliseconds (ms), default 400
    });
  }

  	$scope.addDescripcionItem = function(item){
      $ionicLoading.show({
        template: 'Cargando...'
      });

  		$http({
  			method: "POST",
  			url: "http://200.14.68.107/atacamaGo/addDescripcionItemTuristico.php",
  			data: $httpParamSerializer({
  				"id_item_turistico": $rootScope.id_item_turistico,
  				"titulo"           : item.titulo,
  				"descripcion"      : item.descripcion
  			}),
  			headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      	}).then(function(response){ //ok si guardó correctamente.
      		console.log(response.data);
	       	if(response.data.resultado == "ok"){
	            $rootScope.toast('Descripcion agregada', 'short');

              $rootScope.map.addMarker({
                'position': $rootScope.POS_MARCADOR,
                icon: 'darkcyan',
                'title': item.titulo,
              },function(marker) {

              });
          
	        }
	        else{
	          	$rootScope.toast('error, intenta nuevamente', 'short');
	        }
          $ionicLoading.hide();
	    }, function(){ //Error de conexión
	       	$rootScope.toast('Verifica tu conexión a internet', 'short');
          $ionicLoading.hide();
	    });
	}

  $scope.openModalSubirImagen = function(){
      $rootScope.modalAgregarDescripcion.remove();
      //Modal subirImagen...
      $ionicModal.fromTemplateUrl('templates/subirImagen.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalSubirImagen = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $rootScope.modalAgregarDescripcion.remove();
        $rootScope.modalSubirImagen.show();
      });

    }

    $scope.openModalAgregarVideo = function(){

      //Modal Agregar Video...
      $ionicModal.fromTemplateUrl('templates/agregarVideo.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarVideo = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $rootScope.modalAgregarDescripcion.remove();
        $rootScope.modalAgregarVideo.show();
      });
        
    }

    $scope.openModalAgregarAudio = function(){

      //Modal Agregar Audio...
      $ionicModal.fromTemplateUrl('templates/agregarAudio.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarAudio = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $rootScope.modalAgregarDescripcion.remove();
        $rootScope.modalAgregarAudio.show();
      });
  
    }

    $scope.openModalAgregarDescripcion = function(){
      //$rootScope.modalAgregarDescripcion.remove();
      //Modal Agregar descripcion...
      /*$ionicModal.fromTemplateUrl('templates/agregarDescripcion.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarDescripcion = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $scope.cerrarModalAgregarDescripcion();
        $rootScope.modalAgregarDescripcion.show();
      });*/

    }

    $scope.openModalAgregarComentario = function(){
      $rootScope.modalAgregarDescripcion.remove();
      //Modal Agregar Comentario...
      $ionicModal.fromTemplateUrl('templates/agregarComentario.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarComentario = modal;
        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        
        $rootScope.modalAgregarComentario.show();
      });
    }

    $scope.openModalAgregarCalificar = function(){
      $rootScope.modalAgregarDescripcion.remove();
      //Modal Agregar Calificar...
      $ionicModal.fromTemplateUrl('templates/agregarCalificar.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarCalificar = modal;
        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        
        $rootScope.modalAgregarCalificar.show();
      });
  
    }

    $scope.atrasModalAgregarDescripcion = function() {
        $scope.item = [];
        $rootScope.modalAgregarDescripcion.remove();
    }

}]);