angular.module('starter.controllers')


.controller('modificarCalificarCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','ratingConfig','$httpParamSerializer','$ionicLoading',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,ratingConfig,$httpParamSerializer,$ionicLoading) {
  // With the new view caching in Ionic, Controllers are only called
  
  $scope.ratingSeguridad      = {};
  //$scope.ratingSeguridad.rate = 4;
  $scope.ratingSeguridad.max  = 5;
  
  $scope.ratingAtractivo      = {};
  //$scope.ratingAtractivo.rate = 3;
  $scope.ratingAtractivo.max  = 5;
  
  $scope.ratingHigiene        = {};
  //$scope.ratingHigiene.rate   = 5;
  $scope.ratingHigiene.max    = 5;

  $scope.calificacionesOriginal = [];

  $scope.item = $rootScope.item_seleccionado;


  $scope.generalSeguridad;

  if( width > 321 && width <= 360 ){

    $scope.tituloCalificar = {
      "height"        : "20px",
      "margin-top"    : "5%",  
      "margin-right"  : "1%",
    }

    $scope.magenEstrella = {
      "margin-left"   : "30%"
    }
    
  };

  if( width > 361 && width <= 480 ){

    $scope.tituloCalificar = {
      "height"        : "20px",
      "margin-top"    : "5%",  
      "margin-right"  : "1%",
    }

    $scope.magenEstrella = {
      "margin-left"   : "30%"
    }

  }

  if( width > 481 && width <= 600 ){

    $scope.tituloCalificar = {
      "height"        : "20px",
      "margin-top"    : "5%",  
      "margin-right"  : "1%",
    }

    $scope.magenEstrella = {
      "margin-left"   : "30%"
    }

  }

  if( width == 768  ){

    $scope.tituloCalificar = {
      "height"        : "20px",
      "margin-top"    : "2%",  
      "margin-right"  : "1%",
    }

    $scope.magenEstrella = {
      "margin-left"   : "41%"
    }

  }

  if( $rootScope.tipo_seleccionado == "patrimonio" ){
    $scope.imgCalificar = "img/menuAcciones/invCalificacionesAtractivo.png";
    $scope.estiloBarraModificar = {
      "background-color" : "#825012"
    }
  }
  if( $rootScope.tipo_seleccionado == "servicio" ){
    $scope.imgCalificar = "img/menuAcciones/calificaciones.png";
    $scope.estiloBarraModificar = {
      "background-color" : "#775DA5"
    }
  }
  if( $rootScope.tipo_seleccionado == "item" ){
    $scope.imgCalificar = "img/deja_tu_huella/invCalificacionesServicio.png";
    $scope.estiloBarraModificar = {
      "background-color" : "#3B3C97"
    }
  }
  

    $scope.$on('$ionicView.beforeEnter', function() {
        console.log("beforeenter called");
    });
  if( $rootScope.tipo_seleccionado == "item" ){
        //$scope.generalSeguridad = "Seguridad";
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getCalificacionItemTuristico.php",
          data: $httpParamSerializer({
            "id_item_turistico": $rootScope.item_seleccionado.id,
            "id_usuario"       : localStorage.getItem("id_usuario")

        }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.

          if(response.data.resultado == "ok"){
            $scope.calificaciones         = response.data.calificaciones;
            
            angular.forEach(response.data.calificaciones, function(value, key) {
              $scope.calificacionesOriginal[key] = value.puntaje_usuario              
            });

          }else if(response.data.resultado == "no data"){
            $scope.calificaciones         = response.data.calificaciones;
            
            angular.forEach(response.data.calificaciones, function(value, key) {
              $scope.calificacionesOriginal[key] = value.puntaje_usuario              
            });

          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
    }

    if( $rootScope.tipo_seleccionado == "servicio" ){
        //$scope.generalSeguridad = "General";
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getCalificacionServicio.php",
          data: $httpParamSerializer({
            "id_servicio": $rootScope.item_seleccionado.id,
            "id_usuario"    : localStorage.getItem("id_usuario")
        }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
            //console.log(response.data);
          if(response.data.resultado == "ok"){
            $scope.calificaciones         = response.data.calificaciones;

            angular.forEach(response.data.calificaciones, function(value, key) {
              $scope.calificacionesOriginal[key] = value.puntaje_usuario              
            });
          }else if ( response.data.resultado == "no data" ){
            $rootScope.toast('lugar aún no tiene calificaciones', 'short');
            $scope.calificaciones         = response.data.calificaciones;
            
            angular.forEach(response.data.calificaciones, function(value, key) {
              $scope.calificacionesOriginal[key] = value.puntaje_usuario              
            });

            //$scope.cerrarModalModificarCalificar();
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
      
    }

    if( $rootScope.tipo_seleccionado == "patrimonio" ){
        //$scope.generalSeguridad = "General";
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getCalificacionPatrimonio.php",
          data: $httpParamSerializer({
            "id_patrimonio" : $rootScope.item_seleccionado.id,
            "id_usuario"    : localStorage.getItem("id_usuario")
        }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.

          if(response.data.resultado == "ok"){
            $scope.calificaciones         = response.data.calificaciones;
            
            angular.forEach(response.data.calificaciones, function(value, key) {
              $scope.calificacionesOriginal[key] = value.puntaje_usuario              
            });

          }else if ( response.data.resultado == "no data" ){
            $rootScope.toast('lugar aún no tiene calificaciones', 'short');
            $scope.calificaciones         = response.data.calificaciones;
            
            angular.forEach(response.data.calificaciones, function(value, key) {
              $scope.calificacionesOriginal[key] = value.puntaje_usuario              
            });

            //$scope.cerrarModalModificarCalificar();
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short')
        });
      
    }


  $scope.cerrarModalModificarCalificar = function(){   
    $rootScope.modalModificarCalificar.remove();
  }

  $scope.cerrarItemHuella = function(){
        console.log("cerrar item");
        $rootScope.imagen_deja_tu_huella = false; 
        $rootScope.cerrarItemHuella = false;
        console.log("root SCOPE: ",$rootScope);

        /*$rootScope.map.clear();
        $rootScope.map.off();
        $rootScope.map.remove();*/
        
        $rootScope.modalModificarCalificar.remove();
        $rootScope.modalMenuAccionesModificar.remove();
        $rootScope.map.setClickable(true);
    }

  $scope.addCalificacionItem = function(seguridad, atractivo, higiene){
    $ionicLoading.show({
      template: 'Cargando...'
    });

    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    navigator.geolocation.getCurrentPosition(
      function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        $scope.addCalificacionItemGeo(lat,long,seguridad, atractivo, higiene);
      },function(err) {
        $scope.addCalificacionItemGeo(0,0,seguridad, atractivo, higiene);
      },posOptions
    );
  }

  $scope.addCalificacionItemGeo = function(lat, long, seguridad, atractivo, higiene){
    //console.log(seguridad, atractivo, higiene);
    
    console.log($scope.calificaciones);
    console.log($scope.calificacionesOriginal);

    var contador = 0;
    angular.forEach($scope.calificaciones, function(value, key) {
      if( $scope.calificacionesOriginal[key] == $scope.calificaciones[key].puntaje_usuario ){
        console.log("Hola");
        contador++;
      }
    });

    if(contador == 3){
      $rootScope.toast('Realiza una valoración antes de guardar', 'short');
      $ionicLoading.hide();
    }else{
      $ionicLoading.show({
        template: 'Cargando...'
      });

      if( $rootScope.tipo_seleccionado == "item" ){
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addCalificacionItemTuristico.php",
          data: $httpParamSerializer({
            "id_item_turistico" : $rootScope.item_seleccionado.id,
            "id_usuario"  : localStorage.getItem("id_usuario"),
            "calificaciones" : JSON.stringify($scope.calificaciones),
            "lat" : lat,
            "long" : long
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data);
          if(response.data.resultado == "ok"){
            $rootScope.toast('Calificacion agregada', 'short');
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
          $ionicLoading.hide();
        }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
            $ionicLoading.hide();
        });  
      }

      if( $rootScope.tipo_seleccionado == "servicio" ){
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addCalificacionServicio.php",
          data: $httpParamSerializer({
            "id_servicio" : $rootScope.item_seleccionado.id,
            "id_usuario"  : localStorage.getItem("id_usuario"),
            "calificaciones" : JSON.stringify($scope.calificaciones),
            "lat" : lat,
            "long" : long
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data);
          if(response.data.resultado == "ok"){
            $rootScope.toast('Calificacion agregada', 'short');
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
          $ionicLoading.hide();
        }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
            $ionicLoading.hide();
        });
      }

      if( $rootScope.tipo_seleccionado == "patrimonio" ){
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addCalificacionPatrimonio.php",
          data: $httpParamSerializer({
            "id_patrimonio"  : $rootScope.item_seleccionado.id,
            "id_usuario"     : localStorage.getItem("id_usuario"),
            "calificaciones" : JSON.stringify($scope.calificaciones),
            "lat" : lat,
            "long" : long
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data);
          if(response.data.resultado == "ok"){
            $rootScope.toast('Calificacion agregada', 'short');
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
          $ionicLoading.hide();
        }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
            $ionicLoading.hide();
        });
      }
    }

    

  }

}]);