angular.module('starter.controllers')


.controller('preferenciasDetalleCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$ionicLoading','$ionicModal','$ionicNativeTransitions','$ionicPopup',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$ionicLoading,$ionicModal,$ionicNativeTransitions,$ionicPopup) {
  	console.log($rootScope.cuestionario);
  	$scope.distanciaSeleccionada = "Distancia";
	$scope.selectables           = ['50 metros','100 metros','500 metros','1 kilómetro'];
	$scope.material              = [];
	$scope.radio                 = [];

  	$scope.cerrarModalPreferenciasDetalle = function(){
	    /*if( $rootScope.flagPreferencias == 1){
	    	$rootScope.cuestionario.soloAcom = null;
	    }

	    if( $rootScope.flagPreferencias == 2){

	    }

	    if( $rootScope.flagPreferencias == 3){
	    	$rootScope.cuestionario.gasto = null;
	    }

	    if( $rootScope.flagPreferencias == 4){
	    	$rootScope.cuestionario.moneda = null;
	    }

	    if( $rootScope.flagPreferencias == 5){
	    	$rootScope.cuestionario.duracion = null;
	    }

	    if( $rootScope.flagPreferencias == 6){
	    	$rootScope.cuestionario.tipoViaje.vacaciones = null;
	    	$rootScope.cuestionario.tipoViaje.familiares = null;
	    	$rootScope.cuestionario.tipoViaje.educacion  = null;
	    	$rootScope.cuestionario.tipoViaje.salud      = null;
	    	$rootScope.cuestionario.tipoViaje.religioso  = null;
	    	$rootScope.cuestionario.tipoViaje.compras    = null;
	    	$rootScope.cuestionario.tipoViaje.negocios   = null;
	    }

	    if( $rootScope.flagPreferencias == 7){
	    	$rootScope.cuestionario.transporte = null;
	    }

	    if( $rootScope.flagPreferencias == 8){
			$rootScope.cuestionario.actividades.deportes     = null;
			$rootScope.cuestionario.actividades.casinos      = null;
			$rootScope.cuestionario.actividades.rurales      = null;
			$rootScope.cuestionario.actividades.originarios  = null;
			$rootScope.cuestionario.actividades.exursiones   = null;
			$rootScope.cuestionario.actividades.enoturismo   = null;
			$rootScope.cuestionario.actividades.aventura     = null;
			$rootScope.cuestionario.actividades.especiales   = null;
			$rootScope.cuestionario.actividades.playa        = null;
			$rootScope.cuestionario.actividades.naturaleza   = null;
			$rootScope.cuestionario.actividades.reuniones    = null;
			$rootScope.cuestionario.actividades.gastronomia  = null;
			$rootScope.cuestionario.actividades.museos       = null;
			$rootScope.cuestionario.actividades.astronomicos = null;
			$rootScope.cuestionario.actividades.ocio         = null;
			$rootScope.cuestionario.actividades.cityTour     = null;
			$rootScope.cuestionario.actividades.otro         = null;
	    }

	    if( $rootScope.flagPreferencias == 9){
	    	$rootScope.cuestionario.tipoServicio = false;
	    }

	    if( $rootScope.flagPreferencias == 10){
	    	$rootScope.cuestionario.comida = false;
	    }

	    if( $rootScope.flagPreferencias == 11){
	    	$rootScope.cuestionario.tipoTours = false;
	    }*/

	    $rootScope.modalpreferenciasDetalle.hide();
	    console.log("cerar modal");
  	}  

  	$scope.guardar = function(){

  		$rootScope.modalpreferenciasDetalle.hide();
  	}


	//rescatar la configuracion del usuario
	$http({
	    method: "POST",
	    url: "http://200.14.68.107/atacamaGo/getConfiguracionUsuario.php",
	    data: $httpParamSerializer({
	      'id_usuario' : localStorage.getItem("id_usuario")
	    }),
	    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	}).then(function(response){ //ok si guardó correctamente.
	    console.log(response.data);
	    if(response.data.resultado == "ok"){
	    	$scope.radio.radio = response.data.radio
		    /*switch(response.data.radio) {
		        case '10.000':
		            $scope.distanciaSeleccionada = '10 metros';
		            break;
		        case '20.000':
		            $scope.distanciaSeleccionada = '20 metros';
		            break;
		        case '50.000':
		            $scope.distanciaSeleccionada = '50 metros';
		            break;
		        case '100.000':
		            $scope.distanciaSeleccionada = '100 metros';
		            break;
		        case '500.000':
		            $scope.distanciaSeleccionada = '500 metros';
		            break;
		        case '999.000':
		            $scope.distanciaSeleccionada = '1 kilómetro';
		            break;
		    }*/

	      //seleccionar los checkbox
	      /*$scope.material.audio        = (localStorage.getItem("audio") === "true");
	      $scope.material.video        = (localStorage.getItem("video") === "true");
	      $scope.material.imagen       = (localStorage.getItem("imagen") === "true");*/
	    }
	      
	}, function(){ //Error de conexión
	    $rootScope.toast('Verifica tu conexión a internet',"short");
	});
	 
	$scope.selectDistancia = function(nuevo,viejo){
		$scope.distanciaSeleccionada = nuevo;
	}

 	$scope.guardarConfiguracion = function(datos){
 		console.log($scope.radio.radio);
	    $ionicLoading.show({
	      template: 'Cargando...'
	    });

	    var posOptions = {timeout: 10000, enableHighAccuracy: false};
	    navigator.geolocation.getCurrentPosition(
	      function(position){
	        var lat  = position.coords.latitude;
	        var long = position.coords.longitude;
	        $scope.guardarConfiguracionGeo(lat,long,datos);
	      },function(err) {
	        $scope.guardarConfiguracionGeo(0,0,datos);
	      },posOptions
	    );
	}

	$scope.guardarConfiguracionGeo = function(lat,long,datos){
	    //se guarda el material audiovisual que desea ver el usuario.
	    /*localStorage.setItem("video", datos.video);
	    localStorage.setItem("audio", datos.audio);
	    localStorage.setItem("imagen", datos.imagen); */ 
	    var radio = 0;
	    /*switch($scope.distanciaSeleccionada) {
	    case '10 metros':
	        radio = 10;
	        break;
	    case '20 metros':
	        radio = 20;
	        break;
	    case '50 metros':
	        radio = 50;
	        break;
	    case '100 metros':
	        radio = 100;
	        break;
	    case '500 metros':
	        radio = 500;
	        break;
	    case '1 kilómetro':
	        radio = 999;
	        break;
	    }*/

	    $http({
	      method: "POST",
	      url: "http://200.14.68.107/atacamaGo/guardarConfiguracionUsuario.php",
	      data: $httpParamSerializer({
	        'radio'      : $scope.radio.radio,
	        'id_usuario' : localStorage.getItem("id_usuario"),
	        'lat'        : lat,
	        'long'       : long
	      }),
	      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	    }).then(function(response){ //ok si guardó correctamente.
	      if(response.data.resultado == "ok"){
	        /*$rootScope.radio = radio;
	        console.log(radio);*/
	        $rootScope.toast("Configuración actualizada","short");
	      }
	      else
	        $rootScope.toast("Ha ocurrido un error intenta nuevamente","short");
	      
	      $ionicLoading.hide();
	    }, function(){ //Error de conexión
	      $rootScope.toast('Verifica tu conexión a internet',"short");
	    });
	}

	$scope.goToNuevoLogin = function(){
		$rootScope.modalPreferencias.remove();
		$scope.cerrarModalPreferenciasDetalle();
	}



	$scope.guardarTodo = function(){
		var cuestionario = $rootScope.cuestionario;
		console.log(cuestionario);
		$ionicLoading.show({
		  template: 'Cargando...'
		});

		var posOptions = {timeout: 10000, enableHighAccuracy: false};
		navigator.geolocation.getCurrentPosition(
		  function(position){
		    var lat  = position.coords.latitude;
		    var long = position.coords.longitude;
		    $scope.addCuestionarioGeoTodo(lat,long,cuestionario);
		  },function(err) {
		    $scope.addCuestionarioGeoTodo(0,0,cuestionario);
		   },posOptions
		);

	}

	$scope.addCuestionarioGeoTodo = function(lat,long,cuestionario){
      console.log(cuestionario);

      $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/addPreferencia.php",
        data: $httpParamSerializer({
          "cuestionario" : JSON.stringify(cuestionario),
          "id_usuario"   : localStorage.getItem("id_usuario"),
          "lat"          : lat,
            "long"         : long
      }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si guardó correctamente.
        console.log(response.data)
        if(response.data.resultado == "ok"){
          $ionicLoading.hide();
          //$rootScope.toast('preferencias actualizadas', 'short');
          //$scope.cerrarModalPreferencias();
          //$rootScope.modalPreferencias.remove();
          //$rootScope.modalpreferenciasDetalle.hide();
        }else{
          $rootScope.toast('Debes responder todas las preguntas', 'short');
          $ionicLoading.hide();
        }
        $ionicLoading.hide();
      }, function(){ //Error de conexión
        $rootScope.toast('Verifica tu conexión a internet', 'short');
        $ionicLoading.hide();
      });
     
  }

  $scope.guardarEstadia = function(){
  	var myPopup = $ionicPopup.show({
  	  template: '<p CLASS="fuenteRoboto fuenteModal">¿La duración de estadía es correcta?</p>',
  	  title: 'Aviso',
  	  scope: $scope,
  	  buttons: [
  	    { 
  	        text: '<i class="icon ion-close-round"></i>',
  	        type:'popclose',
  	          onTap: function(e) {

  	          }
  	    },
  	    {
  	      text: '<b>Aceptar</b>',
  	      type: 'button-positive asd',
  	      onTap: function(e) {
  	         $scope.guardarTodo();
  	      }
  	    }
  	  ]
  	});

  	myPopup.then(function(res) {
  	  console.log('Tapped!', res);
  	});
  }


}]);