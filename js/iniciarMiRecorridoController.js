angular.module('starter.controllers')


.controller('iniciarMiRecorridoCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$ionicModal','$httpParamSerializer','$timeout','$sce','$cordovaCamera','$cordovaFileTransfer','$cordovaSocialSharing','$ionicPopup','$ionicLoading','$ionicNativeTransitions','$ionicHistory',function($scope,$http,$state,$rootScope,ionicDatePicker,$ionicModal,$httpParamSerializer,$timeout,$sce,$cordovaCamera,$cordovaFileTransfer,$cordovaSocialSharing,$ionicPopup,$ionicLoading,$ionicNativeTransitions,$ionicHistory) {
    $scope.opcionSeleciconada            = false;
    $scope.verMishuellas                 = false;
    $scope.verTodashuellas               = false;
    $scope.goBack                        = false;
    $scope.titulo                        = "DEJA TU HUELLA";
    $rootScope.desdeOtraVista            = true;
    $scope.showFiltroAtractivo           = false;
    $scope.showFiltroServicio            = false;
    $scope.servicios_seleccionados       = [];
    $scope.patrimonios_seleccionados     = [];
    $scope.miRuta                        = [];
    $scope.watchPositionCont             = 0;
    $rootScope.estadoRutaUsuario         = null;
    $scope.lineaPunteada                 = 2;
    $scope.contadorlink                  = 0;
    $scope.link                          = [];
    $scope.recorridoUsuario              = [];
    $rootScope.imgRecorrido              = "";

    var COPIAPO = new plugin.google.maps.LatLng(-27.3690175,-70.6756632);
    width = window.screen.height;
    var alturaMapa = width - 166;
    $scope.styleMapa = {
        "height" : "100%",
        "width"  : "100%"
    };

    console.log("iniciarMiRecorridoCtrl");
    

    document.addEventListener("deviceready", function() {
    //console.log($rootScope.map);
        
          /*cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled){
            console.log("GPS location is " + (enabled ? "enabled" : "disabled"));
            console.log(enabled);
            if(enabled == false){
              //$ionicHistory.backView().go();
              //$rootScope.map.setClickable(false);
              $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
                "type": "fade",
                "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
                "duration": 0, // in milliseconds (ms), default 400
              });

              $ionicPopup.alert({
                title: 'Aviso!',
                template: 'Debes activar el gps para iniciar el recorrido',
                okText: 'Aceptar', // String (default: 'OK'). The text of the OK button.
                okType: 'btn-morado asd', // String (default: 'button-positive'). The type of the OK button.
              }).then(function(res){
                
                

              });

            }else{
              var div = document.getElementById("map_canvas");
              console.log("iniciar mapa");
              // Initialize the map view
              $rootScope.map = plugin.google.maps.Map.getMap(div,{
                  'backgroundColor' : 'white',
                  'mapType'         : plugin.google.maps.MapTypeId.ROADMAP,
                  'controls' : {
                      'compass'          : true,
                      'myLocationButton' : true,
                      'indoorPicker'     : true,
                      'zoom'             : false
                  },
                  'gestures': {
                      'scroll' : true,
                      'tilt'   : true,
                      'rotate' : true,
                      'zoom'   : true
                  },
                  'camera': {
                      'latLng' : COPIAPO,
                      'tilt'   : 0,
                      'zoom'   : 10,
                      'bearing': 0
                  }
              });

              // Wait until the map is ready status.
              $rootScope.map.addEventListener(plugin.google.maps.event.MAP_READY, onMapReady);

            }
        }, function(error){
            console.error("The following error occurred: "+error);
        }); */


      var div = document.getElementById("map_canvas");
      console.log("iniciar mapa");
      // Initialize the map view
      $rootScope.map = plugin.google.maps.Map.getMap(div,{
          'backgroundColor' : 'white',
          'mapType'         : plugin.google.maps.MapTypeId.ROADMAP,
          'controls' : {
              'compass'          : true,
              'myLocationButton' : true,
              'indoorPicker'     : true,
              'zoom'             : false
          },
          'gestures': {
              'scroll' : true,
              'tilt'   : true,
              'rotate' : true,
              'zoom'   : true
          },
          'camera': {
              'latLng' : COPIAPO,
              'tilt'   : 0,
              'zoom'   : 10,
              'bearing': 0
          }
      });

      // Wait until the map is ready status.
      $rootScope.map.addEventListener(plugin.google.maps.event.MAP_READY, onMapReady);

        
    }, false);

    function onMapReady() {
      $ionicLoading.show({
        template: 'Verificando GPS...'
      });
      var posOptions = {timeout: 10000, enableHighAccuracy: false};
      navigator.geolocation.getCurrentPosition(
        function(position){
          console.log(position);
          $ionicLoading.hide();

          $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/getMenuPatrimonio.php",
            data: {},
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si recato bien los datos
              if(response.data.resultado == "ok"){
                $rootScope.atractivosTuristicos = response.data.sub_tipo_atractivo;
                //console.log($rootScope.atractivosTuristicos);
                $rootScope.atractivosTuristicos.unshift({
                  "id_sub_tipo_atractivo" : -1,
                  "nombre"                : "Ver todos los atractivos",
                  "seleccionado"          : false
                })
                $scope.sub_tipo_atractivo = response.data.sub_tipo_atractivo;
                //console.log($scope.sub_tipo_atractivo);
              }
          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
          });

          $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/getMenuServicios.php",
            data: {},
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si recato bien los datos
              if(response.data.resultado == "ok"){
                $scope.serviciosTuristicosMenu = response.data.tipo_servicio;
                $scope.serviciosTuristicosMenu.unshift({
                  "id_tipo_servicio" : -1,
                  "nombre"           : "Ver todos los servicios",
                  "seleccionado"     : false
                });
                $scope.tipo_servicio = response.data.tipo_servicio;
              }
          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
          });

          /** 
           * [Agregar en la base de datos el niciio del recorrido del turista]
           * @param  {[type]}
           * @return {[type]}
           */
          $http({
              method: "POST",
              url: "http://200.14.68.107/atacamaGo/addRutaRecorridaTuristaMisRecorridos.php",
              data: $httpParamSerializer({
                'id_usuario' : localStorage.getItem("id_usuario")
              }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si recato bien los datos
                //esconder loading
                $ionicLoading.hide().then(function(){});
                
                if(response.data.resultado == "ok"){
                  $rootScope.id_ruta_recorrida_turista = response.data.id_ruta_recorrida_turista;
                  $rootScope.id_ruta_tipica_seleccionada = response.data.id_ruta_recorrida_turista;
                  //if($scope.rutaDinamica == false){ //está oculto, por lo tanto mostrarlo....
                  //  $scope.btnTerminarAventura = true; //ocultar icono y mostrar el "menu aventura"...
                    $scope.entrarUnaVezDinamico = 0;
                    $scope.fechaAnteriorDinamica = new Date();
                    $scope.watchMarcadores();
                  //}
                  //document.getElementById("containerLoading").className = "fadeOut";
                }
          }, function(){ //Error de conexión
              $rootScope.toast('Verifica tu conexión a internet', 'short');
          });



        },function(err) {
          $ionicLoading.hide();
          alert("No hemos podido encontrar tu ubicación");
          
          navigator.geolocation.clearWatch($scope.watchRutaDinamica);
          $scope.watchRutaDinamica = null;

          $rootScope.map.clear();
          $rootScope.map.off();
          $rootScope.map.remove();

          $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
            "type": "slide",
            "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
            "duration": 0, // in milliseconds (ms), default 400
          });

        },posOptions
      );

    }

    function onBtnClicked() {
        map.showDialog();
    }

    $scope.openModalEvento = function(){
      //hacer que el mapa no sea clickeable.
      if(ionic.Platform.isWebView()){
        $rootScope.map.setClickable(false);
      }  
      $rootScope.fromMapa = true;
      //Modal rutas Tipicas...
      $ionicModal.fromTemplateUrl('templates/eventoSeleccionado.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalEventoSeleccionado = modal;
        $rootScope.modalEventoSeleccionado.show();
      });

    }
    
    $scope.cerrarIniciarMiRecorrido = function(){

        console.log( $scope.link );
        var promedioLat = 0;
        var promedioLon = 0;
        var cantidadItems = 0;
        angular.forEach($scope.link, function(value, key) {  
          console.log(value);
          promedioLat += parseFloat(value.latitud);
          promedioLon += parseFloat(value.longitud);
          cantidadItems ++ ;
        });

        

        promedioLat /= cantidadItems;
        promedioLon /= cantidadItems; 
        console.log(promedioLat,promedioLon);

        if( $scope.link.length == 0 ){ //Si no tiene marcadores visitados....
          promedioLat = $rootScope.latUsuario;
          promedioLon = $rootScope.lonUsuario;
          
          var linkMapa = "https://maps.googleapis.com/maps/api/staticmap?center="+promedioLat+","+promedioLon+
                          "&zoom=16" +
                          "&size=1000x1000" +
                          "&maptype=roadmap";
        }else{ //si si tiene marcadores visitados...
          var linkMapa = "https://maps.googleapis.com/maps/api/staticmap?center="+promedioLat+","+promedioLon+
                          /*"&zoom=16" +*/
                          "&size=1000x1000" +
                          "&maptype=roadmap";
        }

        

        angular.forEach($scope.link, function(value, key) {  
          linkMapa = linkMapa + value.link;
        });

        linkMapa += "&path=color:0xff0000ff|weight:5|";
        angular.forEach($scope.recorridoUsuario, function(value, key) {  
          linkMapa += value.link;
        });

        linkMapa = linkMapa.substring(0, linkMapa.length - 1);

        linkMapa = linkMapa + "&key=AIzaSyAC-Lt38Udx7CdlNIZhcLpSAB4SxsS-DCk";
        console.log(linkMapa);

        $rootScope.imgRecorrido = linkMapa;

        if( $rootScope.estadoRutaUsuario == null){
          $rootScope.btnCerrarMiRecorrido = true;
          
          navigator.geolocation.clearWatch($scope.watchRutaDinamica);
          $scope.watchRutaDinamica = null;
          
          $scope.pausarRecorrido();
        }else{
          
          if( $scope.verTodashuellas ){
              $rootScope.map.clear();
              $rootScope.map.off();
          }
          if( $scope.verMishuellas ){
              $rootScope.map.clear();
              $rootScope.map.off();
          }

          navigator.geolocation.clearWatch($scope.watchRutaDinamica);
          $scope.watchRutaDinamica = null;

          $rootScope.map.remove();
          //console.log("este es el rootscope: ",$rootScope);
          //console.log("este es el scope: ",$scope);

          //$rootScope.volverExploraYDescubre = true;
          //console.log("iniciar recorrido: $rootScope.volverExploraYDescubre: ", $rootScope.volverExploraYDescubre);
          //$state.go("app.nuevoHome");
          /*$ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
            "type": "slide",
            "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
            "duration": 0, // in milliseconds (ms), default 400
          });*/  //Lo comente por las transiciones no se si afecta 
        }
        

        

        //$rootScope.map.remove(); comente esto(por transicion)
        
        //console.log("este es el rootscope: ",$rootScope);
        //console.log("este es el scope: ",$scope);

        
    }



    $scope.opcion = function(id){
        if(id == 1){
            $scope.opcionSeleciconada = true;
            $scope.mostrarBotonesRegistrarhuella = true;

            //var COPIAPO = new plugin.google.maps.LatLng(-27.3690175,-70.6756632);
            //
            $rootScope.map.getCameraPosition(function(camera) {
                var lat = camera.target.lat;
                var lng = camera.target.lng;
              
                //console.log( lat,lng);
                var POS_MARCADOR = new plugin.google.maps.LatLng(lat, lng);

                $rootScope.map.addMarker({
                    'position': POS_MARCADOR,
                    'draggable': false,
                    'anchor':  [30, 35],
                    icon: {
                        url: "www/img/deja_tu_huella/deja_huella_azul.png",
                        size: { width: 40, height: 35 },
                    },        
                }, function(marker) {

                    marker.addEventListener(plugin.google.maps.event.MARKER_DRAG_END, function(marker) {
                    marker.getPosition(function(latLng) {
                        //marker.setTitle(latLng.toUrlValue());
                        //marker.showInfoWindow();
                        //var coor = split(",",)
                        console.log(latLng);
                        $rootScope.lat = latLng.lat;
                        $rootScope.lon = latLng.lng;
                      });
                    });
                });
            });
        }

        if(id == 2){
            $scope.opcionSeleciconada = true;
            $scope.verHuella(0);
        }

        if(id == 3){
            $scope.opcionSeleciconada = true;
            $scope.verHuella(1);
        }
    }

    $scope.addHuella = function(){
        /*$rootScope.map.clear();
        $rootScope.map.off();*/
        $rootScope.marcadorNuevoItem.remove();
        $scope.mostrarBotonesRegistrarhuella = false;

        $scope.agregarhuellaPhp($rootScope.lat,$rootScope.lon);
    }

    $scope.descartarHuella = function(){
        // $rootScope.map.clear();
        // $rootScope.map.off();
        $rootScope.marcadorNuevoItem.remove();
        $scope.opcionSeleciconada = false;
        $scope.mostrarBotonesRegistrarhuella = false;
    }

    $scope.verHuella = function(valor){
      if(valor == 0){
        $scope.verMishuellas= true;
        $scope.titulo = "MIS HUELLAS";
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getItemTuristicoUsuario.php",
          data: $httpParamSerializer({
            "id_usuario": localStorage.getItem("id_usuario")
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si recato bien los datos
          //console.log(response.data.item_turistico);
          if(response.data.resultado == "ok"){
             
            $rootScope.map.clear();
            $rootScope.map.off();
            $scope.markerArrayItemUsuario = [];
            $scope.itemTuristicosVer = response.data.item_turistico;
            
            angular.forEach(response.data.item_turistico, function(value, key) {  
              var ubicacion = new plugin.google.maps.LatLng(
              parseFloat(value.direccion_georeferenciada_latitud),
              parseFloat(value.direccion_georeferenciada_longitud) );

              var url = "www/img/deja_tu_huella/mis_huellas.png";

              $rootScope.map.addMarker({
                'position': ubicacion,
                'title': value.nombre,
                'draggable': false,
                'anchor':  [30, 35],
                icon: {
                  url: url,
                  size: { width: 30, height: 35 },
                },
                zIndex: 1
              },function(marker) {
                      //accion al hacer clic en el marcador
                $scope.markerArrayItemUsuario[key] = marker;
                  marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                    $rootScope.tipo_seleccionado = "item";
                    $rootScope.item_seleccionado = value;
                    $scope.openModalMenuAccionesModificar();
                });
              });
                                        
            });
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });
      }

      if(valor == 1){
        $scope.verTodashuellas= true;
        $scope.titulo = "TODAS LAS HUELLAS";
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getItemTuristicoTotal.php",
          data: $httpParamSerializer({}),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si recato bien los datos
           // console.log(response.data.item_turistico);
            if(response.data.resultado == "ok"){
              
              $rootScope.map.clear();
              $rootScope.map.off();
              $scope.markerArrayItemTotal = [];
              $scope.itemTuristicosVerTotal = response.data.item_turistico;

              angular.forEach(response.data.item_turistico, function(value, key) {  
                               
              var ubicacion = new plugin.google.maps.LatLng(
                parseFloat(value.direccion_georeferenciada_latitud),
                parseFloat(value.direccion_georeferenciada_longitud) );

                if( value.id_usuario == localStorage.getItem("id_usuario") ){
                  var url = "www/img/deja_tu_huella/mis_huellas.png";  
                }else{
                  var url = "www/img/deja_tu_huella/deja_huella_azul.png";
                }
                

                $rootScope.map.addMarker({
                  'position': ubicacion,
                  'title': value.nombre,
                  'draggable': false,
                  'anchor':  [30, 35],
                  icon: {
                    url: url,
                    size: { width: 30, height: 35 },
                  },
                  zIndex: 1
                },function(marker) {
                  //accion al hacer clic en el marcador
                  $scope.markerArrayItemTotal[key] = marker;
                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "item";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                  });
                });
                                    
              });
            }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });
      }
         
    }

    $scope.openModalMenuAccionesModificar = function(){

      //Modal menu Acciones Modiciar...
      $ionicModal.fromTemplateUrl('templates/menuAccionesModificar.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalMenuAccionesModificar = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);

        $rootScope.modalMenuAccionesModificar.show();
      
      });
    }

    $scope.mostrarFiltroServicios = function(){
      $rootScope.map.setClickable(false);
      if( $scope.showFiltroServicio ){
        $scope.showFiltroServicio = false;
        $scope.showFiltroAtractivo = false;
      }else{
        $scope.showFiltroServicio = true;
        $scope.showFiltroAtractivo = false;
      }
      
    }

    $scope.mostrarFiltroAtractivos = function(){
      $rootScope.map.setClickable(false);
      if( $scope.showFiltroAtractivo ){
        $scope.showFiltroAtractivo = false;
        $scope.showFiltroServicio = false;
      }else{
        $scope.showFiltroAtractivo = true;
        $scope.showFiltroServicio = false;
      }
    }

    $scope.filtrarServicio = function(){
      $scope.showFiltroServicio = false;
      $scope.showFiltroAtractivo = false;
    }

    $scope.filtrarAtractivo = function(){
      $scope.showFiltroServicio = false;
      $scope.showFiltroAtractivo = false;
    }  


    //Mostrar patrimonios, ocurre cuando se aprieta "mostrar todos" o alguna
    //categoria de patrimionios....
    $scope.mostrarPatrimonios = function(id_sub_tipo_atractivo,index){

      /*document.getElementById("containerLoading").className = "fadeIn";
      if( $scope.popoverPatrimonios.isShown() ){
        $scope.popoverPatrimonios.hide();  
      }*/
      //$scope.showFiltroAtractivo = false;
      console.log(id_sub_tipo_atractivo,index);
      //Cambiar clase al elemento seleccionado
      if( index != -1 ){
        if($scope.sub_tipo_atractivo[index].seleccionado == false)
          $scope.sub_tipo_atractivo[index].seleccionado = true;
        else
          $scope.sub_tipo_atractivo[index].seleccionado = false;
      }

      //Evaluar si se "presionó todos los patrimonios"
      if(id_sub_tipo_atractivo == -1){
        //seleccionar todos los sub tipos atractivos....
        if( $scope.seleccionadoMostrarTodos == false){
          $scope.seleccionadoMostrarTodos = true;
          //además SELECCIONAR todos los elementos
          angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
            value.seleccionado = true;
          });
        }else{
          $scope.seleccionadoMostrarTodos = false;
          //ademas DES SELECCIONAR todos los elementos....
          angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
            value.seleccionado = false;
          });

          //elimnar marcadores de patrimonio...
          $scope.patrimonios = [];
        }
        
        //Cambiar todas los botones a activos...
        //var menuPatrimonio   = document.getElementsByClassName("menuPatrimonio");
        //menuPatrimonio.class = "menuPatrimonio";
      } 

      $scope.showHidePatrimonio = false;
      if($scope.showHidePatrimonio == false){ //está oculto, por lo tanto mostrarlo....
        //console.log("Llamar a php");
        //conseguir patrimonios

        if( id_sub_tipo_atractivo == -1){
          //console.log($scope.patrimonios_seleccionados.length, $scope.sub_tipo_atractivo.length + 1);
          if($scope.patrimonios_seleccionados.length == $scope.sub_tipo_atractivo.length+1){
            //estan todos seleccionados, por lo tanto vaciar el arreglo
            $scope.patrimonios_seleccionados = [];
          }else{

            $scope.patrimonios_seleccionados = [];
            angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
              $scope.patrimonios_seleccionados.push(value.id_sub_tipo_atractivo)     
            });
          }
        }

        if( $scope.patrimonios_seleccionados.length == 0 ){
          $scope.patrimonios_seleccionados.push(id_sub_tipo_atractivo);  
        }else{
          if($scope.patrimonios_seleccionados.indexOf(id_sub_tipo_atractivo) == -1) {
            $scope.patrimonios_seleccionados.push(id_sub_tipo_atractivo);  
          }else
            $scope.patrimonios_seleccionados.splice($scope.patrimonios_seleccionados.indexOf(id_sub_tipo_atractivo), 1);
        }

        if( $scope.patrimonios_seleccionados.length == 0 ){
          $rootScope.map.clear();
          $rootScope.map.off();
          $scope.markerArrayPatrimonio = [];
          $scope.patrimonios = [];

          //console.log($scope.markerArrayServiciosTuristicos);
          $scope.markerArrayServiciosTuristicos = [];

                  angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                   
                        var ubicacion = new plugin.google.maps.LatLng(
                          parseFloat(value.direccion_georeferenciada_latitud),
                          parseFloat(value.direccion_georeferenciada_longitud) );

                        //var url = "www/img/servicios/"+value.icono+".png";
                        var url = "www/img/servicios/"+value.icono+"Oculto.png";

                        $rootScope.map.addMarker({
                          'position': ubicacion,
                          'title': value.nombre_item_turistico,
                          'draggable': false,
                          'anchor':  [30, 45],
                          icon: {
                            url: url,
                            size: { width: 30, height: 45 },
                          },
                          zIndex: 1
                        },function(marker) {
                            //accion al hacer clic en el marcador
                            $scope.markerArrayServiciosTuristicos[key] = marker;
                            marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                              $rootScope.tipo_seleccionado = "servicio";
                              $rootScope.item_seleccionado = value;
                              $scope.openModalMenuAccionesModificar();
                            });
                        });
                        
                  });

                  console.log($scope.markerArrayServiciosTuristicos);

                  $scope.markerArrayItemUsuario = [];
              
                  angular.forEach($scope.itemTuristicosVer, function(value, key) {                 
                    
                    var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                    var url = "www/img/posicionGpsTuristaOculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayItemUsuario[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "item";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                      });
                    });
                                        
                  });

                  $scope.markerArrayItemTotal = [];
              
                  angular.forEach($scope.itemTuristicosVerTotal, function(value, key) {                 
                    
                    var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                    var url = "www/img/posicionGpsTuristaOculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayItemTotal[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "item";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                      });
                    });
                                        
                  });
        }else{
          $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/getPatrimonioNuevo.php",
            data: $httpParamSerializer({
              id_sub_tipo_atractivo     : id_sub_tipo_atractivo,
              patrimonios_seleccionados : JSON.stringify($scope.patrimonios_seleccionados),
              id_usuario                : localStorage.getItem("id_usuario")
            }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si guardó correctamente
              if(response.data.resultado == "ok"){
                //console.log("Lista patrimoinios: ");
                //console.log(response.data);

                var actualizarMarcadores = true;
                //evaluar si se rescatan patrimoinos o no....
                
                if( $scope.seleccionadoMostrarTodos == false ){
                  var contatorMarcadores = 0;
                  angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
                    
                    if( value.seleccionado == false ){
                      contatorMarcadores++;
                    }
                  });
                  
                }

                if( contatorMarcadores == $scope.sub_tipo_atractivo.length ){
                  $scope.patrimonios_seleccionados = [];
                  $scope.showHidePatrimonio = false;
                  
                  $scope.patrimonios = [];
                }else{
                   $scope.patrimonios = response.data.patrimonio;
                   $scope.showHidePatrimonio = true; //mantener visto...
                }
                
                //console.log( $scope.patrimonios );

                $scope.agregarPatrimonios = 1;

                $rootScope.map.clear();
                $rootScope.map.off();

                $scope.markerArrayPatrimonio = [];

                angular.forEach($scope.patrimonios, function(value, key) {

                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/patrimonios/"+value.icono+".png";
                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayPatrimonio[key] = marker;
                      $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                      $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "patrimonio";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                    });
                  });

                  $scope.markerArrayServiciosTuristicos = [];

                  angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                   
                        var ubicacion = new plugin.google.maps.LatLng(
                          parseFloat(value.direccion_georeferenciada_latitud),
                          parseFloat(value.direccion_georeferenciada_longitud) );

                        //var url = "www/img/servicios/"+value.icono+".png";
                        var url = "www/img/servicios/"+value.icono+"Oculto.png";

                        $rootScope.map.addMarker({
                          'position': ubicacion,
                          'title': value.nombre_item_turistico,
                          'draggable': false,
                          'anchor':  [30, 45],
                          icon: {
                            url: url,
                            size: { width: 30, height: 45 },
                          },
                          zIndex: 1
                        },function(marker) {
                            //accion al hacer clic en el marcador
                            $scope.markerArrayServiciosTuristicos[key] = marker;
                            marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                              $rootScope.tipo_seleccionado = "servicio";
                              $rootScope.item_seleccionado = value;
                              $scope.openModalMenuAccionesModificar();
                            });
                        });
                        
                  });

                  $scope.markerArrayItemUsuario = [];
              
                  angular.forEach($scope.itemTuristicosVer, function(value, key) {                 
                    
                    var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                    var url = "www/img/posicionGpsTuristaOculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayItemUsuario[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "item";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                      });
                    });
                                        
                  });

                  $scope.markerArrayItemTotal = [];
              
                  angular.forEach($scope.itemTuristicosVerTotal, function(value, key) {                 
                    
                    var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                    var url = "www/img/posicionGpsTuristaOculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayItemTotal[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "item";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                      });
                    });
                                        
                  });

                  $scope.agregarPatrimonios = 0;

                  //si esta activo la aventura o ruta dinámica mostrar los items dinamicos...
                  if( $scope.btnTerminarAventura == true ){

                    $scope.markerArrayServiciosTuristicosDinamicos = [];
                              
                    angular.forEach($scope.serviciosDinamica, function(value, key) {  
                               
                                    var ubicacion = new plugin.google.maps.LatLng(
                                      parseFloat(value.direccion_georeferenciada_latitud),
                                      parseFloat(value.direccion_georeferenciada_longitud) );

                                    //var url = "www/img/servicios/"+value.icono+".png";
                                    var url = "www/img/servicios/"+value.icono+"Oculto.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.nombre_item_turistico,
                                      'draggable': false,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                        //accion al hacer clic en el marcador
                                        $scope.markerArrayServiciosTuristicosDinamicos[key] = marker;
                                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                          $rootScope.tipo_seleccionado = "servicio";
                                          $rootScope.item_seleccionado = value;
                                          $scope.openModalMenuAccionesModificar();
                                        });
                                    });
                                    
                              });

                    $scope.markerArrayPatrimonioDinamicos = [];

                    angular.forEach($scope.patrimoniosDinamica, function(value, key) {

                                  var ubicacion = new plugin.google.maps.LatLng(
                                    parseFloat(value.direccion_georeferenciada_latitud),
                                    parseFloat(value.direccion_georeferenciada_longitud) );

                                  //var url = "www/img/patrimonios/"+value.icono+".png";
                                  var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                                  $rootScope.map.addMarker({
                                    'position': ubicacion,
                                    'title': value.nombre_item_turistico,
                                    'draggable': false,
                                    'anchor':  [30, 45],
                                    /*icon: {
                                      url: url,
                                      size: { width: 30, height: 45 },
                                    },*/
                                    zIndex: 1
                                  },function(marker) {
                                    //accion al hacer clic en el marcador
                                    $scope.markerArrayPatrimonioDinamicos[key] = marker;
                                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                      $rootScope.tipo_seleccionado = "patrimonio";
                                      $rootScope.item_seleccionado = value;
                                      $scope.openModalMenuAccionesModificar();
                                    });
                                  });
                    });

                    $scope.markerItemDinamicos = [];
                    angular.forEach($scope.itemDinamico, function(value, key) {

                                  var ubicacion = new plugin.google.maps.LatLng(
                                    parseFloat(value.direccion_georeferenciada_latitud),
                                    parseFloat(value.direccion_georeferenciada_longitud) );

                                  //var url = "www/img/patrimonios/"+value.icono+".png";
                                  var url = "www/img/deja_huella_azul/deja_huella_azul.png";

                                  $rootScope.map.addMarker({
                                    'position': ubicacion,
                                    'title': value.nombre_item_turistico,
                                    'draggable': false,
                                    'anchor':  [30, 45],
                                    icon: {
                                      url: url,
                                      size: { width: 30, height: 45 },
                                    },
                                    zIndex: 1
                                  },function(marker) {
                                    //accion al hacer clic en el marcador
                                    $scope.markerItemDinamicos[key] = marker;
                                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                      $rootScope.tipo_seleccionado = "item";
                                      $rootScope.item_seleccionado = value;
                                      $scope.openModalMenuAccionesModificar();
                                    });
                                  });
                    });
                  }
                  //document.getElementById("containerLoading").className = "fadeOut";
                  $scope.bloquearMostrarPatrimonios = false;
              }
          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
          });
        }
        
      }else{ //esta visible, por lo tanto ocultarlo

      }
    }

    //Mostrar Servicios, ocurre cuando se aprieta "mostrar todos" o alguna
    //categoria de servicios....
    $scope.mostrarServiciosTuristicos = function(id_tipo_servicio,index){
      //document.getElementById("containerLoading").className = "fadeIn";
      //$scope.popoverServicios.hide();
      //Cambiar clase al elemento seleccionado
      //$scope.showFiltroServicio = false;

      if( index != -1 ){
        if($scope.tipo_servicio[index].seleccionado == false)
          $scope.tipo_servicio[index].seleccionado = true;
        else
          $scope.tipo_servicio[index].seleccionado = false;
      }

      //Evaluar si se "presionó todos los patrimonios"
      if(id_tipo_servicio == -1){
        //seleccionar todos los sub tipos servicios....
        if( $scope.seleccionadoMostrarTodosServicio == false){
          $scope.seleccionadoMostrarTodosServicio = true;
          //además SELECCIONAR todos los elementos
          angular.forEach($scope.tipo_servicio, function(value, key) {
            value.seleccionado = true;
          });
        }else{
          $scope.seleccionadoMostrarTodosServicio = false;
          //ademas DES SELECCIONAR todos los elementos....
          angular.forEach($scope.tipo_servicio, function(value, key) {
            value.seleccionado = false;
          });

          //elimnar marcadores de patrimonio...
          $scope.serviciosTuristicos = [];
        }
        
        //Cambiar todas los botones a activos...
        //var menuServicios   = document.getElementsByClassName("menuServicios");
        //menuServicios.class = "menuServicios";
      } 

      $scope.showHideServicio = false;
      if($scope.showHideServicio == false){ //está oculto, por lo tanto mostrarlo....

        if( id_tipo_servicio == -1){
          if($scope.servicios_seleccionados.length == $scope.tipo_servicio.length+1){
            //estan todos seleccionados, por lo tanto vaciar el arreglo
            $scope.servicios_seleccionados = [];
          }else{

            $scope.servicios_seleccionados = [];
            angular.forEach($scope.tipo_servicio, function(value, key) {
              $scope.servicios_seleccionados.push(value.id_tipo_servicio)     
            });
          }
        }
        /*---------------------------------*/
        if( $scope.servicios_seleccionados.length == 0 ){
          $scope.servicios_seleccionados.push(id_tipo_servicio);
        }else{
          if($scope.servicios_seleccionados.indexOf(id_tipo_servicio) == -1) {
            $scope.servicios_seleccionados.push(id_tipo_servicio);
          }else
            $scope.servicios_seleccionados.splice($scope.servicios_seleccionados.indexOf(id_tipo_servicio), 1);
        }

        console.log(id_tipo_servicio,index,$scope.servicios_seleccionados);
        if( $scope.servicios_seleccionados.length == 0 ){
          $rootScope.map.clear();
          $rootScope.map.off();

          $scope.markerArrayServiciosTuristicos = [];
          $scope.serviciosTuristicos = [];

          $scope.markerArrayPatrimonio = [];

                angular.forEach($scope.patrimonios, function(value, key) {

                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/patrimonios/"+value.icono+".png";
                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayPatrimonio[key] = marker;
                      $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                      $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "patrimonio";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                    });
                });

                $scope.markerArrayItemUsuario = [];
              
                angular.forEach($scope.itemTuristicosVer, function(value, key) {                 
                    
                  var ubicacion = new plugin.google.maps.LatLng(
                  parseFloat(value.direccion_georeferenciada_latitud),
                  parseFloat(value.direccion_georeferenciada_longitud) );

                  var url = "www/img/posicionGpsTuristaOculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': false,
                    'anchor':  [30, 45],
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayItemUsuario[key] = marker;
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "item";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                    });
                  });
                                        
                });

                $scope.markerArrayItemTotal = [];
              
                angular.forEach($scope.itemTuristicosVerTotal, function(value, key) {                 
                    
                    var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                    var url = "www/img/posicionGpsTuristaOculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayItemTotal[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "item";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                      });
                    });
                                        
                });

        }else{
          //conseguir Servivios tuiristicos
          $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/getServiciosTuristicosNuevo.php",
            data: $httpParamSerializer({
              id_tipo_servicio        : id_tipo_servicio,
              servicios_seleccionados : JSON.stringify($scope.servicios_seleccionados),
              id_usuario              : localStorage.getItem("id_usuario")
            }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si guardó correctamente
              if(response.data.resultado == "ok"){
                //console.log(response.data);

                var actualizarMarcadoresServicios = true;
                //evaluar si se rescatan servicios o no....
                
                if( $scope.seleccionadoMostrarTodosServicio == false ){
                  var contatorMarcadoresServicio = 0;
                  angular.forEach($scope.tipo_servicio, function(value, key) {
                    
                    if( value.seleccionado == false ){
                      contatorMarcadoresServicio++;
                    }
                  });     
                }

                if( contatorMarcadoresServicio == $scope.tipo_servicio.length ){
                  $scope.servicios_seleccionados = [];
                  $scope.showHideServicio        = false;
                  $scope.serviciosTuristicos     = [];
                }else{
                   $scope.serviciosTuristicos    = response.data.servicios;
                   $scope.showHideServicio = true; //mantener visto...
                }
                
                $scope.agregarServicios = 1;
                
                $rootScope.map.clear();
                $rootScope.map.off();
                $scope.markerArrayServiciosTuristicos = [];

                angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                 
                      var ubicacion = new plugin.google.maps.LatLng(
                        parseFloat(value.direccion_georeferenciada_latitud),
                        parseFloat(value.direccion_georeferenciada_longitud) );

                      //var url = "www/img/servicios/"+value.icono+".png";
                      var url = "www/img/servicios/"+value.icono+"Oculto.png";

                      $rootScope.map.addMarker({
                        'position': ubicacion,
                        'title': value.nombre_item_turistico,
                        'draggable': false,
                        'anchor':  [30, 45],
                        icon: {
                          url: url,
                          size: { width: 30, height: 45 },
                        },
                        zIndex: 1
                      },function(marker) {
                          //accion al hacer clic en el marcador
                          $scope.markerArrayServiciosTuristicos[key] = marker;
                          marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                            $rootScope.tipo_seleccionado = "servicio";
                            $rootScope.item_seleccionado = value;
                            $scope.openModalMenuAccionesModificar();
                          });
                      });
                      
                });

                $scope.markerArrayPatrimonio = [];

                angular.forEach($scope.patrimonios, function(value, key) {

                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/patrimonios/"+value.icono+".png";
                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayPatrimonio[key] = marker;
                      $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                      $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "patrimonio";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                    });
                });

                $scope.markerArrayItemUsuario = [];
              
                angular.forEach($scope.itemTuristicosVer, function(value, key) {                 
                    
                  var ubicacion = new plugin.google.maps.LatLng(
                  parseFloat(value.direccion_georeferenciada_latitud),
                  parseFloat(value.direccion_georeferenciada_longitud) );

                  var url = "www/img/posicionGpsTuristaOculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': false,
                    'anchor':  [30, 45],
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayItemUsuario[key] = marker;
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "item";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                    });
                  });
                                        
                });

                $scope.markerArrayItemTotal = [];
              
                angular.forEach($scope.itemTuristicosVerTotal, function(value, key) {                 
                    
                    var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                    var url = "www/img/posicionGpsTuristaOculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayItemTotal[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "item";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                      });
                    });
                                        
                });

                //si esta activo la aventura o ruta dinámica mostrar los items dinamicos...
                if( $scope.btnTerminarAventura == true ){

                    $scope.markerArrayServiciosTuristicosDinamicos = [];
                              
                    angular.forEach($scope.serviciosDinamica, function(value, key) {  
                               
                                    var ubicacion = new plugin.google.maps.LatLng(
                                      parseFloat(value.direccion_georeferenciada_latitud),
                                      parseFloat(value.direccion_georeferenciada_longitud) );

                                    //var url = "www/img/servicios/"+value.icono+".png";
                                    var url = "www/img/servicios/"+value.icono+"Oculto.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.nombre_item_turistico,
                                      'draggable': false,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                        //accion al hacer clic en el marcador
                                        $scope.markerArrayServiciosTuristicosDinamicos[key] = marker;
                                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                          $rootScope.tipo_seleccionado = "servicio";
                                          $rootScope.item_seleccionado = value;
                                          $scope.openModalMenuAccionesModificar();
                                        });
                                    });
                                    
                              });

                    $scope.markerArrayPatrimonioDinamicos = [];

                    angular.forEach($scope.patrimoniosDinamica, function(value, key) {

                                  var ubicacion = new plugin.google.maps.LatLng(
                                    parseFloat(value.direccion_georeferenciada_latitud),
                                    parseFloat(value.direccion_georeferenciada_longitud) );

                                  //var url = "www/img/patrimonios/"+value.icono+".png";
                                  var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                                  $rootScope.map.addMarker({
                                    'position': ubicacion,
                                    'title': value.nombre_item_turistico,
                                    'draggable': false,
                                    'anchor':  [30, 45],
                                    icon: {
                                      url: url,
                                      size: { width: 30, height: 45 },
                                    },
                                    zIndex: 1
                                  },function(marker) {
                                    //accion al hacer clic en el marcador
                                    $scope.markerArrayPatrimonioDinamicos[key] = marker;
                                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                      $rootScope.tipo_seleccionado = "patrimonio";
                                      $rootScope.item_seleccionado = value;
                                      $scope.openModalMenuAccionesModificar();
                                    });
                                  });
                    });

                    $scope.markerItemDinamicos = [];
                    angular.forEach($scope.itemDinamico, function(value, key) {

                                  var ubicacion = new plugin.google.maps.LatLng(
                                    parseFloat(value.direccion_georeferenciada_latitud),
                                    parseFloat(value.direccion_georeferenciada_longitud) );

                                  //var url = "www/img/patrimonios/"+value.icono+".png";
                                  var url = "www/img/posicionGpsTuristaOculto.png";

                                  $rootScope.map.addMarker({
                                    'position': ubicacion,
                                    'title': value.nombre_item_turistico,
                                    'draggable': false,
                                    'anchor':  [30, 45],
                                    icon: {
                                      url: url,
                                      size: { width: 30, height: 45 },
                                    },
                                    zIndex: 1
                                  },function(marker) {
                                    //accion al hacer clic en el marcador
                                    $scope.markerItemDinamicos[key] = marker;
                                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                      $rootScope.tipo_seleccionado = "item";
                                      $rootScope.item_seleccionado = value;
                                      $scope.openModalMenuAccionesModificar();
                                    });
                                  });
                    });
                }

                //document.getElementById("containerLoading").className = "fadeOut";
                $scope.bloquearMostrarServicios = false;
              }
          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
          });
        }
        
      }else{ //esta visible, por lo tanto ocultarlo

      }
    }

    $scope.watchMarcadores = function(){
      console.log("watch marcadores");
      $rootScope.iniciarRutaEncurso();
      $scope.watchRutaDinamica = navigator.geolocation.watchPosition(function(position){
        //console.log(position);
        //console.log($scope.watchRutaTipica);
        if($scope.entrarUnaVezDinamico == 0){
          $rootScope.latUsuario = position.coords.latitude;
          $rootScope.lonUsuario = position.coords.longitude;

          var lat  = position.coords.latitude;
          var long = position.coords.longitude;
            
          $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/addPuntoRutaRecorridaTurista.php",
            data: $httpParamSerializer({
              "id_usuario"     : localStorage.getItem("id_usuario"),
              "id_ruta"        : "",
              "punto_longitud" : long,
              "punto_latitud"  : lat
            }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si recato bien los datos
              //console.log(response.data);
          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
          });

          $scope.mostrarDinamicos(position.coords.latitude,position.coords.longitude); 
          $scope.entrarUnaVezDinamico = 1;
        }
            $scope.now = new Date();
            if ($scope.now - $scope.fechaAnteriorDinamica > 30000) {

          
              $rootScope.latUsuario = position.coords.latitude;
              $rootScope.lonUsuario = position.coords.longitude;

              var lat  = position.coords.latitude;
              var long = position.coords.longitude;
            
              $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/addPuntoRutaRecorridaTurista.php",
                data: $httpParamSerializer({
                  "id_usuario"     : localStorage.getItem("id_usuario"),
                  "id_ruta"        : "",
                  "punto_longitud" : long,
                  "punto_latitud"  : lat
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si recato bien los datos
                  //console.log(response.data);
              }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet', 'short');
              });

              $scope.mostrarDinamicos(lat,long);
              $scope.fechaAnteriorDinamica = $scope.now;
            }

      },function(error){
        console.log(error);
      },{
        timeout : 90000
      });
    };

    $rootScope.iniciarRutaEncurso = function(){
      console.log("add line");
      //marcador posicion
      $scope.watchRutaTipica = navigator.geolocation.watchPosition(function(position){
        //alert( $scope.watchPositionCont );

        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        
        var posision = new plugin.google.maps.LatLng(lat, long);
        $scope.miRuta.push( posision );

        if($scope.watchPositionCont >0){
            $scope.lineaPunteada++;
            //dibujar la ruta
            if($scope.lineaPunteada%2 == 0){

              //console.log($scope.miRuta[$scope.watchPositionCont]);
              $scope.recorridoUsuario.push({
                "link" : $scope.miRuta[$scope.watchPositionCont].lat+","+$scope.miRuta[$scope.watchPositionCont].lng+"|"+$scope.miRuta[$scope.watchPositionCont-1].lat+","+$scope.miRuta[$scope.watchPositionCont-1].lng+"|"
              });

              $rootScope.map.addPolyline({
                points: [
                  $scope.miRuta[$scope.watchPositionCont],
                  $scope.miRuta[$scope.watchPositionCont-1]
                ],
                'color' : '#8A72B0',
                'width': 3,
                'geodesic': true
              }, function(polyline) {
                  $rootScope.posicionUsuario = polyline;
              });
            }
            
        }

        $scope.watchPositionCont+=1;
      },function(error){
        console.log(error);
      },{
        timeout : 30000
      });

      //quitar filtros de marcadores....
      angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
            value.seleccionado = false;
      });
      $scope.seleccionadoMostrarTodos = false;
      $scope.patrimonios = [];
      $scope.markerArrayPatrimonio = [];

      angular.forEach($scope.tipo_servicio, function(value, key) {
        value.seleccionado = false;
      });
      $scope.seleccionadoMostrarTodosServicio = false;
      $scope.serviciosTuristicos = [];
      $scope.markerArrayServiciosTuristicos = [];
      //actualizar el estado de la ruta
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addEstadoIniciarRecorrido.php",
          data: $httpParamSerializer({
            "id_usuario": localStorage.getItem("id_usuario"),
            "id_ruta"   : $rootScope.id_ruta_tipica_seleccionada,
            "estado"    : 1
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si recato bien los datos
            //$rootScope.toast('Comenzando ruta...', 'short');
            $scope.entrarUnaVezRecomendado = 0;
            $scope.fechaAnterior = new Date();
            //$scope.rutaTipica();
            $scope.textoPausarContinuar = "Continuar";
            $scope.pausa = true;
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });
    }

    $scope.mostrarDinamicos = function(lat,long){
            $ionicLoading.show();
            console.log("Mostrar dinmamicos");
            //Obtener patrimonios Recomendacion
              $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/getPatrimonioRutaDinamicaNuevo.php",
                data: $httpParamSerializer({
                  "id_usuario" : localStorage.getItem("id_usuario"),
                  "longitud"   : long,
                  "latitud"    : lat
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si recato bien los datos
                  //console.log(response.data);
                  $scope.patrimoniosDinamica = response.data.patrimonio;
                  //console.log("patrimoinos: ",response.data.patrimonio);

                  //Obtener Servicios Recomendacion
                  $http({
                    method: "POST",
                    url: "http://200.14.68.107/atacamaGo/getServiciosTuristicosRutaDinamicaNuevo.php",
                    data: $httpParamSerializer({
                      "id_usuario" : localStorage.getItem("id_usuario"),
                      "longitud"   : long,
                      "latitud"    : lat
                    }),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                  }).then(function(response){ //ok si recato bien los datos
                      $scope.serviciosDinamica = response.data.servicios;
                      //console.log("Servicios: ",response.data.servicios);

                      $http({
                        method: "POST",
                        url: "http://200.14.68.107/atacamaGo/getItemTuristicoDinamico.php",
                        data: $httpParamSerializer({
                          "id_usuario" : localStorage.getItem("id_usuario"),
                          "longitud"   : long,
                          "latitud"    : lat
                        }),
                        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                      }).then(function(response){
                          $scope.itemDinamico = response.data.item_dinamico;
                          //console.log($scope.itemDinamico);
                          $http({
                            method: "POST",
                            url: "http://200.14.68.107/atacamaGo/getEventosDinamicos.php",
                            data: $httpParamSerializer({
                              "id_usuario" : localStorage.getItem("id_usuario"),
                              "longitud"   : long,
                              "latitud"    : lat
                            }),
                            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                          }).then(function(response){ 

                            //console.log(response);

                              //if(response.data.resultado == "ok"){
                                $scope.eventosDinamicos = response.data.eventos;
                                
                                
                                //llenar mapa con patrimoinos y servicios recmendados...
                                /*$rootScope.map.clear();
                                $rootScope.map.off();*/
                                
                                //ELiminar marcadores de Servicios markerArrayServiciosTuristicosDinamicos
                                angular.forEach($scope.markerArrayServiciosTuristicosDinamicos, function(value, key) { 
                                  $scope.markerArrayServiciosTuristicosDinamicos[key].remove();
                                });
                                $scope.markerArrayServiciosTuristicosDinamicos = [];

                                angular.forEach($scope.serviciosDinamica, function(value, key) {

                                      if( $scope.contadorlink == 0 ){
                                          if(value.visitado == true){
                                            var imgLink = "http://200.14.68.107/atacamaGoMultimedia/servicios/"+value.icono+"Oculto.png";
                                            $scope.link.push({
                                              "link" : "&markers=color:purple%7Clabel:S%7C"+value.direccion_georeferenciada_latitud+","+value.direccion_georeferenciada_longitud,
                                              "latitud" : value.direccion_georeferenciada_latitud,
                                              "longitud" : value.direccion_georeferenciada_longitud
                                            });
                                          }
                                          
                                      }

                                      //console.log(value);
                                      var ubicacion = new plugin.google.maps.LatLng(
                                        parseFloat(value.direccion_georeferenciada_latitud),
                                        parseFloat(value.direccion_georeferenciada_longitud) );

                                      //var url = "www/img/servicios/"+value.icono+".png";
                                      var url = "www/img/servicios/"+value.icono+"Oculto.png";

                                      $rootScope.map.addMarker({
                                        'position': ubicacion,
                                        'title': value.nombre_item_turistico,
                                        'draggable': false,
                                        'anchor':  [30, 45],
                                        icon: {
                                          url: url,
                                          size: { width: 30, height: 45 },
                                        },
                                        zIndex: 1
                                      },function(marker) {
                                          //accion al hacer clic en el marcador
                                          $scope.markerArrayServiciosTuristicosDinamicos[key] = marker;
                                          marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                            $rootScope.tipo_seleccionado = "servicio";
                                            $rootScope.item_seleccionado = value;
                                            $scope.openModalMenuAccionesModificar();
                                          });
                                      });
                                      
                                });

                                //console.log("PATR");
                                //ELiminar marcadores de markerArrayPatrimonioDinamicos
                                angular.forEach($scope.markerArrayPatrimonioDinamicos, function(value, key) { 
                                  $scope.markerArrayPatrimonioDinamicos[key].remove();
                                });
                                $scope.markerArrayPatrimonioDinamicos = [];

                                angular.forEach($scope.patrimoniosDinamica, function(value, key) {

                                    if( $scope.contadorlink == 0 ){
                                        if(value.visitado == true){
                                          imgLink = "http://200.14.68.107/atacamaGoMultimedia/patrimonios/"+value.icono+"Oculto.png";
                                          $scope.link.push({
                                            "link" : "&markers=color:brown%7Clabel:A%7C"+value.direccion_georeferenciada_latitud+","+value.direccion_georeferenciada_longitud,
                                            "latitud" : value.direccion_georeferenciada_latitud,
                                            "longitud" : value.direccion_georeferenciada_longitud

                                          });
                                        }
                                    }

                                    var ubicacion = new plugin.google.maps.LatLng(
                                      parseFloat(value.direccion_georeferenciada_latitud),
                                      parseFloat(value.direccion_georeferenciada_longitud) );

                                    //var url = "www/img/patrimonios/"+value.icono+".png";
                                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.nombre_item_turistico,
                                      'draggable': false,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                      //accion al hacer clic en el marcador
                                      $scope.markerArrayPatrimonioDinamicos[key] = marker;
                                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                        $rootScope.tipo_seleccionado = "patrimonio";
                                        $rootScope.item_seleccionado = value;
                                        $scope.openModalMenuAccionesModificar();
                                      });
                                    });
                                });

                                //console.log("HOLA");
                                //ELiminar marcadores de markerItemDinamicos
                                angular.forEach($scope.markerItemDinamicos, function(value, key) { 
                                  $scope.markerItemDinamicos[key].remove();
                                });
                                $scope.markerItemDinamicos = [];

                                //console.log($scope.itemDinamico);
                                angular.forEach($scope.itemDinamico, function(value, key) {
                                    if( $scope.contadorlink == 0 ){
                                      if(value.visitado == true){
                                        imgLink = "http://200.14.68.107/atacamaGoMultimedia/deja_huella_azul.png";
                                        $scope.link.push({
                                          "link" : "&markers=color:blue%7Clabel:I%7C"+value.direccion_georeferenciada_latitud+","+value.direccion_georeferenciada_longitud,
                                          "latitud" : value.direccion_georeferenciada_latitud,
                                          "longitud" : value.direccion_georeferenciada_longitud

                                        });
                                      }
                                    }
                                    //console.log(value);
                                    var ubicacion = new plugin.google.maps.LatLng(
                                      parseFloat(value.direccion_georeferenciada_latitud),
                                      parseFloat(value.direccion_georeferenciada_longitud) );

                                    //var url = "www/img/patrimonios/"+value.icono+".png";
                                    var url = "www/img/deja_tu_huella/deja_huella_azul.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.nombre_item_turistico,
                                      'draggable': false,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                      //accion al hacer clic en el marcador
                                      $scope.markerItemDinamicos[key] = marker;
                                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                        $rootScope.tipo_seleccionado = "item";
                                        $rootScope.item_seleccionado = value;
                                        $scope.openModalMenuAccionesModificar();
                                      });
                                    });
                                });

                                $scope.contadorlink = 1;
                                /**************MOSTRAR SERVICIOS Y PATRIMONIOS FILTRADOS X EL USUARIO**************/
                                
                                //ELiminar marcadores de markerArrayServiciosTuristicos
                                angular.forEach($scope.markerArrayServiciosTuristicos, function(value, key) { 
                                  $scope.markerArrayServiciosTuristicos[key].remove();
                                });

                                $scope.markerArrayServiciosTuristicos = [];

                                angular.forEach($scope.serviciosTuristicos, function(value, key) {    
                                 
                                      var ubicacion = new plugin.google.maps.LatLng(
                                        parseFloat(value.direccion_georeferenciada_latitud),
                                        parseFloat(value.direccion_georeferenciada_longitud) );

                                      //var url = "www/img/servicios/"+value.icono+".png";
                                      var url = "www/img/servicios/"+value.icono+"Oculto.png";

                                      $rootScope.map.addMarker({
                                        'position': ubicacion,
                                        'title': value.nombre_item_turistico,
                                        'draggable': false,
                                        'anchor':  [30, 45],
                                        icon: {
                                          url: url,
                                          size: { width: 30, height: 45 },
                                        },
                                        zIndex: 1
                                      },function(marker) {
                                          //accion al hacer clic en el marcador
                                          $scope.markerArrayServiciosTuristicos[key] = marker;
                                          marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                            $rootScope.tipo_seleccionado = "servicio";
                                            $rootScope.item_seleccionado = value;
                                            $scope.openModalMenuAccionesModificar();
                                          });
                                      });
                                      
                                });

                                //ELiminar marcadores de markerArrayPatrimonio
                                angular.forEach($scope.markerArrayPatrimonio, function(value, key) { 
                                  $scope.markerArrayPatrimonio[key].remove();
                                });

                                $scope.markerArrayPatrimonio = [];

                                angular.forEach($scope.patrimonios, function(value, key) {

                                    var ubicacion = new plugin.google.maps.LatLng(
                                      parseFloat(value.direccion_georeferenciada_latitud),
                                      parseFloat(value.direccion_georeferenciada_longitud) );

                                    //var url = "www/img/patrimonios/"+value.icono+".png";
                                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                                    $rootScope.map.addMarker({
                                      'position': ubicacion,
                                      'title': value.nombre_item_turistico,
                                      'draggable': false,
                                      'anchor':  [30, 45],
                                      icon: {
                                        url: url,
                                        size: { width: 30, height: 45 },
                                      },
                                      zIndex: 1
                                    },function(marker) {
                                      //accion al hacer clic en el marcador
                                      $scope.markerArrayPatrimonio[key] = marker;
                                      $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                                      $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                        $rootScope.tipo_seleccionado = "patrimonio";
                                        $rootScope.item_seleccionado = value;
                                        $scope.openModalMenuAccionesModificar();
                                      });
                                    });
                                });

                                
                               
                                //llenar mapa con eventos dinamicos...
                                //ELiminar marcadores de markerArrayEventosDinamicos
                                angular.forEach($scope.markerArrayEventosDinamicos, function(value, key) { 
                                  $scope.markerArrayEventosDinamicos[key].remove();
                                });
                                $scope.markerArrayEventosDinamicos = [];
                                    
                                angular.forEach($scope.eventosDinamicos, function(value, key) {  
                                     
                                          var ubicacion = new plugin.google.maps.LatLng(
                                            parseFloat(value.georeferenciacion_evento_latitud),
                                            parseFloat(value.georeferenciacion_evento_longitud) );

                                          var url = "www/img/gps.png";

                                          $rootScope.map.addMarker({
                                            'position': ubicacion,
                                            'title': value.titulo_evento,
                                            'draggable': false,
                                            'anchor':  [30, 45],
                                            icon: {
                                              url: url,
                                              size: { width: 30, height: 45 },
                                            },
                                            zIndex: 1
                                          },function(marker) {
                                              //accion al hacer clic en el marcador
                                              $scope.markerArrayEventosDinamicos[key] = marker;
                                              marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                                $rootScope.tipo_seleccionado = "evento";
                                                $rootScope.evento_seleccionado = value;
                                                $scope.openModalEvento();
                                              });
                                          });
                                          
                                });
                                $ionicLoading.hide();
                          /***************************************************************************************/
                            //document.getElementById("containerLoading").className = "fadeOut";
                          }, function(){ //Error de conexión
                          $rootScope.toast('Verifica tu conexión a internet', 'short');
                        });

                      }, function(){ //Error de conexión
                        $rootScope.toast('Verifica tu conexión a internet', 'short');
                      });                       

                    }, function(){ //Error de conexión
                      $rootScope.toast('Verifica tu conexión a internet', 'short');
                    });

                  }, function(){ //Error de conexión
                    $rootScope.toast('Verifica tu conexión a internet', 'short');
                  });           
    };


    $scope.stopMiRecorrido = function(){

      console.log("terminar vaventura....");
      $scope.entrarUnaVezDinamico = 0;
      $scope.rutaDinamica = false;
        
      navigator.geolocation.clearWatch($scope.watchRutaDinamica);
      $scope.watchRutaDinamica = null;

      //quitar filtros de marcadores....
      angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
            value.seleccionado = false;
      });
      $scope.seleccionadoMostrarTodos = false;
      $scope.patrimonios = [];
      $scope.markerArrayPatrimonio = [];

      angular.forEach($scope.tipo_servicio, function(value, key) {
        value.seleccionado = false;
      });
      $scope.seleccionadoMostrarTodosServicio = false;
      $scope.serviciosTuristicos = [];
      $scope.markerArrayServiciosTuristicos = [];


      $rootScope.map.clear();
      $rootScope.map.off();
      $rootScope.id_ruta_recorrida_turista = null;

      navigator.geolocation.clearWatch($scope.watchRutaTipica);
      $scope.watchRutaTipica = null;

      $scope.cerrarIniciarMiRecorrido();
    }

    $scope.abrirAddHuella = function(){

      $scope.mostrarBotonesRegistrarhuella = true;

      $rootScope.map.getCameraPosition(function(camera) {
          var lat = camera.target.lat;
          var lng = camera.target.lng;
        
          //console.log( lat,lng);
          var POS_MARCADOR = new plugin.google.maps.LatLng(lat, lng);

          $rootScope.map.addMarker({
              'position': POS_MARCADOR,
              'draggable': true,
              'anchor':  [30, 35],
              icon: {
                  url: "www/img/deja_tu_huella/deja_huella_azul.png",
                  size: { width: 40, height: 35 },
              },        
          }, function(marker) {
            if ($rootScope.marcadorNuevoItem != null) {
              $rootScope.marcadorNuevoItem.remove();
            }else{
              console.log("no existe marcador previo");
            }
              $rootScope.marcadorNuevoItem = marker;
              marker.addEventListener(plugin.google.maps.event.MARKER_DRAG_END, function(marker) {
              marker.getPosition(function(latLng) {
                  //marker.setTitle(latLng.toUrlValue());
                  //marker.showInfoWindow();
                  //var coor = split(",",)
                  //console.log(latLng);
                  $rootScope.lat = latLng.lat;
                  $rootScope.lon = latLng.lng;
                });
              });
              $rootScope.map.getCameraPosition(function(latLng) {
                console.log("deja lista la posicion");
                //console.log(latLng);
                $rootScope.lat = latLng.target.lat;
                $rootScope.lon = latLng.target.lng;
                //console.log($rootScope.lat);
                //console.log($rootScope.lon);
              });
          });
      });

      $rootScope.map.setClickable(false);
      var myPopup = $ionicPopup.show({
        template: '<p CLASS="fuenteRoboto fuenteModal">Presionar prolongadamente para mover marcador.</p>',
        title: 'Aviso',
        scope: $scope,
        buttons: [
          { 
              text: '<i class="icon ion-close-round"></i>',
              type:'popclose',
                onTap: function(e) {
                  $rootScope.map.setClickable(true);
                }
          },
          {
            text: '<b>Aceptar</b>',
            type: 'button-positive asd',
            onTap: function(e) {
              $rootScope.map.setClickable(true);
            }
          }
        ]
      });
    }

    $scope.agregarhuellaPhp = function(lat, lon){          
          $rootScope.POS_MARCADOR = new plugin.google.maps.LatLng(lat, lon);
          
          //obtenemos la direccion del lugar....
          var request = {
            'position': $rootScope.POS_MARCADOR
          };

          plugin.google.maps.Geocoder.geocode(request, function(results) {
            if (results.length) {
              var result = results[0];
              var position = result.position; 
              var address = {
                numero: result.subThoroughfare || "",
                calle: result.thoroughfare || "",
                ciudad: result.locality || "",
                region: result.adminArea || "",
                postal: result.postalCode || "",
                pais:   result.country || ""
              };

              
              //GUARDAR LA DIRECCION ...
              $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/addItemTuristico.php",
                data: $httpParamSerializer({
                  "id_usuario"                         : localStorage.getItem("id_usuario"),
                  "direccion_georeferenciada_longitud" : lon,
                  "direccion_georeferenciada_latitud"  : lat,
                  "direccion_item"                     : address.callle,
                  "numero_direccion_item"              : address.numero,
                  "ciudad"                             : address.ciudad,
                  "id_ruta_recorrida_turista"          : $rootScope.id_ruta_recorrida_turista
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si guardó correctamente.
                //console.log(response);
                if(response.data.resultado == "ok"){
    
                  //guardar el id_item_turistico para guardar la descripcion titulos videos etc.
                  $rootScope.id_item_turistico = response.data.id_item_turistico;
                  var id_item_turistico_creado_usuario = response.data.id_item_turistico;

                  $ionicModal.fromTemplateUrl('templates/menuAcciones.html', {
                      scope: $rootScope,
                      animation: 'slide-in-up',
                      backdropClickToClose: false,
                  }).then(function(modal) {
                      $rootScope.modalMenuAcciones = modal;
                      $rootScope.map.setClickable(false);
                      $rootScope.modalMenuAcciones.show();
                  });

                  /*creamos el marcador...*/
                  var POS_MARCADOR = new plugin.google.maps.LatLng(lat, lon);
                  var url = 'www/img/deja_tu_huella/mis_huellas.png';
                  $rootScope.map.addMarker({
                    'position': POS_MARCADOR,
                    'draggable': false,
                    'anchor':  [30, 35],
                    icon: {
                      url: url,
                      size: { width: 30, height: 35 },
                    }
                    
                  },function(marker) {
                      //accion al hacer clic en el marcador
                      
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        var marcadorNuevo = {};
                        $http({
                          method: "POST",
                          url: "http://200.14.68.107/atacamaGo/getItemTuristicoNuevo.php",
                          data: $httpParamSerializer({
                            "id_item_turistico": id_item_turistico_creado_usuario,
                            "id_usuario":localStorage.getItem("id_usuario")
                        }),
                          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                        }).then(function(response){ //ok si guardó correctamente.
                          //console.log(response);
                          if(response.data.resultado == "ok"){
                            
                            marcadorNuevo = {
                              "descripcion"                        : response.data.item_turistico.descripcion,
                              "nombre"                             : response.data.item_turistico.nombre,
                              "direccion_georeferenciada_latitud"  : "",
                              "direccion_georeferenciada_longitud" : "",
                              "foto"                               : response.data.item_turistico.foto,
                              "id"                                 : id_item_turistico_creado_usuario,
                              "id_usuario"                         : response.data.item_turistico.id_usuario,
                              "visitado"                           : response.data.item_turistico.visitado,
                            };

                            $rootScope.tipo_seleccionado = "item";
                            $rootScope.item_seleccionado = marcadorNuevo;
                            $ionicLoading.hide();
                            $scope.openModalMenuAccionesModificar();
                          }else if ( response.data.resultado == "no data" ){
                            //$rootScope.toast('lugar aún no tiene audios', 'short');
                            $ionicLoading.hide();
                          }else{
                            $rootScope.toast('error, intenta nuevamente', 'short');
                            $ionicLoading.hide();
                          }
                        }, function(){ //Error de conexión
                          $rootScope.toast('Verifica tu conexión a internet', 'short');
                          $ionicLoading.hide();
                        });

                      });   
                  });
                  /*fin crear marcador...*/
                  $rootScope.toast('Item agregado', 'short');
                  $ionicLoading.hide();
                }
                else{
                  //$scope.cerrarModalFiguraGps();
                  $rootScope.toast('Sólo se pueden crear ítems en Atacama', 'short');
                  $ionicLoading.hide();
                }

              }, function(){ //Error de conexión
                  $rootScope.toast('Verifica tu conexión a internet', 'short');
                  $ionicLoading.hide();
              });
              //GUARDAR LA DIRECCION ...  
            } else {
              $rootScope.toast('No se encuentra el terreno', 'short');
              $ionicLoading.hide();
            }
          });
    }

    $scope.cerrarModalFiltro = function(){
      $rootScope.map.setClickable(true);
      $scope.showFiltroServicio  = false;
      $scope.showFiltroAtractivo = false;
    }

    $scope.detenerRecorrido = function(){

      console.log( $scope.link );
      var promedioLat = 0;
      var promedioLon = 0;
      var cantidadItems = 0;
      angular.forEach($scope.link, function(value, key) {  
        console.log(value);
        promedioLat += parseFloat(value.latitud);
        promedioLon += parseFloat(value.longitud);
        cantidadItems ++ ;
      });
      promedioLat /= cantidadItems;
      promedioLon /= cantidadItems; 
      console.log(promedioLat,promedioLon);

      var linkMapa = "https://maps.googleapis.com/maps/api/staticmap?center="+promedioLat+","+promedioLon+
                      /*"&zoom=16" +*/
                      "&size=1000x1000" +
                      "&maptype=roadmap";

      angular.forEach($scope.link, function(value, key) {  
        linkMapa = linkMapa + value.link;
      });

      linkMapa += "&path=color:0xff0000ff|weight:5|";
      angular.forEach($scope.recorridoUsuario, function(value, key) {  
        linkMapa += value.link;
      });

      linkMapa = linkMapa.substring(0, linkMapa.length - 1);

      linkMapa = linkMapa + "&key=AIzaSyAC-Lt38Udx7CdlNIZhcLpSAB4SxsS-DCk";
      console.log(linkMapa);

      $rootScope.imgRecorrido = linkMapa;
      $rootScope.btnCerrarMiRecorrido = true;

      navigator.geolocation.clearWatch($scope.watchRutaDinamica);
      $scope.watchRutaDinamica = null;

      $rootScope.estadoRutaUsuario = "completa"      
      $scope.cerrarIniciarMiRecorrido();
      $rootScope.map.setClickable(false);
      //Modal rutas Tipicas...
      $ionicModal.fromTemplateUrl('templates/crearMisRecorridos.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalCrearMisRecorridos = modal;
        $rootScope.modalCrearMisRecorridos.show();
      });

    }

    $scope.pausarRecorrido = function(){

      console.log( $scope.link );
      var promedioLat = 0;
      var promedioLon = 0;
      var cantidadItems = 0;
      angular.forEach($scope.link, function(value, key) {  
        console.log(value);
        promedioLat += parseFloat(value.latitud);
        promedioLon += parseFloat(value.longitud);
        cantidadItems ++ ;
      });
      promedioLat /= cantidadItems;
      promedioLon /= cantidadItems; 
      console.log(promedioLat,promedioLon);

      var linkMapa = "https://maps.googleapis.com/maps/api/staticmap?center="+promedioLat+","+promedioLon+
                      /*"&zoom=16" +*/
                      "&size=1000x1000" +
                      "&maptype=roadmap";

      angular.forEach($scope.link, function(value, key) {  
        linkMapa = linkMapa + value.link;
      });

      linkMapa += "&path=color:0xff0000ff|weight:5|";
      angular.forEach($scope.recorridoUsuario, function(value, key) {  
        linkMapa += value.link;
      });

      linkMapa = linkMapa.substring(0, linkMapa.length - 1);

      linkMapa = linkMapa + "&key=AIzaSyAC-Lt38Udx7CdlNIZhcLpSAB4SxsS-DCk";
      console.log(linkMapa);

      $rootScope.imgRecorrido = linkMapa;
      $rootScope.btnCerrarMiRecorrido = true;

      navigator.geolocation.clearWatch($scope.watchRutaDinamica);
      $scope.watchRutaDinamica = null;

      
      $rootScope.estadoRutaUsuario = "en curso"
      $scope.cerrarIniciarMiRecorrido();
      $rootScope.map.setClickable(false);
      //Modal rutas Tipicas...
      $ionicModal.fromTemplateUrl('templates/crearMisRecorridos.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalCrearMisRecorridos = modal;
        $rootScope.modalCrearMisRecorridos.show();
      });

    }

}]);