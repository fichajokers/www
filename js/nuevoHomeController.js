angular.module('starter.controllers')

.controller('nuevoHomeCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$ionicModal','$window','$httpParamSerializer','$ionicSideMenuDelegate','$ionicPlatform','$ionicPopup',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$ionicModal,$window,$httpParamSerializer,$ionicSideMenuDelegate,$ionicPlatform,$ionicPopup) {
  // With the new view caching in Ionic, Controllers are only called
  
  /*var permissions = cordova.plugins.permissions;
  permissions.requestPermission(permissions.ACCESS_COARSE_LOCATION, success, error);

  function error() {
    console.warn('Camera permission is not turned on');
  }

  function success( status ) {
    if( !status.hasPermission ) error();
  }*/

  /* var permissions = cordova.plugins.permissions;
  permissions.hasPermission(permissions.ACCESS_FINE_LOCATION, function( status ){
    if ( status.hasPermission ) {
      console.log("Yes :D ");
      //alert("Yes :D ");
    }
    else {
      console.warn("No :( ");
      //alert("No :( ");

      permissions.requestPermission(permissions.ACCESS_FINE_LOCATION, success, error);
       
      function error() {
        console.warn('ACCESS_FINE_LOCATION permission is not turned on');
      }
       
      function success( status ) {
        //alert("SIU");
        if( !status.hasPermission ) error();
      }
    }
  }); */

  console.log("nuevoHomecontroller");
  //console.log($rootScope.volverDeServiciosAtractivos);
  /*PRIMERA VES SE SELECCIONA EL MENU "EXPLORA Y DESCUBRE"*/
  $scope.soloUnaVez = false;
  $scope.menuAbierto = false;
  $scope.experienciaSeleccionada = false;
  $scope.serviciosAtractivosImgSeleccionado = false;
  $scope.exploraDescubreImgSeleccionado = false;
  $scope.imgMenuExperiencia = "img/nuevoHome/experienciaAtacama.png";
  $scope.filtroActual = "todas las comunas";
  $scope.$on('$ionicView.afterEnter', function(){
     //$scope.exploraDescubre();
     if ($rootScope.primerIngreso == true) {
        $scope.experienciaAtacama();
        $rootScope.primerIngreso = false;
     }
  });

  /*document.addEventListener("deviceready", function() {
    if( $rootScope.desdeOtraVista ){
     
      $state.go($state.current, {}, {reload: true});
      $rootScope.desdeOtraVista = false;
    }
  });*/

  $scope.reload = function(){
    $state.go($state.current, {}, {reload: true});
  }
  

  width = window.screen.width;

  primerMargin = 0;
  img2 = 0;
  img3 = 0;

  txt2 = 0;
  txt3 = 0;

  /*si es iphone 4*/
  if( width < 321  ){
  	primerMargin = 85;
  	img2 = 80 + primerMargin;
  	img3 = 80 + img2;

  	txt2 = 80 + primerMargin;
  	txt3 = 80 + txt2;
  	$scope.styleExperienciaAtacama  = {
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleExploraDescubre     = {
  		"margin-left" : img2+"px"
  	};
  	$scope.styleServiciosAtractivos = {
  		"margin-left" : img3+"px"
  	};

  	$scope.styleTxtExperienciaAtacama  = {
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleTxtExploraDescubre     = {
  		"margin-left" : img2+"px"
  	};
  	$scope.styleTxtServiciosAtractivos = {
  		"margin-left" : img3+"px"
  	};
  }

  /*si es */
  if( width > 321 && width <= 360 ){
  	primerMargin = 105;
  	img2 = 80 + primerMargin;
  	img3 = 80 + img2;

  	txt2 = 86 + primerMargin;
  	txt3 = 80 + txt2;
  	$scope.styleExperienciaAtacama  = {
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleExploraDescubre     = {
  		"margin-left" : img2+"px"
  	};
  	$scope.styleServiciosAtractivos = {
  		"margin-left" : img3+"px"
  	};

  	$scope.styleTxtExperienciaAtacama  = {
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleTxtExploraDescubre     = {
  		"margin-left" : img2+"px"
  	};
  	$scope.styleTxtServiciosAtractivos = {
  		"margin-left" : img3+"px"
  	};
  }

  /*si es iphone 6*/
  if( width > 360 && width <= 384 ){
  	primerMargin = 110;
  	img2 = 80 + primerMargin;
  	img3 = 80 + img2;

  	txt2 = 80 + primerMargin;
  	txt3 = 80 + txt2;
  	$scope.styleExperienciaAtacama  = {
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleExploraDescubre     = {
  		"margin-left" : img2+"px"
  	};
  	$scope.styleServiciosAtractivos = {
  		"margin-left" : img3+"px"
  	};

  	$scope.styleTxtExperienciaAtacama  = {
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleTxtExploraDescubre     = {
  		"margin-left" : txt2+"px"
  	};
  	$scope.styleTxtServiciosAtractivos = {
  		"margin-left" : txt3+"px"
  	};
  }

  

  /*384-767*/
  if( width > 384 && width <= 599 ){
  	primerMargin = 130;
  	img2 = 130 + primerMargin;
  	img3 = 130 + img2;

  	txt2 = 130 + primerMargin;
  	txt3 = 130 + txt2;
  	$scope.styleExperienciaAtacama  = {
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleExploraDescubre     = {
  		"margin-left" : img2+"px"
  	};
  	$scope.styleServiciosAtractivos = {
  		"margin-left" : img3+"px"
  	};

  	$scope.styleTxtExperienciaAtacama  = {
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleTxtExploraDescubre     = {
  		"margin-left" : txt2+"px"
  	};
  	$scope.styleTxtServiciosAtractivos = {
  		"margin-left" : txt3+"px"
  	};
  }

  /*lenovo tablet*/
  if( width > 599 && width <= 767 ){
    primerMargin = 180;
    img2 = 120 + primerMargin;
    img3 = 120 + img2;

    txt2 = 120 + primerMargin;
    txt3 = 120 + txt2;
    $scope.styleExperienciaAtacama  = {
      "margin-left" : primerMargin+"px"
    };
    $scope.styleExploraDescubre     = {
      "margin-left" : img2+"px"
    };
    $scope.styleServiciosAtractivos = {
      "margin-left" : img3+"px"
    };

    $scope.styleTxtExperienciaAtacama  = {
      "margin-left" : primerMargin+"px"
    };
    $scope.styleTxtExploraDescubre     = {
      "margin-left" : txt2+"px"
    };
    $scope.styleTxtServiciosAtractivos = {
      "margin-left" : txt3+"px"
    };
  }


  /*si es ipad*/
  if( width > 767 && width <= 768 ){
  	primerMargin = 240;
  	img2 = 150 + primerMargin;
  	img3 = 150 + img2;

  	txt2 = 150 + primerMargin;
  	txt3 = 150 + txt2;
  	$scope.styleExperienciaAtacama  = {
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleExploraDescubre     = {
  		"margin-left" : img2+"px"
  	};
  	$scope.styleServiciosAtractivos = {
  		"margin-left" : img3+"px"
  	};

  	$scope.styleTxtExperienciaAtacama  = {
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleTxtExploraDescubre     = {
  		"margin-left" : txt2+"px"
  	};
  	$scope.styleTxtServiciosAtractivos = {
  		"margin-left" : txt3+"px"
  	};
  }

  $scope.serviciosAtractivosSeleccionado = false;
  $scope.exploraDescubreSeleccionado = true;


  /* cordova.getAppVersion(function(version) {
    appVersion = version;
    console.log(version);
    
    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/getVersion.php",
      data: $httpParamSerializer({
        "so"    : "android",
        version : version
      }),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ //ok si recato bien los datos
        if(response.data.resultado == "ok"){
          //alert("OK");
        }else{
          var alertPopup = $ionicPopup.alert({
            title: 'Aviso',
            template: 'Actualice la versión de Atacama Go para seguir utilizando la aplicación',
            okText : 'Actualizar',
            okType : 'btn-morado asd'
          });

          alertPopup.then(function(res) {
            window.open('https://play.google.com/store/apps/details?id=com.uv.atacamago&hl=es','_system');
            //$rootScope.map.setClickable(true);
          });

        }
    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
    });

  }); */

  $scope.getExperiencia = function(){
    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/getExperiencia.php",
      data: $httpParamSerializer({}),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ //ok si recato bien los datos
        //console.log(response);
        if(response.data.resultado == "ok"){
          $rootScope.experiencia = response.data.experiencia;
          $rootScope.experienciaFiltro = response.data.experiencia;
          //console.log($rootScope.experiencia);
        }
    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
    });  
  }


  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/getComunas.php",
    data: $httpParamSerializer({}),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si recato bien los datos
    //console.log(response);
    $scope.comunas = response.data.comuna;
    $scope.comunas.unshift({
      id_comuna : -1,
       nombre : "Todas las comunas"
    });
    //console.log($scope.comunas);
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  })
  
  $scope.getExperiencia();

  $rootScope.experienciaAtacama = function(){
    $scope.experienciaSeleccionada = true;
    $scope.serviciosAtractivosImgSeleccionado = false;
    $scope.exploraDescubreImgSeleccionado = false;
    //console.log("entroooooooo");

    $scope.getExperiencia();
  	$scope.styleTxtExperienciaAtacama  = {
  		"color" : "#AB1765",
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleTxtExploraDescubre     = {
  		"color" : "white",
  		"margin-left" : txt2+"px"
  	};
  	$scope.styleTxtServiciosAtractivos = {
  		"color" : "white",
  		"margin-left" : txt3+"px"
  	};

    $scope.experienciaAtacamaSeleccionado  = true;
    $scope.serviciosAtractivosSeleccionado = false;
    $scope.exploraDescubreSeleccionado     = false;
  }

  $scope.exploraDescubre = function(){
    $scope.experienciaSeleccionada = false;
    $scope.serviciosAtractivosImgSeleccionado = false;
    $scope.exploraDescubreImgSeleccionado = true;
  	$scope.styleTxtExperienciaAtacama  = {
  		"color" : "white",
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleTxtExploraDescubre     = {
  		"color" : "#7162AB",
  		"margin-left" : txt2+"px"
  	};
  	$scope.styleTxtServiciosAtractivos = {
  		"color" : "white",
  		"margin-left" : txt3+"px"
  	};

    $scope.serviciosAtractivosSeleccionado = false;
    $scope.experienciaAtacamaSeleccionado  = false;
    $scope.exploraDescubreSeleccionado     = true;
  }

  $scope.serviciosAtractivos = function(){
    $scope.experienciaSeleccionada = false;
    $scope.serviciosAtractivosImgSeleccionado = true;
    $scope.exploraDescubreImgSeleccionado = false;
  	$scope.styleTxtExperienciaAtacama  = {
  		"color" : "white",
  		"margin-left" : primerMargin+"px"
  	};
  	$scope.styleTxtExploraDescubre     = {
  		"color" : "white",
  		"margin-left" : txt2+"px"
  	};
  	$scope.styleTxtServiciosAtractivos = {
  		"color" : "#764B18",
  		"margin-left" : txt3+"px"
  	};

    $scope.serviciosAtractivosSeleccionado = true;
    $scope.exploraDescubreSeleccionado     = false;
    $scope.experienciaAtacamaSeleccionado  = false;

    //console.log('entro');
  }

  $rootScope.menuOpcion = function(opcion){
  	if(opcion == 1){
  		//alert("rutas tipicas atama");
      $rootScope.volverExploraYDescubre = true;
  		//Modal rutas Tipicas...
  		$ionicModal.fromTemplateUrl('templates/rutasTipicas.html', {
  		  scope: $rootScope,
  		  animation: 'slide-in-up',
  		  backdropClickToClose: false,
  		}).then(function(modal) {
  		  $rootScope.modalRutasTipicas = modal;
  		  $rootScope.modalRutasTipicas.show();
  		});
  		
  	}

  	if(opcion == 2){
  		//alert("deja tu huella");
      $rootScope.volverExploraYDescubre = true;
      $state.go("app.dejaTuHuella");
  	}

  	if(opcion == 3){
      $rootScope.volverExploraYDescubre = true;
  		$state.go("app.iniciarMiRecorrido");
  	}

  	if(opcion == 4){
  		//alert("mis recorridos");
      $state.go("app.misRecorridos");
  	}

  	if(opcion == 5){
      $rootScope.volverServicioYAtractivo2 = true;
  		//Modal eventos...
      $ionicModal.fromTemplateUrl('templates/eventos.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalEventos = modal;
        $rootScope.modalEventos.show();
      });
  	}

    if(opcion == 6){

      $rootScope.volverServicioYAtractivo2 = true;
      $ionicModal.fromTemplateUrl('templates/serviciosTuristicos.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalServiciosTuristicos = modal;
        $rootScope.modalServiciosTuristicos.show();
      });
    }

    if(opcion == 7){
      $rootScope.volverServicioYAtractivo2 = true;
      $ionicModal.fromTemplateUrl('templates/atractivosTuristicos.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAtractivosTuristicos = modal;
        $rootScope.modalAtractivosTuristicos.show();
      });
    }

    if(opcion == 8){
      //Modal Reservas...
      $rootScope.volverExploraYDescubre = true;
      $ionicModal.fromTemplateUrl('templates/reservas.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalReservas = modal;
        $rootScope.modalReservas.show();
      });
    }
  }

  $scope.detalleExperiencia = function(item){
    $rootScope.experienciaSeleccionadaNuevoHome = item;
    $rootScope.id_producto_servicio_turistico = item.id_producto_servicio_turistico;
    $ionicModal.fromTemplateUrl('templates/detalleExperiencia.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalDetalleExperiencia = modal;
      $rootScope.modalDetalleExperiencia.show();
    });
  }

  /*if( $rootScope.flagDesdeNuevoHomeAtractivos == true ){
    $rootScope.flagDesdeNuevoHomeAtractivos = false;
    $ionicModal.fromTemplateUrl('templates/atractivosTuristicos.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalAtractivosTuristicos = modal;
      $rootScope.modalAtractivosTuristicos.show();
    });
  }

  if( $rootScope.flagDesdeNuevoHomeServicios == true ){
    $rootScope.flagDesdeNuevoHomeServicios = false;
    $ionicModal.fromTemplateUrl('templates/serviciosTuristicos.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalServiciosTuristicos = modal;
      $rootScope.modalServiciosTuristicos.show();
    });
  }*/

  $scope.tutorial = function(){
    $state.go("app.tutorial");
  }

  $scope.$watch(function () {
      return $ionicSideMenuDelegate.getOpenRatio();
    },
    function (ratio) {
      if (ratio == 1){
        $scope.menuAbierto = true;
      }else{
        $scope.menuAbierto = false;
      }
    }
  );

  $scope.mostrarFiltro = function(){
    if( $scope.mostrarMenuFiltro ){
      $scope.mostrarMenuFiltro = false;
    }else{
      $scope.mostrarMenuFiltro = true;
    }
  }

  $scope.filtrar = function(filtro){
    console.log(filtro);
    var contador = 0;
    $rootScope.experienciaFiltro = [];

    if(filtro == -1){
      $rootScope.experienciaFiltro = $rootScope.experiencia;
      $scope.filtroActual  = "todas las comunas";
    }else{
      angular.forEach($rootScope.experiencia, function(value, key) {
        if( value.id_comuna == filtro){
          $rootScope.experienciaFiltro[contador] = value;
          contador +=1;
        }
      });

      angular.forEach($scope.comunas, function(value, key) {
        if(value.id_comuna == filtro){
          $scope.filtroActual  = value.nombre;
        }
      });
    }

    $scope.mostrarMenuFiltro = false;

    /*$scope.rutas_tipicas = {};
    if(filtro == 'todas'){
      $scope.rutas_tipicas = $scope.rutas_tipicas_aux;
      $scope.filtroActual  = "Todas las rutas";
      $scope.mostrarMenuFiltro = false;
    }

    if(filtro == 'enRecorrido'){
      var contador = 0;
      angular.forEach($scope.rutas_tipicas_aux, function(value, key) {
        if(value.estado_ruta == 'en curso'){
          $scope.rutas_tipicas[contador] = value;
          contador +=1;
        }      
      });
      $scope.filtroActual  = "En recorrido";
      $scope.mostrarMenuFiltro = false;
    }

    if(filtro == 'recorridas'){
      var contador = 0;
      angular.forEach($scope.rutas_tipicas_aux, function(value, key) {
        if(value.estado_ruta == 'completa'){

          $scope.rutas_tipicas[contador] = value;
          contador +=1;
        }      
      });
      $scope.filtroActual  = "Recorridas";
      $scope.mostrarMenuFiltro = false;
    }

    if(filtro == 'sinRecorrer'){
      var contador = 0;
      angular.forEach($scope.rutas_tipicas_aux, function(value, key) {
        if(value.estado_ruta == null){
          $scope.rutas_tipicas[contador] = value;
          contador +=1;
        }      
      });
      $scope.filtroActual  = "Sin recorrer";
      $scope.mostrarMenuFiltro = false;
    }*/
    //console.log("Rutas_tipicas:",$scope.rutas_tipicas);
  }

  if( $rootScope.volverDeServiciosAtractivos == 'atractivos' ){
    $rootScope.volverDeServiciosAtractivos = null;
    $ionicModal.fromTemplateUrl('templates/atractivosTuristicos.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalAtractivosTuristicos = modal;
      $rootScope.modalAtractivosTuristicos.show();
    });
  }else if ( $rootScope.volverDeServiciosAtractivos == 'servicios' ){
    $rootScope.volverDeServiciosAtractivos = null;
    $ionicModal.fromTemplateUrl('templates/serviciosTuristicos.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalServiciosTuristicos = modal;
      $rootScope.modalServiciosTuristicos.show();
    });
  }else if ( $rootScope.volverDeServiciosAtractivos == 'servicioYatractivos') {
    $rootScope.volverDeServiciosAtractivos = null;
    $scope.serviciosAtractivos();


    //console.log($scope.experienciaAtacamaSeleccionado, $scope.serviciosAtractivosSeleccionado, $scope.exploraDescubreSeleccionado);
  }

  if( $rootScope.volverExploraYDescubre == true){
    console.log("entro $rootScope.volverDeServiciosAtractivos: ",$rootScope.volverExploraYDescubre);
    $rootScope.volverExploraYDescubre = false;
    console.log("se transformo $rootScope.volverDeServiciosAtractivos: ",$rootScope.volverExploraYDescubre);
    //$scope.exploraDescubre();
  }

  if ($rootScope.atrasRecorrerRutaTipicas == true){
    $rootScope.atrasRecorrerRutaTipicas = false;
    console.log("viene de recorrer ruta");
    $scope.exploraDescubre();
    //$scope.menuOpcion("1");
    $rootScope.modalSaberMas.show();
  }

  if ($rootScope.volverExploraYDescubreRutas == true){
    console.log("viene de recorrer ruta Mapa");
    $scope.exploraDescubre();
    $rootScope.volverExploraYDescubreRutas = false;
    $scope.menuOpcion("1");
  }


  if ($rootScope.volverServicioYAtractivo == true){
    $rootScope.volverServicioYAtractivo = false;
    console.log("viene de evento");
    $scope.serviciosAtractivos();
    $scope.menuOpcion("5");
  }

  if ($rootScope.volverServicioYAtractivo2 == true){
    $rootScope.volverServicioYAtractivo2 = false;
    console.log("viene de servicio o atractivo");
    $scope.serviciosAtractivos();
  }

  if ($rootScope.enEvento == true){
    $rootScope.enEvento = false;
    //alert("viene de evento y tiene que volver");
    $scope.serviciosAtractivos();
    $scope.menuOpcion("5");
  }

  if ($rootScope.listaReservas == true){
    $rootScope.listaReservas = false;
    $scope.exploraDescubre();
    $scope.menuOpcion("8");
  }
  
  $ionicPlatform.registerBackButtonAction(function () {
    if (condition) {
      navigator.app.exitApp();
      console.log("entre?");
    } else {
      alert("HELOU");
    }
    console.log("entropadAS?");
  }, 100);

  

  /*$scope.watchRutaTipica = navigator.geolocation.watchPosition(function(position){

        console.log("posicion: ",position);

      },function(error){
        console.log("Error watchPosition: ",error);
      },{
        timeout : 30000
      });*/

  




  /*width = window.screen.height;
  var alturaMapa = width - 166;
  $scope.styleMapa = {
      "height" : "100%",
      "width"  : "100%"
  };

  document.addEventListener("deviceready", function() {
    var COPIAPO = new plugin.google.maps.LatLng(-27.3690175,-70.6756632);
      var div = document.getElementById("map_canvas");
      //console.log("iniciar mapa");
      // Initialize the map view
      $rootScope.map = plugin.google.maps.Map.getMap(div,{
          'backgroundColor' : 'white',
          'mapType'         : plugin.google.maps.MapTypeId.ROADMAP,
          'controls' : {
              'compass'          : true,
              'myLocationButton' : true,
              'indoorPicker'     : true,
              'zoom'             : false
          },
          'gestures': {
              'scroll' : true,
              'tilt'   : true,
              'rotate' : true,
              'zoom'   : true
          },
          'camera': {
              'latLng' : COPIAPO,
              'tilt'   : 0,
              'zoom'   : 10,
              'bearing': 0
          }
      });

      // Wait until the map is ready status.
      $rootScope.map.addEventListener(plugin.google.maps.event.MAP_READY, onMapReady);

  }, false);

  function onMapReady() {
    $rootScope.map.clear();
    $rootScope.map.off();
    $rootScope.map.remove();
  }

  function onBtnClicked() {
      map.showDialog();
  }*/

}]);