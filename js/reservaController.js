angular.module('starter.controllers')


.controller('reservaCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$ionicModal','$httpParamSerializer','$ionicLoading',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$ionicModal,$httpParamSerializer,$ionicLoading) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.misDatos                      = [];
  $scope.fechaSeleccionado             = "Fecha nacimiento";
  $scope.item                          = $rootScope.item_seleccionado;
  $scope.soloVer                       = false;
  $scope.monedaSeleccionada            = "Moneda";
  $scope.selectables                   = ['Peso', 'Dólar'];
  //$rootScope.item_seleccionado       = [];
  //$rootScope.item_seleccionado.id    = $rootScope.id_servicio_turistico;
  //$scope.id_reserva_servicio_turistico = null;
  console.log($rootScope.item_seleccionado);
  console.log($rootScope.id_servicio_turistico);


  $scope.item.id = [];
  $scope.item.id = $rootScope.id_servicio_turistico;


  //console.log("reservactrl");
  //console.log($rootScope.rescatarDesdeBd);
  //console.log("mapaBd: " + $scope.mapaBd);
  $scope.mapaBd = $rootScope.rescatarDesdeBd;

  $scope.cerrarModalReserva = function(){
    //$rootScope.rescatarDesdeBd = false;
    //$rootScope.verReserva      = false;
    $rootScope.fromExperiencia = false;
    ////if(ionic.Platform.isWebView())
    //if($scope.mapaBd == true){
      //$rootScope.map.setClickable(true);
    //}
        
      
    $rootScope.modalReserva.remove();


    $scope.markerArrayPatrimonioReserva = [];
    $scope.marcadorReserva = [];
    $rootScope.modalReservas.remove();
  }

  $scope.atrasReservas = function(){
    //console.log("atras: ",$rootScope);
    $rootScope.modalReserva.hide();
  }

  $scope.textoReserva = "Reservar";
  $scope.textoLaravel = "Solicitud reserva";
  if( $rootScope.rescatarDesdeBd == true){
    $scope.textoReserva = "GUARDAR";
    $scope.textoLaravel = "Modificación reserva";
    $scope.item    = [];
    $scope.item.id = [];
    $scope.item.id = $rootScope.id_servicio_turistico;

    //conseguir coordenadas id_tipo_servicio
    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/getReservaUsuario.php",
      data: $httpParamSerializer({
        "id": $rootScope.id_reserva
      }),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ 
      console.log(response.data);
      if(response.data.resultado == "ok"){
        $scope.id_moneda = response.data.reservas[0].id_moneda;
        $scope.id_reserva_servicio_turistico = response.data.reservas[0].id_reserva_servicio_turistico;

        $scope.misDatos.numHabitaciones      = response.data.reservas[0].numero_habitaciones;
        $scope.misDatos.numPersonas          = response.data.reservas[0].numero_personas;
        $scope.misDatos.adicionales          = response.data.reservas[0].solicitudes_adicionales;
        $scope.misDatos.telefono             = response.data.reservas[0].telefono_cliente;

        /*var d          = new Date(response.data.reservas[0].fecha_inicial_reserva);
        var curr_date  = d.getDate();
        var curr_month = d.getMonth() + 1; //Months are zero based
        var curr_year  = d.getFullYear();
        var fecha      = curr_date+"/"+curr_month+"/"+curr_year;

        $scope.fechaSeleccionadoEntrada = fecha;
        $scope.misDatos.fechaEntrada    = fecha;

        var d          = new Date(response.data.reservas[0].fecha_final_reserva);
        var curr_date  = d.getDate();
        var curr_month = d.getMonth() + 1; //Months are zero based
        var curr_year  = d.getFullYear();
        var fecha      = curr_date+"/"+curr_month+"/"+curr_year;*/
        
        
        $scope.fechaSeleccionadoEntrada = response.data.reservas[0].fecha_inicial_reserva;
        $scope.misDatos.fechaEntrada    = response.data.reservas[0].fecha_inicial_reserva;
        
        $scope.fechaSeleccionadoSalida  = response.data.reservas[0].fecha_final_reserva;
        $scope.misDatos.fechaSalida     = response.data.reservas[0].fecha_final_reserva;
        
        console.log($scope.misDatos);
        console.log($scope.fechaSeleccionadoEntrada);
        console.log($scope.fechaSeleccionadoSalida);

        $rootScope.item_seleccionado = [];
        $rootScope.item_seleccionado.id     = response.data.reservas[0].id_servicio_turistico;
        $rootScope.item_seleccionado.nombre = response.data.reservas[0].nombre_servicio;

        if( $scope.id_moneda == "1" ){
            $scope.monedaSeleccionada   = "Peso";
        }else if( $scope.id_moneda == "2" ){
          $scope.monedaSeleccionada   = "Dólar";
        }else{
          $scope.monedaSeleccionada   = "Moneda";
        }

        if($rootScope.verReserva == true){
          $scope.soloVer = true;
        }
      }
    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
    });
  }

  console.log($scope.item);

  //conseguir coordenadas id_tipo_servicio
  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/getTipoServicio.php",
    data: $httpParamSerializer({
      "id_servicio_turistico": $scope.item.id
    }),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ 
    console.log(response.data);
    if(response.data.resultado == "ok"){
      $scope.id_tipo_servicio = response.data.id_tipo_servicio;
    }
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });
  
  /*Obtener datos del usuario*/
  $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/misDatos.php",
      data: $httpParamSerializer({
        'id_usuario' : localStorage.getItem("id_usuario"),
        'id_agenda'  : localStorage.getItem("id_agenda"),
        'correo'     : localStorage.getItem("correo")
      }),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ //ok si guardó correctamente.
      //console.log(response.data);
      
      $scope.nacimientoBd = new Date(response.data.datos.fecha_nacimiento);
      var dateValue = $scope.nacimientoBd.getDate() + 1;
      $scope.nacimientoBd.setDate(dateValue);
  
      if( $rootScope.rescatarDesdeBd == true ){
        $rootScope.rescatarDesdeBd = false;
      }else{
        var d                           = new Date();
        var curr_date                   = d.getDate();
        var curr_month                  = d.getMonth() + 1; //Months are zero based
        var curr_year                   = d.getFullYear();
        $scope.misDatos.fechaEntrada    = curr_date+"/"+curr_month+"/"+curr_year;
        $scope.misDatos.fechaSalida     = curr_date+"/"+curr_month+"/"+curr_year;
        
        $scope.fechaSeleccionadoEntrada = $scope.misDatos.fechaEntrada;
        $scope.fechaSeleccionadoSalida  = $scope.misDatos.fechaSalida;

        $scope.fechaSeleccionadoEntradaAux = $scope.misDatos.fechaEntrada;
        $scope.fechaSeleccionadoSalidaAux  = $scope.misDatos.fechaSalida;
      }

      $scope.misDatos.correo   = localStorage.getItem("correo");/*response.data.datos.correo_usuario;*/
      $scope.misDatos.nombre   = response.data.datos.nombre;
      $scope.misDatos.materno  = response.data.datos.apellido_materno;
      $scope.misDatos.paterno  = response.data.datos.apellido_paterno;
      $scope.misDatos.nombreCompleto = $scope.misDatos.nombre + " " + $scope.misDatos.paterno + " " + $scope.misDatos.materno;


    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
  });

  $scope.openDatePicker = function(tipo){
    if($scope.soloVer){

    }else{
      var ipObj1 = {
        callback: function (val) {  //Mandatory
          console.log('Return value from the datepicker popup is : ' + val, new Date(val));
          var d                    = new Date(val)
          var curr_date            = d.getDate();
          var curr_month           = d.getMonth() + 1; //Months are zero based
          var curr_year            = d.getFullYear();
          $scope.misDatos.fechaNac = curr_date+"/"+curr_month+"/"+curr_year;
          if(tipo == 'entrada'){
            $scope.fechaSeleccionadoEntrada = $scope.misDatos.fechaNac;
            $scope.misDatos.fechaEntrada = $scope.misDatos.fechaNac;
          }else{
            $scope.fechaSeleccionadoSalida  = $scope.misDatos.fechaNac;
            $scope.misDatos.fechaSalida = $scope.misDatos.fechaNac;
          }
          console.log($scope.misDatos.fechaNac);
          console.log( $scope.misDatos.fechaSalida );
          console.log( $scope.misDatos.fechaEntrada );
        },
        inputDate: new Date(),      //Optional
        mondayFirst: true,          //Optional
        closeOnSelect: false,       //Optional
      };

      ionicDatePicker.openDatePicker(ipObj1);
    }
  };

  $scope.selectMoneda = function(nuevo,viejo){
    $scope.monedaSeleccionada = nuevo;
    
  }

  $scope.reservar = function(misDatos){
    $ionicLoading.show({
      template: 'Cargando...'
    });

    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    navigator.geolocation.getCurrentPosition(
      function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        $scope.reservarGeo(lat,long,misDatos);
      },function(err) {
        $scope.reservarGeo(0,0,misDatos);
      },posOptions
    );
  }

  $scope.reservarGeo = function(lat,long,misDatos){
    if($scope.soloVer){
        
    }else{
      console.log(misDatos);
      flagCamposLlenos = 0;
      var count        = 0;
      
      var d            = new Date()
      var curr_date    = d.getDate();
      var curr_month   = d.getMonth() + 1; //Months are zero based
      var curr_year    = d.getFullYear();
      var hoy          = curr_date+"/"+curr_month+"/"+curr_year;

      for (var p in misDatos) {
        misDatos.hasOwnProperty(p) && count++;
      }

      console.log( $scope.monedaSeleccionada );

      if( $scope.monedaSeleccionada == "Moneda" ){
        $rootScope.toast("debes seleccionar moneda","short");
        flagCamposLlenos = 1;
        $ionicLoading.hide();
      }else if( count<=8 ){
        
        $rootScope.toast("debes llenar todos los campos","short");
        flagCamposLlenos = 1;
        $ionicLoading.hide();
        /*}else if( ( misDatos.fechaEntrada > misDatos.fechaSalida ) ){
          $rootScope.toast("La fecha de entrada no puede ser posterior a la fecha de salida","short");
        }else if( misDatos.fechaEntrada < hoy){
          $rootScope.toast("La fecha de entrada no puede ser menor a la fecha actual","short");
        }else if( misDatos.fechaSalida < hoy){
          $rootScope.toast("La fecha de salida no puede ser menor a la fecha actual","short");*/
      }else{

        if( $scope.monedaSeleccionada == "Peso" ){
          $scope.monedaSeleccionadaBd   = 1;
        }else if( $scope.monedaSeleccionada == "Dólar" ){
          $scope.monedaSeleccionadaBd   = 2;
        }else{
          $scope.monedaSeleccionadaBd   = null;
        }

        $ionicLoading.show({
          template: 'Reservando...'
        }).then(function(){
           console.log("The loading indicator is now displayed");
        });

        $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/getIdTipoServicio.php",
            data: $httpParamSerializer({
              'id_servicio' : $scope.item.id
            }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
              //console.log(response.data)
              if( response.data.resultado == "ok"){
                var nombreReserva      = $scope.misDatos.nombre;
                var correoReserva      = $scope.misDatos.correo;
                var telefonoReserva    = misDatos.telefono;
                var id_servicio        = $rootScope.item_seleccionado.id;
                var nombre_servicio    = $rootScope.item_seleccionado.nombre;
                var id_tipo_servicio   = response.data.id_tipo_servicio;          
                var asuntoContacto     = $scope.misDatos.nombre;
                var nPerReserva        = misDatos.numPersonas;        
                var fechaEntReserva    = misDatos.fechaEntrada ;
                var fechaSalReserva    = misDatos.fechaSalida;
                var comentarioContacto = misDatos.adicionales;
                var nHabReserva        = misDatos.numHabitaciones;
                var reserva            = $rootScope.item_seleccionado.reserva;
                console.log("RESERVA: " + reserva);

                  $http({
                    method: "POST",
                    url: "http://200.14.68.107/atacama/public/servicioParticular/enviarReservaParticular",
                    data: $httpParamSerializer({
                      "nombreReserva"                 : nombreReserva,
                      "correoReserva"                 : correoReserva,
                      "telefonoReserva"               : telefonoReserva,
                      "id_servicio"                   : id_servicio,
                      "nombre_servicio"               : nombre_servicio,
                      "id_tipo_servicio"              : id_tipo_servicio,
                      "asuntoContacto"                : asuntoContacto,
                      "nPerReserva"                   : nPerReserva,
                      "fechaEntReserva"               : fechaEntReserva,
                      "fechaSalReserva"               : fechaSalReserva,
                      "comentarioContacto"            : comentarioContacto,
                      "nHabReserva"                   : nHabReserva,
                      "id_usuario"                    : localStorage.getItem("id_usuario"),
                      "id_reserva_servicio_turistico" : $scope.id_reserva_servicio_turistico,
                      "reserva"                       : reserva,
                      "textoLaravel"                  : $scope.textoLaravel,
                      "id_moneda"                     : $scope.monedaSeleccionadaBd,
                      "lat"                           : lat,
                      "long"                          : long,
                      "id_producto"                   : $scope.item.id_producto_servicio_turistico,
                      "nombre_producto"               : $scope.item.nombre_producto
                    }),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                  }).then(function(response){ //ok si guardó correctamente.
                    $ionicLoading.hide().then(function(){
                      console.log("The loading indicator is now hidden");
                    });
                    if(response.data.exito){
                      $rootScope.toast('Reserva realizada correctamente', 'short');
                      $scope.cerrarModalReserva();
                      $scope.misDatos.telefono        = "";
                      $scope.misDatos.numPersonas     = "";
                      $scope.misDatos.fechaEntrada    = $scope.fechaSeleccionadoEntradaAux;
                      $scope.misDatos.fechaSalida     = $scope.fechaSeleccionadoSalidaAux;
                      $scope.misDatos.adicionales     = "";
                      $scope.misDatos.numHabitaciones = "";

                    }else{
                      //$rootScope.toast('No se pudo realizar la reserva, intenta luego', 'short');
                      $rootScope.toast(response.data.error, 'short');
                    }

                  }, function(){ //Error de conexión
                      $rootScope.toast('Verifica tu conexión a internet', 'short')
                  });
              }else{
                $ionicLoading.hide();
                $rootScope.toast('No se pudo realizar la reserva, intenta luego', 'short');
              }
          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
        });
      }
    }
    
  }

}]);