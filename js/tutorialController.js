angular.module('starter.controllers')


.controller('tutorialCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','$cordovaFileTransfer','$cordovaCamera','$httpParamSerializer','$ionicLoading','$ionicPopup','$ionicModal',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,$cordovaFileTransfer,$cordovaCamera,$httpParamSerializer,$ionicLoading,$ionicPopup,$ionicModal) {
  // With the new view caching in Ionic, Controllers are only called

  altura = window.screen.height - 76;
  alturaReal = window.screen.height - 76;
  
  $scope.myStyle={
    "height" : altura+"px",
    "width"  : "100%"
  }

  $scope.estiloIonSlide={
    "height" : alturaReal+"px"
  }
  
  $scope.nuevoHome = function(){   
    $state.go("app.nuevoHome");
  }

}]);