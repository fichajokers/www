angular.module('starter.controllers')


.controller('menuAccionesModificarCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$ionicModal','$httpParamSerializer','$timeout','$sce','$cordovaCamera','$cordovaFileTransfer','$cordovaSocialSharing','$ionicPopup','$ionicLoading','$ionicNativeTransitions','$ionicPlatform', function($scope,$http,$state,$rootScope,ionicDatePicker,$ionicModal,$httpParamSerializer,$timeout,$sce,$cordovaCamera,$cordovaFileTransfer,$cordovaSocialSharing,$ionicPopup,$ionicLoading,$ionicNativeTransitions,$ionicPlatform) {
  // With the new view caching in Ionic, Controllers are only called



    $scope.id_usuario_local = localStorage.getItem("id_usuario")
    width = window.screen.width;
    console.log("width: ",width);
    console.log ("tipo_seleccionado ",$scope.tipo_seleccionado);
    /*384-767 HUAWEI*/
      if( width > 321 && width <= 360 ){
        console.log("esto es un huawei");

        $scope.tituloSP ={
          "height"            : "20px",
          "margin-right"      : "1%",
          "margin-left"       : "5%",
        }

        $scope.tituloHuella = {
          "height"            : "20px",
          "margin-top"        : "5%",
          "margin-right"      : "1%",
        };

        $scope.contenedorServicio = {
          "width"             : "100%",  
          "height"            : "60px",
          "background-color"  : "#7162AB",  
          "margin-top"        : "-5px", 
          "margin-top"        : "-2%",
        };

        $scope.contenedorItem = {
          "width"             : "100%",  
          "height"            : "60px",  
          "background-color"  : "#3A2B86",
          "margin-top"        : "-2%",
        };

        $scope.contenedorAtractivo = {
          "width"             : "100%",  
          "height"            : "60px",  
          "background-color"  : "#745A3A",   
          "margin-top"        : "-2%",
        };

        $scope.icono = {
          "width"         : "45px",
          "margin-top"    : "0%",
          "float"         : "left",
          "margin-left"   : "2%",
          "margin-right"  : "2%",
          "z-index"       : "10",
          "margin-bottom" : "3%",
          "height"        : "43px",
        };

        $scope.iconoItem = {
          'width': '50px',
          'margin': '0% 2% 3%',
          'float': 'left',
          'z-index': '10',
          'height': '50px',
        };

        $scope.noVisitado = {
          'margin-left': '-8%',
          'margin-top': '0.5%',
        };

        $scope.visitado = {
          'margin-left': '-7%',
          'margin-top': '0.5%',
        };

        $scope.visitadoItem = {
          'margin-left': '-5%',
          'margin-top': '2.5%',
        };

        $scope.noVisitadoItem = {
          'margin-left': '-7%',
          'margin-top': '2.5%',
        };

        $scope.parrafoVisitanos ={
          'margin-left': '14%',
        }
      }

      /*lenovo tablet*/
      if( width > 361 && width <= 480 ){
        console.log("esto es un talet");

        $scope.tituloSP ={
          "height"            : "20px",
          "margin-right"      : "1%",
          "margin-left"       : "5%",
        }

        $scope.tituloHuella = {
          "height"            : "20px",
          "margin-top"        : "5%",
          "margin-right"      : "1%",
        };

        $scope.contenedorServicio = {
          "width"             : "100%",  
          "height"            : "60px",
          "background-color"  : "#7162AB",  
          "margin-top"        : "-5px", 
          "margin-top"        : "-1%",
        };

        $scope.contenedorItem = {
          "width"             : "100%",  
          "height"            : "65px",  
          "background-color"  : "#3A2B86",
          "margin-top"        : "-1%",
        };

        $scope.contenedorAtractivo = {
          "width"             : "100%",  
          "height"            : "65px",  
          "background-color"  : "#745A3A",   
          "margin-top"        : "-1%",
        };

        $scope.icono = {
          "width"         : "47px",
          "margin-top"    : "0%",
          "float"         : "left",
          "margin-left"   : "2%",
          "margin-right"  : "4%",
          "z-index"       : "10",
          "margin-bottom" : "3%",
          "height"        : "47px",
        };

        $scope.iconoItem = {
          'width': '55px',
          'margin': '0% 4% 3%',
          'float': 'left',
          'z-index': '10',
          'height': '55px',
        };

        $scope.noVisitadoItem = {
          'margin-left': '-6%',
          'margin-top': '2.5%',
        };

        $scope.noVisitado = {
          'margin-left': '-8%',
          'margin-top': '1.5%',
        };

        $scope.visitado = {
          'margin-left': '-7%',
          'margin-top': '1.5%',
        };

        $scope.visitadoItem = {
          'margin-left': '-4%',
          'margin-top': '2.5%',
        };

        $scope.parrafoVisitanos ={
          'margin-left': '12%',
        }
      }
      
      /*lenovo tablet*/
      if( width > 481 && width <= 600 ){
        console.log("esto es un talet");

        $scope.tituloSP ={
          "height"            : "20px",
          "margin-right"      : "1%",
          "margin-left"       : "5%",
        }

        $scope.tituloHuella = {
          "height"            : "20px",
          "margin-top"        : "5%",
          "margin-right"      : "1%",
        };

        $scope.contenedorServicio = {
          "width"             : "100%",  
          "height"            : "60px",
          "background-color"  : "#7162AB",  
          "margin-top"        : "-5px", 
          "margin-top"        : "-1%",
        };

        $scope.contenedorItem = {
          "width"             : "100%",  
          "height"            : "70px",  
          "background-color"  : "#3A2B86",
          "margin-top"        : "-1%",
        };

        $scope.contenedorAtractivo = {
          "width"             : "100%",  
          "height"            : "70px",  
          "background-color"  : "#745A3A",
          "margin-top"        : "-5px",   
          "margin-top"        : "-1%",
        };

        $scope.icono = {
          "width"         : "47px",
          "margin-top"    : "0%",
          "float"         : "left",
          "margin-left"   : "4%",
          "margin-right"  : "4%",
          "z-index"       : "10",
          "margin-bottom" : "3%",
          "height"        : "47px",
        };

        $scope.iconoItem = {
          'width': '60px',
          'margin': '0% 5% 6%',
          'float': 'left',
          'z-index': '10',
          'height': '60px',
        };

        $scope.visitadoItem = {
          'margin-left': '-3%',
          'margin-top': '3.5%',
        };

        $scope.noVisitadoItem = {
          'margin-left': '-4.5%',
          'margin-top': '3.5%',
        };

        $scope.noVisitado = {
          'margin-left': '-6%',
          'margin-top': '1.5%',
        };

        $scope.visitado = {
          'margin-left': '-5%',
          'margin-top': '1.5%',
        };

        $scope.parrafoVisitanos ={
          'margin-left': '9%',
        }
      }

      /*tablet blanca*/
      if( width == 768 ){
        console.log("esto es un talet blanco");

        $scope.tituloSP ={
          "height"            : "20px",
          "margin-right"      : "1%",
          "margin-left"       : "1%",
        }

        $scope.tituloHuella = {
          "height"            : "20px",
          "margin-top"        : "2%",
          "margin-right"      : "1%",
        };

        $scope.contenedorServicio = {
          "width"             : "100%",  
          "height"            : "60px",
          "background-color"  : "#7162AB",  
          "margin-top"        : "-5px", 
          "margin-top"        : "-1%",
        };

        $scope.contenedorItem = {
          "width"             : "100%",  
          "height"            : "70px",  
          "background-color"  : "#3A2B86",
          "margin-top"        : "-1%",
        };

        $scope.contenedorAtractivo = {
          "width"             : "100%",  
          "height"            : "70px",  
          "background-color"  : "#745A3A",
          "margin-top"        : "-5px",   
          "margin-top"        : "-1%",
        };

        $scope.icono = {
          "width"         : "47px",
          "margin-top"    : "0%",
          "float"         : "left",
          "margin-left"   : "4%",
          "margin-right"  : "4%",
          "z-index"       : "10",
          "margin-bottom" : "3%",
          "height"        : "47px",
        };

        $scope.iconoItem = {
          'width': '60px',
          'margin': '0% 5% 6%',
          'float': 'left',
          'z-index': '10',
          'height': '60px',
        };

        $scope.visitadoItem = {
          'margin-left': '-2.5%',
          'margin-top': '2.5%',
        };

        $scope.noVisitadoItem = {
          'margin-left': '-3.5%',
          'margin-top': '2.5%',
        };

        $scope.noVisitado = {
          'margin-left': '-5%',
          'margin-top': '1%',
        };

        $scope.visitado = {
          'margin-left': '-4.5%',
          'margin-top': '1%',
        };

        $scope.parrafoVisitanos ={
          'margin-left': '9%',
        };
      }

    console.log($rootScope.item_seleccionado);

    $rootScope.id_servicio_turistico = $rootScope.item_seleccionado.id;

    console.log("rootScope: ",$rootScope);
    //console.log($rootScope.tipo_seleccionado);
    $scope.esAndroid = false;
    if( ionic.Platform.isAndroid() ){
      $scope.esAndroid = true;
    }else{
      $scope.esAndroid = false;
    }


    $scope.menuAccionesContent = {
      "margin-bottom" : "50px"
    }

    $scope.flechaDown = false;
    $scope.noEdita    = true;
    $scope.sernatur   = [];
    $scope.calificacionesOriginal = [];

    $scope.anchoPantalla       = window.screen.width;
    $scope.pantallaGrande      = false;
    $scope.pantallaChica       = true;
    $scope.noEdita             = true;
    $scope.tieneWeb            = false;

    if( $scope.anchoPantalla >= 768){
      $scope.pantallaGrande  = true;
      $scope.pantallaChica   = false;
    }

    $scope.item2 = $rootScope.item_seleccionado;

    angular.forEach($scope.item2.correos, function(value, key) {
      if( value.tipo_contacto == 'web' )
        $scope.tieneWeb = true;
    });

    //modificar elemento clasificacion servicio...
    if(window.screen.width >=768){
      $scope.estiloTablet={
        "position"    : "absolute",
        "color"       : "#444444",
        "text-align"  : "center",
        "margin-left" : "60PX",
        "margin-top"  : "-34px"
      };
    }else{
      $scope.estiloTablet={
        "color"       : "#444444",
        "text-align"  : "center"
      };
    }
    // el id_item_seleccionado es el cual lo utilizaremos para mostrar la informacion del item
    $scope.item = [];
    console.log( "menu acciones modificar" );
    $scope.tieneImagen = true;

    //asignar a $scope.item el item seleccionado...


    if( $rootScope.tipo_seleccionado == "item" ){
      console.log( $rootScope.item_seleccionado );
      var url="http://200.14.68.107/atacamaGoMultimedia/";
      $scope.item = $rootScope.item_seleccionado;

      $scope.topMenuAcciones = "topMenuAcciones_50";
      
      if( $scope.item.foto == "logoFic.png" || $scope.item.foto == "" || $scope.item.foto == null ){
          $scope.tieneImagen = false;
        }
        
      if( $scope.item.foto )
        $scope.foto = url+$scope.item.foto;

      console.log($rootScope.item_seleccionado.id_usuario, localStorage.getItem("id_usuario") );
      if($rootScope.item_seleccionado.id_usuario == localStorage.getItem("id_usuario")){
        console.log("EDITAR");
        $scope.noEdita = false;
      }

      //almacenar geolocalizacion y usuario
      $scope.watchPosicionTurista = navigator.geolocation.watchPosition(function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        
        console.log(lat,long);
        console.log($scope);
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addRegistroGeolocalizacionItem.php",
          data: $httpParamSerializer({
            "id_item"    : $scope.item.id,
            "id_usuario" : localStorage.getItem("id_usuario"),
            "latitud"    : lat,
            "longitud"   : long
            
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si recato bien los datos
            console.log(response.data);
              navigator.geolocation.clearWatch($scope.watchPosicionTurista);
              $scope.watchRutaDinamica = null;
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });

      },function(error){
        console.log(error);
      },{
        timeout : 30000
      });

        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getCalificacionItemTuristico.php",
          data: $httpParamSerializer({
            "id_item_turistico": $scope.item.id,
            "id_usuario"       : localStorage.getItem("id_usuario")

        }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.

          if(response.data.resultado == "ok"){
            $scope.calificaciones         = response.data.calificaciones;
            
            angular.forEach(response.data.calificaciones, function(value, key) {
              $scope.calificacionesOriginal[key] = value.puntaje_usuario              
            });

          }else if(response.data.resultado == "no data"){
            $scope.calificaciones         = response.data.calificaciones;
            
            angular.forEach(response.data.calificaciones, function(value, key) {
              $scope.calificacionesOriginal[key] = value.puntaje_usuario              
            });

          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short')
        });

    }

    if( $rootScope.tipo_seleccionado == "servicio" ){
      var myEl = angular.element( document.querySelector( '#ion-content' ) );
      myEl.addClass('has-subheader');

      var url="http://200.14.68.107/atacamaGoMultimedia/";
      $scope.item = $rootScope.item_seleccionado;

      $scope.topMenuAcciones = "topMenuAcciones_100";
      
      console.log($scope.item.sernatur, $scope.item.registro_vigente)
      if ($scope.item.sernatur == true && $scope.item.registro_vigente == true){
        $scope.sernatur.foto = "img/ticket.png";
        $scope.sernatur.texto = "Servicio vigente registrado en SERNATUR";
      }

      if ($scope.item.sernatur == false){
        $scope.sernatur.foto = "img/no_existe.png";
        $scope.sernatur.texto = "Servicio no registrado en SERNATUR";
      }

      if ($scope.item.sernatur == true && $scope.item.registro_vigente == false){
        $scope.sernatur.foto = "img/pendiente.png";
        $scope.sernatur.texto = "Servicio no vigente registrado en SERNATUR";
      }


      if( $scope.item.foto == "logoFic.png" || $scope.item.foto == "" || $scope.item.foto == null ){
          $scope.tieneImagen = false;
        }
        
      if( $scope.item.foto )
        $scope.foto = url+$scope.item.foto;

      //almacenar geolocalizacion y usuario
      $scope.watchPosicionTurista = navigator.geolocation.watchPosition(function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        
        /*console.log(lat,long);
        
        console.log($scope);*/


        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addRegistroGeolocalizacionServicio.php",
          data: $httpParamSerializer({
            "id_item"    : $scope.item.id,
            "id_usuario" : localStorage.getItem("id_usuario"),
            "latitud"    : lat,
            "longitud"   : long
            
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si recato bien los datos
            console.log(response.data);
              navigator.geolocation.clearWatch($scope.watchPosicionTurista);
              $scope.watchRutaDinamica = null;
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });

      },function(error){
        console.log(error);
      },{
        timeout : 30000
      });

    }

    if( $rootScope.tipo_seleccionado == "patrimonio" ){

      //Se saco la imagen del logo, porque el patrimonio no posee un logo ?????
      $scope.tieneImagen = false;
      //Hasta aqui
      
      var myEl = angular.element( document.querySelector( '#ion-content' ) );
      myEl.addClass('has-subheader');

      var url="http://200.14.68.107/atacamaGoMultimedia/";
      $scope.item = $rootScope.item_seleccionado;
      //console.log($rootScope.item_seleccionado.galeria);

      $scope.topMenuAcciones = "topMenuAcciones_100";
      
      if( $scope.item.foto == "logoFic.png" || $scope.item.foto == "" || $scope.item.foto == null ){
          $scope.tieneImagen = false;
        }
        
      if( $scope.item.foto )
        $scope.foto = url+$scope.item.foto;

      //almacenar geolocalizacion y usuario
      $scope.watchPosicionTurista = navigator.geolocation.watchPosition(function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        
        console.log(lat,long);
        console.log($scope);
        
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addRegistroGeolocalizacionPatrimonio.php",
          data: $httpParamSerializer({
            "id_item"    : $scope.item.id,
            "id_usuario" : localStorage.getItem("id_usuario"),
            "latitud"    : lat,
            "longitud"   : long,
            "galeria"    : $rootScope.item_seleccionado.galeria
            
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si recato bien los datos
            console.log(response.data);
              navigator.geolocation.clearWatch($scope.watchPosicionTurista);
              $scope.watchRutaDinamica = null;
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });

      },function(error){
        console.log(error);
      },{
        timeout : 30000
      });
    }
    
    
    /*$timeout( function(){
      var altura = document.getElementById("texto").offsetHeight;
      altura = altura + document.getElementById("iconos").offsetHeight;
      altura = altura + document.getElementById("idImagen").offsetHeight;

      console.log(altura);
      if( window.screen.width >=768 ){ //Tablet
        if( altura >= 846 ){ //370
          $scope.flechaDown = true;
          document.getElementById("animationFlecha").classList.add("bounce");
          document.getElementById("animationFlecha").classList.add("animated");

          $timeout( function(){
            $scope.flechaDown = false;
          },2000);
        }
      }else{
        if( altura >= 370 ){ //370 //Celular
          $scope.flechaDown = true;
          document.getElementById("animationFlecha").classList.add("bounce");
          document.getElementById("animationFlecha").classList.add("animated");

          $timeout( function(){
            $scope.flechaDown = false;
          },2000);
        }
      }

    }, 500);*/
    // if ($rootScope.cerrarItemHuella == true){
    //   console.log("se cerro modal automaticamente");
    //   $rootScope.cerrarItemHuella = false;
    //   $scope.cerrarModalMenuAccionesModificar();
    // }

    $scope.cerrarModalMenuAccionesModificar = function(){
      console.log("cerrar modal");
      $rootScope.imagen_deja_tu_huella = false; 
      //if( ionic.Platform.isAndroid() )
      $rootScope.map.setClickable(true);
      //elimino el modal, para que se recargue la próxima vez que se llame...
      $rootScope.modalMenuAccionesModificar.remove();
    }

    $scope.cerrarModalMenuAccionesModificarEyD = function(){
      console.log("cerrarModalMenuAccionesModificarEyD");
      $rootScope.imagen_deja_tu_huella = false; 
      //if( ionic.Platform.isAndroid() )
      $rootScope.map.setClickable(true);
      //elimino el modal, para que se recargue la próxima vez que se llame...
      $rootScope.modalMenuAccionesModificar.remove();

      $rootScope.volverExploraYDescubre = true;

      $rootScope.map.clear();
      $rootScope.map.off();
      $rootScope.map.remove();

      $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
          "type": "slide",
          "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
          "duration": 400, // in milliseconds (ms), default 400
      });
    }

    $scope.openModalModificarDescripcion = function(){

      //Modal Modificar Descripcion...
      $ionicModal.fromTemplateUrl('templates/modificarDescripcion.html', {
        scope: $rootScope,
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalModificarDescripcion = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);
        
        $rootScope.modalModificarDescripcion.show();

      });
  
    } 

    $scope.openModalCompartir = function(){
      //Modal Modificar Descripcion...
      $ionicModal.fromTemplateUrl('templates/compartir.html', {
        scope: $rootScope,
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalModificarCompartir = modal;
        
        $rootScope.modalModificarCompartir.show();

      });
    }

    $scope.openModalModificarImagen = function(){

      //Modal Modificar Imagen...
      $ionicModal.fromTemplateUrl('templates/modificarImagen.html', {
        scope: $rootScope,
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalModificarImagen = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        
        $rootScope.modalModificarImagen.show();

      });

    }


    $scope.openModalModificarVideo = function(){

      //Modal Modificar Video...
      $ionicModal.fromTemplateUrl('templates/modificarVideo.html', {
        scope: $rootScope,
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalModificarVideo = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        
        $rootScope.modalModificarVideo.show();

      });

    }

    $scope.openModalModificarAudio = function(){

      //Modal Modificar Audio...
      $ionicModal.fromTemplateUrl('templates/modificarAudio.html', {
        scope: $rootScope,
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalModificarAudio = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        
        $rootScope.modalModificarAudio.show();

      });

    }

    $scope.openModalModificarComentario = function(){

      //Modal Modificar Comentario...
      $ionicModal.fromTemplateUrl('templates/modificarComentario.html', {
        scope: $rootScope,
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalModificarComentario = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        
        $rootScope.modalModificarComentario.show();

      });

    }
    
    $scope.openModalModificarCalificar = function(){

      //Modal Modificar Calificar...
      $ionicModal.fromTemplateUrl('templates/modificarCalificar.html', {
        scope: $rootScope,
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalModificarCalificar = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        
        $rootScope.modalModificarCalificar.show();

      });

    }

    $scope.openModalDenunciar = function(){

      //Modal Denunciar...
      $ionicModal.fromTemplateUrl('templates/denunciar.html', {
        scope: $rootScope,
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalDenunciar = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        
        $rootScope.modalDenunciar.show();

      });

    }

    //para transformar el texto a html, ya que no pasaba la etiqueta style=".."
    $scope.trustAsHtml = function(string) {
      return $sce.trustAsHtml(string);
    };

    //----------------Subir imagenes-------------------//
    $scope.takePhoto = function () {
        var options = {
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.CAMERA,
            mediaType       : Camera.MediaType.PICTURE,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            saveToPhotoAlbum: false
        };
   
        $cordovaCamera.getPicture().then(function (imageData) {
            console.log(imageData);
            imageData = imageData.replace(/^(.+?\.(png|jpe?g)).*$/i, '$1');
            console.log(imageData);
            var image           = document.getElementById('imagenSacadaFoto');
            image.style.display = 'block';
            image.src           = imageData;
            $scope.rutaImagen   = imageData;
            $scope.tieneSrc     = true;
            $scope.nombreImagen = Math.floor((Math.random() * 9999999) + 1)+"_"+imageData.substr(imageData.lastIndexOf('/') + 1);
        }, function (err) {
            console.log(err);
            // An error occured. Show a message to the user
        });
    }
                
    $scope.choosePhoto = function () {
        var options = {
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType      : Camera.PictureSourceType.PHOTOLIBRARY,
            mediaType       : Camera.MediaType.PICTURE,
            correctOrientation : true
        };
   
        $cordovaCamera.getPicture(options).then(function (imageData) {
            console.log(imageData);
            imageData = imageData.replace(/^(.+?\.(png|jpe?g)).*$/i, '$1');
            console.log(imageData);
            var image = document.getElementById('imagenSacadaFoto');
            image.style.display   = 'block';
            image.src             = imageData;
            $scope.rutaImagen     = imageData;
            $scope.tieneSrc = true;
            $scope.nombreImagen   = Math.floor((Math.random() * 9999999) + 1)+"_"+imageData.substr(imageData.lastIndexOf('/') + 1);
        }, function (err) {
            //alert(err);
            // An error occured. Show a message to the user
        });
    }

    $scope.actualizarImgPrincipal = function(){
        var options = {
          fileKey: "fotoItem",
          fileName: $scope.nombreImagen,
          chunkedMode: false
        };

        if( $rootScope.tipo_seleccionado == "item" ){
            $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirImagenItem.php", $scope.rutaImagen, options).then(function(result) {
              $rootScope.toast('Imagen agregada', 'short');
            }, function(err) {
              $rootScope.toast('Error agregando imagen', 'short');
              console.log(err);
            }, function (progress) {
                // constant progress updates
            });

            $http({
              method: "POST",
              url: "http://200.14.68.107/atacamaGo/editarImagenPrincipalItemTuristico.php",
              data: $httpParamSerializer({
                "id_item_turistico"   : $rootScope.item_seleccionado.id,
                "id_usuario"          : localStorage.getItem("id_usuario"),
                "nombre_archivo_item" : $scope.nombreImagen,
                "tipo_archivo_item"   : 2
              }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
              console.log(response.data);
              if(response.data.resultado == "ok"){
                $scope.foto = $scope.rutaImagen;
                $rootScope.item_seleccionado.foto = "files/item/"+$scope.nombreImagen;
                $rootScope.toast("imagen agregada.","short");
              }else
                $rootScope.toast("Ha ocurrido un error intenta nuevamente","short");
              
            }, function(){ //Error de conexión
              $rootScope.toast('Verifica tu conexión a internet',"short");
            });
        }
        
    }

    $scope.compartirFacebook = function(tipo){
        /*$ionicLoading.show({
            template: 'Cargando...'
        });*/
        
         var foto = "";
        if($scope.foto){
          var foto = $scope.foto.split('/').pop();  
        }
        var nombreServicio = $scope.item.nombre;
        //console.log($scope.item);
        //console.log($scope.item.comuna);
        var comuna = $scope.item.comuna;
        //console.log("TIPO: " + tipo);

        var link = "";

        if(tipo == 'servicio'){
          $rootScope.linkFacebook = encodeURI("http://www.atacama-go.cl/servicio/inicio/" + $scope.item.id + "/" + foto + "/" + $scope.item.nombre_tipo_servicio + "/" + nombreServicio);
            
        }else{
          $rootScope.linkFacebook = encodeURI("http://www.atacama-go.cl/atractivo/inicio/" + $scope.item.id + "/" + foto + "/" + comuna + "/" + nombreServicio);
        }

        console.log( $rootScope.linkFacebook );

        /*console.log("compartirFacebook",link);
        $ionicLoading.hide();
        $cordovaSocialSharing.shareViaFacebook(link,null,link)
        .then(function(result) {
            console.log("ok",result);
        }, function(err) {
            $ionicLoading.hide();
            console.log("error",err);
            $ionicPopup.alert({
              title: 'Aviso!',
              template: 'Debes tener la aplicación de facebook para compartir la imagen',
              okText: 'Aceptar', // String (default: 'OK'). The text of the OK button.
              okType: 'btn-morado', // String (default: 'button-positive'). The type of the OK button.
            });
        });*/
    }

    $scope.compartirTwitter  = function(tipo){
        /*$ionicLoading.show({
            template: 'Cargando...'
        });*/

         var foto = "";
        if($scope.foto){
          var foto = $scope.foto.split('/').pop();  
        }
        var nombreServicio = $scope.item.nombre;
        var link = "";
        if(tipo == 'servicio'){
          $rootScope.linkTwitter = encodeURI("http://www.atacama-go.cl/servicio/inicio/" + $scope.item.id + "/" + foto + "/" + $scope.item.nombre_tipo_servicio + "/" + nombreServicio);
        }else{
          $rootScope.linkTwitter = encodeURI("http://www.atacama-go.cl/atractivo/inicio/" + $scope.item.id + "/" + foto + "/" + $scope.item.comuna + "/" + nombreServicio);
        }
        /*console.log("compartirTwitter", link);
        $ionicLoading.hide();
        $cordovaSocialSharing.shareViaTwitter("www.atacama-go.cl", null, link)
        .then(function(result) {
            $ionicLoading.hide();
            console.log("ok",result);
        }, function(err) {
            $ionicPopup.alert({
              title: 'Aviso!',
              template: 'Debes tener la aplicación de twitter para compartir la imagen',
              okText: 'Aceptar', // String (default: 'OK'). The text of the OK button.
              okType: 'btn-morado', // String (default: 'button-positive'). The type of the OK button.
            });
        });*/

        /*$cordovaSocialSharing.canShareVia("twitter", "This is your subject", "www/imagefile.png", "https://www.thepolyglotdeveloper.com").then(function(result) {
            $cordovaSocialSharing.shareViaTwitter("This is your message", "This is your subject", "www/imagefile.png", "https://www.thepolyglotdeveloper.com");
        }, function(error) {
            alert("Cannot share on Twitter");
        });*/
    }

    /*$ionicPlatform.registerBackButtonAction(function (event) {
      event.preventDefault();
    }, 100);*/

    $scope.openModalReserva = function(){

      //Modal menu Acciones Modiciar...
      $ionicModal.fromTemplateUrl('templates/reserva.html', {
        scope: $rootScope,
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalReserva = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
        //  $rootScope.map.setClickable(false);

        $rootScope.modalReserva.show();
      
      });
    }

    $scope.mailtoCorreo = function(link) {
      window.open(`mailto:${link}`, '_system');
    }

    $scope.hrefweb = function(link){
      window.open(link,'_system');
    }

    if( $scope.tipo_seleccionado == 'servicio' ){
      $scope.compartirFacebook('servicio');
      $scope.compartirTwitter('servicio');
    }else{
      $scope.compartirFacebook('patrimonio');
      $scope.compartirTwitter('patrimonio');
    }

    $scope.addItemVisitado = function(){

      var myPopup = $ionicPopup.show({
        template: '<p CLASS="fuenteRoboto fuenteModal">¿Marcar como visitado?.</p>',
        title: 'Aviso',
        scope: $scope,
        buttons: [
          { 
              text: '<i class="icon ion-close-round"></i>',
              type:'popclose',
                onTap: function(e) {

                }
          },
          {
            text: '<b>Aceptar</b>',
            type: 'button-positive asd',
            onTap: function(e) {
              $scope.entraUnaVez = 0;
              $scope.watchPosicionTurista2 = navigator.geolocation.watchPosition(function(position){
                if( $scope.entraUnaVez == 0 ){
                  var lat  = position.coords.latitude;
                  var long = position.coords.longitude;
                  
                  console.log(lat,long);
                  
                  $http({
                    method: "POST",
                    url: "http://200.14.68.107/atacamaGo/addItemVisitado.php",
                    data: $httpParamSerializer({
                      "id_item"    : $scope.item.id,
                      "id_usuario" : localStorage.getItem("id_usuario"),
                      "latitud"    : lat,
                      "longitud"   : long
                      
                    }),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                  }).then(function(response){ //ok si recato bien los datos
                      $scope.entraUnaVez = 1;
                      console.log(response.data);
                      $scope.item.visitado = true;
                      navigator.geolocation.clearWatch($scope.watchPosicionTurista2);
                      $scope.watchRutaDinamica = null;
                  }, function(){ //Error de conexión
                    $rootScope.toast('Verifica tu conexión a internet', 'short');
                  });
                }
                
              },function(error){
                console.log(error);
                $rootScope.toast('No se puede obtener tu ubicación', 'short');
              },{
                timeout : 30000
              });
            }
          }
        ]
      });

    }

    $scope.addAtractivoVisitado = function(){
      var myPopup = $ionicPopup.show({
        template: '<p CLASS="fuenteRoboto fuenteModal">¿Marcar como visitado?.</p>',
        title: 'Aviso',
        scope: $scope,
        buttons: [
          { 
              text: '<i class="icon ion-close-round"></i>',
              type:'popclose',
                onTap: function(e) {

                }
          },
          {
            text: '<b>Aceptar</b>',
            type: 'button-positive asd',
            onTap: function(e) {
              $scope.entraUnaVez = 0;
              $scope.watchPosicionTurista2 = navigator.geolocation.watchPosition(function(position){
                if( $scope.entraUnaVez == 0 ){
                  var lat  = position.coords.latitude;
                  var long = position.coords.longitude;
                  
                  console.log(lat,long);
                  
                  $http({
                    method: "POST",
                    url: "http://200.14.68.107/atacamaGo/addPatrimonioVisitado.php",
                    data: $httpParamSerializer({
                      "id_patrimonio" : $scope.item.id,
                      "id_usuario"    : localStorage.getItem("id_usuario"),
                      "latitud"       : lat,
                      "longitud"      : long
                      
                    }),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                  }).then(function(response){ //ok si recato bien los datos
                      $scope.entraUnaVez = 1;
                      console.log(response.data);
                      $scope.item.visitado = true;
                      navigator.geolocation.clearWatch($scope.watchPosicionTurista2);
                      $scope.watchRutaDinamica = null;
                  }, function(){ //Error de conexión
                    $rootScope.toast('Verifica tu conexión a internet', 'short');
                    navigator.geolocation.clearWatch($scope.watchPosicionTurista2);
                  });
                }
                
              },function(error){
                console.log(error);
                $rootScope.toast('No se puede obtener tu ubicación', 'short');
              },{
                timeout : 30000
              });
            }
          }
        ]
      });
    }

    $scope.addServicioVisitado = function(){
      var myPopup = $ionicPopup.show({
        template: '<p CLASS="fuenteRoboto fuenteModal">¿Marcar como visitado?.</p>',
        title: 'Aviso',
        scope: $scope,
        buttons: [
          { 
              text: '<i class="icon ion-close-round"></i>',
              type:'popclose',
                onTap: function(e) {

                }
          },
          {
            text: '<b>Aceptar</b>',
            type: 'button-positive asd',
            onTap: function(e) {
              $scope.entraUnaVez = 0;
              $scope.watchPosicionTurista2 = navigator.geolocation.watchPosition(function(position){
                if( $scope.entraUnaVez == 0 ){
                  var lat  = position.coords.latitude;
                  var long = position.coords.longitude;
                  
                  console.log(lat,long);
                  
                  $http({
                    method: "POST",
                    url: "http://200.14.68.107/atacamaGo/addServicioVisitado.php",
                    data: $httpParamSerializer({
                      "id_servicio" : $scope.item.id,
                      "id_usuario"  : localStorage.getItem("id_usuario"),
                      "latitud"     : lat,
                      "longitud"    : long
                      
                    }),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                  }).then(function(response){ //ok si recato bien los datos
                      $scope.entraUnaVez = 1;
                      console.log(response.data);
                      $scope.item.visitado = true;
                      navigator.geolocation.clearWatch($scope.watchPosicionTurista2);
                      $scope.watchRutaDinamica = null;
                  }, function(){ //Error de conexión
                    $rootScope.toast('Verifica tu conexión a internet', 'short');
                    navigator.geolocation.clearWatch($scope.watchPosicionTurista2);
                  });
                }
                
              },function(error){
                $rootScope.toast('No se puede obtener tu ubicación', 'short');
                console.log(error);
              },{
                timeout : 30000
              });
            }
          }
        ]
      });
    }
    
    $scope.eliminarItem = function(){
      //alert($rootScope.item_seleccionado.id);

      $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/deleteItem.php",
        data: $httpParamSerializer({
          "id_item_turistico" : $rootScope.item_seleccionado.id,
          
        }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si recato bien los datos
          console.log(response.data);
          if( response.data.resultado == "ok"){
            $rootScope.toast('Huella Eliminada Correctamente', 'short');
            $scope.cerrarModalMenuAccionesModificar();
            $rootScope.verMisHuellasFromEliminar();
          }
      }, function(){ //Error de conexión
        $rootScope.toast('Verifica tu conexión a internet', 'short');
        navigator.geolocation.clearWatch($scope.watchPosicionTurista2);
      });
    }

}]);