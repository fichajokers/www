angular.module('starter.controllers')


.controller('agregarCalificarCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$cordovaCapture','ratingConfig','$httpParamSerializer','$ionicLoading','$ionicModal','$ionicNativeTransitions',function($scope,$http,$state,$rootScope,ionicDatePicker,$cordovaCapture,ratingConfig,$httpParamSerializer,$ionicLoading,$ionicModal,$ionicNativeTransitions) {
  // With the new view caching in Ionic, Controllers are only called
  
  $scope.cerrarModalAgregarCalificar = function(){

    $rootScope.map.clear();
    $rootScope.map.off();
    $rootScope.map.remove();
    $rootScope.modalMenuAcciones.remove();
    $rootScope.modalAgregarCalificar.remove();
    $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
        "type": "slide",
        "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
        "duration": 400, // in milliseconds (ms), default 400
    });
  }

  $scope.atrasModalAgregarCalificar = function(){   
    $rootScope.modalAgregarCalificar.remove();
  }

  $scope.calificacionesAntiguo = [];

  $scope.ratingSeguridad = {};
  $scope.ratingSeguridad.rate = 0;
  $scope.ratingSeguridad.max = 5;

  $scope.ratingAtractivo = {};
  $scope.ratingAtractivo.rate = 0;
  $scope.ratingAtractivo.max = 5;

  $scope.ratingHigiene = {};
  $scope.ratingHigiene.rate = 0;
  $scope.ratingHigiene.max = 5;

  $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getCalificacionItemTuristico.php",
          data: $httpParamSerializer({
            "id_item_turistico": $rootScope.id_item_turistico,
            "id_usuario"       : localStorage.getItem("id_usuario")

        }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.

          if(response.data.resultado == "ok"){
            $scope.calificaciones = response.data.calificaciones;
          }else if(response.data.resultado == "no data"){
            $scope.calificaciones = response.data.calificaciones;
          }else{
            $rootScope.toast('error, intenta nuevamente', 'short');
          }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short')
  });


  $scope.addCalificacionItem = function(seguridad, atractivo, higiene){
    console.log(seguridad, atractivo, higiene);

    var contador = 0;
    if($scope.calificacionesAntiguo.length == 0){
      angular.forEach($scope.calificaciones, function(value, key) {
        $scope.calificacionesAntiguo[key] = value.puntaje_usuario              
      });
    }else{
      angular.forEach($scope.calificaciones, function(value, key) {
        if( $scope.calificacionesAntiguo[key] == $scope.calificaciones[key].puntaje_usuario ){
          console.log("Hola");
          contador++;
        }
      });  
    }
    
    if(contador == 3){
      $rootScope.toast('Realiza una valoración antes de guardar', 'short');
      angular.forEach($scope.calificaciones, function(value, key) {
        $scope.calificacionesAntiguo[key] = value.puntaje_usuario              
      });

    }else{
      $ionicLoading.show({
        template: 'Cargando...'
      });

      $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/addCalificacionItemTuristico.php",
        data: $httpParamSerializer({
          "id_item_turistico" : $rootScope.id_item_turistico,
          "id_usuario"        : localStorage.getItem("id_usuario"),
          "calificaciones"    : JSON.stringify($scope.calificaciones)
        }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si guardó correctamente.
        console.log(response.data);
        if(response.data.resultado == "ok"){
          $rootScope.toast('Calificacion agregada', 'short');
        }else{
          $rootScope.toast('error, intenta nuevamente', 'short');
        }
        $ionicLoading.hide();
      }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
          $ionicLoading.hide();
      });

      angular.forEach($scope.calificaciones, function(value, key) {
        $scope.calificacionesAntiguo[key] = value.puntaje_usuario              
      });
    }
    
  }

  $scope.openModalSubirImagen = function(){
      $rootScope.modalAgregarCalificar.remove();
      //Modal subirImagen...
      $ionicModal.fromTemplateUrl('templates/subirImagen.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalSubirImagen = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $rootScope.modalAgregarCalificar.remove();
        $rootScope.modalSubirImagen.show();
      });

    }

    $scope.openModalAgregarVideo = function(){

      //Modal Agregar Video...
      $ionicModal.fromTemplateUrl('templates/agregarVideo.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarVideo = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $rootScope.modalAgregarCalificar.remove();
        $rootScope.modalAgregarVideo.show();
      });
        
    }

    $scope.openModalAgregarAudio = function(){

      //Modal Agregar Audio...
      $ionicModal.fromTemplateUrl('templates/agregarAudio.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarAudio = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $rootScope.modalAgregarCalificar.remove();
        $rootScope.modalAgregarAudio.show();
      });
  
    }

    $scope.openModalAgregarDescripcion = function(){
      $rootScope.modalAgregarCalificar.remove();
      //Modal Agregar descripcion...
      /*$ionicModal.fromTemplateUrl('templates/agregarDescripcion.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarDescripcion = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $scope.cerrarModalAgregarCalificar();
        $rootScope.modalAgregarDescripcion.show();
      });*/


    }

    $scope.openModalAgregarComentario = function(){

      //Modal Agregar Comentario...
      $ionicModal.fromTemplateUrl('templates/agregarComentario.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarComentario = modal;
        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $rootScope.modalAgregarCalificar.remove();
        $rootScope.modalAgregarComentario.show();
      });
    }

    $scope.openModalAgregarCalificar = function(){
      /*$rootScope.modalAgregarCalificar.remove();
      //Modal Agregar Calificar...
      $ionicModal.fromTemplateUrl('templates/agregarCalificar.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarCalificar = modal;
        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        $scope.cerrarModalAgregarCalificar();
        $rootScope.modalAgregarCalificar.show();
      });*/

  
    }

}]);