angular.module('starter.controllers')



.controller('saberMasCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaGeolocation','$ionicModal','ratingConfig','$ionicLoading','$ionicPopup','$sce','$cordovaFileTransfer','$timeout','$ionicNativeTransitions',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaGeolocation,$ionicModal,ratingConfig,$ionicLoading,$ionicPopup, $sce,$cordovaFileTransfer,$timeout, $ionicNativeTransitions) {

    console.log("saber mas controller")
    console.log($rootScope.rutaSeleccionada);
    console.log($rootScope);
    console.log($scope.rutasOffline);
   // console.log(localStorage.getItem('rutasOffline'));
    $scope.ruta = $rootScope.rutaSeleccionada;
    $scope.estado_ruta = "";
    $scope.tieneRutaContinuar = false;
    $scope.serviciosMarkerOffline = "";
    $scope.calificacion_promedio = $scope.ruta.calificacion_promedio;
    $scope.ruta.calificacion_usuario_orig = $scope.ruta.calificacion_usuario;

    console.log("galeria : ",$scope.ruta.galeria);

    //SABER SI ESTA DESCARGARDA LA RUTA

    if(localStorage.getItem('rutasOffline') == 'undefined' || localStorage.getItem('rutasOffline') == null){
      console.log("undefined");
    }
    else{
      
      $scope.rutasOffline = JSON.parse(localStorage.getItem('rutasOffline'));
      console.log($scope.rutasOffline);

    }
    
    $scope.descargar=true;
    $scope.eliminar=false;
    $scope.actualziar=false;

    angular.forEach($scope.rutasOffline, function(value, key) {    

        if( $scope.ruta.id_ruta == $scope.rutasOffline[key].ruta.id_ruta){
          $scope.descargar=false;
          $scope.eliminar=true;
          $scope.actualziar=true;
        }
    });
    
    if($scope.ruta.estado_ruta == null){
      console.log("estado_ruta: null");
      $scope.estado_ruta = "";
      $scope.tieneRutaContinuar = true;
    }

    if($scope.ruta.estado_ruta == "en curso"){
      console.log("estado_ruta: en curso");
      $scope.estado_ruta = "CONTINUAR RUTA";
      $scope.tieneRutaContinuar = true;
    }

    if($scope.ruta.estado_ruta == "completa"){
      console.log("estado_ruta: completa");
      $scope.estado_ruta = "CONTINUAR RUTA";
      $scope.tieneRutaContinuar = true;
    }

    //ver si esta cerca
    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    navigator.geolocation.getCurrentPosition(
      function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
          
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getPermisoRuta.php",
          data: $httpParamSerializer({
            "id_ruta": $scope.ruta.id_ruta,
            "latitud": lat,
            "longitud": long
          }),
          headers : {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response){
          console.log(response.data.esta_cerca);
          if(response.data.esta_cerca==true){
            $scope.mostrarBoton = true;  
          }else{
            $scope.mostrarBoton = false;
          }

          //BORRAR DESPUES 
          $scope.mostrarBoton = true;
        });

      },function(err) {

 
      },posOptions
    );
    //fin esta cerca

//atras saber mas
    $scope.cerrarModalSaberMas = function(){
       $rootScope.modalSaberMas.hide();
       $rootScope.modalRutasTipicas.show();

       /*$ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
          "type": "slide",
          "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
          "duration": 0, // in milliseconds (ms), default 400
        });*/
  	}

    $scope.volverrutasTipicas = function(){
      $rootScope.modalSaberMas.hide();
      $rootScope.modalRutasTipicas.hide();
      //$rootScope.volverExploraYDescubre = true;
      
      $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
        "type": "slide",
        "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
        "duration": 0, // in milliseconds (ms), default 400
      });
    }

  	$scope.slideChanged3 = function(index) {
      $scope.slideIndex = index;
    };

    $scope.addCalificacion = function(ruta,calificacion){
      console.log("calificacion: ",calificacion);
      console.log("rutaaa", ruta);
      if (calificacion > 0) {
        console.log("su calificacion es mayor que 0");

        var myPopup = $ionicPopup.show({
          template: '<p CLASS="fuenteRoboto fuenteModal">¿Calificar ruta con '+ $scope.ruta.calificacion_usuario+' estrellas?</p>',
          title: '<h4 class="izquierda"> <img src="img/preferencias/logo.png" class="corazonCalificar" >Calificación</h4>',
          cssClass: 'headRosado',
          scope: $scope,
          buttons: [
            { 
                text: '<i class="icon ion-close-round"></i>',
                type:'popclose',
                  onTap: function(e) {
                    $scope.ruta.calificacion_usuario = 0;
                  }
            },
            {
              text: '<img src="img/login/btnIngresar.png" lt class="btnNaranjaFondo"></img><b>ACEPTAR</b><div>&nbsp;</div>',
              type: 'botonMorado',
              onTap: function(e) {
                  $ionicLoading.show({
                    template: 'Cargando...'
                  });


                  var posOptions = {timeout: 10000, enableHighAccuracy: false};
                  navigator.geolocation.getCurrentPosition(
                    function(position){
                      var lat  = position.coords.latitude;
                      var long = position.coords.longitude;
                      $scope.addCalificacionGeo(lat,long,ruta,calificacion);
                    },function(err) {
                      $scope.addCalificacionGeo(0,0,ruta,calificacion);
                    },posOptions
                  );
              }
            }
          ]
        });

        myPopup.then(function(res) {
          console.log('Tapped!', res);
        });
      }

      if (calificacion == 0) {
        console.log("su calificacion es 0");
      }
      

    }

    $scope.addCalificacionGeo = function(lat,long,ruta,calificacion){
      console.log("Calificacion: " + calificacion);
      console.log("Ruta: " + ruta);
      $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/addCalificacionRutasTipicas.php",
          data: $httpParamSerializer({
            id_usuario   : localStorage.getItem("id_usuario"),
            id_ruta      : $scope.ruta.id_ruta,
            calificacion : $scope.ruta.calificacion_usuario,
            "lat"        : lat,
            "long"       : long
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ 
          console.log(response.data);
          $ionicLoading.hide();

          if(response.data.resultado == "ok"){
            //console.log(response.data.item_turistico);
            //$scope.marcadores = response.data.item_turistico;
            $rootScope.toast('Ruta calificada', 'short');
            console.log("ultimo scope: ", $rootScope);
          }

        }, function(){ //Error de conexión
          $scope.ruta.calificacion_usuario = $scope.ruta.calificacion_usuario_orig;
          $ionicLoading.hide();
          $rootScope.toast('Verifica tu conexión a internet', 'short');
      });
    }
    
    $scope.recorrerRuta = function(){
      $rootScope.modalSaberMas.hide();
      $rootScope.modalRutasTipicas.hide();
      $rootScope.rutaRecorrer = $rootScope.rutaSeleccionada;

      $ionicNativeTransitions.stateGo('app.recorrerRutaTipica', {inherit:false}, {
        "type"      : "slide",
        "direction" : "up", // 'left|right|up|down', default 'left' (which is like 'next')
        "duration"  : 400, // in milliseconds (ms), default 400
      });



    }



    $scope.recorrerRutaOffline = function(){
      
      $rootScope.rutaRecorrer = $rootScope.rutaSeleccionada;
      if( localStorage.getItem('rutasOffline') == 'undefined' || localStorage.getItem('rutasOffline') == null ){
        var myPopup = $ionicPopup.show({
          template: '<p CLASS="fuenteRoboto fuenteModal">Debe descargar el mapa</p>',
          title: '<h4 class="izquierda"> <img src="img/rutasTipicas/eliminar_ruta.png" class="corazonCalificar" >Error</h4>',
          cssClass: 'headRosado',
          scope: $scope,
          buttons: [
            { 
                text: '<i class="icon ion-close-round"></i>',
                type:'popclose',
                  onTap: function(e) {
                  
                  }
            },
            {
              text: '<img src="img/login/btnIngresar.png" lt class="btnNaranjaFondo"></img><b>ACEPTAR</b><div>&nbsp;</div>',
              type: 'botonMorado',
              onTap: function(e) {
                
                 
              }
            }
          ]
        });
      }
      else{
        $rootScope.modalSaberMas.hide();
        $rootScope.modalRutasTipicas.hide();
        $state.go("app.recorrerRutaTipicaOffline");

      }

    }


    $scope.guardarRutaOffline = function(){
      $scope.rutasOffline = [];
      $scope.serviciosMarker = [];
      $scope.mostrarPorcentaje = true;
      $scope.progressPercent = 25;
      $scope.estado = "Descargando ruta";





      console.log($scope.rutasOffline);
      //localStorage.setItem('rutasOffline', JSON.stringify($scope.rutasOffline));
      // RUTA
      // 
      
      $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/getCoordenadasRutaTipica.php",
        data: $httpParamSerializer({
          "id_ruta": $rootScope.rutaSeleccionada.id_ruta
        }),
         headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si recato bien los datos
          ///evaluar si existen rutas o es la primera vez
          //localStorage.removeItem('rutasOffline');
          $scope.progressPercent = 50;
          $scope.estado = "Descargando servicios";
          console.log(response);
          
          $scope.rutaResponse = response.data.ruta; 



          //SERVICIOS

          $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/getDetalleRutaServicios.php",
            data: $httpParamSerializer({
            "id_ruta": $rootScope.rutaSeleccionada.id_ruta
            }),
             headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si recato bien los datos
              ///evaluar si existen rutas o es la primera vez
              //localStorage.removeItem('rutasOffline');
              $scope.progressPercent = 75;
              $scope.estado = "Descargando patrimonios";
              $scope.serviciosResponse = response.data.servicios;


              //PATRIMONIOS
              $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/getDetalleRutaPatrimonios.php",
                data: $httpParamSerializer({
                "id_ruta": $rootScope.rutaSeleccionada.id_ruta
                }),
                 headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){
                  $scope.progressPercent = 100;
                  $scope.patrimoniosResponse = response.data.patrimonio;
                  
                  $scope.descargar=false;
                  $scope.eliminar=true;
                  $scope.actualziar=true;
                  if( localStorage.getItem('rutasOffline') == 'undefined' || localStorage.getItem('rutasOffline') == null ){
                      
                      //Borrar dp
                      //$scope.rutasOffline = [];
                      $scope.rutasOffline.push({
                          "ruta" : $rootScope.rutaSeleccionada,
                          "mapa" : $scope.rutaResponse,
                          "servicios": $scope.serviciosResponse,
                          "patrimonios": $scope.patrimoniosResponse
                      });
                      console.log("ruta tipica ofline",$scope.rutasOffline);
                      // Put the object into storage
                      localStorage.setItem('rutasOffline', JSON.stringify($scope.rutasOffline));

                  }else{

                    // Retrieve the object from storage
                    //localStorage.removeItem("rutasOffline");

                    console.log($rootScope.rutaSeleccionada);
                    console.log(response.data.ruta);

                    
                    //console.log(localStorage.getItem('rutasOffline'));
                    retrievedObject = localStorage.getItem('rutasOffline');
                    $scope.rutasOffline = JSON.parse(retrievedObject);
                    //Borrar dp
                    //$scope.rutasOffline = [];
                    
                    angular.forEach($scope.rutasOffline, function(value, key) {    
                        if( $rootScope.rutaSeleccionada.id_ruta == $scope.rutasOffline[key].ruta.id_ruta){
                          console.log("entro al if 22222");
                          $scope.rutasOffline.splice(key,1);
                        }
                    });

                    $scope.rutasOffline.push({
                      "ruta" : $rootScope.rutaSeleccionada,
                      "mapa" : $scope.rutaResponse,
                      "servicios": $scope.serviciosResponse,
                      "patrimonios": $scope.patrimoniosResponse
                    });
                    console.log("al guardar: ",$scope.rutasOffline);
                    console.log("ruta tipica ofline existente",$scope.rutasOffline);
                    localStorage.setItem('rutasOffline', JSON.stringify($scope.rutasOffline));
                    
                  }


                  $scope.progressPercent = 100;
                  
                  //$scope.serviciosMarkerOffline = response.data.servicios;
                  //localStorage.setItem('serviciosOffline', JSON.stringify(response.data.servicios));
                  //console.log(localStorage.getItem("serviciosOffline"));
                  
                  $scope.estado = "Descarga completa";
                  $scope.descargarMapa();

                  console.log($scope.rutasOffline);

              });
              

              


          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
          });

      }, function(){ //Error de conexión
        $rootScope.toast('Verifica tu conexión a internet', 'short');
      });



      



    }

    $scope.trustAsHtml = function(string) {
      return $sce.trustAsHtml(string);
    };


    function listDir(path){
  window.resolveLocalFileSystemURL(path,
    function (fileSystem) {
      var reader = fileSystem.createReader();
      reader.readEntries(
        function (entries) {
          console.log(entries);
        },
        function (err) {
          console.log(err);
        }
      );
    }, function (err) {
      console.log(err);
    }
  );
}

$scope.eliminarZipMapa = function(){

  window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dir) {
  dir.getFile("mapa.zip", {create:false}, function(fileEntry) {
              fileEntry.remove(function(){
                  console.log("eliminado correctamente");
              },function(error){
                  // Error deleting the file
                  console.log(error);
              },function(){
                 // The file doesn't exist
                  console.log("archivo no e iste");
              });
  });
});
}
//



$scope.mostrarPorcentaje = false;
$scope.estado = "";


$scope.descargarMapa = function(){
    console.log(localStorage.getItem("tieneMapa"));
    if(localStorage.getItem("tieneMapa") != "si"){

      $scope.mostrarPorcentaje = true;
      console.log(cordova.file.dataDirectory);
      $cordovaFileTransfer.download("http://200.14.68.107/atacamaGo/prueba/mapa.zip", cordova.file.dataDirectory+"mapa.zip")
      .then(function(result) {
          console.log(result.nativeURL);

          zip.unzip(cordova.file.dataDirectory+"mapa.zip", cordova.file.dataDirectory, function(es){

            console.log(es);
            $scope.estado = "Descarga completada";
            localStorage.setItem("tieneMapa","si");
            $scope.eliminarZipMapa();


          }, function(progressEvent) {
            //descomprimiendo mapa
            $scope.estado = "Extrayendo";
            $timeout(function () {
                $scope.progressPercent = (progressEvent.loaded/progressEvent.total)*100;

                console.log($scope.progressPercent);

                if( $scope.progressPercent == 100 ){
                  $scope.popUp();
                }
            });

          });


        }, function(err) {
          alert(err);
          console.log(err);
        }, function (progress) {
          //descargando mapa

          $scope.estado = "Descargando mapa";
          $timeout(function () {

          $scope.progressPercent = (progress.loaded / progress.total) * 100;
          console.log($scope.progressPercent);
          });
        });
    }//fin if localstorage
    else{
      $scope.popUp();
    }

    }






    $scope.eliminarRuta = function(){
      console.log("eliminando");
      
      retrievedObject = localStorage.getItem('rutasOffline');
      $scope.rutasOffline = JSON.parse(retrievedObject);
      console.log($scope.rutasOffline);

      var myPopup = $ionicPopup.show({
        template: '<p CLASS="fuenteRoboto fuenteModal">¿Está seguro que desea eliminar la ruta?</p>',
        title: '<h4 class="izquierda"> <img src="img/rutasTipicas/eliminar_ruta.png" class="corazonCalificar" >Aviso</h4>',
        cssClass: 'headRosado',
        scope: $scope,
        buttons: [
          { 
              text: '<i class="icon ion-close-round"></i>',
              type:'popclose',
                onTap: function(e) {
                
                }
          },
          {
            text: '<img src="img/login/btnIngresar.png" lt class="btnNaranjaFondo"></img><b>ACEPTAR</b><div>&nbsp;</div>',
            type: 'botonMorado',
            onTap: function(e) {
                $ionicLoading.show({
                  template: 'Cargando...'
                });

                angular.forEach($scope.rutasOffline, function(value, key) {    
                    if( $rootScope.rutaSeleccionada.id_ruta == $scope.rutasOffline[key].ruta.id_ruta){
                      console.log("entro al if");
                      $scope.rutasOffline.splice(key,1);
                    }
                });

                console.log($scope.rutasOffline);
                localStorage.setItem('rutasOffline', JSON.stringify($scope.rutasOffline));
                //console.log($scope.rutaOffline);
                $ionicLoading.hide();
                //console.log("se elimino ruta");
                $scope.descargar=true;
                $scope.eliminar=false;
                $scope.actualziar=false;
                
            }
          }
        ]
      });

      myPopup.then(function(res) {
        console.log('Tapped!', res);
      });

    }


    $scope.continarRuta = function(){
      var networkState = navigator.connection.type;
      console.log(networkState);
      if(networkState == "none"){
        $scope.recorrerRutaOffline();
      }
      else{
        $scope.recorrerRuta();
      }
    }


    $scope.popUp = function(){
      var myPopup = $ionicPopup.show({
        template: '<p CLASS="fuenteRoboto fuenteModal">Ya puedes recorrer la ruta sin conexión</p>',
        title: '<h4 class="izquierda"><i class="icon ion-alert"></i> AVISO</h4>',
        cssClass: 'headRosado',
        scope: $scope,
        buttons: [
          { 
              text: '<i class="equisMenuModalAzul" style="float:right !important;"></i>',
              type:'popclose',
                onTap: function(e) {
                
                }
          },
          {
            text: '<img src="img/login/btnIngresar.png" lt class="btnNaranjaFondo"></img><b>ACEPTAR</b><div>&nbsp;</div>',
            type: 'botonMorado',
            onTap: function(e) {
                
               
            }
          }
        ]
      });
    }



}]);