angular.module('starter.controllers')


.controller('serviciosTuristicosCtrl', ['$scope','$http','$state','$rootScope','$ionicNativeTransitions',function($scope,$http,$state,$rootScope,$ionicNativeTransitions) {
  // With the new view caching in Ionic, Controllers are only called
  console.log("serviciosTuristicosCtrl scope: ",$scope);

  

  $scope.cerrarModalServiciosTuristicos = function(){
    $rootScope.volverServicioYAtractivo2 = false;
    $scope.modalServiciosTuristicos.hide();
  }

  width = window.screen.height;
  var alturaMapa = width - 166;
  $scope.styleMapa = {
      "height" : "100%",
      "width"  : "100%",

  };
  
  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/getMenuServicios.php",
    data: {},
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si recato bien los datos
      if(response.data.resultado == "ok"){
        $scope.tipo_servicio = response.data.tipo_servicio;

        // $scope.tipo_servicio.unshift({
        //   "nombre"           : "Ver todos los servicios",
        //   "id_tipo_servicio" : -1,
        //   "seleccionado"     : false
        // });
        
      }
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });
  
  $scope.mostrarServiciosTuristicos = function(id_tipo_servicio,index){
    console.log("id_tipo_servicio ", id_tipo_servicio, " index ",index );
    
    $scope.modalServiciosTuristicos.hide();
    
  	$rootScope.flagMostrarServiciosTuristicos = true;
  	$rootScope.mostrarServiciosTuristicos = {
  		"id_tipo_servicio" : id_tipo_servicio,
      "index"            : index
    };
  	$state.go('app.serviciosAtractivos');
  }

  
}]);