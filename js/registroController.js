angular.module('starter.controllers')


.controller('registroCtrl', ['$scope','$http','$state','ionicDatePicker','$ionicNativeTransitions','$cordovaToast','$httpParamSerializer','$rootScope','$cordovaFileTransfer','$cordovaCamera','$ionicLoading','$cordovaPush','$ionicPlatform',function($scope,$http,$state,ionicDatePicker,$ionicNativeTransitions,$cordovaToast,$httpParamSerializer,$rootScope,$cordovaFileTransfer,$cordovaCamera,$ionicLoading,$cordovaPush,$ionicPlatform) {

  $scope.registro           = [];
  $scope.sexoSelecionado    = "Sexo";
  $scope.paisSelecionado    = "PAIS";
  $scope.fechaSeleccionado  = "FECHA NACIMIENTO";
  $scope.selectables        = ['Masculino', 'Femenino'];
  $scope.nombre_pais        = [];
  $scope.id_pais            = [];
  $scope.rutaImagen         = "";
  $scope.nombreImagen       = "";
  $scope.registro           = [];
  $scope.tieneSrc           = false;
  $rootScope.volverRegistro = true;

  /**
   * [Rescatar todos los países desde la báse de datos.]
   * 
   * @return {[Array]}  $scope.nombre_pais [Array con nombre de paises]
   * @return {[Array]}  $scope.id_pais     [array con id del nombre de paises]
   */
  /*$http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/paises.php",
    data: $httpParamSerializer({}),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si guardó correctamente.
    //console.log(response.data.pais);
    for(var i=0;i<response.data.pais.length;i++){
      $scope.nombre_pais.push( response.data.pais[i].nombre_pais );
      $scope.id_pais.push( response.data.pais[i].id_pais );
    }


    console.log($scope.paises);
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });*/

  /**
   * [Mostrar 'cargando' cuando se esta cargando la vista]
   */
	$scope.$on('$ionicView.afterEnter', function(){
    /*setTimeout(function(){
      //document.getElementById("containerLoading").style.display = "none";
      document.getElementById("containerLoading").className = "fadeOut";
    },100);*/
  });

  $scope.validateEmail = function(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
  }
  
  /**
   * [cambiar el valor del select 'vista']
   */
  $scope.selectSexo = function(nuevo,viejo){
  	//console.log(viejo+" "+nuevo);
  	$scope.sexoSelecionado = nuevo;
  }

  /**
   * [cambiar el valor del select 'vista']
   */
  $scope.selectPais = function(nuevo,viejo){
    //console.log(viejo+" "+nuevo);
    $scope.paisSelecionado = nuevo;
  }

  /**
   * [Obtener y dar formato a la fecha de nacimientto]
   * @return {[String]}  $scope.fechaSeleccionado [fecha cno formato 'DD/MM/YYYY']
   */
  var ipObj1 = {
    callback: function (val) {  //Mandatory
      console.log('Return value from the datepicker popup is : ' + val, new Date(val));
		  var d                 = new Date(val)
			var curr_date         = d.getDate();
			var curr_month        = d.getMonth() + 1; //Months are zero based
			var curr_year         = d.getFullYear();
			$scope.registro.fechaNac = curr_date+"/"+curr_month+"/"+curr_year;
			$scope.fechaSeleccionado = $scope.registro.fechaNac;
    },
    inputDate: new Date(),      //Optional
    mondayFirst: true,          //Optional
    closeOnSelect: false,       //Optional
  };

  $scope.openDatePicker = function(){
    ionicDatePicker.openDatePicker(ipObj1);
  };

  $scope.cancelar = function(){
  	$ionicNativeTransitions.stateGo('login', {inherit:false}, {
    	"type": "slide",
    	"direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
    	"duration": 400, // in milliseconds (ms), default 400
	  });
  }

  $scope.registrarse = function(registro){
    $ionicLoading.show({
      template: 'Cargando...'
    });

    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    navigator.geolocation.getCurrentPosition(
      function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        $scope.registrarseGeo(lat,long,registro);
      },function(err) {
        $scope.registrarseGeo(0,0,registro);
      },posOptions
    );
  }

  /**
   * [accion cuando se registra el usuario]
   * @param  {[Array]} registro [array con toda la info del formulario]
   */
  $scope.registrarseGeo = function(lat,long,registro){
    //validar que esten los campos llenos...
    console.log(registro);
    console.log( $scope.validateEmail(registro.correo) );
    
    $scope.nombreComp = registro.nombre;

    if(registro.nombre){
      var nombres = registro.nombre.split(" ");
      registro.paterno = "";
      registro.materno = "";
      
      nombres.forEach(function(element,index) {
        if(index == 0)
          registro.nombre = element

        if(index == 1)
          registro.paterno = element

        if(index == 2)
          registro.materno = element

      });

      console.log(registro);
    }

    flagCamposLlenos = 0;
    var count = 0;
    for (var p in registro) {
      registro.hasOwnProperty(p) && count++;
    }

    if( $scope.validateEmail(registro.correo) == false ){
      flagCamposLlenos = 1;
      $rootScope.toast("Correo inválido","short");  
    }

    if( registro.correo==null    || registro.correo== ""  || 
        registro.nombre==null    || registro.nombre== ""  || 
        registro.paterno==null   || registro.paterno== "" || 
        /*registro.materno==null   || registro.materno== "" ||*/
        registro.password2==null || registro.password2== "" ||
        $scope.validateEmail(registro.correo) == false ){

      if(!registro.paterno){
        $rootScope.toast("Debes ingresar nombre y apellido","short");
        registro.nombre = $scope.nombreComp;
      }else{
        $rootScope.toast("Debes llenar todos los campos","short");
        registro.nombre = $scope.nombreComp;
      }
      
      $ionicLoading.hide();
      flagCamposLlenos = 1;
    }
    //validar password
    if( flagCamposLlenos == 0 ){
      if( registro.password == registro.password2){

        $ionicLoading.show({
          template: 'Cargando...'
        });

        /*var options = {
          fileKey: "perfil",
          fileName: $scope.nombreImagen,
          chunkedMode: false
        };

        $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirImagen.php", $scope.rutaImagen, options).then(function(result) {
            console.log("SUCCESS:")
            console.log(result.response);
        }, function(err) {
          console.log("ERROR: ");
          console.log(err);
        }, function (progress) {
            // constant progress updates
        });*/
        
        if(registro.sexo == "Masculino")
          registro.sexo = "M";
        else if ( registro.sexo == "Femenino" )
          registro.sexo = "F";

        for (var i=0;i<$scope.nombre_pais.length; i++) {
          if( $scope.nombre_pais[i] == registro.pais)
            registro.pais = $scope.id_pais[i]
        }

        registro.correo = angular.lowercase(registro.correo);
        
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacama/public/usuario/guardarUsuarioMovil",
          data: $httpParamSerializer({
            "correo"     : registro.correo,
            "nombre"     : registro.nombre,
            "apellido_p" : registro.paterno,
            "apellido_m" : registro.materno,
            "password"   : registro.password,
            "pais"       : registro.pais,
            "genero"     : registro.sexo,
            "nacimiento" : registro.fechaNac,
            "foto"       : $scope.nombreImagen,
            "lat"        : lat,
            "long"       : long
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data.resultado);
          if(response.data.resultado == "Registrado"){
            $rootScope.toast("Usuario registrado correctamente","short");

            $http({
              method: "POST",
              url: "http://200.14.68.107/atacama/public/usuario/mensajeBienvenidaMovil",
              data: $httpParamSerializer({
                "nombre"   : registro.nombre + " " + registro.paterno + " " + registro.materno,
                'correo'   : registro.correo,
                'pass'     : registro.password
              }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si recato bien los datos
              console.log(response);
              $rootScope.toast("Contraseña enviada al correo","short");
      
                  
              $http({
                method: "POST",
                url: "http://200.14.68.107/atacama/public/sesion/doLogin",
                data: $httpParamSerializer({
                  "email"    : registro.correo,
                  "password" : registro.password
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si guardó correctamente.
                if(response.data.href){
                  console.log(response.data);
                  //guardar datos del usuario en local Storage
                  registro.correo = angular.lowercase(registro.correo);
                  localStorage.setItem("id_usuario", response.data.id_usuario);
                  localStorage.setItem("id_agenda",  response.data.id_email.id_agenda);
                  localStorage.setItem("correo",  registro.correo);

                  //Conseguir y alamacenar el CGM                  
                  $scope.getGcm(response.data.id_usuario);

                  //alert("ocultar cargando deśpues de GCM");
                  $ionicLoading.hide();

                  //ir al home
                  //alert("antes de ir al home");
                  $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
                    "type": "slide",
                    "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
                    "duration": 400, // in milliseconds (ms), default 400
                  });
                }
                else{
                  $rootScope.toast('Usuario y/o contraseña incorrecta', 'short');
                  $ionicLoading.hide();
                }

              }, function(){ //Error de conexión
                  $rootScope.toast('Verifica tu conexión a internet', 'short');
                  $ionicLoading.hide();
              });

            }, function(){ //Error de conexión
              $rootScope.toast('Verifica tu conexión a internet', 'short');
              $ionicLoading.hide();
            });

            //$rootScope.toast("Usuario ya registrado, debe recuperar contraseña","short");

            //$ionicLoading.hide();

            //ir al Login
            /*$ionicNativeTransitions.stateGo('login', {inherit:false}, {
              "type": "slide",
              "direction": "up",
              "duration": 400,
            });*/
          }
          else{
            $rootScope.toast("Usuario ya se encuentra registrado","short");
            registro.nombre = $scope.nombreComp;
          }
            $ionicLoading.hide();

        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet',"short");
          registro.nombre = $scope.nombreComp;
          $ionicLoading.hide();
        });
      }else{
        $rootScope.toast("Contraseñas no coinciden","short");
        registro.nombre = $scope.nombreComp;
        $ionicLoading.hide();
      }    
    }
     
  }

  $scope.choosePhoto = function () {
    var options = {
      destinationType : Camera.DestinationType.FILE_URI,
      sourceType      : Camera.PictureSourceType.PHOTOLIBRARY,
      mediaType       : Camera.MediaType.PICTURE,
      correctOrientation : true
    };
   
    $cordovaCamera.getPicture(options).then(function (imageData) {
      imageData = imageData.replace(/^(.+?\.(png|jpe?g)).*$/i, '$1');
      var image = document.getElementById('imagenItemRegistro');
      image.style.display   = 'block';
      image.src             = imageData;
      $scope.rutaImagen     = imageData;
      $scope.tieneSrc = true;
      $scope.nombreImagen   = Math.floor((Math.random() * 9999999) + 1)+"_"+imageData.substr(imageData.lastIndexOf('/') + 1);
    }, function (err) {
      //alert(err);
      // An error occured. Show a message to the user
    });
  }
  
  $scope.getGcm = function(id_usuario){
    $ionicPlatform.ready(function () {
      var options = {
        android: {
          senderID: "928988279941"
        },
        ios: {
          alert: "true",
          badge: "true",
          sound: "true"
        },
        windows: {}
      };
      
      var androidConfig = {
          "senderID": "928988279941",
      };

      $cordovaPush.register(androidConfig).then(function(result) {
        //alert(result);
        //console.log("GCM: " + result);
      }, function(err) {
        // Error
      });

      $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
        switch(notification.event) {
          case 'registered':
            if (notification.regid.length > 0 ) {
              //alert('registration ID = ' + notification.regid);
            }
            break;

          case 'message':
            // this is the actual push notification. its format depends on the data model from the push server
            //alert('message = ' + notification.message + ' msgCount = ' + notification.msgcnt);
            break;

          case 'error':
            //alert('GCM error = ' + notification.msg);
            break;

          default:
            //alert('An unknown GCM event has occurred');
            break;
        }
        if($scope.unaVez == 0){
          $scope.unaVez = 1;
          $scope.gcmMovil = notification.regid;
          //alert($scope.gcmMovil);
          $http({
            method: "POST",
            url: "http://200.14.68.107/atacamaGo/addDispositivo.php",
            data: $httpParamSerializer({
              gcm        : $scope.gcmMovil,
              ssoo       : 'android',
              id_usuario : localStorage.getItem("id_usuario")
            }),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
          }).then(function(response){ //ok si recato bien los datos
              console.log(response);
              
          }, function(){ //Error de conexión
            $rootScope.toast('Verifica tu conexión a internet', 'short');
          });
        } 
          
      });
      /*
      //console.log($cordovaPushV5);
      // initialize
        console.log($cordovaPushV5);
        $cordovaPushV5.initialize(options).then(function() {
          alert("ENTRO A LA FUNCION $cordovaPushV5.initialize");

          // start listening for new notifications
          $cordovaPushV5.onNotification();
          // start listening for errors
          $cordovaPushV5.onError();

          // register to get registrationId
          $cordovaPushV5.register().then(function(registrationId) {
            alert("$cordovaPushV5.register()");
            console.log(registrationId);
            console.log("sdfadsfd");
            $http({
              method: "POST",
              url: "http://200.14.68.107/atacamaGo/addDispositivo.php",
              data: $httpParamSerializer({
                "id_usuario" : id_usuario,
                "gcm"        : registrationId,
                "ssoo"       : ionic.Platform.platform()
              }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si guardó correctamente.
                console.log(response);
                 response.resultado.id_dispositivo;
            }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet', 'short')
            });

          })
        });
        */
    });

  }

}]);