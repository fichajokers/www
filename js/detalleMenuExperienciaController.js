angular.module('starter.controllers')


.controller('detalleMenuExperienciaCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaGeolocation','$ionicModal','ratingConfig','$ionicLoading','$sce','$ionicNativeTransitions',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaGeolocation,$ionicModal,ratingConfig,$ionicLoading,$sce,$ionicNativeTransitions) {
    console.log("detalle MENU experiencia controller");
    console.log($rootScope.experienciaDetalleMostrar);
    $scope.item = $rootScope.experienciaSeleccionadaNuevoHome;
    $scope.iconoMenu = "";
    $scope.menuSeleccionado = $rootScope.experienciaDetalleMostrar;
    $scope.ocultarPeso = false;
    $scope.ocultarDolar = false;
    

    width = window.screen.width;
    console.log("width: ",width);
    /*384-767 HUAWEI*/
      if( width > 321 && width <= 360 ){
        console.log("esto es un huawei");

        $scope.h1DetalleExperiencia = {
              'width':          '75%',
              'text-transform': 'uppercase', 
              'size':           '50px', 
              'overflow':       'hidden', 
              'margin-left':    '55px', 
              'margin-right':   '55px',
        };

        $scope.incluyeDetalleMenu = {
              'margin-left': '2%',
        };
        $scope.vigenciaDetalleMenu = {
              'margin-left': '1%',
        };
        $scope.traerDetalleMenu = {
              'margin-left': '3%',
        };
        $scope.reservaDetalleMenu = {
              'margin-left': '1%',
        };

        $scope.margenCaja = {
              'margin-top': '30%',
        }
        
      }

      /*lenovo tablet*/
      if( width > 361 && width <= 480 ){
        console.log("esto es un talet");

        $scope.h1DetalleExperiencia = {
              'width': '80%',
              'text-transform': 'uppercase', 
              'size':           '50px', 
              'overflow':       'hidden', 
              'margin-left':    '55px', 
              'margin-right':   '55px',
        };

        $scope.programaDetalleMenu = {
              'margin-left': '2%',
        };
        $scope.incluyeDetalleMenu = {
              'margin-left': '3%',
        };
        $scope.vigenciaDetalleMenu = {
              'margin-left': '3%',
        };
        $scope.traerDetalleMenu = {
              'margin-left': '4%',
        };
        $scope.reservaDetalleMenu = {
              'margin-left': '2%',
        };
      }


      if( width > 481 && width <= 600 ){
        console.log("esto es un talet");

        $scope.h1DetalleExperiencia = {
              'width': '85%',
              'text-transform': 'uppercase', 
              'size':           '50px', 
              'overflow':       'hidden', 
              'margin-left':    '55px', 
              'margin-right':   '55px',
        };

        $scope.programaDetalleMenu = {
              'margin-left': '3%',
        };
        $scope.incluyeDetalleMenu = {
              'margin-left': '4%',
        };
        $scope.vigenciaDetalleMenu = {
              'margin-left': '4%',
        };
        $scope.traerDetalleMenu = {
              'margin-left': '5%',
        };
        $scope.reservaDetalleMenu = {
              'margin-left': '3%',
        };
      }

      if( width == 768  ){
        console.log("es el tablet blanco");

        $scope.programaDetalleMenu = {
              'margin-left': '5%',
        };
        $scope.incluyeDetalleMenu = {
              'margin-left': '6%',
        };
        $scope.vigenciaDetalleMenu = {
              'margin-left': '5%',
        };
        $scope.traerDetalleMenu = {
              'margin-left': '6%',
        };
        $scope.reservaDetalleMenu = {
              'margin-left': '5%',
        };

        $scope.logo = {
              'height' : '200px',
        }
      }

    $scope.nombreMenu = "PROGRAMA";
    $scope.cerrarModalDetalleMenuExperiencia = function(){
      $rootScope.modalDetalleMenuExperiencia.hide();
  	}

    $scope.goToNuevoHome = function(){
      $rootScope.modalDetalleExperiencia.remove();
      $rootScope.modalDetalleMenuExperiencia.hide(); 
    }
    
    $scope.menu = function(id){
      var identificador = id;
      $rootScope.experienciaDetalleMostrar = identificador;
      $scope.menuSeleccionado = id;
      

      if( $rootScope.experienciaDetalleMostrar == 1 ){
          $scope.nombreMenu = "PROGRAMA";
          $scope.cuerpo     = $scope.item.detalle_paquete_turistico ;
          $scope.img        = "img/experiencia/programa.png";
          $scope.iconoMenu  = "img/experiencia/imgPrograma.png";
          $scope.mostrarBtnReservar = false;
      }

      if( $rootScope.experienciaDetalleMostrar == 2 ){
          $scope.nombreMenu = "INCLUYE";
          $scope.cuerpo     = $rootScope.experienciaSeleccionada.incluye_paquete_turistico;
          $scope.img        = "img/experiencia/incluye.png";
          $scope.iconoMenu  = "img/experiencia/imgIncluye.png";
          $scope.mostrarBtnReservar = false;
      }

      if( $rootScope.experienciaDetalleMostrar == 3 ){
          $scope.nombreMenu = "VIGENCIA";
          $scope.cuerpo     = $rootScope.experienciaSeleccionada.vigencia_paquete_turistico;
          $scope.img        = "img/experiencia/vigencia.png";
          $scope.iconoMenu  = "img/experiencia/imgVigencia.png";
          $scope.mostrarBtnReservar = false;
      }

      if( $rootScope.experienciaDetalleMostrar == 4 ){
          $scope.nombreMenu = "TRAER";
          $scope.cuerpo     = $rootScope.experienciaSeleccionada.turista_debe_traer;
          $scope.img        = "img/experiencia/traer.png";
          $scope.iconoMenu  = "img/experiencia/imgTraer.png";
          $scope.mostrarBtnReservar = false;
      }

      if( $rootScope.experienciaDetalleMostrar == 5 ){
          $scope.nombreMenu = "RESERVAR";
          $scope.cuerpo     = "";
          $scope.img        = "";
          $scope.iconoMenu  = "img/experiencia/imgReservar.png";

          $scope.mostrarBtnReservar = true;


          if ($scope.item.precio_pesos == null) {
            $scope.ocultarPeso = true;
          }

          if ($scope.item.precio_dolar == null) {
            $scope.ocultarDolar = true;
          }

      }

    }

    $scope.menu( $rootScope.experienciaDetalleMostrar );

    $scope.trustAsHtml = function(string) {
      return $sce.trustAsHtml(string);
    };

    $scope.openModalReserva = function(){
      $rootScope.item_seleccionado = $scope.item;
      $rootScope.id_servicio_turistico = $scope.item.id_servicio_turistico;
      $rootScope.rescatarDesdeBd = false;
      $rootScope.fromExperiencia = true;
      //Modal menu Acciones Modiciar...
      $ionicModal.fromTemplateUrl('templates/reserva.html', {
        scope: $rootScope,
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalReserva = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
        //  $rootScope.map.setClickable(false);

        $rootScope.modalReserva.show();
      })  
    }


}]);