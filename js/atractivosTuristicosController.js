angular.module('starter.controllers')


.controller('atractivosTuristicosCtrl', ['$scope','$http','$state','$rootScope',function($scope,$http,$state,$rootScope) {
  // With the new view caching in Ionic, Controllers are only called
  console.log("PlaylistCtrl");

  $scope.cerrarModalAtractivosTuristicos = function(){
    $rootScope.volverServicioYAtractivo2 = false;
  	$scope.modalAtractivosTuristicos.hide();
  }

  $scope.mostrarAtractivosTuristicos = function(id_sub_tipo_atractivo,index){
  	//console.log(id_sub_tipo_atractivo,index);
  	$scope.modalAtractivosTuristicos.hide();
  	$rootScope.flagMostrarAtractivosTuristicos = true;
  	$rootScope.mostrarAtractivosTuristicos = {
  		"id_sub_tipo_atractivo" : id_sub_tipo_atractivo,
  		"index"                 : index};
  	$state.go('app.serviciosAtractivos');
  }

  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/getMenuPatrimonio.php",
    data: {},
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si recato bien los datos
      //console.log(response);
      if(response.data.resultado == "ok"){
        $rootScope.sub_tipo_atractivo = response.data.sub_tipo_atractivo;

        // $rootScope.sub_tipo_atractivo.unshift({
        //   "nombre"                : "Ver todos los atractivos",
        //   "id_sub_tipo_atractivo" : -1,
        //   "seleccionado"          : false
        // });

        console.log($scope.sub_tipo_atractivo);
      }
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });


}]);