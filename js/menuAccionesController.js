angular.module('starter.controllers')


.controller('menuAccionesCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$ionicModal','$cordovaCamera','$cordovaFileTransfer','$httpParamSerializer','$ionicLoading','$ionicNativeTransitions','$ionicPopup' ,function($scope,$http,$state,$rootScope,ionicDatePicker,$ionicModal,$cordovaCamera,$cordovaFileTransfer,$httpParamSerializer,$ionicLoading,$ionicNativeTransitions,$ionicPopup) {
  // With the new view caching in Ionic, Controllers are only called
  $scope.guardarImg = false;
  $scope.item = [];

  $scope.cerrarModalMenuAcciones = function(){
    console.log("cerar modal");
    //if( ionic.Platform.isAndroid() )
    $rootScope.map.clear();
    $rootScope.map.off();
    $rootScope.map.remove();
    
    $rootScope.modalMenuAcciones.remove();
    $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
        "type": "slide",
        "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
        "duration": 400, // in milliseconds (ms), default 400
    });
    $rootScope.modalRutasTipicas.show();
    $rootScope.modalSaberMas.show();
  }


    $scope.openModalSubirImagen = function(){
      

      //Modal subirImagen...
      $ionicModal.fromTemplateUrl('templates/subirImagen.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalSubirImagen = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
      
        $rootScope.modalSubirImagen.show();
      });

    }

    $scope.openModalAgregarVideo = function(){

      //Modal Agregar Video...
      $ionicModal.fromTemplateUrl('templates/agregarVideo.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarVideo = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        
        $rootScope.modalAgregarVideo.show();
      });
        
    }

    $scope.openModalAgregarAudio = function(){

      //Modal Agregar Audio...
      $ionicModal.fromTemplateUrl('templates/agregarAudio.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarAudio = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
      
        $rootScope.modalAgregarAudio.show();
      });
  
    }

    $scope.openModalAgregarDescripcion = function(){

      //Modal Agregar descripcion...
      /*$ionicModal.fromTemplateUrl('templates/agregarDescripcion.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarDescripcion = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        
        $rootScope.modalAgregarDescripcion.show();
      });*/

    }

    $scope.openModalAgregarComentario = function(){
      //Modal Agregar Comentario...
      $ionicModal.fromTemplateUrl('templates/agregarComentario.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarComentario = modal;
        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        
        $rootScope.modalAgregarComentario.show();
      });
    }

    $scope.openModalAgregarCalificar = function(){
      //Modal Agregar Calificar...
      $ionicModal.fromTemplateUrl('templates/agregarCalificar.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalAgregarCalificar = modal;
        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);  
        
        $rootScope.modalAgregarCalificar.show();
      });
  
    }

    //----------------Subir imagenes-------------------//
    $scope.takePhoto = function () {
        var options = {
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.CAMERA,
            mediaType       : Camera.MediaType.PICTURE,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            saveToPhotoAlbum: false
        };
   
        $cordovaCamera.getPicture().then(function (imageData) {
            console.log(imageData);
            imageData = imageData.replace(/^(.+?\.(png|jpe?g)).*$/i, '$1');
            console.log(imageData);
            var image           = document.getElementById('imagenSacadaFoto333');
            image.style.display = 'block';
            image.src           = imageData;
            $scope.rutaImagen   = imageData;
            $scope.tieneSrc     = true;
            $scope.nombreImagen = Math.floor((Math.random() * 9999999) + 1)+"_"+imageData.substr(imageData.lastIndexOf('/') + 1);
            $scope.guardarImg = true;
        }, function (err) {
            //alert(err);
            // An error occured. Show a message to the user
        });
    }
                
    $scope.choosePhoto = function () {
        var options = {
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType      : Camera.PictureSourceType.PHOTOLIBRARY,
            mediaType       : Camera.MediaType.PICTURE,
            correctOrientation : true
        };
   
        $cordovaCamera.getPicture(options).then(function (imageData) {
          if( imageData == null){
            $rootScope.toast("Debes seleccionar una imagén de tu movil.","short");
          }else{
            console.log(imageData);
            imageData = imageData.replace(/^(.+?\.(png|jpe?g)).*$/i, '$1');
            console.log(imageData);
            var image = document.getElementById('imagenSacadaFoto333');
            image.style.display   = 'block';
            image.src             = imageData;
            $scope.rutaImagen     = imageData;
            $scope.tieneSrc = true;
            $scope.nombreImagen   = Math.floor((Math.random() * 9999999) + 1)+"_"+imageData.substr(imageData.lastIndexOf('/') + 1);
            $scope.guardarImg = true;
          }
        }, function (err) {
            //alert(err);
            // An error occured. Show a message to the user
        });
    }

    $scope.actualizarImgPrincipal1 = function(){
      var options = {
        fileKey: "fotoItem",
        fileName: $scope.nombreImagen,
        chunkedMode: false
      };

      $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirImagenItem.php", $scope.rutaImagen, options).then(function(result) {
        $rootScope.toast('Imagen agregada', 'short');
      }, function(err) {
        $rootScope.toast('Error agregando imagen', 'short');
        console.log(err);
      }, function (progress) {
          // constant progress updates
      });

      $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/editarImagenPrincipalItemTuristico.php",
        data: $httpParamSerializer({
          "id_item_turistico"   : $rootScope.id_item_turistico,
          "id_usuario"          : localStorage.getItem("id_usuario"),
          "nombre_archivo_item" : $scope.nombreImagen,
          "tipo_archivo_item"   : 2
        }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si guardó correctamente.
        console.log(response.data);
        if(response.data.resultado == "ok"){
          $rootScope.toast("imagen agregada.","short");

        }else
          $rootScope.toast("Ha ocurrido un error intenta nuevamente","short");
              
      }, function(){ //Error de conexión
        $rootScope.toast('Verifica tu conexión a internet',"short");
      });
    }


    $scope.addDescripcionItem = function(item){
          $ionicLoading.show({
            template: 'Cargando...'
          });

          if(item.titulo == undefined || item.titulo == null){
            $rootScope.toast('Titulo obligatorio',"short");
            $ionicLoading.hide();
          }else{
            $http({
              method: "POST",
              url: "http://200.14.68.107/atacamaGo/addDescripcionItemTuristico.php",
              data: $httpParamSerializer({
                "id_item_turistico": $rootScope.id_item_turistico,
                "titulo"           : item.titulo,
                "descripcion"      : item.descripcion
              }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si guardó correctamente.
                console.log(response.data);
                if(response.data.resultado == "ok"){
                    $rootScope.toast('Descripcion agregada', 'short');
                    /*$rootScope.map.addMarker({
                      'position': $rootScope.POS_MARCADOR,
                      icon: 'darkcyan',
                      'title': item.titulo,
                    },function(marker) {

                    });*/
                    console.log( typeof($scope.rutaImagen) );
                    console.log( $scope.rutaImagen )
                    if (typeof($scope.rutaImagen) != "undefined"){
                      $scope.actualizarImgPrincipal1();
                    }           
                }
                else{
                    $rootScope.toast('error, intenta nuevamente', 'short');
                }
                $ionicLoading.hide();
            }, function(){ //Error de conexión
                $rootScope.toast('Verifica tu conexión a internet', 'short');
                $ionicLoading.hide();
            });
          }
  
      }


      $scope.atrasModalMenuAcciones = function() {

          $rootScope.map.setClickable(true);
          $rootScope.modalMenuAcciones.remove();
      }
    /*---------------------------- MODIFICAR -------------------------------*/
    /*
    //Modal Modificar Descripcion...
    $ionicModal.fromTemplateUrl('templates/modificarDescripcion.html', {
      scope: $rootScope,
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalModificarDescripcion = modal;
    });

    $scope.openModalModificarDescripcion = function(){
      //hacer que el mapa no sea clickeable.
      if( ionic.Platform.isAndroid() )
        $rootScope.map.setClickable(false);  
      
      $rootScope.modalModificarDescripcion.show();
    }

    //Modal Modificar Imagen...
    $ionicModal.fromTemplateUrl('templates/modificarImagen.html', {
      scope: $rootScope,
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalModificarImagen = modal;
    });

    $scope.openModalModificarImagen = function(){
      //hacer que el mapa no sea clickeable.
      if( ionic.Platform.isAndroid() )
        $rootScope.map.setClickable(false);  
      
      $rootScope.modalModificarImagen.show();
    }

    //Modal Modificar Video...
    $ionicModal.fromTemplateUrl('templates/modificarVideo.html', {
      scope: $rootScope,
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalModificarVideo = modal;
    });

    $scope.openModalModificarVideo = function(){
      //hacer que el mapa no sea clickeable.
      if( ionic.Platform.isAndroid() )
        $rootScope.map.setClickable(false);  
      
      $rootScope.modalModificarVideo.show();
    }

    //Modal Modificar Audio...
    $ionicModal.fromTemplateUrl('templates/modificarAudio.html', {
      scope: $rootScope,
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalModificarAudio = modal;
    });

    $scope.openModalModificarAudio = function(){
      //hacer que el mapa no sea clickeable.
      if( ionic.Platform.isAndroid() )
        $rootScope.map.setClickable(false);  
      
      $rootScope.modalModificarAudio.show();
    }

    //Modal Modificar Comentario...
    $ionicModal.fromTemplateUrl('templates/modificarComentario.html', {
      scope: $rootScope,
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalModificarComentario = modal;
    });

    $scope.openModalModificarComentario = function(){
      //hacer que el mapa no sea clickeable.
      if( ionic.Platform.isAndroid() )
        $rootScope.map.setClickable(false);  
      
      $rootScope.modalModificarComentario.show();
    }
    
    //Modal Modificar Calificar...
    $ionicModal.fromTemplateUrl('templates/modificarCalificar.html', {
      scope: $rootScope,
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalModificarCalificar = modal;
    });

    $scope.openModalModificarCalificar = function(){
      //hacer que el mapa no sea clickeable.
      if( ionic.Platform.isAndroid() )
        $rootScope.map.setClickable(false);  
      
      $rootScope.modalModificarCalificar.show();
    }
    */
    /*---------------------------- MODIFICAR -------------------------------*/
}]);