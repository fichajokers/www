angular.module('starter.controllers')


.controller('modalRegistroCtrl', ['$scope','$http','$state','ionicDatePicker','$ionicNativeTransitions','$cordovaToast','$httpParamSerializer','$rootScope','$cordovaFileTransfer','$cordovaCamera','$ionicLoading',function($scope,$http,$state,ionicDatePicker,$ionicNativeTransitions,$cordovaToast,$httpParamSerializer,$rootScope,$cordovaFileTransfer,$cordovaCamera,$ionicLoading) {

	$scope.registro          = [];
	$scope.sexoSelecionado   = "Sexo";
  $scope.paisSelecionado  = "PAIS";
	$scope.fechaSeleccionado = "FECHA NACIMIENTO";
	$scope.selectables       = ['Masculino', 'Femenino'];
  $scope.nombre_pais       = [];
  $scope.id_pais           = [];
  $scope.rutaImagen        = "";
  $scope.nombreImagen      = "";
  $scope.registro          = [];

  /**
   * [Rescatar todos los países desde la báse de datos.]
   * 
   * @return {[Array]}  $scope.nombre_pais [Array con nombre de paises]
   * @return {[Array]}  $scope.id_pais     [array con id del nombre de paises]
   */
  
  $scope.cerrarModalModalRegistro = function(){   
    $rootScope.modalRegistro.hide();
  }

  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/paises.php",
    data: $httpParamSerializer({}),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si guardó correctamente.
    //console.log(response.data.pais);
    for(var i=0;i<response.data.pais.length;i++){
      $scope.nombre_pais.push( response.data.pais[i].nombre_pais );
      $scope.id_pais.push( response.data.pais[i].id_pais );
    }


    //console.log($scope.paises);
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });

  /**
   * [Mostrar 'cargando' cuando se esta cargando la vista]
   */
	$scope.$on('$ionicView.afterEnter', function(){
    setTimeout(function(){
      //document.getElementById("containerLoading").style.display = "none";
      document.getElementById("containerLoading").className = "fadeOut";
    },100);
  });
  
  /**
   * [cambiar el valor del select 'vista']
   */
  $scope.selectSexo = function(nuevo,viejo){
  	//console.log(viejo+" "+nuevo);
  	$scope.sexoSelecionado = nuevo;
  }

  /**
   * [cambiar el valor del select 'vista']
   */
  $scope.selectPais = function(nuevo,viejo){
    //console.log(viejo+" "+nuevo);
    $scope.paisSelecionado = nuevo;
  }

  /**
   * [Obtener y dar formato a la fecha de nacimientto]
   * @return {[String]}  $scope.fechaSeleccionado [fecha cno formato 'DD/MM/YYYY']
   */
  var ipObj1 = {
    callback: function (val) {  //Mandatory
      console.log('Return value from the datepicker popup is : ' + val, new Date(val));
		  var d                 = new Date(val)
			var curr_date         = d.getDate();
			var curr_month        = d.getMonth() + 1; //Months are zero based
			var curr_year         = d.getFullYear();
			$scope.registro.fechaNac = curr_date+"/"+curr_month+"/"+curr_year;
			$scope.fechaSeleccionado = $scope.registro.fechaNac;
    },
    inputDate: new Date(),      //Optional
    mondayFirst: true,          //Optional
    closeOnSelect: false,       //Optional
  };

  $scope.openDatePicker = function(){
    ionicDatePicker.openDatePicker(ipObj1);
  };

  $scope.cancelar = function(){
  	$scope.cerrarModalModalRegistro();
    /*$ionicNativeTransitions.stateGo('login', {inherit:false}, {
    	"type": "slide",
    	"direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
    	"duration": 400, // in milliseconds (ms), default 400
	  });*/
  }

  $scope.registrarse = function(registro){
    $ionicLoading.show({
      template: 'Cargando...'
    });

    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    navigator.geolocation.getCurrentPosition(
      function(position){
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        $scope.registrarseGeo(lat,long,registro);
      },function(err) {
        $scope.registrarseGeo(0,0,registro);
      },posOptions
    );
  }

  /**
   * [accion cuando se registra el usuario]
   * @param  {[Array]} registro [array con toda la info del formulario]
   */
  $scope.registrarseGeo = function(lat,long,registro){
    //validar que esten los campos llenos...
    console.log(registro);
    flagCamposLlenos = 0;
    var count = 0;
    for (var p in registro) {
      registro.hasOwnProperty(p) && count++;
    }

    if( registro.correo==null    || registro.correo== ""  || 
        registro.nombre==null    || registro.nombre== ""  || 
        registro.paterno==null   || registro.paterno== "" || 
        registro.materno==null   || registro.materno== "" ||
        registro.password2==null || registro.password2== ""){
      $rootScope.toast("debes llenar todos los campos","short");
      flagCamposLlenos = 1;
    }
    //validar password
    if( flagCamposLlenos == 0 ){
      if( registro.password == registro.password2){

        $ionicLoading.show({
          template: 'Cargando...'
        });

        var options = {
          fileKey: "perfil",
          fileName: $scope.nombreImagen,
          chunkedMode: false
        };

        $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirImagen.php", $scope.rutaImagen, options).then(function(result) {
            console.log("SUCCESS:")
            console.log(result.response);
        }, function(err) {
          console.log("ERROR: ");
          console.log(err);
        }, function (progress) {
            // constant progress updates
        });
        
        if(registro.sexo == "Masculino")
          registro.sexo = "M";
        else if ( registro.sexo == "Femenino" )
          registro.sexo = "F";

        for (var i=0;i<$scope.nombre_pais.length; i++) {
          if( $scope.nombre_pais[i] == registro.pais)
            registro.pais = $scope.id_pais[i]
        }

        registro.correo = angular.lowercase(registro.correo);
        
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacama/public/usuario/guardarUsuarioMovil",
          data: $httpParamSerializer({
            "correo"     : registro.correo,
            "nombre"     : registro.nombre,
            "apellido_p" : registro.paterno,
            "apellido_m" : registro.materno,
            "password"   : registro.password,
            "pais"       : registro.pais,
            "genero"     : registro.sexo,
            "nacimiento" : registro.fechaNac,
            "foto"       : $scope.nombreImagen,
            "lat"        : lat,
            "long"       : long
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente.
          console.log(response.data.resultado);
          if(response.data.resultado == "Registrado"){
            $rootScope.toast("Usuario registrado correctamente","short");
            $http({
              method: "POST",
              url: "http://200.14.68.107/atacama/public/usuario/mensajeBienvenidaMovil",
              data: $httpParamSerializer({
                "nombre"   : registro.nombre + " " + registro.paterno + " " + registro.materno,
                'correo'   : registro.correo,
                'pass'     : registro.password
              }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ //ok si recato bien los datos
              console.log(response);
              $rootScope.toast("Contraseña enviada al correo","short");
            }, function(){ //Error de conexión
              $rootScope.toast('Verifica tu conexión a internet', 'short');
            });

            $ionicLoading.hide();
            $scope.cerrarModalModalRegistro();

            //ir al Login
            /*$ionicNativeTransitions.stateGo('login', {inherit:false}, {
              "type": "slide",
              "direction": "up",
              "duration": 400,
            });*/
          }
          else
            $rootScope.toast("Usuario ya se encuentra registrado","short");
            $ionicLoading.hide();
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet',"short");
          $ionicLoading.hide();
        });
      }else{
        $rootScope.toast("Contraseñas no coinciden","short");
        $ionicLoading.hide();
      }    
    }
     
  }

  $scope.choosePhoto = function () {
    var options = {
      destinationType : Camera.DestinationType.FILE_URI,
      sourceType      : Camera.PictureSourceType.PHOTOLIBRARY,
      mediaType       : Camera.MediaType.PICTURE,
      correctOrientation : true
    };
   
    $cordovaCamera.getPicture(options).then(function (imageData) {        
      imageData = imageData.replace(/^(.+?\.(png|jpe?g)).*$/i, '$1');

      var image = document.getElementById('imagenItemRegistro');
      image.style.display   = 'block';
      image.src             = imageData;
      $scope.rutaImagen     = imageData;
      $scope.tieneSrc = true;
      $scope.nombreImagen   = Math.floor((Math.random() * 9999999) + 1)+"_"+imageData.substr(imageData.lastIndexOf('/') + 1);
    }, function (err) {
        //alert(err);
        // An error occured. Show a message to the user
    });
  }
  
}]);