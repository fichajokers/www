angular.module('starter.controllers')


.controller('homeCtrl', ['$scope','$http','$state','$ionicNativeTransitions','$ionicPopup','$rootScope','$ionicModal','$cordovaGeolocation','$cordovaToast','$httpParamSerializer','$ionicPopover','$ionicPlatform',function($scope,$http,$state,$ionicNativeTransitions,$ionicPopup,$rootScope,$ionicModal,$cordovaGeolocation,$cordovaToast,$httpParamSerializer,$ionicPopover,$ionicPlatform) {

  //Variable para que no se haga zoom a un marcador al mostrar por primera vez el mapa
  $scope.auxMarcador1                     = 0;
  //cambiar menu si se selecciona crear marcador,(cambiara a true la variable)
  //que es la funcion openModalFiguraGps().
  $scope.btnCrearMarcador                 = false;
  
  //contador cantidad de veces que entra a watch position
  $scope.watchPositionCont                = 0;
  $scope.miRuta                           = [];
  $rootScope.id_item_turistico            = 0;
  $scope.marcadores                       = [];
  $scope.markerArray                      = [];
  
  $scope.patrimonios                      = [];
  $scope.markerArrayPatrimonio            = [];
  
  $scope.serviciosTuristicos              = [];
  $scope.markerArrayServiciosTuristicos   = [];
  
  $scope.latLng                           = [];
  $rootScope.map                          = [];
  $rootScope.id_item_turistico_modificar  = null;
  $rootScope.modalMenuAccionesModificar   = null;
  $scope.overlaytype                      = 0;
  
  $rootScope.tipo_seleccionado            = "";
  
  $rootScope.patrimonio_seleccionado      = "";
  $rootScope.servicio_seleccionado        = "";
  
  $scope.showHideItem                     = false; //false oculto true visible
  $scope.showHidePatrimonio               = false; //0 oculto 1 visible
  $scope.showHideServicio                 = false; //0 oculto 1 visible
  
  $scope.sub_tipo_atractivo               = [];
  $scope.tipo_servicio                    = [];
  $scope.patrimonios_seleccionados        = [];
  $scope.servicios_seleccionados          = [];
  $scope.seleccionadoMostrarTodos         = false;
  $scope.seleccionadoMostrarTodosServicio = false;
  $scope.auxiliarPantalla                 = 0;
  $scope.auxLatPantalla                   = 0;
  $scope.auxLngPantalla                   = 0;
  $scope.agregarPatrimonios               = 0;
  $scope.agregarServicios                 = 0;

  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/getMenuPatrimonio.php",
    data: $httpParamSerializer({}),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si recato bien los datos
      //console.log(response);
      if(response.data.resultado == "ok"){
        $scope.sub_tipo_atractivo = response.data.sub_tipo_atractivo;
      }
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });

  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/getMenuServicios.php",
    data: $httpParamSerializer({}),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si recato bien los datos
      if(response.data.resultado == "ok"){
        $scope.tipo_servicio = response.data.tipo_servicio;
      }
  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });

  document.addEventListener("deviceready", function() {
    var div = document.getElementById("map_canvas");
    var COPIAPO                = new plugin.google.maps.LatLng(-27.3690175,-70.6756632);

    // Initialize the map view
    $rootScope.map = plugin.google.maps.Map.getMap(div,{
      'backgroundColor' : 'white',
      'mapType'         : plugin.google.maps.MapTypeId.ROADMAP,
      'controls' : {
        'compass'          : true,
        'myLocationButton' : true,
        'indoorPicker'     : true,
        'zoom'             : true
      },
      'gestures': {
        'scroll' : true,
        'tilt'   : true,
        'rotate' : true,
        'zoom'   : true
      },
      'camera': {
        'latLng' : COPIAPO,
        'tilt'   : 0,
        'zoom'   : 10,
        'bearing': 0
      }
    });

    // Wait until the map is ready status.
    $rootScope.map.addEventListener(plugin.google.maps.event.MAP_READY, onMapReady);
  }, false);

    function onMapReady() {
    }

    function onBtnClicked() {
      map.showDialog();
    }

    //conseguir coordenadas de item_turistico
    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/getItemTuristico.php",
      data: $httpParamSerializer({}),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ 
        if(response.data.resultado == "ok"){
          //console.log(response.data.item_turistico);
          $scope.marcadores = response.data.item_turistico;
          $scope.watchMarcadores();
        }
    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
    });

    $scope.mostrarMarcadores = function(){
      //var contador = 0  //contador que indica si es la primera repeticion del watPosition..
      document.getElementById("containerLoading").className = "fadeIn";
      if($scope.showHideItem == false){ //está oculto, por lo tanto mostrarlo....
        $scope.showHideItem = true;  
      }else{ //esta visible, por lo tanto ocultarlo
        $scope.showHideItem = false;
      }

    }

    //mostrar la figura del gps, para crear un item turístico
    $scope.openModalFiguraGps = function(){
      document.getElementById("contenedorMarcador").className = "fadeIn";
      $scope.btnCrearMarcador = true;
    }

    //se oculta la figura del gps, cuando ya se creó o se cancelo 
    //el crear item turístico
    $scope.cerrarModalFiguraGps = function(){
      document.getElementById("contenedorMarcador").className = "fadeOut";
      $scope.btnCrearMarcador = false;
    }

    //Modal Crear nuevo Marcador (itemTurístico)...
    $ionicModal.fromTemplateUrl('templates/crearMarcador.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalCrearMarcador = modal;
    });

    //Popover con las rutas
    $ionicPopover.fromTemplateUrl('templates/menuRutas.html', {
        scope: $scope,
        animation: 'slide-in-up',
    }).then(function(popover) {
        $scope.popover = popover;
    });

    // Triggered on a button click, or some other target
    $scope.openPopover = function($event) {
      //hacer que el mapa no sea clickeable.
      //if( ionic.Platform.isAndroid() )
        $rootScope.map.setClickable(false);

      $scope.popover.show($event);

    }; 

    // Execute action on hide popover 
    // (cuando cierro el popover hacer clickeable el mapa)
    $scope.$on('popover.hidden', function() {
      // hacer clickeable el mapa
      $rootScope.map.setClickable(true);
    });
    
    //Popover menu patrimonios
    $ionicPopover.fromTemplateUrl('templates/menuPatrimonios.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popoverPatrimonios = popover;
    });

    // Triggered on a button click, or some other target
    $scope.openPopoverPatrimonios = function($event) {
      //hacer que el mapa no sea clickeable.
      //if( ionic.Platform.isAndroid() )
        $rootScope.map.setClickable(false);

      $scope.popoverPatrimonios.show($event);

    };


    // Execute action on hide popover
    $scope.$on('popover.hidden', function() {
      // hacer clickeable el mapa
      $rootScope.map.setClickable(true);
    });
    
    //Popover menuServicios
    $ionicPopover.fromTemplateUrl('templates/menuServicios.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popoverServicios = popover;
    });

    // Triggered on a button click, or some other target
    $scope.openPopoverServicios = function($event) {
      //hacer que el mapa no sea clickeable.
      //if( ionic.Platform.isAndroid() )
        $rootScope.map.setClickable(false);

      $scope.popoverServicios.show($event);

    };

    //abrir el modal cuando apretamos un item, patrimonio, servicio
    //en el mapa
    $scope.openModalMenuAccionesModificar = function(){

      //Modal menu Acciones Modiciar...
      $ionicModal.fromTemplateUrl('templates/menuAccionesModificar.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalMenuAccionesModificar = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);

        $rootScope.modalMenuAccionesModificar.show();
      
      });
    }

    $scope.a = function(){
      alert("ASdads");
    }

    //Modal menu Acciones... (igual al modal menuAccionesModificar),
    //pero este es para crear un nuevo item turístico, por lo tanto no es
    //modificar, sino agregar.
    $ionicModal.fromTemplateUrl('templates/menuAcciones.html', {
      scope: $rootScope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $rootScope.modalMenuAcciones = modal;
    });

    $scope.openModalMenuAcciones = function(){
      document.getElementById("contenedorMarcador").className = "fadeOut";
      //ahora hay que crear el marcador con la latitud y longitud seleccionada 
      //conseguimos la latitud y longitud seleccionada
      if( true ){
        $rootScope.map.getCameraPosition(function(camera) {
          var lat = camera.target.lat;
          var lng = camera.target.lng;
          
          $rootScope.POS_MARCADOR = new plugin.google.maps.LatLng(lat, lng);
          
          //obtenemos la direccion del lugar....
          var request = {
            'position': $rootScope.POS_MARCADOR
          };

          plugin.google.maps.Geocoder.geocode(request, function(results) {
            if (results.length) {
              var result = results[0];
              var position = result.position; 
              var address = {
                numero: result.subThoroughfare || "",
                calle: result.thoroughfare || "",
                ciudad: result.locality || "",
                region: result.adminArea || "",
                postal: result.postalCode || "",
                pais:   result.country || ""
              };
              
              //GUARDAR LA DIRECCION ...
              $http({
                method: "POST",
                url: "http://200.14.68.107/atacamaGo/addItemTuristico.php",
                data: $httpParamSerializer({
                  "id_usuario": localStorage.getItem("id_usuario"),
                  "direccion_georeferenciada_longitud" : lng,
                  "direccion_georeferenciada_latitud" : lat,
                  "direccion_item" : address.callle,
                  "numero_direccion_item" : address.numero,
                  "ciudad" : address.ciudad
                }),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
              }).then(function(response){ //ok si guardó correctamente.
                //console.log(response);
                if(response.data.resultado == "ok"){
                  //guardar el id_item_turistico para guardar la descripcion titulos videos etc.
                  $rootScope.id_item_turistico = response.data.id_item_turistico;
                  var id_item_turistico_creado_usuario = response.data.id_item_turistico;

                  /*creamos el marcador...*/
                  var POS_MARCADOR = new plugin.google.maps.LatLng(lat, lng);
                   
                  $rootScope.map.addMarker({
                    'position': POS_MARCADOR,
                    icon: 'darkcyan'
                  },function(marker) {
                      //accion al hacer clic en el marcador
                      
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        var marcadorNuevo = {};
                        $http({
                          method: "POST",
                          url: "http://200.14.68.107/atacamaGo/getItemTuristicoNuevo.php",
                          data: $httpParamSerializer({
                            "id_item_turistico": id_item_turistico_creado_usuario,
                            "id_usuario":localStorage.getItem("id_usuario")
                        }),
                          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
                        }).then(function(response){ //ok si guardó correctamente.
                          //console.log(response);
                          if(response.data.resultado == "ok"){
                            
                            marcadorNuevo = {
                              "descripcion"                        : response.data.item_turistico.descripcion,
                              "nombre"                             : response.data.item_turistico.nombre,
                              "direccion_georeferenciada_latitud"  : "",
                              "direccion_georeferenciada_longitud" : "",
                              "foto"                               : response.data.item_turistico.foto,
                              "id"                                 : id_item_turistico_creado_usuario
                            };

                            $rootScope.tipo_seleccionado = "item";
                            $rootScope.item_seleccionado = marcadorNuevo;
                            $scope.openModalMenuAccionesModificar();

                          }else if ( response.data.resultado == "no data" ){
                            $rootScope.toast('lugar aún no tiene audios', 'short');
                          }else{
                            $rootScope.toast('error, intenta nuevamente', 'short');
                          }
                        }, function(){ //Error de conexión
                          $rootScope.toast('Verifica tu conexión a internet', 'short');
                        });

                      });   
                  });
                  /*fin crear marcador...*/
                  $scope.cerrarModalFiguraGps();
                  $rootScope.toast('Item agregado', 'short');
                }
                else
                  $rootScope.toast('error, intenta nuevamente', 'short');

              }, function(){ //Error de conexión
                  $rootScope.toast('Verifica tu conexión a internet', 'short')
              });
              //GUARDAR LA DIRECCION ...       
            } else {
              $rootScope.toast('Verifica tu conexión a internet', 'short')
            }
          });

        });
      }

      //hacer que el mapa no sea clickeable.
      //if( ionic.Platform.isAndroid() )
        $rootScope.map.setClickable(false);    
      
      $rootScope.modalMenuAcciones.show();

    }

    //Mostrar patrimonios, ocurre cuando se aprieta "mostrar todos" o alguna
    //categoria de patrimionios....
    $scope.mostrarPatrimonios = function(id_sub_tipo_atractivo,index){
      document.getElementById("containerLoading").className = "fadeIn";

      //Cambiar clase al elemento seleccionado
      if( index != -1 ){
        if($scope.sub_tipo_atractivo[index].seleccionado == false)
          $scope.sub_tipo_atractivo[index].seleccionado = true;
        else
          $scope.sub_tipo_atractivo[index].seleccionado = false;
      }

      //Evaluar si se "presionó todos los patrimonios"
      if(id_sub_tipo_atractivo == -1){
        //seleccionar todos los sub tipos atractivos....
        if( $scope.seleccionadoMostrarTodos == false){
          $scope.seleccionadoMostrarTodos = true;
          //además SELECCIONAR todos los elementos
          angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
            value.seleccionado = true;
          });
        }else{
          $scope.seleccionadoMostrarTodos = false;
          //ademas DES SELECCIONAR todos los elementos....
          angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
            value.seleccionado = false;
          });

          //elimnar marcadores de patrimonio...
          $scope.patrimonios = [];
        }
        
        //Cambiar todas los botones a activos...
        var menuPatrimonio   = document.getElementsByClassName("menuPatrimonio");
        menuPatrimonio.class = "menuPatrimonio";
      } 

      $scope.showHidePatrimonio = false;
      if($scope.showHidePatrimonio == false){ //está oculto, por lo tanto mostrarlo....
        //console.log("Llamar a php");
        //conseguir patrimonios

        if( id_sub_tipo_atractivo == -1){
          //console.log($scope.patrimonios_seleccionados.length, $scope.sub_tipo_atractivo.length + 1);
          if($scope.patrimonios_seleccionados.length == $scope.sub_tipo_atractivo.length+1){
            //estan todos seleccionados, por lo tanto vaciar el arreglo
            $scope.patrimonios_seleccionados = [];
          }else{

            $scope.patrimonios_seleccionados = [];
            angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
              $scope.patrimonios_seleccionados.push(value.id_sub_tipo_atractivo)     
            });
          }
        }

        if( $scope.patrimonios_seleccionados.length == 0 ){
          $scope.patrimonios_seleccionados.push(id_sub_tipo_atractivo);  
        }else{
          if($scope.patrimonios_seleccionados.indexOf(id_sub_tipo_atractivo) == -1) {
            $scope.patrimonios_seleccionados.push(id_sub_tipo_atractivo);  
          }else
            $scope.patrimonios_seleccionados.splice($scope.patrimonios_seleccionados.indexOf(id_sub_tipo_atractivo), 1);
        }

        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getPatrimonioNuevo.php",
          data: $httpParamSerializer({
            id_sub_tipo_atractivo     : id_sub_tipo_atractivo,
            patrimonios_seleccionados : JSON.stringify($scope.patrimonios_seleccionados),
            id_usuario                : localStorage.getItem("id_usuario")
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente
            if(response.data.resultado == "ok"){
              //console.log("Lista patrimoinios: ");
              //console.log(response.data);

              var actualizarMarcadores = true;
              //evaluar si se rescatan patrimoinos o no....
              
              if( $scope.seleccionadoMostrarTodos == false ){
                var contatorMarcadores = 0;
                angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
                  
                  if( value.seleccionado == false ){
                    contatorMarcadores++;
                  }
                });
                
              }

              if( contatorMarcadores == $scope.sub_tipo_atractivo.length ){
                $scope.patrimonios_seleccionados = [];
                $scope.showHidePatrimonio = false;
                
                $scope.patrimonios = [];
              }else{
                 $scope.patrimonios = response.data.patrimonio;
                 $scope.showHidePatrimonio = true; //mantener visto...
              }
              
              console.log( $scope.patrimonios );

              $scope.agregarPatrimonios = 1;

              $rootScope.map.clear();
              $rootScope.map.off();

              $scope.markerArrayPatrimonio = [];

              angular.forEach($scope.patrimonios, function(value, key) {

                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "www/img/patrimonios/"+value.icono+".png";
                  var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayPatrimonio[key] = marker;
                    $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                    $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "patrimonio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });
                });

                $scope.markerArrayServiciosTuristicos = [];

                angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                 
                      var ubicacion = new plugin.google.maps.LatLng(
                        parseFloat(value.direccion_georeferenciada_latitud),
                        parseFloat(value.direccion_georeferenciada_longitud) );

                      //var url = "www/img/servicios/"+value.icono+".png";
                      var url = "www/img/servicios/"+value.icono+"Oculto.png";

                      $rootScope.map.addMarker({
                        'position': ubicacion,
                        'title': value.nombre_item_turistico,
                        icon: {
                          url: url,
                          size: { width: 30, height: 45 },
                          anchor:  [10, 10],
                        },
                        zIndex: 1
                      },function(marker) {
                          //accion al hacer clic en el marcador
                          $scope.markerArrayServiciosTuristicos[key] = marker;
                          marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                            $rootScope.tipo_seleccionado = "servicio";
                            $rootScope.item_seleccionado = value;
                            $scope.openModalMenuAccionesModificar();
                          });
                      });
                      
                });
                $scope.agregarPatrimonios = 0;
                document.getElementById("containerLoading").className = "fadeOut";
            }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });
        
      }else{ //esta visible, por lo tanto ocultarlo

      }
    }

    //Mostrar Servicios, ocurre cuando se aprieta "mostrar todos" o alguna
    //categoria de servicios....
    $scope.mostrarServiciosTuristicos = function(id_tipo_servicio,index){
      document.getElementById("containerLoading").className = "fadeIn";
      
      //Cambiar clase al elemento seleccionado
      if( index != -1 ){
        if($scope.tipo_servicio[index].seleccionado == false)
          $scope.tipo_servicio[index].seleccionado = true;
        else
          $scope.tipo_servicio[index].seleccionado = false;
      }

      //Evaluar si se "presionó todos los patrimonios"
      if(id_tipo_servicio == -1){
        //seleccionar todos los sub tipos servicios....
        if( $scope.seleccionadoMostrarTodosServicio == false){
          $scope.seleccionadoMostrarTodosServicio = true;
          //además SELECCIONAR todos los elementos
          angular.forEach($scope.tipo_servicio, function(value, key) {
            value.seleccionado = true;
          });
        }else{
          $scope.seleccionadoMostrarTodosServicio = false;
          //ademas DES SELECCIONAR todos los elementos....
          angular.forEach($scope.tipo_servicio, function(value, key) {
            value.seleccionado = false;
          });

          //elimnar marcadores de patrimonio...
          $scope.serviciosTuristicos = [];
        }
        
        //Cambiar todas los botones a activos...
        var menuServicios   = document.getElementsByClassName("menuServicios");
        menuServicios.class = "menuServicios";
      } 

      $scope.showHideServicio = false;
      if($scope.showHideServicio == false){ //está oculto, por lo tanto mostrarlo....

        if( id_tipo_servicio == -1){
          if($scope.servicios_seleccionados.length == $scope.tipo_servicio.length+1){
            //estan todos seleccionados, por lo tanto vaciar el arreglo
            $scope.servicios_seleccionados = [];
          }else{

            $scope.servicios_seleccionados = [];
            angular.forEach($scope.tipo_servicio, function(value, key) {
              $scope.servicios_seleccionados.push(value.id_tipo_servicio)     
            });
          }
        }
        /*---------------------------------*/
        if( $scope.servicios_seleccionados.length == 0 ){
          $scope.servicios_seleccionados.push(id_tipo_servicio);
        }else{
          if($scope.servicios_seleccionados.indexOf(id_tipo_servicio) == -1) {
            $scope.servicios_seleccionados.push(id_tipo_servicio);
          }else
            $scope.servicios_seleccionados.splice($scope.servicios_seleccionados.indexOf(id_tipo_servicio), 1);
        }

        //conseguir Servivios tuiristicos
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getServiciosTuristicosNuevo.php",
          data: $httpParamSerializer({
            id_tipo_servicio        : id_tipo_servicio,
            servicios_seleccionados : JSON.stringify($scope.servicios_seleccionados),
            id_usuario                : localStorage.getItem("id_usuario")
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente
            if(response.data.resultado == "ok"){
              console.log(response.data);

              var actualizarMarcadoresServicios = true;
              //evaluar si se rescatan servicios o no....
              
              if( $scope.seleccionadoMostrarTodosServicio == false ){
                var contatorMarcadoresServicio = 0;
                angular.forEach($scope.tipo_servicio, function(value, key) {
                  
                  if( value.seleccionado == false ){
                    contatorMarcadoresServicio++;
                  }
                });     
              }

              if( contatorMarcadoresServicio == $scope.tipo_servicio.length ){
                $scope.servicios_seleccionados = [];
                $scope.showHideServicio        = false;
                $scope.serviciosTuristicos     = [];
              }else{
                 $scope.serviciosTuristicos    = response.data.servicios;
                 $scope.showHideServicio = true; //mantener visto...
              }
              
              $scope.agregarServicios = 1;
              
              $rootScope.map.clear();
              $rootScope.map.off();
              $scope.markerArrayServiciosTuristicos = [];

              angular.forEach($scope.serviciosTuristicos, function(value, key) {  
               
                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/servicios/"+value.icono+".png";
                    var url = "www/img/servicios/"+value.icono+"Oculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                        anchor:  [10, 10],
                      },
                      zIndex: 1
                    },function(marker) {
                        //accion al hacer clic en el marcador
                        $scope.markerArrayServiciosTuristicos[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "servicio";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                        });
                    });
                    
              });

              $scope.markerArrayPatrimonio = [];

              angular.forEach($scope.patrimonios, function(value, key) {

                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "www/img/patrimonios/"+value.icono+".png";
                  var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayPatrimonio[key] = marker;
                    $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                    $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "patrimonio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });
              });

                document.getElementById("containerLoading").className = "fadeOut";

            }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });

      }else{ //esta visible, por lo tanto ocultarlo

      }
    }

    $scope.distanciaEntreCoordenadas = function(lat1,long1,lat2,long2) {
      
      var degtorad = 0.01745329;
      var radtodeg = 57.29577951;
      
      var lat1h    = lat1;
      var lat2h    = lat2;
      var long1h   = long1;
      var long2h   = long2;
      var lat1     = parseFloat(lat1h);
      var lat2     = parseFloat(lat2h);
      var long1    = parseFloat(long1h);
      var long2    = parseFloat(long2h);

      
      var dlong  = (long1 - long2);
      var dvalue = (Math.sin(lat1 * degtorad) * Math.sin(lat2 * degtorad))
            + (Math.cos(lat1 * degtorad) * Math.cos(lat2 * degtorad)
            * Math.cos(dlong * degtorad));
      var dd     = Math.acos(dvalue) * radtodeg;
      var miles  = (dd * 69.16);
      miles      = (miles * 100)/100;
      var km     = (dd * 111.302);
      km         = (km * 100)/100;

      return km*1000;
    }

    $scope.watchMarcadores = function(){
      var contador = 0  //contador que indica si es la primera repeticion del watPosition..
       
        //preguntar por la ubicacion del usuario
        var watch = $cordovaGeolocation.watchPosition({
          timeout : 30000,
          enableHighAccuracy: false // may cause errors if true
        }).then(
          null,
          function(err) {
            console.log(err);
              $cordovaToast.show('Por favor activa el GPS', 'short', 'bottom').then(function(success) {
            }, function (error) {
              console.log("The toast was not shown due to " + error);
            });
          },
          function(position) {
            
            var lat  = position.coords.latitude;
            var long = position.coords.longitude;
            
            if( $scope.showHideItem == true ){ //si está visible....  
              //Calcular distancia entre items( desde bd) y mi posicion
              var distancia = $scope.distanciaEntreCoordenadas(
                $scope.lat,
                $scope.long,
                value.direccion_georeferenciada_latitud,
                value.direccion_georeferenciada_longitud);  

              /*angular.forEach($scope.marcadores, function(value, key) {

                if(distancia <= $rootScope.radio){ //dentro del rango...
                  //console.log("dentro del rango, distancia: "+distancia+" , nombre marker: "+value.nombre_item_turistico);
                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: 'www/img/posicionGpsMarcador.png',
                      size: { width: 25, height: 35 },
                      anchor:  [10, 10],
                    }
                  },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArray[key] = marker;
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "item";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                  });
                  
                }else{
                  //console.log("Fuera del rango, distancia: "+distancia+" , nombre marker: "+value.nombre_item_turistico);
                  /*if ($scope.markerArray[key]){
                    //$scope.markerArray[key].remove();
                  }
                 
                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: 'www/img/posicionGpsTuristaOculto.png',
                      size: { width: 25, height: 35 },
                      anchor:  [10, 10],
                    },
                      zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArray[key] = marker;
                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "item";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });   

                }//FIN ELSE
              });
              //FIN ANGULAR FOREACH*/
              /*console.log("antes del foreach");
              angular.forEach($scope.patrimoniosRutaDinamica, function(value, key) {

                  console.log("Dentro del foreach 3c3c");

                  var distancia = $scope.distanciaEntreCoordenadas(
                    lat,
                    long,
                    value.direccion_georeferenciada_latitud,
                    value.direccion_georeferenciada_longitud); 

                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "http://200.14.68.107/atacama/public/"+value.ruta_icono_servicio;
                  if(distancia <= $rootScope.radio)
                    var url = "www/img/patrimonios/"+value.icono+".png";
                  else
                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayPatrimonioRutaDinamica[key] = marker;
                    //$scope.markerArrayPatrimonio[key].setVisible(false);
                    $scope.markerArrayPatrimonioRutaDinamica[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                    $scope.markerArrayPatrimonioRutaDinamica[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "patrimonio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });
                });

              document.getElementById("containerLoading").className = "fadeOut";
            }else{
              $rootScope.map.clear();
              $rootScope.map.off();
              $scope.markerArrayPatrimonioRutaDinamica = [];
              //console.log("esta visible, se ocultará");
              /*angular.forEach($scope.marcadores, function(value, key) {
                if($scope.markerArray[key])
                  $scope.markerArray[key].remove();
              });*/
            }
/*---------------------------------------------------------------------------------------------*/
//console.log($scope.showHidePatrimonio, $scope.showHideItem);
            /*if( $scope.showHidePatrimonio == true || $scope.showHideItem == true){ //si está visible....

              if( $scope.agregarPatrimonios == 1 ){
                console.log("patrimoinio Activos....");  
              
                $rootScope.map.clear();
                $rootScope.map.off();
                $scope.markerArrayPatrimonio = [];

                angular.forEach($scope.patrimonios, function(value, key) {

                  var distancia = $scope.distanciaEntreCoordenadas(
                    lat,
                    long,
                    value.direccion_georeferenciada_latitud,
                    value.direccion_georeferenciada_longitud); 

                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "http://200.14.68.107/atacama/public/"+value.ruta_icono_servicio;
                  if(distancia <= $rootScope.radio)
                    var url = "www/img/patrimonios/"+value.icono+".png";
                  else
                    var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayPatrimonio[key] = marker;
                    //$scope.markerArrayPatrimonio[key].setVisible(false);
                    $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                    $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "patrimonio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });
                });

                $scope.agregarPatrimonios = 0;
                document.getElementById("containerLoading").className = "fadeOut";
              }

              //cambiar  a icono oculto o normaldependiendo de la distancia....
              angular.forEach($scope.markerArrayPatrimonio, function(value, key) {
                var distancia = $scope.distanciaEntreCoordenadas(
                    lat,
                    long,
                    value.direccion_georeferenciada_latitud,
                    value.direccion_georeferenciada_longitud);

                if( $scope.markerArrayPatrimonio[key] ){
                  if(distancia <= $rootScope.radio){
                    $scope.markerArrayPatrimonio[key].setIcon({
                      'url': "www/img/patrimonios/"+value.icono+".png",
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    });
                  }else{
                    $scope.markerArrayPatrimonio[key].setIcon({
                      'url': "www/img/patrimonios/"+value.icono+"_oculto.png",
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    });
                      
                  }
                }    
              });

              //console.log($scope.patrimonios);
              //document.getElementById("containerLoading").className = "fadeOut";
              //Calcular distancia entre items( desde bd) y mi posicion  
              /*angular.forEach($scope.patrimonios, function(value, key) {
                var distancia = $scope.distanciaEntreCoordenadas(
                  lat,
                  long,
                  value.direccion_georeferenciada_latitud,
                  value.direccion_georeferenciada_longitud);  

                if(distancia <= $rootScope.radio){ //dentro del rango...
                  
                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "http://200.14.68.107/atacama/public/"+value.ruta_icono_servicio;
                  var url = "www/img/patrimonios/"+value.icono+".png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    },
                    zIndex: 1
                  },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayPatrimonio[key] = marker;
                      $scope.markerArrayPatrimonio[key].setVisible(false);
                      $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                      $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "patrimonio";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                  });
                  
                }else{
                 
                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "http://200.14.68.107/atacama/public/"+value.ruta_icono_servicio_oculto;
                  var url = "www/img/patrimonios/"+value.icono+"_oculto.png";   

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayPatrimonio[key] = marker;
                    $scope.markerArrayPatrimonio[key].setVisible(false);
                    $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                    $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                    $scope.markerArrayPatrimonio[key].setIcon({
                      'url': "http://www.sadepi.cl/webequipo/assets/img/equipo/foto_fabian_cuad.jpg",
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    });

                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "patrimonio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });   

                }//FIN ELSE
              });
              //FIN ANGULAR FOREACH
            }else{
              if($scope.showHideServicio == false){
                $rootScope.map.clear();
                $rootScope.map.off();
                $scope.markerArrayPatrimonio = [];
                document.getElementById("containerLoading").className = "fadeOut";
              }
              
              //console.log("esta visible, se ocultará");
              /*angular.forEach($scope.patrimonios, function(value, key) {
                if( $scope.markerArrayPatrimonio[key] )
                $scope.markerArrayPatrimonio[key].remove();
              });*/
              
              /*console.log("borrar todos los patrimonios..");
              angular.forEach($scope.markerArrayPatrimonio, function(value, key) {
                if( $scope.markerArrayPatrimonio[key] ){
                  $scope.markerArrayPatrimonio[key].remove();  
                  $scope.markerArrayPatrimonio.splice($scope.markerArrayPatrimonio.indexOf(key), 1);
                }    
              });
              console.log("markerArray: ", $scope.markerArrayPatrimonio);
              
            }*/
/*---------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------*/
            /*if( $scope.showHideServicio == true || $scope.showHideItem == true ){ //si está visible....
              
              if( $scope.agregarServicios == 1 ){
                console.log("servicios Activos....");

                angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                  var distancia = $scope.distanciaEntreCoordenadas(
                    lat,
                    long,
                    value.direccion_georeferenciada_latitud,
                    value.direccion_georeferenciada_longitud);  
 

                 
                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );


                    //var url = "http://200.14.68.107/atacama/public/"+value.ruta_icono_servicio;

                    if(distancia <= $rootScope.radio)
                      var url = "www/img/servicios/"+value.icono+".png";
                    else
                      var url = "www/img/servicios/"+value.icono+"Oculto.png";
                    

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                        anchor:  [10, 10],
                      },
                      zIndex: 1
                    },function(marker) {
                        //accion al hacer clic en el marcador
                        $scope.markerArrayServiciosTuristicos[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "servicio";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                        });
                    });
                    
                });

                $scope.agregarServicios = 0;
                document.getElementById("containerLoading").className = "fadeOut";
              }
              
              //cambiar  a icono oculto o normaldependiendo de la distancia....
              angular.forEach($scope.markerArrayServiciosTuristicos, function(value, key) {
                var distancia = $scope.distanciaEntreCoordenadas(
                    lat,
                    long,
                    value.direccion_georeferenciada_latitud,
                    value.direccion_georeferenciada_longitud);

                if( $scope.markerArrayServiciosTuristicos[key] ){
                  if(distancia <= $rootScope.radio){
                    $scope.markerArrayServiciosTuristicos[key].setIcon({
                      'url': "www/img/servicios/"+value.icono+".png",
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    });
                  }else{
                    $scope.markerArrayServiciosTuristicos[key].setIcon({
                      'url': "www/img/servicios/"+value.icono+"Oculto.png",
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    });
                      
                  }
                }    
              });

              
              //Calcular distancia entre items( desde bd) y mi posicion
              /*angular.forEach($scope.serviciosTuristicos, function(value, key) {  
                var distancia = $scope.distanciaEntreCoordenadas(
                  lat,
                  long,
                  value.direccion_georeferenciada_latitud,
                  value.direccion_georeferenciada_longitud);  

                if(distancia <= $rootScope.radio){ //dentro del rango...
                  //console.log("dentro del rango, distancia: "+distancia+" , nombre marker: "+value.nombre_item_turistico);
                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );


                  var url = "http://200.14.68.107/atacama/public/"+value.ruta_icono_servicio;
                  

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    },
                    zIndex: 1
                  },function(marker) {
                      //accion al hacer clic en el marcador
                      $scope.markerArrayServiciosTuristicos[key] = marker;
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "servicio";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                      });
                  });
                  
                }else{
                 
                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  var url = "http://200.14.68.107/atacama/public/"+value.ruta_icono_servicio_oculto;

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayServiciosTuristicos[key] = marker;
                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "servicio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });   

                }//FIN ELSE
              });
              //FIN ANGULAR FOREACH
            }else{
              if($scope.showHidePatrimonio == false){
                $rootScope.map.clear();
                $rootScope.map.off();
                $scope.markerArrayServiciosTuristicos = [];
                document.getElementById("containerLoading").className = "fadeOut";
              }
              
              /*angular.forEach($scope.serviciosTuristicos, function(value, key) {
                if( $scope.markerArrayServiciosTuristicos[key] )
                $scope.markerArrayServiciosTuristicos[key].remove();
              });*/

              /*angular.forEach($scope.markerArrayServiciosTuristicos, function(value, key) {
                if( $scope.markerArrayServiciosTuristicos[key] )
                $scope.markerArrayServiciosTuristicos[key].remove();
              });
            }*/
/*---------------------------------------------------------------------------------------------*/


              //dibujar la ruta
              var posision = new plugin.google.maps.LatLng(lat, long);
              $scope.miRuta.push( posision );

              if( $scope.watchPositionCont > 0 ){
                
                $rootScope.map.addPolyline({
                points: [
                  $scope.miRuta[$scope.watchPositionCont],
                  $scope.miRuta[$scope.watchPositionCont-1]
                ],
                'color' : '#AA00FF',
                'width': 3,
                'geodesic': true
                }, function(polyline) {
                  /*setTimeout(function() {
                    polyline.remove();
                  }, 3000);*/
                });
            
          }
            
          $scope.watchPositionCont+=1;
          contador++;
          angular.forEach($scope.marcadores, function(value, key) {
            if( $scope.markerArray[key] )
            $scope.markerArray[key].remove();
          });
          
          /*angular.forEach($scope.patrimonios, function(value, key) {
            if( $scope.markerArrayPatrimonio[key] )
            $scope.markerArrayPatrimonio[key].remove();
          });*/
          
          /*angular.forEach($scope.serviciosTuristicos, function(value, key) {
            if( $scope.markerArrayServiciosTuristicos[key] )
            $scope.markerArrayServiciosTuristicos[key].remove();
          });*/
          

          /*$rootScope.map.getVisibleRegion(function(latLngBounds) {
            //console.log(latLngBounds.northeast.toUrlValue() + ", " + latLngBounds.southwest.toUrlValue());
            //console.log(latLngBounds.northeast.lat);
            //console.log(latLngBounds.northeast.lng);

            if ($scope.auxiliarPantalla == 0){
              $scope.auxLatPantalla = latLngBounds.northeast.toUrlValue();
              $scope.auxLngPantalla = latLngBounds.southwest.toUrlValue();

              $scope.auxiliarPantalla = 1;
            }else{
                if($scope.auxLatPantalla != latLngBounds.northeast.toUrlValue() &&
                   $scope.auxLngPantalla != latLngBounds.southwest.toUrlValue() ){

                  //actualizo las variables de lat y lng...
                  $scope.auxLatPantalla = latLngBounds.northeast.toUrlValue();
                  $scope.auxLngPantalla = latLngBounds.southwest.toUrlValue();

                  var asdq = 0;
                  //mostrar solo los patrimionios en pantalla
                  angular.forEach($scope.markerArrayPatrimonio, function(value, key) {
                    if( $scope.markerArrayPatrimonio[key] ){
                      //console.log($scope.markerArrayPatrimonio[key].lat, $scope.markerArrayPatrimonio[key].lng, latLngBounds.northeast.toUrlValue(), latLngBounds.southwest.toUrlValue());
                      if( $scope.markerArrayPatrimonio[key].lat <= latLngBounds.northeast.lat &&
                          $scope.markerArrayPatrimonio[key].lat >= latLngBounds.southwest.lat &&
                          $scope.markerArrayPatrimonio[key].lng <= latLngBounds.northeast.lng &&
                          $scope.markerArrayPatrimonio[key].lng >= latLngBounds.southwest.lng
                        ){
                          //Mostrar Patrimonio....
                          $scope.markerArrayPatrimonio[key].setVisible(true);
                          asdq++;
                      }else{
                        $scope.markerArrayPatrimonio[key].setVisible(false);
                      }
                    }
                  });

                  console.log(asdq);
                  asdq = 0;
                                  
                }
            }

          });*/

        });
        /* FIN WATCH POSITION*/
    }

    $ionicPlatform.onHardwareBackButton(function() {
      ionic.Platform.exitApp();
    });
}]);