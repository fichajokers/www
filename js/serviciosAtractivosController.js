angular.module('starter.controllers')

.controller('serviciosAtractivosCtrl', ['$scope','$http','$state','$ionicNativeTransitions','$ionicPopup','$rootScope','$ionicModal','$cordovaGeolocation','$cordovaToast','$httpParamSerializer','$ionicPopover','$ionicPlatform','$ionicLoading','$cordovaFile','$interval','$cordovaAppVersion','$sce',function($scope,$http,$state,$ionicNativeTransitions,$ionicPopup,$rootScope,$ionicModal,$cordovaGeolocation,$cordovaToast,$httpParamSerializer,$ionicPopover,$ionicPlatform,$ionicLoading,$cordovaFile,$interval,$cordovaAppVersion,$sce) {
	//Variable para que no se haga zoom a un marcador al mostrar por primera vez el mapa
  console.log("serviciosAtractivosCtrl");
  console.log("scope :", $scope);
  console.log("rootscope: ", $rootScope);
	$scope.auxMarcador1                     = 0;
	//cambiar menu si se selecciona crear marcador,(cambiara a true la variable)
	//que es la funcion openModalFiguraGps().
	$scope.btnCrearMarcador                 = false;
	  
	//contador cantidad de veces que entra a watch position
	$scope.watchPositionCont                = 0;
	$scope.miRuta                           = [];
	$rootScope.id_item_turistico            = 0;
	$scope.marcadores                       = [];
	$scope.markerArray                      = [];
	 
	$scope.markerArrayItemRutaDinamica       = [];
	$scope.markerArrayServiciosRutaDinamica  = [];
	$scope.markerArrayPatrimonioRutaDinamica = [];
	 
	$scope.patrimonios                      = [];
	$scope.markerArrayPatrimonio            = [];
	  
	$scope.serviciosTuristicos              = [];
	$scope.markerArrayServiciosTuristicos   = [];
	  
	$scope.latLng                           = [];
	$rootScope.map                          = [];
	$rootScope.id_item_turistico_modificar  = null;
	$rootScope.modalMenuAccionesModificar   = null;
	$scope.overlaytype                      = 0;
	 
	$rootScope.tipo_seleccionado            = "";
	  
	$rootScope.patrimonio_seleccionado      = "";
	$rootScope.servicio_seleccionado        = "";
	  
	$scope.showHideItem                     = false; //false oculto true visible
	$scope.showHidePatrimonio               = false; //0 oculto 1 visible
	$scope.showHideServicio                 = false; //0 oculto 1 visible
	$scope.rutaDinamica                     = false; //0 oculto 1 visible
	  
	$scope.sub_tipo_atractivo               = [];
	$scope.tipo_servicio                    = [];
	$scope.patrimonios_seleccionados        = [];
	$scope.servicios_seleccionados          = [];
	$scope.seleccionadoMostrarTodos         = false;
	$scope.seleccionadoMostrarTodosServicio = false;
	$scope.auxiliarPantalla                 = 0;
	$scope.auxLatPantalla                   = 0;
	$scope.auxLngPantalla                   = 0;
	$scope.agregarPatrimonios               = 0;
	$scope.agregarServicios                 = 0;
	  
	$scope.serviciosDinamica                = [];
	$scope.patrimoniosDinamica              = [];
	$scope.agregarMarkerDinamico            = 0;
	$scope.auxiliarMostrarDinamica          = 0;
	$rootScope.btnRecorriendoRuta           = false;
	$scope.entrarUnaVezRecomendado          = 0;
	$scope.entrarUnaVezDinamico             = 0;
	$scope.pausa                            = false;
	$scope.textoPausarContinuar             = "pausar";
	$scope.btnTerminarAventura              = false;
	$scope.btnAgregarItemRutaTipica         = false;
	$scope.btnOpcionesMarcador              = false;
	$scope.btnVolverVerItem                 = false;
	$scope.bloquearMostrarPatrimonios       = false;
	$scope.bloquearMostrarServicios         = false;
	$scope.btnMostrarReservaEnMapa          = false;
	$scope.menuVerItemsTrurista             = false;
	$scope.menuVerItemsTruristaTotal        = false;

	$scope.markerArrayEventosDinamicosRuta  = [];
	$scope.eventosDinamicosRuta             = [];

	$rootScope.id_ruta_recorrida_turista    = null;
  $scope.textServiciosAtractivos          = "";

  $scope.showFiltroAtractivo              = false;
  $scope.showFiltroServicio               = false;
  $scope.atractivoSeleccionado            = "";
  $scope.servicioSeleccionado             = "";


  

	$http({
	  method: "POST",
	  url: "http://200.14.68.107/atacamaGo/getMenuServicios.php",
	  data: $httpParamSerializer({}),
	  headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	}).then(function(response){ //ok si recato bien los datos
	    if(response.data.resultado == "ok"){
	      $scope.tipo_servicio = response.data.tipo_servicio;

        $scope.serviciosTuristicosMenu = response.data.tipo_servicio;
        $scope.serviciosTuristicosMenu.unshift({
          "id_tipo_servicio" : -1,
          "nombre"           : "Ver todos los servicios",
          "seleccionado"     : false
        });


        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getMenuPatrimonio.php",
          data: $httpParamSerializer({}),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si recato bien los datos
            //console.log(response);
            if(response.data.resultado == "ok"){
              $scope.sub_tipo_atractivo = response.data.sub_tipo_atractivo;

              $rootScope.atractivosTuristicos = response.data.sub_tipo_atractivo;
              console.log($rootScope.atractivosTuristicos);
              $rootScope.atractivosTuristicos.unshift({
                "id_sub_tipo_atractivo" : -1,
                "nombre"                : "Ver todos los atractivos",
                "seleccionado"          : false
              });


                document.addEventListener("deviceready", function() {
                    var div = document.getElementById("map_canvas222");
                    var COPIAPO = new plugin.google.maps.LatLng(-27.3690175,-70.6756632);

                    // Initialize the map view
                    $rootScope.map = plugin.google.maps.Map.getMap(div,{
                      'backgroundColor' : 'white',
                      'mapType'         : plugin.google.maps.MapTypeId.ROADMAP,
                      'controls' : {
                        'compass'          : true,
                        'myLocationButton' : true,
                        'indoorPicker'     : true,
                        'zoom'             : false
                      },
                      'gestures': {
                        'scroll' : true,
                        'tilt'   : true,
                        'rotate' : true,
                        'zoom'   : true
                      },
                      'camera': {
                        'latLng' : COPIAPO,
                        'tilt'   : 0,
                        'zoom'   : 10,
                        'bearing': 0
                      }
                    });

                    // Wait until the map is ready status.
                    $rootScope.map.addEventListener(plugin.google.maps.event.MAP_READY, onMapReady);
                }, false);
                
            }
        }, function(){ //Error de conexión
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });

	    }
	}, function(){ //Error de conexión
	  $rootScope.toast('Verifica tu conexión a internet', 'short');
	});

  

	

    function onMapReady() {
      //$interval(callAtInterval, 10000);
      $rootScope.map.setPadding(50,10,0,0);
      if($rootScope.flagMostrarAtractivosTuristicos == true){
        $scope.textServiciosAtractivos = "Atractivos turísticos";
        $rootScope.flagMostrarAtractivosTuristicos = false;
        $scope.showBotonFiltroAtractivo = true;
        $scope.imgHeader="img/rutasTipicas/atractivoTuristico.png";
        $rootScope.volverDeServiciosAtractivos = 'atractivos';

        var id_sub_tipo_atractivo = $rootScope.mostrarAtractivosTuristicos.id_sub_tipo_atractivo;
        var index                 = $rootScope.mostrarAtractivosTuristicos.index;
        console.log(id_sub_tipo_atractivo,index);

        angular.forEach($scope.atractivosTuristicos, function(value, key) {
          
          if( value.id_sub_tipo_atractivo == id_sub_tipo_atractivo ){
            console.log("atractivo seleccionado: ",value.id_sub_tipo_atractivo);
            $scope.icono = "img/atractivos_blancos/"+value.id_sub_tipo_atractivo+".png";
            console.log("icono: "+$scope.icono);
            $scope.atractivosTuristicos[key].seleccionado == true; 
            $scope.atractivoSeleccionado = value.nombre;
            //$scope.icono = value.id_sub_tipo_atractivo;
            
          }
        })
        console.log("id_sub_tipo_atractivo: "+ id_sub_tipo_atractivo);
        $scope.mostrarPatrimonios(id_sub_tipo_atractivo,index+1);
      }

      if($rootScope.flagMostrarServiciosTuristicos == true){
        $scope.textServiciosAtractivos = "Servicios turísticos";
        $rootScope.flagMostrarServiciosTuristicos = false;
        $scope.showBotonFiltroServicio = true;
        $scope.imgHeader="img/rutasTipicas/servicioTuristico.png";

        $rootScope.volverDeServiciosAtractivos = 'servicios';

        var id_tipo_servicio = $rootScope.mostrarServiciosTuristicos.id_tipo_servicio;
        var index            = $rootScope.mostrarServiciosTuristicos.index;
        console.log("idetipo: ",id_tipo_servicio," index ",index);

        angular.forEach($scope.serviciosTuristicosMenu, function(value, key) {
          
          if( value.id_tipo_servicio == id_tipo_servicio ){
            console.log("servicio seleccionado: ",value);
            $scope.icono = "img/servicios_blancos/"+value.id_tipo_servicio+".png";
            console.log("icono: "+$scope.icono);
            $scope.serviciosTuristicosMenu[key].seleccionado == true; 
            $scope.servicioSeleccionado = value.nombre;
          }
        }) 

        $scope.mostrarServiciosTuristicos(id_tipo_servicio,index+1);
      }

      document.addEventListener('backbutton', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $rootScope.map.remove();
        $rootScope.volverDeServiciosAtractivos = true;
      }, false);
    }

    function onBtnClicked() {
      map.showDialog();
    }

    //Mostrar Servicios, ocurre cuando se aprieta "mostrar todos" o alguna
    //categoria de servicios....
    $scope.mostrarServiciosTuristicos = function(id_tipo_servicio,index){
      //document.getElementById("containerLoading").className = "fadeIn";
      //$scope.popoverServicios.hide();
      //Cambiar clase al elemento seleccionado
      //index= index;
      $ionicLoading.show({
        template: 'Cargando...'
      });
      //console.log("entro :"+$scope.tipo_servicio[1].seleccionado, " index ", index, " id_tipo_servicio ",id_tipo_servicio);
      //console.log("id_tipo_servicio:  "+id_tipo_servicio+"       index :"+index+ "$scope.tipo_servicio[index].seleccionado: "+$scope.tipo_servicio[index].seleccionado);
      if( index != -1 ){
        if($scope.tipo_servicio[index].seleccionado == false)
          $scope.tipo_servicio[index].seleccionado = true;
        else
          $scope.tipo_servicio[index].seleccionado = false;
      }



      // angular.forEach($scope.serviciosTuristicosMenu, function(value, key) {
      //     if (value.id_tipo_servicio == id_tipo_servicio){
      //       console.log("cantidad seleccionada: " + $scope.servicios_seleccionados.length);
      //       $scope.servicioSeleccionado = value.nombre;
      //       console.log("deja el nombre del seleccionado");


      //       // if ($scope.servicios_seleccionados.length > 1){
      //       //   $scope.selAnt2 = true;
      //       // }else{
      //       //   $scope.selAnt2 = false;
      //       // }

      //       angular.forEach($scope.servicios_seleccionados, function(value, key) {
      //         if (value == id_tipo_servicio &&  $scope.servicios_seleccionados.length <= 2){
      //           $scope.servicios_seleccionados.splice(key - 1, 1);
      //         }
      //       });

      //       if ($scope.servicios_seleccionados.length <= 1 /*&& !$scope.selAnt2*/) {
      //         console.log("mantiene nombre");
      //         if ($scope.serviciosTuristicosMenu.length != 0 && $scope.servicios_seleccionados.length != 0) {
      //           $scope.servicioSeleccionado = $scope.serviciosTuristicosMenu[$scope.servicios_seleccionados[0]].nombre;
      //         }
      //       }else{
      //         $scope.servicioSeleccionado = "dsa";
      //         console.log("deja vacio");
      //       }
      //     }
      // }); 




      //Evaluar si se "presionó todos los patrimonios"
      if(id_tipo_servicio == -1){
        //seleccionar todos los sub tipos servicios....
        if( $scope.seleccionadoMostrarTodosServicio == false){
          $scope.seleccionadoMostrarTodosServicio = true;
          //además SELECCIONAR todos los elementos
          angular.forEach($scope.tipo_servicio, function(value, key) {
            value.seleccionado = true;
          });
        }else{
          $scope.seleccionadoMostrarTodosServicio = false;
          //ademas DES SELECCIONAR todos los elementos....
          angular.forEach($scope.tipo_servicio, function(value, key) {
            value.seleccionado = false;
          });

          //elimnar marcadores de patrimonio...
          $scope.serviciosTuristicos = [];
        }
        
        //Cambiar todas los botones a activos...
        /*var menuServicios   = document.getElementsByClassName("menuServicios");
        menuServicios.class = "menuServicios";*/
      } 

      $scope.showHideServicio = false;
      if($scope.showHideServicio == false){ //está oculto, por lo tanto mostrarlo....

        if( id_tipo_servicio == -1){
          if($scope.servicios_seleccionados.length == $scope.tipo_servicio.length+1){
            //estan todos seleccionados, por lo tanto vaciar el arreglo
            $scope.servicios_seleccionados = [];
          }else{

            $scope.servicios_seleccionados = [];
            angular.forEach($scope.tipo_servicio, function(value, key) {
              $scope.servicios_seleccionados.push(value.id_tipo_servicio)     
            });
          }
        }
        /*---------------------------------*/
        if( $scope.servicios_seleccionados.length == 0 ){
          $scope.servicios_seleccionados.push(id_tipo_servicio);
        }else{
          if($scope.servicios_seleccionados.indexOf(id_tipo_servicio) == -1) {
            $scope.servicios_seleccionados.push(id_tipo_servicio);
          }else
            $scope.servicios_seleccionados.splice($scope.servicios_seleccionados.indexOf(id_tipo_servicio), 1);
        }




        //conseguir Servivios tuiristicos
        $http({
          method: "POST",
          url: "http://200.14.68.107/atacamaGo/getServiciosTuristicosNuevo.php",
          data: $httpParamSerializer({
            id_tipo_servicio        : id_tipo_servicio,
            servicios_seleccionados : JSON.stringify($scope.servicios_seleccionados),
            id_usuario              : localStorage.getItem("id_usuario")
          }),
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).then(function(response){ //ok si guardó correctamente
            if(response.data.resultado == "ok"){
               //$scope.servicioSeleccionado = "";
               //
              
               console.log($scope.servicios_seleccionados[0]);
              //Mostrar nombre vacio cunado hay mas de 1 servicio seleeccionado
              if($scope.servicios_seleccionados.length > 1){
                $scope.servicioSeleccionado = "";
              }
              else{
                $scope.servicioSeleccionado = $scope.serviciosTuristicosMenu[$scope.servicios_seleccionados[0]].nombre;
              }


              var actualizarMarcadoresServicios = true;
              //evaluar si se rescatan servicios o no....
              //console.log(response.data.)
              if( $scope.seleccionadoMostrarTodosServicio == false ){
                var contatorMarcadoresServicio = 0;
                angular.forEach($scope.tipo_servicio, function(value, key) {
                  
                  if( value.seleccionado == false ){
                    contatorMarcadoresServicio++;
                  }
                });     
              }

              if( contatorMarcadoresServicio == $scope.tipo_servicio.length ){
                $scope.servicios_seleccionados = [];
                $scope.showHideServicio        = false;
                $scope.serviciosTuristicos     = [];
              }else{
                 $scope.serviciosTuristicos    = response.data.servicios;
                 $scope.showHideServicio = true; //mantener visto...
              }
              
              $scope.agregarServicios = 1;
              
              $rootScope.map.clear();
              $rootScope.map.off();
              $scope.markerArrayServiciosTuristicos = [];

              angular.forEach($scope.serviciosTuristicos, function(value, key) {  
               
                    var ubicacion = new plugin.google.maps.LatLng(
                      parseFloat(value.direccion_georeferenciada_latitud),
                      parseFloat(value.direccion_georeferenciada_longitud) );

                    //var url = "www/img/servicios/"+value.icono+".png";
                    var url = "www/img/servicios/"+value.icono+"Oculto.png";

                    $rootScope.map.addMarker({
                      'position': ubicacion,
                      'title': value.nombre_item_turistico,
                      'draggable': false,
                      'anchor':  [30, 45],
                      icon: {
                        url: url,
                        size: { width: 30, height: 45 },
                      },
                      zIndex: 1
                    },function(marker) {
                        //accion al hacer clic en el marcador
                        $scope.markerArrayServiciosTuristicos[key] = marker;
                        marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                          $rootScope.tipo_seleccionado = "servicio";
                          $rootScope.item_seleccionado = value;
                          $scope.openModalMenuAccionesModificar();
                        });
                    });
                    
              });

              $scope.markerArrayPatrimonio = [];

              angular.forEach($scope.patrimonios, function(value, key) {

                  var ubicacion = new plugin.google.maps.LatLng(
                    parseFloat(value.direccion_georeferenciada_latitud),
                    parseFloat(value.direccion_georeferenciada_longitud) );

                  //var url = "www/img/patrimonios/"+value.icono+".png";
                  var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': false,
                    'anchor':  [30, 45],
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayPatrimonio[key] = marker;
                    $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
                    $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "patrimonio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });
              });

              $scope.markerArrayItemUsuario = [];
            
              angular.forEach($scope.itemTuristicosVer, function(value, key) {                 
                  
                var ubicacion = new plugin.google.maps.LatLng(
                parseFloat(value.direccion_georeferenciada_latitud),
                parseFloat(value.direccion_georeferenciada_longitud) );

                var url = "www/img/posicionGpsTuristaOculto.png";

                $rootScope.map.addMarker({
                  'position': ubicacion,
                  'title': value.nombre_item_turistico,
                  'draggable': false,
                  'anchor':  [30, 45],
                  icon: {
                    url: url,
                    size: { width: 30, height: 45 },
                  },
                  zIndex: 1
                },function(marker) {
                  //accion al hacer clic en el marcador
                  $scope.markerArrayItemUsuario[key] = marker;
                    marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "item";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                  });
                });
                                      
              });

              $scope.markerArrayItemTotal = [];
            
              angular.forEach($scope.itemTuristicosVerTotal, function(value, key) {                 
                  
                  var ubicacion = new plugin.google.maps.LatLng(
                  parseFloat(value.direccion_georeferenciada_latitud),
                  parseFloat(value.direccion_georeferenciada_longitud) );

                  var url = "www/img/posicionGpsTuristaOculto.png";

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    'draggable': false,
                    'anchor':  [30, 45],
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                    },
                    zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayItemTotal[key] = marker;
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                        $rootScope.tipo_seleccionado = "item";
                        $rootScope.item_seleccionado = value;
                        $scope.openModalMenuAccionesModificar();
                    });
                  });
                                      
              });

              //si esta activo la aventura o ruta dinámica mostrar los items dinamicos...
              if( $scope.btnTerminarAventura == true ){

                  $scope.markerArrayServiciosTuristicosDinamicos = [];
                            
                  angular.forEach($scope.serviciosDinamica, function(value, key) {  
                             
                                  var ubicacion = new plugin.google.maps.LatLng(
                                    parseFloat(value.direccion_georeferenciada_latitud),
                                    parseFloat(value.direccion_georeferenciada_longitud) );

                                  //var url = "www/img/servicios/"+value.icono+".png";
                                  var url = "www/img/servicios/"+value.icono+"Oculto.png";

                                  $rootScope.map.addMarker({
                                    'position': ubicacion,
                                    'title': value.nombre_item_turistico,
                                    'draggable': false,
                                    'anchor':  [30, 45],
                                    icon: {
                                      url: url,
                                      size: { width: 30, height: 45 },
                                    },
                                    zIndex: 1
                                  },function(marker) {
                                      //accion al hacer clic en el marcador
                                      $scope.markerArrayServiciosTuristicosDinamicos[key] = marker;
                                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                        $rootScope.tipo_seleccionado = "servicio";
                                        $rootScope.item_seleccionado = value;
                                        $scope.openModalMenuAccionesModificar();
                                      });
                                  });
                                  
                            });

                  $scope.markerArrayPatrimonioDinamicos = [];

                  angular.forEach($scope.patrimoniosDinamica, function(value, key) {

                                var ubicacion = new plugin.google.maps.LatLng(
                                  parseFloat(value.direccion_georeferenciada_latitud),
                                  parseFloat(value.direccion_georeferenciada_longitud) );

                                //var url = "www/img/patrimonios/"+value.icono+".png";
                                var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

                                $rootScope.map.addMarker({
                                  'position': ubicacion,
                                  'title': value.nombre_item_turistico,
                                  'draggable': false,
                                  'anchor':  [30, 45],
                                  icon: {
                                    url: url,
                                    size: { width: 30, height: 45 },
                                  },
                                  zIndex: 1
                                },function(marker) {
                                  //accion al hacer clic en el marcador
                                  $scope.markerArrayPatrimonioDinamicos[key] = marker;
                                  marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                    $rootScope.tipo_seleccionado = "patrimonio";
                                    $rootScope.item_seleccionado = value;
                                    $scope.openModalMenuAccionesModificar();
                                  });
                                });
                  });

                  $scope.markerItemDinamicos = [];
                  angular.forEach($scope.itemDinamico, function(value, key) {

                                var ubicacion = new plugin.google.maps.LatLng(
                                  parseFloat(value.direccion_georeferenciada_latitud),
                                  parseFloat(value.direccion_georeferenciada_longitud) );

                                //var url = "www/img/patrimonios/"+value.icono+".png";
                                var url = "www/img/posicionGpsTuristaOculto.png";

                                $rootScope.map.addMarker({
                                  'position': ubicacion,
                                  'title': value.nombre_item_turistico,
                                  'draggable': false,
                                  'anchor':  [30, 45],
                                  icon: {
                                    url: url,
                                    size: { width: 30, height: 45 },
                                  },
                                  zIndex: 1
                                },function(marker) {
                                  //accion al hacer clic en el marcador
                                  $scope.markerItemDinamicos[key] = marker;
                                  marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                                    $rootScope.tipo_seleccionado = "item";
                                    $rootScope.item_seleccionado = value;
                                    $scope.openModalMenuAccionesModificar();
                                  });
                                });
                  });
              }

              //document.getElementById("containerLoading").className = "fadeOut";
              $ionicLoading.hide();
              $scope.bloquearMostrarServicios = false;
            }
        }, function(){ //Error de conexión
          $ionicLoading.hide();
          $rootScope.toast('Verifica tu conexión a internet', 'short');
        });

      }else{ //esta visible, por lo tanto ocultarlo

      }
    }

	$scope.mostrarPatrimonios = function(id_sub_tipo_atractivo,index){

	  //document.getElementById("containerLoading").className = "fadeIn";
	  /*if( $scope.popoverPatrimonios.isShown() ){
	    $scope.popoverPatrimonios.hide();  
	  }*/

    $ionicLoading.show({
      template: 'Cargando...'
    });

    $scope.showFiltroAtractivo = false;
    $scope.atractivoSeleccionado = $scope.sub_tipo_atractivo[index].nombre;
	  //Cambiar clase al elemento seleccionado
	  if( index != -1 ){
	    if($scope.sub_tipo_atractivo[index].seleccionado == false)
	      $scope.sub_tipo_atractivo[index].seleccionado = true;
	    else
	      $scope.sub_tipo_atractivo[index].seleccionado = false;
	  }

	  //Evaluar si se "presionó todos los patrimonios"
	  if(id_sub_tipo_atractivo == -1){
	    //seleccionar todos los sub tipos atractivos....
	    if( $scope.seleccionadoMostrarTodos == false){
	      $scope.seleccionadoMostrarTodos = true;
	      //además SELECCIONAR todos los elementos
	      angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
	        value.seleccionado = true;
	      });
	    }else{
	      $scope.seleccionadoMostrarTodos = false;
	      //ademas DES SELECCIONAR todos los elementos....
	      angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
	        value.seleccionado = false;
	      });

	      //elimnar marcadores de patrimonio...
	      $scope.patrimonios = [];
	    }
	    
	    //Cambiar todas los botones a activos...
	    /*var menuPatrimonio   = document.getElementsByClassName("menuPatrimonio");
	    menuPatrimonio.class = "menuPatrimonio";*/
	  } 

	  $scope.showHidePatrimonio = false;
	  if($scope.showHidePatrimonio == false){ //está oculto, por lo tanto mostrarlo....
	    //console.log("Llamar a php");
	    //conseguir patrimonios

	    if( id_sub_tipo_atractivo == -1){
	      //console.log($scope.patrimonios_seleccionados.length, $scope.sub_tipo_atractivo.length + 1);
	      if($scope.patrimonios_seleccionados.length == $scope.sub_tipo_atractivo.length+1){
	        //estan todos seleccionados, por lo tanto vaciar el arreglo
	        $scope.patrimonios_seleccionados = [];
	      }else{

	        $scope.patrimonios_seleccionados = [];
	        angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
	          $scope.patrimonios_seleccionados.push(value.id_sub_tipo_atractivo)     
	        });
	      }
	    }

	    if( $scope.patrimonios_seleccionados.length == 0 ){
	      $scope.patrimonios_seleccionados.push(id_sub_tipo_atractivo);  
	    }else{
	      if($scope.patrimonios_seleccionados.indexOf(id_sub_tipo_atractivo) == -1) {
	        $scope.patrimonios_seleccionados.push(id_sub_tipo_atractivo);  
	      }else
	        $scope.patrimonios_seleccionados.splice($scope.patrimonios_seleccionados.indexOf(id_sub_tipo_atractivo), 1);
	    }

	    $http({
	      method: "POST",
	      url: "http://200.14.68.107/atacamaGo/getPatrimonioNuevo.php",
	      data: $httpParamSerializer({
	        id_sub_tipo_atractivo     : id_sub_tipo_atractivo,
	        patrimonios_seleccionados : JSON.stringify($scope.patrimonios_seleccionados),
          id_usuario                : localStorage.getItem("id_usuario")
	      }),
	      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
	    }).then(function(response){ //ok si guardó correctamente
	        if(response.data.resultado == "ok"){
	          //console.log("Lista patrimoinios: ");
	          //console.log(response.data);

            //Mostrar nombre vacio cunado hay mas de 1 servicio seleeccionado
            if($scope.patrimonios_seleccionados.length > 1){
              $scope.atractivoSeleccionado = "";
            }
            else{
              angular.forEach($scope.atractivosTuristicos,function(value, key){
                if( value.id_sub_tipo_atractivo == $scope.patrimonios_seleccionados[0] ){
                  $scope.atractivoSeleccionado = value.nombre;  
                }
                
              });
              
            }

	          var actualizarMarcadores = true;
	          //evaluar si se rescatan patrimoinos o no....
	          
	          if( $scope.seleccionadoMostrarTodos == false ){
	            var contatorMarcadores = 0;
	            angular.forEach($scope.sub_tipo_atractivo, function(value, key) {
	              
	              if( value.seleccionado == false ){
	                contatorMarcadores++;
	              }
	            });
	            
	          }

	          if( contatorMarcadores == $scope.sub_tipo_atractivo.length ){
	            $scope.patrimonios_seleccionados = [];
	            $scope.showHidePatrimonio = false;
	            
	            $scope.patrimonios = [];
	          }else{
	             $scope.patrimonios = response.data.patrimonio;
	             $scope.showHidePatrimonio = true; //mantener visto...
	          }
	          
	          console.log( $scope.patrimonios );

	          $scope.agregarPatrimonios = 1;

	          $rootScope.map.clear();
	          $rootScope.map.off();

	          $scope.markerArrayPatrimonio = [];

	          angular.forEach($scope.patrimonios, function(value, key) {

	              var ubicacion = new plugin.google.maps.LatLng(
	                parseFloat(value.direccion_georeferenciada_latitud),
	                parseFloat(value.direccion_georeferenciada_longitud) );

	              //var url = "www/img/patrimonios/"+value.icono+".png";
	              var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

	              $rootScope.map.addMarker({
	                'position': ubicacion,
	                'title': value.nombre_item_turistico,
	                'draggable': false,
	                'anchor':  [30, 45],
	                icon: {
	                  url: url,
	                  size: { width: 30, height: 45 },
	                },
	                zIndex: 1
	              },function(marker) {
	                //accion al hacer clic en el marcador
	                $scope.markerArrayPatrimonio[key] = marker;
	                $scope.markerArrayPatrimonio[key].lat = parseFloat(value.direccion_georeferenciada_latitud);
	                $scope.markerArrayPatrimonio[key].lng = parseFloat(value.direccion_georeferenciada_longitud);

	                marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
	                  $rootScope.tipo_seleccionado = "patrimonio";
	                  $rootScope.item_seleccionado = value;
	                  $scope.openModalMenuAccionesModificar();
	                });
	              });
	            });

	            $scope.markerArrayServiciosTuristicos = [];

	            angular.forEach($scope.serviciosTuristicos, function(value, key) {  
	             
	                  var ubicacion = new plugin.google.maps.LatLng(
	                    parseFloat(value.direccion_georeferenciada_latitud),
	                    parseFloat(value.direccion_georeferenciada_longitud) );

	                  //var url = "www/img/servicios/"+value.icono+".png";
	                  var url = "www/img/servicios/"+value.icono+"Oculto.png";

	                  $rootScope.map.addMarker({
	                    'position': ubicacion,
	                    'title': value.nombre_item_turistico,
	                    'draggable': false,
	                    'anchor':  [30, 45],
	                    icon: {
	                      url: url,
	                      size: { width: 30, height: 45 },
	                    },
	                    zIndex: 1
	                  },function(marker) {
	                      //accion al hacer clic en el marcador
	                      $scope.markerArrayServiciosTuristicos[key] = marker;
	                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
	                        $rootScope.tipo_seleccionado = "servicio";
	                        $rootScope.item_seleccionado = value;
	                        $scope.openModalMenuAccionesModificar();
	                      });
	                  });
	                  
	            });

	            $scope.markerArrayItemUsuario = [];
	        
	            angular.forEach($scope.itemTuristicosVer, function(value, key) {                 
	              
	              var ubicacion = new plugin.google.maps.LatLng(
	              parseFloat(value.direccion_georeferenciada_latitud),
	              parseFloat(value.direccion_georeferenciada_longitud) );

	              var url = "www/img/posicionGpsTuristaOculto.png";

	              $rootScope.map.addMarker({
	                'position': ubicacion,
	                'title': value.nombre_item_turistico,
	                'draggable': false,
	                'anchor':  [30, 45],
	                icon: {
	                  url: url,
	                  size: { width: 30, height: 45 },
	                },
	                zIndex: 1
	              },function(marker) {
	                //accion al hacer clic en el marcador
	                $scope.markerArrayItemUsuario[key] = marker;
	                  marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
	                    $rootScope.tipo_seleccionado = "item";
	                    $rootScope.item_seleccionado = value;
	                    $scope.openModalMenuAccionesModificar();
	                });
	              });
	                                  
	            });

	            $scope.markerArrayItemTotal = [];
	        
	            angular.forEach($scope.itemTuristicosVerTotal, function(value, key) {                 
	              
	              var ubicacion = new plugin.google.maps.LatLng(
	              parseFloat(value.direccion_georeferenciada_latitud),
	              parseFloat(value.direccion_georeferenciada_longitud) );

	              var url = "www/img/posicionGpsTuristaOculto.png";

	              $rootScope.map.addMarker({
	                'position': ubicacion,
	                'title': value.nombre_item_turistico,
	                'draggable': false,
	                'anchor':  [30, 45],
	                icon: {
	                  url: url,
	                  size: { width: 30, height: 45 },
	                },
	                zIndex: 1
	              },function(marker) {
	                //accion al hacer clic en el marcador
	                $scope.markerArrayItemTotal[key] = marker;
	                  marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
	                    $rootScope.tipo_seleccionado = "item";
	                    $rootScope.item_seleccionado = value;
	                    $scope.openModalMenuAccionesModificar();
	                });
	              });
	                                  
	            });

	            $scope.agregarPatrimonios = 0;

	            //si esta activo la aventura o ruta dinámica mostrar los items dinamicos...
	            if( $scope.btnTerminarAventura == true ){

	              $scope.markerArrayServiciosTuristicosDinamicos = [];
	                        
	              angular.forEach($scope.serviciosDinamica, function(value, key) {  
	                         
	                              var ubicacion = new plugin.google.maps.LatLng(
	                                parseFloat(value.direccion_georeferenciada_latitud),
	                                parseFloat(value.direccion_georeferenciada_longitud) );

	                              //var url = "www/img/servicios/"+value.icono+".png";
	                              var url = "www/img/servicios/"+value.icono+"Oculto.png";

	                              $rootScope.map.addMarker({
	                                'position': ubicacion,
	                                'title': value.nombre_item_turistico,
	                                'draggable': false,
	                                'anchor':  [30, 45],
	                                icon: {
	                                  url: url,
	                                  size: { width: 30, height: 45 },
	                                },
	                                zIndex: 1
	                              },function(marker) {
	                                  //accion al hacer clic en el marcador
	                                  $scope.markerArrayServiciosTuristicosDinamicos[key] = marker;
	                                  marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
	                                    $rootScope.tipo_seleccionado = "servicio";
	                                    $rootScope.item_seleccionado = value;
	                                    $scope.openModalMenuAccionesModificar();
	                                  });
	                              });
	                              
	                        });

	              $scope.markerArrayPatrimonioDinamicos = [];

	              angular.forEach($scope.patrimoniosDinamica, function(value, key) {

	                            var ubicacion = new plugin.google.maps.LatLng(
	                              parseFloat(value.direccion_georeferenciada_latitud),
	                              parseFloat(value.direccion_georeferenciada_longitud) );

	                            //var url = "www/img/patrimonios/"+value.icono+".png";
	                            var url = "www/img/patrimonios/"+value.icono+"_oculto.png";

	                            $rootScope.map.addMarker({
	                              'position': ubicacion,
	                              'title': value.nombre_item_turistico,
	                              'draggable': false,
	                              'anchor':  [30, 45],
	                              icon: {
	                                url: url,
	                                size: { width: 30, height: 45 },
	                              },
	                              zIndex: 1
	                            },function(marker) {
	                              //accion al hacer clic en el marcador
	                              $scope.markerArrayPatrimonioDinamicos[key] = marker;
	                              marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
	                                $rootScope.tipo_seleccionado = "patrimonio";
	                                $rootScope.item_seleccionado = value;
	                                $scope.openModalMenuAccionesModificar();
	                              });
	                            });
	              });

	              $scope.markerItemDinamicos = [];
	              angular.forEach($scope.itemDinamico, function(value, key) {

	                            var ubicacion = new plugin.google.maps.LatLng(
	                              parseFloat(value.direccion_georeferenciada_latitud),
	                              parseFloat(value.direccion_georeferenciada_longitud) );

	                            //var url = "www/img/patrimonios/"+value.icono+".png";
	                            var url = "www/img/posicionGpsTuristaOculto.png";

	                            $rootScope.map.addMarker({
	                              'position': ubicacion,
	                              'title': value.nombre_item_turistico,
	                              'draggable': false,
	                              'anchor':  [30, 45],
	                              icon: {
	                                url: url,
	                                size: { width: 30, height: 45 },
	                              },
	                              zIndex: 1
	                            },function(marker) {
	                              //accion al hacer clic en el marcador
	                              $scope.markerItemDinamicos[key] = marker;
	                              marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
	                                $rootScope.tipo_seleccionado = "item";
	                                $rootScope.item_seleccionado = value;
	                                $scope.openModalMenuAccionesModificar();
	                              });
	                            });
	              });
	            }
	            //document.getElementById("containerLoading").className = "fadeOut";
              $ionicLoading.hide();
	            $scope.bloquearMostrarPatrimonios = false;
	        }
	    }, function(){ //Error de conexión
	      $rootScope.toast('Verifica tu conexión a internet', 'short');
        $ionicLoading.hide();
	    });
	    
	  }else{ //esta visible, por lo tanto ocultarlo

	  }
	}   
	
	$scope.openModalMenuAccionesModificar = function(){

      console.log("vengo del mapa servicios turisticos o atractivos");

      $rootScope.volverMapaServicioAtractivo = true;

      //Modal menu Acciones Modiciar...
      $ionicModal.fromTemplateUrl('templates/menuAccionesModificar.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalMenuAccionesModificar = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          $rootScope.map.setClickable(false);

        $rootScope.modalMenuAccionesModificar.show();
      
      });
  }

  $scope.mostrarFiltroServicios = function(){
    $scope.tipo = "Servicios turísticos";
    if( $scope.showFiltroServicio ){
      $scope.showFiltroServicio = false;
      $scope.showFiltroAtractivo = false;
    }else{
      $scope.showFiltroServicio = true;
      $scope.showFiltroAtractivo = false;
    }
    
  }

  $scope.mostrarFiltroAtractivos = function(){
    $scope.tipo = "Atractivos turísticos";
    if( $scope.showFiltroAtractivo ){
      $scope.showFiltroAtractivo = false;
      $scope.showFiltroServicio = false;
    }else{
      $scope.showFiltroAtractivo = true;
      $scope.showFiltroServicio = false;
    }
  }

  $scope.cerrarServiciosAtractivos = function(){
    //$rootScope.map.clear();
    $rootScope.map.off();
    $rootScope.map.remove();
    //$state.go("app.nuevoHome");
    $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
      "type": "slide",
      "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
      "duration": 400, // in milliseconds (ms), default 400
    });
  }

  $scope.cerrarServiciosAtractivosTotal = function(){
    //$rootScope.map.clear();
    $rootScope.map.off();
    $rootScope.map.remove();
    console.log('ESTA ES EL SCOPE', $rootScope.volverDeServiciosAtractivos);
    $rootScope.volverDeServiciosAtractivos = "servicioYatractivos";
    console.log('ESTA ES EL SCOPE', $rootScope.volverDeServiciosAtractivos);

    $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
      "type": "slide",
      "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
      "duration": 400, // in milliseconds (ms), default 400
    });

  }

  $scope.cerrarModalFiltro = function(){
    $scope.showFiltroServicio  = false;
    $scope.showFiltroAtractivo = false;
  }

  $scope.trustAsHtml = function(string) {
    return $sce.trustAsHtml(string);
  };

}]);
