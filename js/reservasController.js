angular.module('starter.controllers')


.controller('reservasCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaGeolocation','$ionicModal','ratingConfig','$ionicLoading','$ionicPopup',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaGeolocation,$ionicModal,ratingConfig,$ionicLoading,$ionicPopup) {
  // With the new view caching in Ionic, Controllers are only called
  
  $scope.agregarMarkerDinamico       = 0;
  $scope.markerArrayItemRutaDinamica = [];
  $scope.calificacion                = {};
  $scope.filtro                      = "todas";
  $scope.filtroActual                = "";
  $scope.filtroSeleccionado          = "Todos";
  $scope.selectables                 = ['En curso', 'Cerradas', 'Todos'];
  $scope.mostrarMenuFiltro           = false;
  
    //conseguir coordenadas de item_turistico
    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/getReservasUsuario.php",
      data: $httpParamSerializer({
        "id_usuario": localStorage.getItem("id_usuario")
      }),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ 
      if(response.data.resultado == "ok"){
        console.log(response.data.reservas);
        $scope.reservas = response.data.reservas;

        // angular.forEach($scope.reservas, function(value, key) {
        //   if ($reservas[key].estado_reserva == "cancelado"){
        //     $reservas[key].anularReserva = false;
        //   }else{
        //     console.log("$reservas[key].anularReserva : ",$reservas[key].anularReserva);
        //     $reservas[key].anularReserva = true;
        //   }
        // });
        

        angular.forEach($scope.reservas, function(value, key) {
          /*var val = value.fecha_inicial_reserva;
          var d                                      = new Date(val)
          var curr_date                              = d.getDate();
          var curr_month                             = d.getMonth() + 1; //Months are zero based
          var curr_year                              = d.getFullYear();
          $scope.reservas[key].fecha_inicial_reserva = curr_date+"/"+curr_month+"/"+curr_year;

          var val = value.fecha_final_reserva;
          var d                                      = new Date(val)
          var curr_date                              = d.getDate();
          var curr_month                             = d.getMonth() + 1; //Months are zero based
          var curr_year                              = d.getFullYear();
          $scope.reservas[key].fecha_final_reserva = curr_date+"/"+curr_month+"/"+curr_year;*/
          //console.log("$scope.reservas[key].estado_reserva : ",$scope.reservas[key].estado_reserva);
          if ($scope.reservas[key].estado_reserva == "cancelado"){
            //console.log("$reservas[key].anularReserva : ",$scope.reservas[key].anularReserva);
            $scope.reservas[key].anularReserva = false;
          }else{
            //console.log("$reservas[key].anularReserva : ",$scope.reservas[key].anularReserva);
            $scope.reservas[key].anularReserva = true;
          }
      
          $scope.reservas[key].fecha_inicial_reserva = value.fecha_inicial_reserva;
          $scope.reservas[key].fecha_final_reserva   = value.fecha_final_reserva;

        });

        $scope.reservasAux = $scope.reservas;
      }
    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
    });

    $scope.ocultarModalReservas = function(){
      //$rootScope.map.clear();
      //$rootScope.map.off();
      //$rootScope.restaurarMarcadoresMapa();
      $scope.markerArrayPatrimonioReserva = [];
      $scope.marcadorReserva = [];

      if(ionic.Platform.isWebView())
        //$rootScope.map.setClickable(true);
      $rootScope.modalReservas.hide();
    }

    $scope.cerrarModalReservas = function(){
      //$rootScope.map.clear();
      //$rootScope.map.off();
      //$rootScope.restaurarMarcadoresMapa();
      $scope.markerArrayPatrimonioReserva = [];
      $scope.marcadorReserva = [];

      //if(ionic.Platform.isWebView())
      //  $rootScope.map.setClickable(true);
      $rootScope.modalReservas.remove();
    }

    $scope.cerrarModalReservasModificar = function(){
      //$rootScope.restaurarMarcadoresMapa();
      $scope.markerArrayPatrimonioReserva = [];
      $scope.marcadorReserva = [];
      $rootScope.modalReservas.remove();
    }

    

    $scope.mostrarReservaEnMapa = function(id_servicio){
      $http({
        method: "POST",
        url: "http://200.14.68.107/atacamaGo/mostrarReservaEnMapa.php",
        data: $httpParamSerializer({
          "id_servicio" : id_servicio,
          "id_usuario"  : localStorage.getItem("id_usuario")
        }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ 
        if(response.data.resultado == "ok"){
          console.log(response.data);
          $rootScope.marcadorReservaVer = response.data.servicios;
          
          if(ionic.Platform.isWebView()){
            //$rootScope.map.clear();
            //$rootScope.map.off();

            /*if( $rootScope.existeServicioMapa(id_servicio) == false ){
              $scope.markerArrayPatrimonioReserva = [];

              angular.forEach($scope.marcadorReserva, function(value, key) {
                console.log(value);
                console.log($scope.marcadorReserva[key]);
                var ubicacion = new plugin.google.maps.LatLng(
                parseFloat(value.direccion_georeferenciada_latitud),
                parseFloat(value.direccion_georeferenciada_longitud) );

                var url = "www/img/servicios/"+value.icono+"Oculto.png";;


                $rootScope.map.animateCamera({
                  target: {
                    lat: parseFloat(value.direccion_georeferenciada_latitud),
                    lng: parseFloat(value.direccion_georeferenciada_longitud)},
                  zoom: 17,
                  duration: 500
                }, function() {

                  $rootScope.map.addMarker({
                    'position': ubicacion,
                    'title': value.nombre_item_turistico,
                    icon: {
                      url: url,
                      size: { width: 30, height: 45 },
                      anchor:  [10, 10],
                    },
                      zIndex: 1
                  },function(marker) {
                    //accion al hacer clic en el marcador
                    $scope.markerArrayPatrimonioReserva[key]     = marker;
                      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                      $rootScope.tipo_seleccionado = "servicio";
                      $rootScope.item_seleccionado = value;
                      $scope.openModalMenuAccionesModificar();
                    });
                  });

                });

              });
            }else{
              console.log("RETURN = FALSE");
              angular.forEach($scope.marcadorReserva, function(value, key) {
                // solo mover la camara, porque ya esta el marcador en el mapa
                $rootScope.map.animateCamera({
                  target: {
                    lat: parseFloat(value.direccion_georeferenciada_latitud),
                    lng: parseFloat(value.direccion_georeferenciada_longitud)},
                  zoom: 17,
                  duration: 500
                }, function() {});
                
              });
              
            }*/
            
            $scope.ocultarModalReservas();
            $state.go("app.reservaVer");
          }
            
        }
      }, function(){ //Error de conexión
        $rootScope.toast('Verifica tu conexión a internet', 'short');
      });
    }

    //abrir el modal cuando apretamos un item, patrimonio, servicio
    //en el mapa
    $scope.openModalMenuAccionesModificar = function(){

      //Modal menu Acciones Modiciar...
      $ionicModal.fromTemplateUrl('templates/menuAccionesModificar.html', {
        scope: $rootScope,
        animation: 'slide-in-up',
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalMenuAccionesModificar = modal;        
        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
          //$rootScope.map.setClickable(false);

        $rootScope.modalMenuAccionesModificar.show();
      
      });
    }

    $scope.openModalMensajesReserva = function(id,estado_reserva){

      $rootScope.id_reserva_servicio_turistico = id;
      $rootScope.estado_reserva                = estado_reserva;
      //Modal Modificar Audio...
      $ionicModal.fromTemplateUrl('templates/reservasMensajes.html', {
        scope: $rootScope,
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalMensajes = modal;

        //hacer que el mapa no sea clickeable.
        if( ionic.Platform.isAndroid() )
          //$rootScope.map.setClickable(false);  
        
        $rootScope.modalMensajes.show();

      });

    }

    $scope.anularReserva = function(id){
      console.log("id: ", id);

      $ionicPopup.alert({
        title: 'AVISO',
        cssClass: 'headNaranjo',
        template: '¿Está seguro que desea anular la reserva?',
        buttons: [
          {
            text: '<i class="icon ion-close-round"></i>',
            type:'popclose',
            onTap: function(e) {
              //$rootScope.map.setClickable(true);
            }
          },{
          text: '<img src="img/login/btnIngresar.png" lt class="btnNaranjaFondo"></img><b>ACEPTAR</b><div>&nbsp;</div>',
          type: 'botonMorado',
          onTap: function(e) {
            $ionicLoading.show({
              template: 'Cargando...'
            });

            $http({
              method: "POST",
              url: "http://200.14.68.107/atacamaGo/anularReserva.php",
              data: $httpParamSerializer({
                "id": id
              }),
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).then(function(response){ 
              console.log(response.data);
              if(response.data.resultado == "ok"){

                angular.forEach($scope.reservas, function(value, key) {
                  if( value.id_mensaje == id ){
                    value.estado_reserva = 'cancelado';
                  }

                });

                angular.forEach($scope.reservasAux, function(value, key) {
                  if( value.id_mensaje == id ){
                    value.estado_reserva = 'cancelado';
                    console.log( $scope.reservasAux[key] );
                  }

                });

                console.log("FILTRO: " +  $scope.filtroActual);
                if( $scope.filtroActual == 'En curso' ){
                  console.log("filtrar");
                  $scope.filtrar( $scope.filtroActual );
                }

                $rootScope.toast('Reserva cancelada', 'short');  
              }
              $ionicLoading.hide();
            }, function(){ //Error de conexión
              $rootScope.toast('Verifica tu conexión a internet', 'short');
              $ionicLoading.hide();
            });
          }
        }
        ]
      });
    }

    $scope.openModalReserva = function(){

      //Modal menu Acciones Modiciar...
      $ionicModal.fromTemplateUrl('templates/reserva.html', {
        scope: $rootScope,
        backdropClickToClose: false,
      }).then(function(modal) {
        $rootScope.modalReserva = modal;

        //hacer que el mapa no sea clickeable.
        //if( ionic.Platform.isAndroid() )
        //  $rootScope.map.setClickable(false);

        $rootScope.modalReserva.show();
      
      });
    }

    $scope.modificarReserva = function(id_reserva,id_servicio_turistico,reserva){
      $rootScope.item_seleccionado     = reserva;
      $rootScope.id_reserva            = id_reserva;
      $rootScope.id_servicio_turistico = id_servicio_turistico;
      $rootScope.rescatarDesdeBd = true;
      $rootScope.verReserva = false;
      //$scope.cerrarModalReservasModificar();
      $scope.openModalReserva();
      
    }

    $scope.verReserva = function(id_reserva,id_servicio_turistico,reserva){
      $rootScope.item_seleccionado     = reserva;
      $rootScope.id_reserva            = id_reserva;
      $rootScope.id_servicio_turistico = id_servicio_turistico;
      $rootScope.rescatarDesdeBd = true;
      $rootScope.verReserva = true;
      //$scope.cerrarModalReservasModificar();
      $scope.openModalReserva();

    }

    $scope.selectFiltro = function(nuevo,viejo){
      $scope.filtroSeleccionado = nuevo;
      $scope.filtrar(nuevo);
    }


    $scope.cerrarReservas = function(){
      console.log("cerar modal");
      //if( ionic.Platform.isAndroid() )
        //$rootScope.map.setClickable(true);
    
       $rootScope.modalReservas.remove();
    }



    //* PRUEBA BARRA**//

    $scope.mostrarFiltro = function(){
      console.log("click");
      if( $scope.mostrarMenuFiltro ){
        $scope.mostrarMenuFiltro = false;
      }else{
        $scope.mostrarMenuFiltro = true;
      }
    }

    $scope.filtrar = function(filtro){
      //console.log($scope.filtro);
      console.log(filtro);
      $scope.filtroActual = filtro;
      $scope.reservas = {};
      if(filtro == 'En curso'){
        var contador = 0;
        angular.forEach($scope.reservasAux, function(value, key) {
          if(value.estado_reserva == 'pendiente' || value.estado_reserva == 'reservado' || value.estado_reserva == 'reservado pendiente pago'){
            $scope.reservas[contador] = value;
            contador +=1;
          }      
        });

        $scope.mostrarMenuFiltro = false;
        $scope.filtroSeleccionado          = "En curso";
      }

      if(filtro == 'Cerradas'){
        var contador = 0;
        angular.forEach($scope.reservasAux, function(value, key) {
          if(value.estado_reserva == 'cancelado' || value.estado_reserva == 'pagado' ){
            $scope.reservas[contador] = value;
            contador +=1;
          }      
        });

        $scope.mostrarMenuFiltro = false;
        $scope.filtroSeleccionado          = "Cerradas";
      }

      if(filtro == 'Todos'){
        var contador = 0;
        angular.forEach($scope.reservasAux, function(value, key) {
            $scope.reservas[contador] = value;
            contador +=1;
        });

        $scope.mostrarMenuFiltro = false;
        $scope.filtroSeleccionado          = "Todos";
      }
    }
}]);