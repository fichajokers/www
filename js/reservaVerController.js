angular.module('starter.controllers')


.controller('reservaVerCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaGeolocation','$ionicModal','ratingConfig','$ionicLoading','$ionicPopup','$ionicNativeTransitions',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaGeolocation,$ionicModal,ratingConfig,$ionicLoading,$ionicPopup,$ionicNativeTransitions) {
  $scope.nombre = "lalal 123";
  // With the new view caching in Ionic, Controllers are only called
    document.addEventListener("deviceready", function() {
        var div = document.getElementById("map_canvas222");
        var COPIAPO = new plugin.google.maps.LatLng(-27.3690175,-70.6756632);

        // Initialize the map view
        $rootScope.map = plugin.google.maps.Map.getMap(div,{
          'backgroundColor' : 'white',
          'mapType'         : plugin.google.maps.MapTypeId.ROADMAP,
          'controls' : {
            'compass'          : true,
            'myLocationButton' : true,
            'indoorPicker'     : true,
            'zoom'             : false
          },
          'gestures': {
            'scroll' : true,
            'tilt'   : true,
            'rotate' : true,
            'zoom'   : true
          },
          'camera': {
            'latLng' : COPIAPO,
            'tilt'   : 0,
            'zoom'   : 10,
            'bearing': 0
          }
        });

        // Wait until the map is ready status.
        $rootScope.map.addEventListener(plugin.google.maps.event.MAP_READY, onMapReady);
    }, false);

      function onMapReady() {
        $scope.markerArrayPatrimonioReserva = [];
        console.log($rootScope.marcadorReservaVer);

        angular.forEach($rootScope.marcadorReservaVer, function(value, key) {
          console.log(value);
          
          $scope.nombre = value.nombre;
          console.log($scope.nombre);

          var ubicacion = new plugin.google.maps.LatLng(
          parseFloat(value.direccion_georeferenciada_latitud),
          parseFloat(value.direccion_georeferenciada_longitud) );

          var url = "www/img/servicios/"+value.icono+"Oculto.png";;


          $rootScope.map.animateCamera({
            target: {
              lat: parseFloat(value.direccion_georeferenciada_latitud),
              lng: parseFloat(value.direccion_georeferenciada_longitud)},
            zoom: 17,
            duration: 500
          }, function() {

            $rootScope.map.addMarker({
              'position': ubicacion,
              'title': value.nombre_item_turistico,
              icon: {
                url: url,
                size: { width: 30, height: 45 },
                anchor:  [10, 10],
              },
                zIndex: 1
            },function(marker) {
              //accion al hacer clic en el marcador
              $scope.markerArrayPatrimonioReserva[key]     = marker;
                marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
                $rootScope.tipo_seleccionado = "servicio";
                $rootScope.item_seleccionado = value;
                $scope.openModalMenuAccionesModificar();
              });
            });

          });
        });

        document.addEventListener('backbutton', function (event) {
          event.preventDefault();
          event.stopPropagation();
          $rootScope.map.remove();
          $rootScope.volverDeServiciosAtractivos = true;
        }, false);
      }

      function onBtnClicked() {
        map.showDialog();
      }

      $scope.openModalMenuAccionesModificar = function(){
        //Modal menu Acciones Modiciar...
        $ionicModal.fromTemplateUrl('templates/menuAccionesModificar.html', {
          scope: $rootScope,
          animation: 'slide-in-up',
          backdropClickToClose: false,
        }).then(function(modal) {
          $rootScope.modalMenuAccionesModificar = modal;
          $rootScope.map.setClickable(false);
          $rootScope.modalMenuAccionesModificar.show();
        });
      }
      
      $scope.atrasReservaVer = function(){
        $rootScope.map.clear();
        $rootScope.map.off();
        $rootScope.map.remove();
        //$state.go("app.nuevoHome");

        $rootScope.listaReservas = true;
        $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
          "type": "slide",
          "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
          "duration": 400, // in milliseconds (ms), default 400
        });
      }

      $scope.cerrarReservaVer = function(){
        $rootScope.map.clear();
        $rootScope.map.off();
        $rootScope.map.remove();
        //$state.go("app.nuevoHome");

        $ionicNativeTransitions.stateGo('app.nuevoHome', {inherit:false}, {
          "type": "slide",
          "direction": "up", // 'left|right|up|down', default 'left' (which is like 'next')
          "duration": 400, // in milliseconds (ms), default 400
        });
      }
}]);