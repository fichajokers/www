angular.module('starter.controllers')


.controller('experienciaAtacamaCompartirCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaGeolocation','$ionicModal','ratingConfig','$ionicLoading','$sce','$cordovaSocialSharing','$ionicPopup',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaGeolocation,$ionicModal,ratingConfig,$ionicLoading,$sce,$cordovaSocialSharing,$ionicPopup) {
    console.log("detalle experiencia controller: "+ $rootScope.id_producto_servicio_turistico);
    $rootScope.experienciaDetalleMostrar = null;
    $scope.item = $rootScope.experienciaSeleccionadaNuevoHome;

    $rootScope.linkTwitter  = encodeURI("http://www.atacama-go.cl/ver_servicio/"+$scope.item.id_producto_servicio_turistico);
    $rootScope.linkFacebook = encodeURI("http://www.atacama-go.cl/ver_servicio/"+$scope.item.id_producto_servicio_turistico);

    $scope.cerrarModalExperienciaAtacamaCompartir = function(){
    	console.log("cerrar ventana");
    	$rootScope.modalExperienciaAtacamaCompartir.hide();
  	}

    $scope.goToNuevoHome = function(){
      $rootScope.modalDetalleExperiencia.remove();
      $rootScope.modalExperienciaAtacamaCompartir.hide();
    }

    $scope.trustAsHtml = function(string) {
      return $sce.trustAsHtml(string);
    };

    $scope.compartirFacebook = function(){
          $ionicLoading.show({
              template: 'Cargando...'
          });

          link = encodeURI($rootScope.linkFacebook);  

          console.log("compartirFacebook",link);
          $ionicLoading.hide();
          $cordovaSocialSharing.shareViaFacebook(link,null,link)
          .then(function(result) {
              console.log("ok",result);
          }, function(err) {
              $ionicLoading.hide();
              console.log("error",err);
              $ionicPopup.alert({
                title: 'Aviso!',
                template: 'Debes tener la aplicación de facebook para compartir la imagen',
                okText: 'Aceptar', // String (default: 'OK'). The text of the OK button.
                okType: 'btn-morado asd', // String (default: 'button-positive'). The type of the OK button.
              });
          });
      }

      $scope.compartirTwitter  = function(){
          link = encodeURI($rootScope.linkTwitter);

          console.log("compartirTwitter", link);
          $ionicLoading.hide();
          $cordovaSocialSharing.shareViaTwitter("www.atacama-go.cl", null, link)
          .then(function(result) {
              $ionicLoading.hide();
              console.log("ok",result);
          }, function(err) {
              $ionicPopup.alert({
                title: 'Aviso!',
                template: 'Debes tener la aplicación de twitter para compartir la imagen',
                okText: 'Aceptar', // String (default: 'OK'). The text of the OK button.
                okType: 'btn-morado asd', // String (default: 'button-positive'). The type of the OK button.
              });
          });
      }

}]);