angular.module('starter.controllers')


.controller('misDatosCtrl', ['$scope','$http','$state','$rootScope','ionicDatePicker','$httpParamSerializer','$cordovaCamera','$cordovaFileTransfer','$timeout','$ionicLoading',function($scope,$http,$state,$rootScope,ionicDatePicker,$httpParamSerializer,$cordovaCamera,$cordovaFileTransfer,$timeout,$ionicLoading) {
  // With the new view caching in Ionic, Controllers are only called
  console.log($rootScope.modalMisDatos);

  $scope.misDatos          = [];
  $scope.sexoSelecionado   = "Sexo";
  $scope.paisSelecionado   = "Pais";
  $scope.fechaSeleccionado = "Fecha nacimiento"
  $scope.selectables       = ['Masculino', 'Femenino'];
  $scope.id_pais           = [];
  $scope.nombre_pais       = [];
  //$scope.paisSelecionado   = "";
  //$scope.sexoSelecionado   = "";
  $scope.rutaImagen        = "";
  $scope.nombreImagen      = "";

  /**
   * [Rescatar todos los países desde la base de datos.]
   * 
   * @return {[Array]}  $scope.nombre_pais [Array con nombre de paises]
   * @return {[Array]}  $scope.id_pais     [array con id del nombre de paises]
   */
  $http({
    method: "POST",
    url: "http://200.14.68.107/atacamaGo/paises.php",
    data: $httpParamSerializer({}),
    headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
  }).then(function(response){ //ok si guardó correctamente.
    
    for(var i=0;i<response.data.pais.length;i++){
      $scope.nombre_pais.push( response.data.pais[i].nombre_pais );
      $scope.id_pais.push( response.data.pais[i].id_pais );
    }
    /*Obtener datos del usuario*/
    $http({
      method: "POST",
      url: "http://200.14.68.107/atacamaGo/misDatos.php",
      data: $httpParamSerializer({
        'id_usuario' : localStorage.getItem("id_usuario"),
        'id_agenda'  : localStorage.getItem("id_agenda"),
        'correo'     : localStorage.getItem("correo")
      }),
      headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
    }).then(function(response){ //ok si guardó correctamente.
      console.log(response.data);
      
      /*$scope.nacimientoBd = new Date(response.data.datos.fecha_nacimiento);
      var dateValue = $scope.nacimientoBd.getDate();
      $scope.nacimientoBd.setDate(dateValue);*/
      
      if ( response.data.datos.fecha_nacimiento == null){
        $scope.fechaSeleccionado = "fecha de nacimiento";
        
        $scope.nacimientoBd = new Date();
        var dateValue = $scope.nacimientoBd.getDate();
        $scope.nacimientoBd.setDate(dateValue);
      }else{

        var fechaNac = response.data.datos.fecha_nacimiento
        var parts = fechaNac.split("/");
        $scope.nacimientoBd = new Date(parts[2], parts[1] - 1, parts[0]);

        /*var d                 = new Date(response.data.datos.fecha_nacimiento);
        var curr_date         = d.getDate() + 1;
        var curr_month        = d.getMonth() + 1; //Months are zero based
        var curr_year         = d.getFullYear();
        $scope.misDatos.fechaNac = curr_date+"/"+curr_month+"/"+curr_year;
        $scope.fechaSeleccionado = $scope.misDatos.fechaNac;*/
        $scope.fechaSeleccionado = response.data.datos.fecha_nacimiento;
        $scope.misDatos.fechaNac = response.data.datos.fecha_nacimiento;
      }
        


      $scope.misDatos.correo   = localStorage.getItem("correo");/*response.data.datos.correo_usuario;*/
      $scope.misDatos.nombre   = response.data.datos.nombre + " " + response.data.datos.apellido_paterno + " " + response.data.datos.apellido_materno;
      $scope.misDatos.materno  = response.data.datos.apellido_materno;
      $scope.misDatos.paterno  = response.data.datos.apellido_paterno;
      $scope.misDatos.password = "fake";
      if( response.data.datos.foto_perfil != null){
        if( response.data.datos.foto_perfil.includes("https") ){
          $scope.misDatos.foto     = response.data.datos.foto_perfil;
        }else{
          $scope.misDatos.foto     = "http://200.14.68.107/atacamaGoMultimedia/files/perfil/"+response.data.datos.foto_perfil;
        }
      }

      console.log( $scope.misDatos.foto  );

      /*var d = document.getElementById("imgfotoPerfil");
      d.className += " heightCien";*/
      for(var i=0;i<$scope.id_pais.length;i++){
        if($scope.id_pais[i] == response.data.datos.id_pais){
          $scope.paisSelecionado = $scope.nombre_pais[i];
          $scope.misDatos.pais   = $scope.paisSelecionado;
        }
      }

      console.log( $scope.paisSelecionado );
      if( $scope.paisSelecionado.length <=0 ){
        $scope.paisSelecionado = "Pais";

      }
      
      if( response.data.datos.genero_usuario == "F" ){
        $scope.sexoSelecionado   = "Femenino";
        $scope.misDatos.sexo = "Femenino";
      }
      else if( response.data.datos.genero_usuario == "M" ){
        $scope.sexoSelecionado   = "Masculino";
        $scope.misDatos.sexo = "Masculino";
      }
      else{
        $scope.sexoSelecionado   = "Sexo";
        $scope.misDatos.sexo = "Sexo";
      }



    }, function(){ //Error de conexión
      $rootScope.toast('Verifica tu conexión a internet', 'short');
  });

  }, function(){ //Error de conexión
    $rootScope.toast('Verifica tu conexión a internet', 'short');
  });


  $scope.createContact = function(newUser){
    alert("holA");
  }

  $scope.cerrarModalMisDatos = function(){
    console.log("cerar modal");
    //if( ionic.Platform.isAndroid() )
      //$rootScope.map.setClickable(true);
    
    $rootScope.modalMisDatos.remove();
  }

 
  $scope.selectSexo = function(nuevo,viejo){
    $scope.sexoSelecionado = nuevo;
    
  }

  
  $scope.openDatePicker = function(){
    var ipObj1 = {
      callback: function (val) {  //Mandatory
        
        console.log('Return value from the datepicker popup is : ' + val, new Date(val));
        var d                    = new Date(val)
        var curr_date            = d.getDate();
        var curr_month           = d.getMonth() + 1; //Months are zero based
        var curr_year            = d.getFullYear();
        $scope.misDatos.fechaNac = curr_date+"/"+curr_month+"/"+curr_year;
        $scope.fechaSeleccionado = $scope.misDatos.fechaNac;

      },
      inputDate: $scope.nacimientoBd,//Optional
      mondayFirst: true,          //Optional
      closeOnSelect: false,       //Optional
    };
  
    ionicDatePicker.openDatePicker(ipObj1);
  
  };

  $scope.selectPais = function(nuevo,viejo){
    $scope.paisSelecionado = nuevo;
  }

  $scope.takePhoto = function () {
        var options = {
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.CAMERA,
            mediaType       : Camera.MediaType.PICTURE,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            saveToPhotoAlbum: false
        };
   
        $ionicLoading.show({
            template: 'Cargando...'
        });

        $cordovaCamera.getPicture().then(function (imageData) {
            imageData = imageData.replace(/^(.+?\.(png|jpe?g)).*$/i, '$1');
            $scope.rutaImagen   = imageData;
            $scope.misDatos.foto = imageData;
            $scope.tieneSrc     = true;
            $scope.nombreImagen = Math.floor((Math.random() * 9999999) + 1)+"_"+imageData.substr(imageData.lastIndexOf('/') + 1);

            $ionicLoading.hide();
        }, function (err) {
            console.log(err);
            $ionicLoading.hide();
            // An error occured. Show a message to the user
        });
  }

  $scope.choosePhoto = function () {
    var options = {
      destinationType : Camera.DestinationType.FILE_URI,
      sourceType      : Camera.PictureSourceType.PHOTOLIBRARY,
      mediaType       : Camera.MediaType.PICTURE,
      correctOrientation : true
    };
   
    $ionicLoading.show({
      template: 'Cargando...'
    });

    $cordovaCamera.getPicture(options).then(function (imageData) {
      imageData = imageData.replace(/^(.+?\.(png|jpe?g)).*$/i, '$1');
      console.log("ruta imagen:" + imageData);
      
      $scope.rutaImagen    = imageData;
      $scope.misDatos.foto = imageData;
      $scope.nombreImagen  = Math.floor((Math.random() * 9999999) + 1)+"_"+imageData.substr(imageData.lastIndexOf('/') + 1);
      console.log("ruta con key: " + $scope.nombreImagen);
      $ionicLoading.hide();
    }, function (err) {
        $ionicLoading.hide();
        //alert(err);
        // An error occured. Show a message to the user
    });
  }

  $scope.guardarMisDatos = function(registro){
    
    $ionicLoading.show({
      template: 'Cargando...'
    });
    var nombreAux = registro.nombre;
    

    var nombres = registro.nombre.split(" ");
    registro.paterno = null;
    registro.materno = null;
    
    var nombreCompleto = "";
    nombres.forEach(function(element,index) {
      console.log(element);

      if(index == 0){
        registro.nombre = element;
        nombreCompleto = registro.nombre;
      }

      if(index == 1){
        registro.paterno = element;
        nombreCompleto = nombreCompleto + " " + registro.paterno;
      }
      
      if(index == 2){
        registro.materno = element;
        nombreCompleto = nombreCompleto + " " + registro.materno;
      }

    });

    
    //registro.paterno = "PATE";
  //$scope.misDatos.nombre = "";
    //$scope.misDatos.nombre = nombreAux;
    flagCamposLlenos = 0;
    var options = {
      fileKey: "perfil",
      fileName: $scope.nombreImagen,
      chunkedMode: false
    };

    console.log(registro);
    if( registro.correo==null    || registro.correo== ""  || 
        registro.nombre==null    || registro.nombre== ""  || 
        registro.paterno==null   || registro.paterno== "" 
        /*registro.materno==null   || registro.materno== ""*/){

      if(!registro.paterno){
        $rootScope.toast("Debes ingresar nombre y apellido","short");
      }else{
        $rootScope.toast("Debes llenar todos los campos","short");  
      }
      $ionicLoading.hide();
      flagCamposLlenos = 1;
    }

    if( flagCamposLlenos == 0 ){
      $cordovaFileTransfer.upload("http://200.14.68.107/atacamaGo/subirImagen.php", $scope.rutaImagen, options).then(function(result) {
          console.log("SUCCESS:")
          console.log(result.response);
      }, function(err) {
        console.log("ERROR: ");
        console.log(err);
      }, function (progress) {
          // constant progress updates
      });

      if(registro.sexo == "Masculino")
        registro.sexo = "M";
       else if ( registro.sexo == "Femenino" )
        registro.sexo = "F";
       else if ( registro.sexo == "Sexo" )
        registro.sexo = null;

      if(registro.password == "fake")
        registro.password = null;

      for (var i=0;i<$scope.nombre_pais.length; i++) {
        if( $scope.nombre_pais[i] == registro.pais)
          registro.pais = $scope.id_pais[i]
      }

      $http({
        method: "POST",
        url: "http://200.14.68.107/atacama/public/usuario/modificarUsuarioMovil",
        data: $httpParamSerializer({
          "id_usuario" : localStorage.getItem("id_usuario"),
          "correo"     : registro.correo,
          "nombre"     : registro.nombre,
          "apellido_p" : registro.paterno,
          "apellido_m" : registro.materno,
          "password"   : registro.password,
          "pais"       : registro.pais,
          "genero"     : registro.sexo,
          "nacimiento" : registro.fechaNac,
          "foto"       : $scope.nombreImagen,
          "coreo_ant"  : localStorage.getItem("correo")
        }),
        headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
      }).then(function(response){ //ok si guardó correctamente.
        console.log(response.data);
        if(response.data.resultado == "ok"){
          registro.nombre = nombreCompleto;
          $rootScope.toast("Datos modificados","short");
          localStorage.setItem("correo",  registro.correo);
          $ionicLoading.hide();

          //$scope.cerrarModalMisDatos();
        }
        else{
          $rootScope.toast("Ha ocurrido un error intenta nuevamente","short");
          $ionicLoading.hide();
        }
        
      }, function(){ //Error de conexión
        $rootScope.toast('Verifica tu conexión a internet',"short");
        $ionicLoading.hide();
      });
    }
    
    
  };

}]);